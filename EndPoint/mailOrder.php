<?php
/**
 * =======================================================================================================
 * NOMBRE         : mailOrder.php
 * PATH           : C:/xampp/htdocs/www/754 SMS LINKS BBVA/EndPoint/mailOrder.php
 * AUTOR          : GETSEMANI AVILA QUEZADA ? ? ?_? ??
 * FECHA CREACION : 22/7/2020
 * DESARROLLO     : project.name
 *
 * Copyright (c) 2020. GessyAvila91. All rights reserved.
 * =======================================================================================================
 */

class mailOrder {

    protected $auth = 'Basic cmUuZXNwYXJ6YUBtYXZpLm14Om11ZWJsZXNhbWVyaWNhXzE=';

    function mailOrderPost ($Request) {
        $request = new HttpRequest();
        $request->setUrl('https://neko.flap.com.mx/api/v1/private/invoices');
        $request->setMethod(HTTP_METH_POST);
        $request->setHeaders(array(
            'Cache-Control' => 'no-cache',
            // Crear usuario para el enpoint y sacar su autorizacion
            // Usuario Usado re.esparza@mavi.mx
            'Authorization' => $this->auth,
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        ));

        $request->setBody($Request);

        try {
            $response = $request->send();
            echo $response->getBody();
        } catch (HttpException $ex) {
            echo $ex;
        }
    }


}