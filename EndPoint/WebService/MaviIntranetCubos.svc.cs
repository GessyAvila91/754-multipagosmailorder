using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace IntranetMaviCubos
{

    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class Service1 : IMaviIntranetCubos
    {
        //--------- Getsemani Avila Quezada ? ? ?_? ?? Servicio Referencias OrderMail BBVA ? ? ?_? ?? -------------
        public bool mcMultipagoBBVAReferencia(string Invoice, string Reference) {
            try     {
                //hacer referencia al server
                Elmentos.Servidores server = new Elmentos.Servidores();
                //se escoge el nombre del servidor que se pudo en la clase Servidores dentro de la carpeta Elementos
                server = Elmentos.Servidores.ProserverTMP;

                if (EstatusConexion(server)) {
                    metodos = new CapaDatos.MetodosSql();
                }

                metodos.MultipagoBBVAReferencia(GetSqlCon(server), Invoice, Reference);

                return true;
            } catch (Exception bnp) {
                return false;
            }
        }
        //--------- Getsemani Avila Quezada ? ? ?_? ?? Servicio Respuestas OrderMail BBVA ? ? ?_? ?? -------------
        public bool mcMultipagoBBVARespuesta(string Invoice, string Reference) {
            try     {
                //hacer referencia al server
                Elmentos.Servidores server = new Elmentos.Servidores();
                //se escoge el nombre del servidor que se pudo en la clase Servidores dentro de la carpeta Elementos
                server = Elmentos.Servidores.ProserverTMP;

                if (EstatusConexion(server)) {
                    metodos = new CapaDatos.MetodosSql();
                }

                metodos.MultipagoBBVARespuesta(GetSqlCon(server),int IdMultipagoOrdenesCorreosBBVA, String Origen, String NombreFactura, float Cantidad, String NombreCliente, String CorreoCliente, String Referencia, String Descripcion, String Token, String FechaCreacion, String FechaActualizado, bool Expira, String FechaExpiracion, String Moneda, String Lenguaje, int Servicio, String UrlCodigoQr, bool Pagado, String Pago, String PlantillaFactura, String PlantillaPago, String UrlRespuesta, String CodigoRespuesta, String MensajeRespuesta);

                return true;
            } catch (Exception bnp) {
                return false;
            }
        }
    }
}
