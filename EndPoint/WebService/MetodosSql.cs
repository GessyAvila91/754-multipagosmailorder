using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Net.Mail;

namespace IntranetMaviCubos.CapaDatos
{
    public class MetodosSql
    {
        //--------- Getsemani Avila Quezada ? ? ?_? ?? Insert para Referencia de Multipagos OrderMail BBVA ? ? ?_? ?? -------------
        public void MultipagoBBVAReferencia(SqlConnection conn , string Invoice, string Reference) {
            //Invoice = 'Factura1',0.00|'Factura2',0.01|'Factura3',0.02
            //Reference = ''
            String query = "Insert Into CXCCReferenciaMultipagoBBVA (Fecha, Referencia, Factura, Cantidad) Values ";

            //String queryValues = " (Getdate(),'','',0.0),";
            String queryValues = " ";
            //'Factura1',0.00|'Factura2',0.01|'Factura3',0.02

            string[] Invoices = Invoice.Split('|');

            foreach (var value in Invoices)
            {
                queryValues = queryValues + "(GetDate(),'" + Reference + "'," + value + "),";
            }
            queryValues = queryValues.TrimEnd(',');

            Console.WriteLine( queryValues);

            SqlCommand command = new SqlCommand(query + queryValues, conn);//
            SqlDataAdapter da = new SqlDataAdapter(command);
            DataSet ds = new DataSet();
            da.Fill(ds);
        }


    }
}