using System.Collections.Generic;
using System.ServiceModel;

namespace IntranetMaviCubos
{
    [ServiceContract]
    public interface IMaviIntranetCubos {

        //--------- Getsemani Avila Quezada ? ? ?_? ?? Insert para Referencias de Multipagos OrderMail BBVA ? ? ?_? ?? -------------
        [OperationContract]
        bool mcMultipagoBBVAReferencia(string Invoice, string Reference);
        //--------- Getsemani Avila Quezada ? ? ?_? ?? Insert para Respuestas de Multipagos OrderMail BBVA ? ? ?_? ?? -------------
        [OperationContract]
        bool mcMultipagoBBVARespuesta(int IdMultipagoOrdenesCorreosBBVA, string Origen, string NombreFactura, float Cantidad, string NombreCliente, string CorreoCliente, string Referencia, string Descripcion, string Token, string FechaCreacion, string FechaActualizado, bool Expira, string FechaExpiracion, string Moneda, string Lenguaje, int Servicio, string UrlCodigoQr, bool Pagado, string Pago, string PlantillaFactura, string PlantillaPago, string UrlRespuesta, string CodigoRespuesta, string MensajeRespuesta);

    }

}

