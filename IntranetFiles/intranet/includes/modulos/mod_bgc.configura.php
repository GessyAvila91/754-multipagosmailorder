<?php
//echo "paginamodconfs";
require_once("D:/inetpub/wwwroot/phpapache/includes/class.phpmailer.php");  //este es producion
//require_once("C:/Program Files/wamp/www/phpapache/includes/class.phpmailer.php"); // para pruebas

class SubModulo extends Modulo{
    private $datos;
    private $sql;

    public function __construct(){
        $GLOBALS['pathConv']  = "template_htm/recibofolioconvenio/";
        $GLOBALS['pathCobra'] = "template_htm/gestionescobranza/";
    }

    function _cargacuota(){
        $this->tpl = "gestionescobranza/carga.archivo.htm";
       
        if(is_uploaded_file($_FILES['archivo']['tmp_name'])){
            $sql = "TRUNCATE TABLE param_cuota";
            $rq = mysql_query($sql);    
            
            $sql = "LOAD DATA LOCAL INFILE '".str_replace("\\", "/", $_FILES['archivo']['tmp_name'])."' 
                INTO TABLE `param_cuota` 
                    FIELDS TERMINATED BY ',' 
                        OPTIONALLY ENCLOSED BY '".chr(34)."' 
                    LINES TERMINATED BY '\\r\\n' IGNORE 1 LINES";
            $rq = mysql_query($sql);

            if(!$rq){ $this->msgaviso = "Ocurrio un error: " . mysql_error(); }
                else { $this->msgaviso = "Se ha cargado la informaci&oacute;n"; }
        }

        return array(
            "blkField,blkHead" => array(
                'tipo'        => array("Nivel/Departamento"),
                'cuota'       => array("Cuota"),
                'descripcion' => array("Descripci&oacute;n"),
                'docto'       => array("Tipo Docto."),
                'dias_inac_0' => array("Dias Inactivos <br/>Mayor a", "", "right"),
                'dias_inac_1' => array("Dias Inactivos <br/>Menor a", "", "right"),
                'dias_venc_0' => array("Dias Vencidos <br/>Mayor a", "", "right"),
                'dias_venc_1' => array("Dias Vencidos <br/>Mayor a", "", "right")
            ),
            "blkMain" => "SELECT * FROM param_cuota"
        );
    }

    function _carga_relacion_niveles(){
        $this->tpl = "gestionescobranza/carga.relacion.niveles.especiales.htm";

        if(is_uploaded_file($_FILES['archivo']['tmp_name'])){
            $ruta = pathinfo($_FILES['archivo']['name']);

            if ($ruta['extension'] == 'csv') {
                $sql = "TRUNCATE TABLE relacion_niveles_especiales";
                $rq = mysql_query($sql);

                $sql = "LOAD DATA LOCAL INFILE '".str_replace("\\", "/", $_FILES['archivo']['tmp_name'])."' 
                    INTO TABLE `relacion_niveles_especiales` 
                        FIELDS TERMINATED BY ',' 
                            OPTIONALLY ENCLOSED BY '".chr(34)."' 
                        LINES TERMINATED BY '\\r\\n' IGNORE 1 LINES (niveles,consolidado)";
                $rq = mysql_query($sql);

                if(!$rq){ $this->msgaviso = "Ocurrio un error: ".mysql_error(); }
                else{
                    $this->msgaviso = "Se ha cargado la informaci&oacute;n";
                    $sql = "UPDATE relacion_niveles_especiales SET fecha_carga = NOW(), usuario_carga = '".$_SESSION['user']."'";
                    mysql_query($sql);
                }

            } else{$this->msgaviso = "Seleccione un archivo con extension CSV";}
        }

        return array(
            "blk"  => "SELECT * FROM bitacora_gestioncobranza_intelisis.relacion_niveles_especiales",
            "blk1" => "SELECT * FROM bitacora_gestioncobranza_intelisis.relacion_niveles_especiales"
        );
    }

    /**
	 * Se proporciona interfaz para cargar el catalogo de promotores mediante un CSV
	 */
    function _cat_prom(){
        $this->tpl = "gestionescobranza/carga.catalogo.html";

        if(is_uploaded_file($_FILES['archivo']['tmp_name'])){
            $sql = "TRUNCATE TABLE catalogo_promotores";
            $rq = mysql_query($sql);

            $error = false;
            $log = array();
            $archivo = file(str_replace("\\", "/", $_FILES['archivo']['tmp_name']));

            foreach($archivo as $num_linea => $linea){
                $line = explode(',', $linea);

                if(strcmp($line[5], "PRE INTERMEDIA") == 0){ $line[5] = "PREINTERMEDIA"; }
                if(strcmp($line[5], "PRE ESPECIAL") == 0){ $line[5] = "PRE-ESPECIAL"; }

                $linea = array(
                    'ruta_ma'        => $line[0],
                    'ruta_gastos'    => (intval($line[1]) > 0)? $line[1] : 'DEFAULT',
                    'nombre'         => $line[2],
                    'nivel_cobranza' => trim($line[3]),
                    'nom_unicaja'    => (intval($line[4]) > 0)? $line[4] : 'DEFAULT',
                    'nom_intelisis'  => $line[5],
                    'grupo'          => trim($line[6])
                );

                $sql = "INSERT INTO catalogo_promotores (ruta_ma, ruta_gastos, nombre, 
                        nivel_cobranza, nom_unicaja, nom_intelisis, grupo) VALUES (
                    '".$linea['ruta_ma']."',        '".$linea['ruta_gastos']."', '".$linea['nombre']."',
                    '".$linea['nivel_cobranza']."', '".$linea['nom_unicaja']."', '".$linea['nom_intelisis']."',
                    '".$linea['grupo']."')";
                $rq = mysql_query($sql);

                if(!$rq){
                    $error = true;
                    $log[] = mysql_error();
                }
            }

            if($error){
                $errores = implode(', ', $log);
                $this->msgaviso = "Ocurrio un error: ".$errores;

            } else{ $this->msgaviso = "Se ha cargado la informaci&oacute;n"; }
        }

        return array(
            "blkField,blkHead" => array(
                'ruta_ma'        => array("RUTA M.A."),
                'ruta_gastos'    => array("RUTA GTOS"),
                'nombre'         => array("NOMBRE DE AGENTE"),
                'nivel_cobranza' => array("NIVEL"),
                'nom_unicaja'    => array("NOMINA UNICAJA"),
                'nom_intelisis'  => array("NOMINA INTELISIS"),
                'grupo'          => array("GRUPO")
            ),
            "blkMain" => "SELECT * FROM catalogo_promotores"
        );
    }

    function _asig_manual(){
        $this->tpl = "gestionescobranza/carga.asignacion.html";

        if(is_uploaded_file($_FILES['archivo']['tmp_name'])){
            $sql = "TRUNCATE TABLE `varios`.`_mapps_asignacion_manual`";
            $rq = mysql_query($sql);

            $sql = "LOAD DATA LOCAL INFILE '".str_replace("\\", "/", $_FILES['archivo']['tmp_name'])."' 
                INTO TABLE `varios`.`_mapps_asignacion_manual` 
                    FIELDS TERMINATED BY ',' 
                        OPTIONALLY ENCLOSED BY '".chr(34)."' 
                    LINES TERMINATED BY '\\r\\n' IGNORE 1 LINES";
            $rq = mysql_query($sql);

            if(!$rq){ $this->msgaviso = "Ocurrio un error: ".mysql_error(); }
                else{ $this->msgaviso = "Se ha cargado la informaci&oacute;n"; }
        }

        return array(
            "blkField,blkHead" => array(
                'cuenta' => array("Cliente"),
                'agente' => array("Agente"),
                'tipo'   => array("Tipo")
            ),
            "blkMain" => "SELECT * FROM `varios`.`_mapps_asignacion_manual`"
        );
    }

	/**
	 * Muestra el formulario de captura
	 */
    function _captura(){
        $this->tpl = "gestionescobranza/captura.bitacora.htm";
        $bloque = array("blkPost" => empty($_POST)? array() : $_POST);

        if(!empty($_POST)){
            $rq = mysql_query("SELECT * FROM config ORDER BY id DESC LIMIT 1");
            $config = mysql_fetch_assoc($rq);

            if($config['intelisis'] == 1){
                $query = "SELECT agente FROM data_c086 d WHERE BINARY(cuenta) = BINARY('".$_POST['cuenta']."') 
                    AND BINARY(factura) = BINARY('".$_POST['factura']."')";
            } else{
                $query = "SELECT rp.nom_unicaja FROM data_c086 d LEFT JOIN ruta_promotor rp ON rp.ruta = d.rutacobro
                    WHERE BINARY(cuenta) = BINARY('".$_POST['cuenta']."') AND BINARY(factura) = BINARY('".$_POST['factura']."')";
            }

            $rq = mysql_query($query);

            if($rq && mysql_num_rows($rq) > 0){
                $nomina = mysql_result($rq, 0);
                $query = "SELECT count(Distinct cuenta) AS cuentas FROM data_c086 
                        LEFT JOIN (SELECT ruta, nombre FROM ruta_promotor WHERE nom_unicaja = '".$nomina."') AS ruta 
                            ON ruta.ruta = data_c086.rutacobro
                    WHERE ruta.nombre IS NOT NULL";

                $cartera['cuentas'] = mysql_result(mysql_query($query), 0);

            } else{ $cartera['cuentas'] = 0; }

            $sql = "SELECT 
                    C086.cuenta,  C086.factura,          C086.fechaultimopago, C086.fechafactura, C086.saldocapital, C086.druta, 
                    C086.articulo, C086.diasinactividad, C086.diasvencidos,    C086.intereses,    C086.pzo,

                    I231.nomb AS nombre,           I231.domi AS domicilio,       I231.entr AS entre,
                    I231.colo AS colonia,          I231.ciud AS ciudad,          I231.esta AS estado, 
                    I231.ruta AS rutacobro,        I231.ava1 AS aval,            I231.domiava1 AS aval_domicilio,
                    I231.coloava1 AS aval_colonia, I231.ciudava1 AS aval_ciudad, I231.tele AS telefono,
                    I231F.abonos,                  I231F.importe,                I231F.vencido AS saldovencido,

                    CASE WHEN I231F.saldo IS NULL THEN 0 ELSE I231F.saldo END AS saldo,
                    fn_cuota(C086.diasinactividad, C086.diasvencidos, C086.cuenta, C086.factura) AS cuota,

                    PROM.nivel_cobranza AS etapa,  PROM.ruta AS ruta,              PROM.nom_unicaja AS promotor,
                    PROM.nombre AS nombrepromotor, PROM.nom_intelisis AS intelisis 
                FROM data_c086 C086 
                    LEFT JOIN data_231 I231 ON I231.cuen = C086.cuenta 
                    LEFT JOIN data_231F I231F ON I231F.cuen = C086.cuenta AND I231F.refcia = C086.factura 
                    LEFT JOIN ruta_promotor PROM ON PROM.ruta = CAST(C086.rutacobro AS UNSIGNED) 
                WHERE BINARY(C086.cuenta) = BINARY('".$_POST['cuenta']."') 
                    AND BINARY(C086.factura) = BINARY('".$_POST['factura']."')";

            $rq = $this->mysql_query($sql);
            $dt = @mysql_fetch_assoc($rq);

            if(!empty($dt)){
                $this->tpl = "gestionescobranza/captura.bitacora.registro.htm";

                $bloque = array(
                    "blkResult"  => array($dt),
                    "blkResulv"  => "SELECT * FROM catalogo_resultado ORDER BY orden",
                    "cartera"    => array($cartera),
                    "capturista" => array($_SESSION['user']),
                    "config"     => array($config)
                );

            } else { $this->msgaviso = "No se encontro coincidencia cuenta-factura"; }
        }

        return $bloque;
    }

    function _captura_int(){
        $this->tpl = "gestionescobranza/captura.bitacora.htm";
        $bloque = array("blkPost" => empty($_POST) ? array() : $_POST);

        if(!empty($_POST)){
            $rq = mysql_query("SELECT * FROM config LIMIT 1");
            $config = mysql_fetch_assoc($rq);

            $query = "SELECT agente FROM bitacora_gestioncobranza_intelisis.data_c086 d 
                WHERE BINARY(cuenta) = BINARY('".$_POST['cuenta']."') AND BINARY(factura) = BINARY('".$_POST['factura']."') 
                UNION ALL 
                    SELECT agente FROM bitacora_gestioncobranza_intcob.data_c086 d 
                    WHERE BINARY(cuenta) = BINARY('".$_POST['cuenta']."') AND BINARY(factura) = BINARY('".$_POST['factura']."')";

            $rq = mysql_query($query);

            if($rq && mysql_num_rows($rq) > 0){
                $nomina = mysql_result($rq, 0);
                $query = "SELECT count(Distinct cuenta) AS cuentas FROM data_c086 WHERE agente = '".$nomina."'";
                $query = "SELECT sum(CUENTAS) AS cuentas FROM(
                        SELECT count(Distinct cuenta) AS cuentas FROM bitacora_gestioncobranza_intelisis.data_c086 
                        WHERE agente = '".$nomina."'				
                    UNION ALL	
                        SELECT count(Distinct cuenta) AS cuentas FROM bitacora_gestioncobranza_intcob.data_c086 
                        WHERE agente = '".$nomina."') a";

                $cartera['cuentas'] = mysql_result(mysql_query($query), 0);

            } else { $cartera['cuentas'] = 0; }

            $consulta = "SELECT 'MAVI' AS origen, 
                    C086.cuenta,       C086.factura,  C086.mov,                C086.fechaultimopago,     C086.fechafactura,
                    C086.saldocapital, C086.druta,    C086.articulo,           C086.diasinactividad,     C086.diasvencidos,
                    C086.intereses,    C086.pzo,      C086.rutacobro AS etapa, C086.agente AS intelisis, C086.rutacobro_int AS ruta,
								
                    I231.nomb AS nombre,           I231.domi AS domicilio,       I231.entr AS entre,
                    I231.colo AS colonia,          I231.ciud AS ciudad,          I231.esta AS estado,
                    I231.ruta AS rutacobro,        I231.ava1 AS aval,            I231.domiava1 AS aval_domicilio,
                    I231.coloava1 AS aval_colonia, I231.ciudava1 AS aval_ciudad, I231.tele AS telefono,

                    I231F.abonos,                  I231F.importe,                I231F.vencido AS saldovencido,

                    CASE WHEN I231F.saldo IS NULL THEN 0 ELSE I231F.saldo END AS saldo,
                    PROM.nom_intelisis AS promotor, PROM.nombre AS nombrepromotor,
                    O.PROCESO as proceso, O.ESTATUS as ESTATUS,
                    CASE
                        WHEN O.ESTATUS = 100 THEN 1
                        WHEN O.ESTATUS = 101 THEN 1
                        WHEN O.ESTATUS IS NULL THEN 1
                        WHEN O.PROCESO NOT IN ('localizac', 'cobroaval', 'defuncion', 'legal', 'insolvenc') THEN 1
                        ELSE 0
                    END cteEstatus,
                    P.IDCLIENTE as vecinales, Q.IDCLIENTE as referencias 
                FROM bitacora_gestioncobranza_intelisis.data_c086 C086 
                    LEFT JOIN bitacora_gestioncobranza_intelisis.data_231 I231 ON BINARY(I231.cuen) = BINARY(C086.cuenta) 
                    LEFT JOIN bitacora_gestioncobranza_intelisis.data_231F I231F ON BINARY(I231F.cuen) = BINARY(C086.cuenta) 
                        AND BINARY(I231F.refcia) = BINARY(C086.factura) 
                    LEFT JOIN bitacora_gestioncobranza_intelisis.catalogo_promotores PROM 
                        ON BINARY(PROM.nom_intelisis) = BINARY(C086.agente) 
                    LEFT JOIN (
                        SELECT A.* FROM bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta A 
                            INNER JOIN (
                                    SELECT MAX(replace(replace(substring(IDPROCESO, (SELECT LENGTH(IDPROCESO) - 18 + 1), 18), '/', ''), ':', '')) AS ULTIMO_REG,
                                        idcliente 
								    FROM bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta 
								    GROUP BY idcliente) B 
                                ON B.idcliente = A.idcliente 
                                AND B.ULTIMO_REG = (SELECT replace(replace(substring(A.IDPROCESO, (SELECT length(A.IDPROCESO) - 18 + 1), 18), '/', ''), ':', ''))
                        ) O  ON C086.cuenta = O.IDCLIENTE 
                    LEFT JOIN (
                            SELECT IDCLIENTE FROM bitacora_gestioncobranza_intelisis.cob_procesos_descripcion_finca_vec 
                            WHERE IDPROCESO = 'TEMPORAL' GROUP BY IDCLIENTE
                        ) P ON C086.cuenta = P.IDCLIENTE 
                    LEFT JOIN (
                            SELECT IDCLIENTE FROM bitacora_gestioncobranza_intelisis.cob_procesos_contactos 
								WHERE IDPROCESO = 'TEMPORAL' GROUP BY IDCLIENTE
                        ) Q ON C086.cuenta = Q.IDCLIENTE 

                WHERE BINARY( C086.cuenta) = BINARY('".$_POST['cuenta']."') 
                    AND BINARY( C086.factura) = BINARY('".$_POST['factura']."') 
                UNION ALL 
                    SELECT 'MAVICOB' AS origen,
                        C086.cuenta,       C086.factura, C086.mov,                C086.fechaultimopago,     C086.fechafactura,
                        C086.saldocapital, C086.druta,   C086.articulo,           C086.diasinactividad,     C086.diasvencidos,
                        C086.intereses,    C086.pzo,     C086.rutacobro AS etapa, C086.agente AS intelisis, C086.rutacobro_int AS ruta,

                        I231.nomb AS nombre,           I231.domi AS domicilio,       I231.entr AS entre,
                        I231.colo AS colonia,          I231.ciud AS ciudad,          I231.esta AS estado,
                        I231.ruta AS rutacobro,        I231.ava1 AS aval,            I231.domiava1 AS aval_domicilio,
                        I231.coloava1 AS aval_colonia, I231.ciudava1 AS aval_ciudad, I231.tele AS telefono,

                        I231F.abonos,                  I231F.importe,                I231F.vencido AS saldovencido,
                        CASE WHEN I231F.saldo IS NULL THEN 0 ELSE I231F.saldo END AS saldo,
                        PROM.nom_intelisis AS promotor, PROM.nombre AS nombrepromotor,
                        O.PROCESO as proceso, O.ESTATUS as ESTATUS,
                        CASE
                            WHEN O.ESTATUS = 100 THEN 1
                            WHEN O.ESTATUS = 101 THEN 1
                            WHEN O.ESTATUS IS NULL THEN 1
                            WHEN O.PROCESO NOT IN ('localizac', 'cobroaval', 'defuncion', 'legal', 'insolvenc') THEN 1
                            ELSE 0
                        END cteEstatus,
                        P.IDCLIENTE as vecinales, Q.IDCLIENTE as referencias 
                    FROM bitacora_gestioncobranza_intcob.data_c086 C086 
                        LEFT JOIN bitacora_gestioncobranza_intcob.data_231 I231 ON BINARY(I231.cuen) = BINARY(C086.cuenta) 
                        LEFT JOIN bitacora_gestioncobranza_intcob.data_231F I231F ON BINARY(I231F.cuen) = BINARY(C086.cuenta) 
                            AND BINARY(I231F.refcia) = BINARY(C086.factura) 
                        LEFT JOIN bitacora_gestioncobranza_intcob.catalogo_promotores PROM 
                            ON BINARY(PROM.nom_intelisis) = BINARY(C086.agente) 
                        LEFT JOIN (
                            SELECT A.* FROM bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta A 
                                INNER JOIN (
                                    SELECT MAX(replace(replace(substring(IDPROCESO, (SELECT LENGTH(IDPROCESO) - 18 + 1), 18), '/', ''), ':', '')) AS ULTIMO_REG,
                                        idcliente 
                                    FROM bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta 
                                    GROUP BY idcliente
                                ) B ON B.idcliente = A.idcliente AND B.ULTIMO_REG = (SELECT replace(replace(substring(A.IDPROCESO, (SELECT length(A.IDPROCESO) - 18 + 1), 18), '/', ''), ':', '')) 
                            ) O ON C086.cuenta = O.IDCLIENTE 
                        LEFT JOIN (
                            SELECT IDCLIENTE FROM bitacora_gestioncobranza_intelisis.cob_procesos_descripcion_finca_vec 
                                WHERE IDPROCESO = 'TEMPORAL' GROUP BY IDCLIENTE 
                            ) P ON C086.cuenta = P.IDCLIENTE 
                        LEFT JOIN (
                            SELECT IDCLIENTE FROM bitacora_gestioncobranza_intelisis.cob_procesos_contactos 
                                WHERE IDPROCESO = 'TEMPORAL' GROUP BY IDCLIENTE 
                            ) Q ON C086.cuenta = Q.IDCLIENTE 

                    WHERE BINARY( C086.cuenta) = BINARY('".$_POST['cuenta']."') 
                        AND BINARY( C086.factura) = BINARY('".$_POST['factura']."')";

            $rq = $this->mysql_query($consulta) or die (mysql_error());
            $dt = @mysql_fetch_assoc($rq);

            if(!empty($dt)){
                $this->tpl = "gestionescobranza/captura.bitacora.registro.htm";
                $bloque = array(
                    "blkResult"  => array($dt),
                    "blkResulv"  => "SELECT * FROM catalogo_resultado ORDER BY nombre",
                    "cartera"    => array($cartera),
                    "capturista" => array($_SESSION['user']),
                    "config"     => array($config)
                );

            } else { $this->msgaviso = "No se encontro coincidencia cuenta-factura"; }
        }

        return $bloque;
    }

    function _cargar_imgs_embargos(){
        $this->tpl = "gestionescobranza/formulario_cargar_imgs_android.htm";
        include('../conexion_android/conexion_android.php');

        date_default_timezone_set("America/Mexico_City");
        $cont = 0;

        foreach($_FILES as $key){
            $temp = explode(".", $key["name"]);
            $extension = end($temp);
            $error = 0;
            $allowedExts = array("gif", "jpeg", "jpg", "png");

            if((($key["type"] == "image/gif") || ($key["type"] == "image/jpeg") || ($key["type"] == "image/jpg")
                    || ($key["type"] == "image/pjpeg") || ($key["type"] == "image/x-png") || ($key["type"] == "image/png"))
                    && ($key["size"] < 1000000) && in_array($extension, $allowedExts)){

                if ($key["error"] > 0){ $this->msgaviso = "Return Code: ".$key["error"]."<br>"; }
                else{
                    $fecha = date('y-m-d');
                    $time = time();
                    $cont++;

                    $nombre = $key['tmp_name'];

                    $tsql = "INSERT INTO COB_ARCHIVOS_EMBARGOS(NOMBRE,ARCHIVO) VALUES(?, CAST(? AS VARBINARY(MAX)));";

                    // Open data as a stream.
                    $pic = fopen($key['tmp_name'], "rb");
                    $data = fread($pic, filesize($key['tmp_name']));
                    $sqlpic = $data;

                    fclose($pic);

                    $nombre = "embargos_".$fecha."_".$time."_".$cont;

                    // Define the parameter array, Note the specification of the PHPTYPE and SQLTYPE.
                    $params = array($nombre, $sqlpic);

                    $prepare = @odbc_prepare($id_conn_ws, $tsql);
                    // Execute the query.
                    $stmt = odbc_execute($prepare, $params);

                    $this->msgaviso = "Imagen(es) Subida(s) Correctamente";
                }
            } else{
                $error++;

                echo '<script type="text/javascript">
					alert("El archivo: \"'.$key['name'].'\" tiene un formato invalido ó supera el tamaño maximo valido de 1mb");
				  </script>';
            }
        }
    }

	function _mostrar_imgs_embargos(){
		$this->tpl = "gestionescobranza/mostrar_imgs_emabargos.htm";
	}

	function _interfaz_telefonos_gestiones(){
		$this->tpl = "gestionescobranza/interfaz_reporte_telefonos_gestiones.htm";
		$GLOBALS['nomina']= $_SESSION['nomina'];
	}

	function _gestor_correos_rep_tel(){
		$this->tpl = "gestionescobranza/gestor_correos_rep_tel.htm";
	}

	function _reporte_telefonos_gestiones(){
		$this->tpl = "gestionescobranza/reporte_telefonos_gestiones.htm";
	}

    function _registro(){
            $rq = mysql_query("SELECT * FROM config ORDER BY id DESC LIMIT 1");
            $config = mysql_fetch_assoc($rq);
        
            $GLOBALS['_link'] =  "inicio.php?mod=bgc.configura&ac=captura_int";

            if(!empty($_POST)){
                    $folio_adjudicacion = $_POST['folio_adjudicacion'];
                    $monto_adjudicacion = $_POST['monto_adjudicacion'];
                    $factura_adjudicacion = $_POST['factura_adjudicacion'];
                    $gastos_adjudicacion = $_POST['gastos_adjudicacion'];
                    $_POST['comentario'] = str_replace(array('\n','\r'), '', $_POST['comentario']);

                    unset($_POST['folio_adjudicacion']);
                    unset($_POST['monto_adjudicacion']);
                    unset($_POST['factura_adjudicacion']);
                    unset($_POST['gastos_adjudicacion']);
                    //inicio de negativa 1
                    switch($_POST['TipoNegativa']){
                        case 1:$val = "NEGATIVA DE PAGO EXPRESA (MANIFESTADA)";break;
                        case 2:$val = "AGRESION FISICA";break;
                        case 3:$val = "CTE Y/O AVAL SE ESCONDE O LO NIEGAN";break;
                        case 4:$val = "CTE Y/O AVAL NIEGA IDENTIDAD";break;
                        case 5:$val = "CTE Y/O AVAL ASESORADO POR UN ABOGADO";break;
                        case 6:$val = "CTE SOLICITA FUERZA PÚBLICA";break;
                    }
					////Campo para guardar si existen numero de telefonos para las gestiones Aviso, Recado, Convenio LCT y Promesa de Pago
					
					$numero="";
					if(isset($_POST['numero']))
					{
						$numero = $_POST['numero'];
					}
					
                    //final de negativa 1
                    $sql =  "INSERT INTO registros (
                                cuenta, mov, factura, fechaultimopago, fechafactura, saldocapital, druta, articulo,
                                diasinactividad, diasvencidos, intereses, cuota, nombre, domicilio, entre, colonia,
                                ciudad, estado, rutacobro, aval, aval_domicilio, aval_colonia, aval_ciudad, telefono,
                                saldo, importe, fecha_captura, hora_captura, hora_captura_final, fecha_inicio,
                                hora_inicio, fecha_final, hora_final, resultado, valor_resultado, proximavisita,
                                comentario, pago_promesa, pago_importe, pago_quien, recibo, promotor,
                                etapa, ruta, intelisis, saldovencido, cartera, folio, capturista, pzo, abonos,
                                imp_convenio, imp_inicial, num_convenio,numero,origen
                            )
                            VALUES ('".$_POST['cuenta']."','".$_POST['mov']."','".$_POST['factura']."',
                                '".$_POST['fechaultimopago']."','".$_POST['fechafactura']."',".$_POST['saldocapital'].",
                                ".$_POST['druta'].",'".$_POST['articulo']."',".$_POST['diasinactividad'].",
                                ".$_POST['diasvencidos'].",".$_POST['intereses'].",'".$_POST['cuota']."',
                                '".$_POST['nombre']."','".$_POST['domicilio']."','".$_POST['entre']."',
                                '".$_POST['colonia']."','".$_POST['ciudad']."','".$_POST['estado']."',
                                '".$_POST['rutacobro']."','".$_POST['aval']."','".$_POST['aval_domicilio']."',
                                '".$_POST['aval_colonia']."','".$_POST['aval_ciudad']."','".$_POST['telefono']."',
                                ".$_POST['saldo'].",".$_POST['importe'].",'".$_POST['fecha_captura']."',
                                '".$_POST['hora_captura']."','".date('H:i:s')."','".$_POST['fecha_inicio']."',
                                '".$_POST['hora_inicio']."','".$_POST['fecha_final']."','".$_POST['hora_final']."',
                                '".$_POST['resultado']."',
                                '".($_POST['resultado']=="negativa"?$val:$_POST['valor_resultado'])."',
                                '".$_POST['proximavisita']."',
                                '".($_POST['ComentarioGestion'] != ""?$_POST['ComentarioGestion']:$_POST['comentario'])."',
                                '".$_POST['pago_promesa']."','".($_POST['pago_importe'] == "" ? "0" : $_POST['pago_importe'])."',
                                '".$_POST['pago_quien']."','".$_POST['recibo']."','".$_POST['promotor']."','".$_POST['etapa']."',
                                '".$_POST['ruta']."','".$_POST['intelisis']."',".$_POST['saldovencido'].",".$_POST['cartera'].",
                                ".$_POST['folio'].",'".$_POST['capturista']."',
                                ".$_POST['pzo'].",".($_POST['abonos'] == "" ? "0" : $_POST['abonos']).",
                                ".($_POST['imp_convenio'] == "" ? "0" : $_POST['imp_convenio']).",
                                ".($_POST['imp_inicial'] == "" ? "0" : $_POST['imp_inicial']).",'".$_POST['num_convenio']."', '".$numero."','".$_POST['origen']."')";

                    $rq = mysql_query($sql) or die("ERROR " . mysql_error());
                    if( $rq ) {
                            $id = mysql_insert_id();
                            //Jose
                            if( $_POST['resultado'] == "localizac" ) {
                                    $fecha = date( "Y/m/d" );
                                    $hora = date( "H:i:s" );

                                    $idProceso = $_POST['intelisis'] . $_POST['cuenta'] . $fecha . $hora;

                                    $sql = "
                                        SELECT * FROM bitacora_gestioncobranza_intelisis.cob_procesos_descripcion_finca_vec
                                        WHERE IDCLIENTE = '".$_POST['cuenta']."' AND IDPROCESO='TEMPORAL'";
                                    $exec = mysql_query( $sql ) or die( mysql_error() );
                                    $rows = mysql_num_rows( $exec );
                                    if( $rows > 0 ) {
                                            $numVecinos = $rows;
                                            $vecinos = 'CAPTURAR VECINOS';
                                    } else {
                                            $numVecinos = 0;
                                            $vecinos = 'SIN VECINOS';
                                            $descripcionAlrededor = $_POST['comentSinVecins'];
                                    }
                                    $sql = mysql_query( "SELECT Nomina FROM agentes WHERE Agente = ( SELECT Celula FROM agentes WHERE Agente = '".$_POST['intelisis']."' )" );
                                    $exec = mysql_fetch_assoc( $sql );
                                    $jefe = $exec['Nomina'];

                                    $datos = array(
                                        'idProceso'         => strtoupper( $idProceso ),
                                        'idCliente'         => strtoupper( $_POST['cuenta'] ),
                                        'proceso'           => 'localizac',
                                        'estatus'           => 3,
                                        'fecha'             => $fecha,
                                        'hora'              => $hora,
                                        'agente'            => strtoupper( $_POST['intelisis'] ),
                                        'jefe'              => strtoupper( $jefe ),
                                        'numVecinos'        => $numVecinos,
                                        'vecinos'           => $vecinos,
                                        'descripcionAlred'  => ( $descripcionAlrededor != "" ) ? strtoupper( $descripcionAlrededor ) : false,
                                        'origen'            => 0,
                                        'empresa'           => strtoupper( $_POST['empresa'] ),
                                        'telefono'          => $_POST['telefono'],
                                        'comentarioEmpleo'  => strtoupper( $_POST['comentarioEmpleo'] ),
                                        'motivo'            => strtoupper( $_POST['comentario'] ),
                                        'nombreAval'        => strtoupper( $_POST['nombreAval'] ),
                                        'telAval'           => strtoupper( $_POST['telAval'] ),
                                        'domicilioAval'     => strtoupper( $_POST['domicilioAval'] )
                                    );
                                    
                                            $sql = "
                                                INSERT INTO cob_fichas_procesos_estcuenta(
                                                        IDPROCESO, IDCLIENTE, PROCESO, ESTATUS, FECHA, HORA, AGENTE, JEFE, NUMERO_VECINOS,
                                                        VECINOS, DESCRIPCION_ALREDEDOR, ORIGEN )
                                                    VALUES(
                                                        '".$datos['idProceso']."',                      '".$datos['idCliente']."',
                                                        '".$datos['proceso']."',                         ".$datos['estatus'].",
                                                        '".$datos['fecha']."',                          '".$datos['hora']."',
                                                        '".$datos['agente']."',                         '".$datos['jefe']."',
                                                         ".$datos['numVecinos'].",                       '".$datos['vecinos']."',
                                                        '".utf8_decode( $datos['descripcionAlred'] )."', ".$datos['origen']."
                                                    )";
                                            mysql_query( $sql ) or die( mysql_error() );
                                    
                                            $sql = "
                                                INSERT INTO cob_procesos_contactos(
                                                        IDPROCESO, IDCLIENTE, IDCTO, NOMBRE, PARENTESCO, TELEFONO, ESTATUS )
                                                    VALUES(
                                                        '".$datos['idProceso']."',  '".$datos['idCliente']."',
                                                        'EMPRESA',                  '".utf8_decode( $datos['empresa'] )."',
                                                        'LABORAL',                  '".$datos['telefono']."',
                                                        1
                                                    )";
                                            mysql_query( $sql ) or die( mysql_error() );
                                            //Fin de insercion___________
                                            //Insercion en la tabla cob_procesos_estado_contacto unicamente para tener los campos de la empresa
                                            $sql = "
                                                INSERT INTO cob_procesos_estado_contacto(
                                                        IDPROCESO, IDCLIENTE, IDCONTACTO, IDAGENTE, FECHA, HORA, RESULTADO )
                                                    VALUES(
                                                        '".$datos['idProceso']."',  '".$datos['idCliente']."',
                                                        'EMPRESA',                  '".$datos['agente']."',
                                                        '".$datos['fecha']."',      '".$datos['hora']."',
                                                        '".utf8_decode( $datos['comentarioEmpleo'] )."'
                                                    )";
                                            mysql_query( $sql ) or die( mysql_error() );
                                            //Fin de insercion___________
                                    
                                                    $sql = "
                                                        INSERT INTO cob_procesos_contactos(
                                                                IDPROCESO, IDCLIENTE, IDCTO, NOMBRE, PARENTESCO, TELEFONO, DIRECCION, ESTATUS )
                                                            VALUES(
                                                                '".$datos['idProceso']."',  '".$datos['idCliente']."',
                                                                'AVAL',                     '".utf8_decode( $datos['nombreAval'] )."',
                                                                'AVAL',                     '".$datos['telAval']."',
                                                                '".utf8_decode( $datos['domicilioAval'] )."',  1
                                                            )";
                                                    mysql_query( $sql ) or die( mysql_error() );
                                    //      }
                                            //Fin de insercion___________


                                                    $sql = "
                                                        UPDATE cob_procesos_descripcion_finca_vec
                                                        SET IDPROCESO = '".$datos['idProceso']."' 
                                                        WHERE IDCLIENTE = '".$datos['idCliente']."' 
                                                        AND IDPROCESO = 'TEMPORAL';
                                                    ";
                                                    mysql_query( $sql ) or die( mysql_error() );
                                                    $sql = "
                                                        UPDATE cob_procesos_descripcion_fisica_vec
                                                        SET IDPROCESO = '".$datos['idProceso']."' 
                                                        WHERE IDCLIENTE = '".$datos['idCliente']."' 
                                                        AND IDPROCESO = 'TEMPORAL';
                                                    ";
                                                    mysql_query( $sql ) or die( mysql_error() );
                                                    $sql = "
                                                        UPDATE cob_procesos_contactos
                                                        SET IDPROCESO = '".$datos['idProceso']."' 
                                                        WHERE IDCLIENTE = '".$datos['idCliente']."' 
                                                        AND IDPROCESO = 'TEMPORAL';
                                                    ";
                                                    mysql_query( $sql ) or die( mysql_error() );
                                                    $sql = "
                                                        UPDATE cob_procesos_estado_contacto
                                                        SET IDPROCESO = '".$datos['idProceso']."' 
                                                        WHERE IDCLIENTE = '".$datos['idCliente']."' 
                                                        AND IDPROCESO = 'TEMPORAL';
                                                    ";
                                                    mysql_query( $sql ) or die( mysql_error() );


                            }
                            //_______________
                            //inicio cobro al aval
                            if ( $_POST['resultado'] == "cobroaval") {

                                    $sql = mysql_query( "SELECT Nomina FROM agentes WHERE Agente = ( SELECT Celula FROM agentes WHERE Agente = '".$_POST['intelisis']."' )" );
                                    $exec = mysql_fetch_assoc( $sql );
                                    $jefe = $exec['Nomina'];
                                    //date( "H:i:s" )
                                    $idAval = $_SESSION[ 'nomina' ] . $_POST[ 'cuenta' ] . date( "Y/m/d" ) . date( "H" ) . ":" . date( "i" ) . ":" . date( "s" );

                                    if ( trim( $_POST[ 'valor_resultado' ] ) == "Defuncion" || trim( $_POST[ 'valor_resultado' ] ) == "Cte no vive en Domicilio" ) {
                                            $strQuery = "INSERT INTO cob_fichas_procesos_estcuenta( IDPROCESO, IDCLIENTE, PROCESO, ESTATUS, FECHA, HORA, AGENTE, ORIGEN, COMENTARIO_RECHAZO, VECINOS, NUMERO_VECINOS, DESCRIPCION_ALREDEDOR, JEFE)
                                                        VALUE( '" . $idAval . "', '" . $_POST['cuenta'] . "', '" . $_POST['resultado'] . "', 3, DATE_FORMAT(NOW(), '%Y/%m/%d'), TIME(NOW()), '" . $_SESSION['nomina'] . "', 0 , 'WEB', 'CAPTURAR VECINOS', 2, '' , '" . $jefe . "')";
                                            mysql_query( $strQuery ) or die( "ERROR ESTATUS CUENTA: " . mysql_error( ) );
                                            $strQuery = "INSERT INTO cob_procesos_descripcion_fisica_vec
                                                        ( IDPROCESO, IDCLIENTE, IDAGENTE, IDVECINO, FECHA, HORA, EJERCICIO, QUINCENA, SEXO, ESTATURA, COMPLEXION, COLOR_PIEL, CABELLO, COLOR_CABELLO, NOMBRE_VECINO, COMENTARIO_VECINO )
                                                        VALUE( '" . $idAval . "' , '" . $_POST['cuenta'] . "' , '" . $_SESSION['nomina'] . "' , 'VECINO1' , DATE(NOW()) , TIME(NOW()), DATE_FORMAT(NOW(), '%Y'), 12345, '" . $_POST['exSexo1'] . "',
                                                               '" . $_POST['exEstatura1'] . "' , '" . $_POST['exComplexion1'] . "' , '" . $_POST['exColorPiel1'] . "' , '" . $_POST['exCabello1'] . "' , '" . $_POST['exColorCabello1'] . "',
                                                               '" . $_POST['exNombre1'] . "', '" . $_POST['exComentario1'] . "')";
                                            mysql_query( $strQuery ) or die( "ERROR FISICA VECINO1: " . mysql_error( ) );
                                            $strQuery = "INSERT INTO cob_procesos_descripcion_fisica_vec
                                                        ( IDPROCESO, IDCLIENTE, IDAGENTE, IDVECINO, FECHA, HORA, EJERCICIO, QUINCENA, SEXO, ESTATURA, COMPLEXION, COLOR_PIEL, CABELLO, COLOR_CABELLO, NOMBRE_VECINO, COMENTARIO_VECINO )
                                                        VALUE( '" . $idAval . "' , '" . $_POST['cuenta'] . "' , '" . $_SESSION['nomina'] . "' , 'VECINO2' , DATE(NOW()) , TIME(NOW()), DATE_FORMAT(NOW(), '%Y'), 12345, '" . $_POST['exSexo2'] . "',
                                                               '" . $_POST['exEstatura2'] . "' , '" . $_POST['exComplexion2'] . "' , '" . $_POST['exColorPiel2'] . "' , '" . $_POST['exCabello2'] . "' , '" . $_POST['exColorCabello2'] . "',
                                                               '" . $_POST['exNombre2'] . "', '" . $_POST['exComentario2'] . "')";
                                            mysql_query( $strQuery ) or die( "ERROR FISICA VECINO2: " . mysql_error( ) );
                                            
                                            $strQuery = "INSERT INTO cob_procesos_descripcion_finca_vec
                                                        ( IDPROCESO, IDCLIENTE, IDAGENTE, IDVECINO, FECHA, HORA, EJERCICIO, QUINCENA, TIPODEFINCA, COCHERA, CANCEL, NIVELES, COLOR, NUMERO_DE_CASA )
                                                        VALUE( '" . $idAval . "' , '" . $_POST['cuenta'] . "' , '" . $_SESSION['nomina'] . "' , 'VECINO1' , DATE(NOW()) , TIME(NOW()), DATE_FORMAT(NOW(), '%Y'), 12345, '" . $_POST['exTipoFinca1'] . "',
                                                               '" . $_POST['exCochera1'] . "' , '" . $_POST['exCancel1'] . "' , '" . $_POST['exNiveles1'] . "' , '" . $_POST['exColor1'] . "' , '" . $_POST['exNumero1'] . "')";
                                            mysql_query( $strQuery ) or die( "ERROR FINCA VECINO1: " . mysql_error( ) );
                                            $strQuery = "INSERT INTO cob_procesos_descripcion_finca_vec
                                                        ( IDPROCESO, IDCLIENTE, IDAGENTE, IDVECINO, FECHA, HORA, EJERCICIO, QUINCENA, TIPODEFINCA, COCHERA, CANCEL, NIVELES, COLOR, NUMERO_DE_CASA )
                                                        VALUE( '" . $idAval . "' , '" . $_POST['cuenta'] . "' , '" . $_SESSION['nomina'] . "' , 'VECINO2' , DATE(NOW()) , TIME(NOW()), DATE_FORMAT(NOW(), '%Y'), 12345, '" . $_POST['exTipoFinca2'] . "',
                                                               '" . $_POST['exCochera2'] . "' , '" . $_POST['exCancel2'] . "' , '" . $_POST['exNiveles2'] . "' , '" . $_POST['exColor2'] . "' , '" . $_POST['exNumero2'] . "')";
                                            mysql_query( $strQuery ) or die( "ERROR FINCA VECINO2: " . mysql_error( ) );

                                    } else {
                                            $strQuery = "INSERT INTO cob_fichas_procesos_estcuenta( IDPROCESO, IDCLIENTE, PROCESO, ESTATUS, FECHA, HORA, AGENTE, ORIGEN, COMENTARIO_RECHAZO, VECINOS, NUMERO_VECINOS, DESCRIPCION_ALREDEDOR, JEFE)
                                                        VALUE( '" . $idAval . "', '" . $_POST['cuenta'] . "', '" . $_POST['resultado'] . "', 3, DATE_FORMAT(NOW(), '%Y/%m/%d'), TIME(NOW()), '" . $_SESSION['nomina'] . "', 0 , 'WEB', 'SIN VECINOS', 0, '' , '" . $jefe . "')";
                                            mysql_query( $strQuery ) or die( "ERROR ESTATUS CUENTA: " . mysql_error( ) );
                                    }
                       
                            }
                            //fin cobro al aval
                            //inicio de negativa 2
                            if($_POST['resultado'] == "negativa"){
                                    $strQuery = "INSERT INTO tipo_finca(id_registro, tipo, cochera, cancel, niveles, color)
                                                VALUE(".$id.", '".$_POST['TipoFinca']."', '".$_POST['Cochera']."', '".$_POST['Cancel']."', '".$_POST['Niveles']."', '".$_POST['Color']."')";
                                    mysql_query($strQuery) or die(mysql_error());
                                    if($_POST['Visible'] == 1){
                                            $strQuery = "INSERT INTO solvencia(id_registro, articulo, cantidad, estado)
                                                        VALUE(".$id.",'TV','".$_POST['tv1']."','".$_POST['tv2']."')";
                                            mysql_query($strQuery) or die(mysql_error());
                                                    $strQuery = "INSERT INTO solvencia(id_registro, articulo, cantidad, estado)
                                                                VALUE(".$id.",'PANTALLA','".$_POST['pantalla1']."','".$_POST['pantalla2']."')";
                                            mysql_query($strQuery) or die(mysql_error());
                                                    $strQuery = "INSERT INTO solvencia(id_registro, articulo, cantidad, estado)
                                                                VALUE(".$id.",'COMPONENTE','".$_POST['componente1']."','".$_POST['componente2']."')";
                                            mysql_query($strQuery) or die(mysql_error());
                                                    $strQuery = "INSERT INTO solvencia(id_registro, articulo, cantidad, estado)
                                                                VALUE(".$id.",'TEATRO','".$_POST['teatro1']."','".$_POST['teatro2']."')";
                                            mysql_query($strQuery) or die(mysql_error());
                                                    $strQuery = "INSERT INTO solvencia(id_registro, articulo, cantidad, estado)
                                                                VALUE(".$id.",'SALA','".$_POST['sala1']."','".$_POST['sala2']."')";
                                            mysql_query($strQuery) or die(mysql_error());
                                                    $strQuery = "INSERT INTO solvencia(id_registro, articulo, cantidad, estado)
                                                                VALUE(".$id.",'COMEDOR','".$_POST['comedor1']."','".$_POST['comedor2']."')";
                                            mysql_query($strQuery) or die(mysql_error());
                                                    $strQuery = "INSERT INTO solvencia(id_registro, articulo, cantidad, estado)
                                                                VALUE(".$id.",'LIBRERO','".$_POST['librero1']."','".$_POST['librero2']."')";
                                            mysql_query($strQuery) or die(mysql_error());
                                                    $strQuery = "INSERT INTO solvencia(id_registro, articulo, cantidad, estado)
                                                                VALUE(".$id.",'REFRIGERADOR','".$_POST['refrigerador1']."','".$_POST['refrigerador2']."')";
                                            mysql_query($strQuery) or die(mysql_error());
                                                    $strQuery = "INSERT INTO solvencia(id_registro, articulo, cantidad, estado)
                                                                VALUE(".$id.",'ESTUFA','".$_POST['estufa1']."','".$_POST['estufa2']."')";
                                            mysql_query($strQuery) or die(mysql_error());
                                                    $strQuery = "INSERT INTO solvencia(id_registro, articulo, cantidad, estado)
                                                                VALUE(".$id.",'HORNO','".$_POST['horno1']."','".$_POST['horno2']."')";
                                            mysql_query($strQuery) or die(mysql_error());
                                    }
                                    if(intval($_POST['TipoNegativa']) > 1){
                                            $strQuery = "INSERT INTO quien(id_registro, quien, valor_quien)
                                                         VALUE(".$id.",'".$_POST['PorQuien']."','".$_POST['NombreQuien']."')";
                                            mysql_query($strQuery);
                                    }
                                    $estatura = "";
                                    switch($_POST['Estatura']){
                                        case "Baja":$estatura = "1.50-1.70(Baja)";break;
                                        case "Media":$estatura = "1.70-1.80(Media)";break;
                                        case "Alta":$estatura = "Mas de 1.80(Alta)";break;
                                    }
                                    $strQuery = "INSERT INTO descripcion_fisica(id_registro, sexo, estatura, complexion, colorpiel, cabello, colorcabello)
                                                VALUE(".$id.",'".$_POST['Sexo']."','".$estatura."','".$_POST['Complexion']."','".$_POST['ColorPiel']."','".$_POST['Cabello']."','".$_POST['ColorCabello']."')";
                                    mysql_query($strQuery);
                                    if(intval($_POST['VecinoVisible']) == 1){
                                            $estatura = "";
                                            switch($_POST['EstaturaVecino2']){
                                                case "Baja":$estatura = "1.50-1.70(Baja)";break;
                                                case "Media":$estatura = "1.70-1.80(Media)";break;
                                                case "Alta":$estatura = "Mas de 1.80(Alta)";break;
                                            }
                                            $strQuery = "INSERT INTO descripcion_vecino(id_registro, sexo, estatura, complexion, colorpiel, cabello, colorcabello, tipo, cochera, cancel, niveles, color, nombre, domicilio)
                                                         VALUE(".$id.",'".$_POST['SexoVecino2']."','".$estatura."','".$_POST['ComplexionVecino2']."','".$_POST['ColorPielVecino2']."','".$_POST['CabelloVecino2']."','".$_POST['ColorCabelloVecino2']."', '".$_POST['TipoFincaVecino2']."', '".$_POST['CocheraVecino2']."', '".$_POST['CancelVecino2']."', '".$_POST['NivelesVecino2']."', '".$_POST['ColorVecino2']."','".$_POST['NombreVecino2']."','".$_POST['DomicilioVecino2']."')";
                                            mysql_query($strQuery);
                                            $estatura = "";
                                            switch($_POST['EstaturaVecino1']){
                                                case "Baja":$estatura = "1.50-1.70(Baja)";break;
                                                case "Media":$estatura = "1.70-1.80(Media)";break;
                                                case "Alta":$estatura = "Mas de 1.80(Alta)";break;
                                            }
                                            $strQuery = "INSERT INTO descripcion_vecino(id_registro, sexo, estatura, complexion, colorpiel, cabello, colorcabello, tipo, cochera, cancel, niveles, color, nombre, domicilio)
                                                         VALUE(".$id.",'".$_POST['SexoVecino1']."','".$estatura."','".$_POST['ComplexionVecino1']."','".$_POST['ColorPielVecino1']."','".$_POST['CabelloVecino1']."','".$_POST['ColorCabelloVecino1']."', '".$_POST['TipoFincaVecino1']."', '".$_POST['CocheraVecino1']."', '".$_POST['CancelVecino1']."', '".$_POST['NivelesVecino1']."', '".$_POST['ColorVecino1']."','".$_POST['NombreVecino1']."','".$_POST['DomicilioVecino1']."')";
                                            mysql_query( $strQuery);
                                    }
                                    $sql = mysql_query( "SELECT Nomina FROM agentes WHERE Agente = ( SELECT Celula FROM agentes WHERE Agente = '".$_POST['intelisis']."' )" );
                                    $exec = mysql_fetch_assoc( $sql );
                                    $jefe = $exec['Nomina'];
                                    $idNegativa = $_SESSION[ 'nomina' ] . $_POST[ 'cuenta' ] . date( "Y/m/d" ) . date( "H" ) . ":" . date( "i" ) . ":" . date( "s" );
                                    $strQuery = "INSERT INTO cob_procesos_conexionneg( id ,IDPROCESO)
                                                        VALUE( '" . $id . "' , '" . $idNegativa . "' )";
                                    mysql_query( $strQuery );
                                    if ( intval( $_POST[ 'VecinoVisible' ] ) == 1 ) {
                                            $strQuery = "INSERT INTO cob_fichas_procesos_estcuenta( IDPROCESO, IDCLIENTE, PROCESO, ESTATUS, FECHA, HORA, AGENTE, ORIGEN, COMENTARIO_RECHAZO, VECINOS, NUMERO_VECINOS, DESCRIPCION_ALREDEDOR, JEFE)
                                                        VALUE( '" . $idNegativa . "', '" . $_POST['cuenta'] . "', '" . $_POST['resultado'] . "', 3, DATE_FORMAT(NOW(), '%Y/%m/%d'), TIME(NOW()), '" . $_SESSION['nomina'] . "', 0 , 'WEB', 'CAPTURAR VECINOS', 2, '" . $_POST[ 'ComentarioGestion' ] . "', '" . $jefe . "')";
                                            mysql_query( $strQuery ) or die( "ERROR ESTATUS CUENTA: " . mysql_error( ) );
                                            $strQuery = "INSERT INTO cob_procesos_descripcion_finca_vec
                                                        ( IDPROCESO, IDCLIENTE, IDAGENTE, IDVECINO, FECHA, HORA, EJERCICIO, QUINCENA, TIPODEFINCA, COCHERA, CANCEL, NIVELES, COLOR, NUMERO_DE_CASA )
                                                        VALUE( '" . $idNegativa . "' , '" . $_POST[ 'cuenta' ] . "' , '" . $_SESSION[ 'nomina' ] . "' , 'VECINO1' , DATE(NOW()) , TIME(NOW()), DATE_FORMAT(NOW(), '%Y'), 12345, '" . $_POST[ 'TipoFincaVecino1' ] . "',
                                                               '" . $_POST[ 'CocheraVecino1' ] . "' , '" . $_POST[ 'CancelVecino1' ] . "' , '" . $_POST[ 'NivelesVecino1' ] . "' , '" . $_POST[ 'ColorVecino1' ] . "' , '" . $_POST[ 'DomicilioVecino1' ] . "')";
                                            mysql_query( $strQuery ) or die( "ERROR FINCA VECINO1: " . mysql_error( ) );
                                            $strQuery = "INSERT INTO cob_procesos_descripcion_finca_vec
                                                        ( IDPROCESO, IDCLIENTE, IDAGENTE, IDVECINO, FECHA, HORA, EJERCICIO, QUINCENA, TIPODEFINCA, COCHERA, CANCEL, NIVELES, COLOR, NUMERO_DE_CASA )
                                                        VALUE( '" . $idNegativa . "' , '" . $_POST[ 'cuenta' ] . "' , '" . $_SESSION[ 'nomina' ] . "' , 'VECINO2' , DATE(NOW()) , TIME(NOW()), DATE_FORMAT(NOW(), '%Y'), 12345, '" . $_POST[ 'TipoFincaVecino2' ] . "',
                                                               '" . $_POST[ 'CocheraVecino2' ] . "' , '" . $_POST[ 'CancelVecino2' ] . "' , '" . $_POST[ 'NivelesVecino2' ] . "' , '" . $_POST[ 'ColorVecino2' ] . "' , '" . $_POST[ 'DomicilioVecino2' ] . "')";
                                            mysql_query( $strQuery ) or die( "ERROR FINCA VECINO1: " . mysql_error( ) );
                                            $estatura = "";
                                            switch($_POST['EstaturaVecino1']){
                                                case "Baja":$estatura = "1.50-1.70(Baja)";break;
                                                case "Media":$estatura = "1.70-1.80(Media)";break;
                                                case "Alta":$estatura = "Mas de 1.80(Alta)";break;
                                            }
                                            $strQuery = "INSERT INTO cob_procesos_descripcion_fisica_vec
                                                        ( IDPROCESO, IDCLIENTE, IDAGENTE, IDVECINO, FECHA, HORA, EJERCICIO, QUINCENA, SEXO, ESTATURA, COMPLEXION, COLOR_PIEL, CABELLO, COLOR_CABELLO, NOMBRE_VECINO, COMENTARIO_VECINO )
                                                        VALUE( '" . $idNegativa . "' , '" . $_POST['cuenta'] . "' , '" . $_SESSION['nomina'] . "' , 'VECINO1' , DATE(NOW()) , TIME(NOW()), DATE_FORMAT(NOW(), '%Y'), 12345, '" . $_POST['SexoVecino1'] . "',
                                                               '" . $estatura . "' , '" . $_POST['ComplexionVecino1'] . "' , '" . $_POST['ColorPielVecino1'] . "' , '" . $_POST['CabelloVecino1'] . "' , '" . $_POST['CabelloVecino1'] . "',
                                                               '" . $_POST['NombreVecino1'] . "', '')";
                                            mysql_query( $strQuery ) or die( "ERROR FISICA VECINO1: " . mysql_error( ) );
                                            $estatura = "";
                                            switch($_POST['EstaturaVecino1']){
                                                case "Baja":$estatura = "1.50-1.70(Baja)";break;
                                                case "Media":$estatura = "1.70-1.80(Media)";break;
                                                case "Alta":$estatura = "Mas de 1.80(Alta)";break;
                                            }
                                            $strQuery = "INSERT INTO cob_procesos_descripcion_fisica_vec
                                                        ( IDPROCESO, IDCLIENTE, IDAGENTE, IDVECINO, FECHA, HORA, EJERCICIO, QUINCENA, SEXO, ESTATURA, COMPLEXION, COLOR_PIEL, CABELLO, COLOR_CABELLO, NOMBRE_VECINO, COMENTARIO_VECINO )
                                                        VALUE( '" . $idNegativa . "' , '" . $_POST['cuenta'] . "' , '" . $_SESSION['nomina'] . "' , 'VECINO2' , DATE(NOW()) , TIME(NOW()), DATE_FORMAT(NOW(), '%Y'), 12345, '" . $_POST['SexoVecino2'] . "',
                                                               '" . $estatura . "' , '" . $_POST['ComplexionVecino2'] . "' , '" . $_POST['ColorPielVecino2'] . "' , '" . $_POST['CabelloVecino2'] . "' , '" . $_POST['CabelloVecino2'] . "',
                                                               '" . $_POST['NombreVecino2'] . "', '')";
                                            mysql_query( $strQuery ) or die( "ERROR FISICA VECINO2: " . mysql_error( ) );
                                    } else {
                                            $strQuery = "INSERT INTO cob_fichas_procesos_estcuenta( IDPROCESO, IDCLIENTE, PROCESO, ESTATUS, FECHA, HORA, AGENTE, ORIGEN, COMENTARIO_RECHAZO, VECINOS, NUMERO_VECINOS, DESCRIPCION_ALREDEDOR, JEFE)
                                                        VALUE( '" . $idNegativa . "', '" . $_POST['cuenta'] . "', '" . $_POST['resultado'] . "', 3, DATE_FORMAT(NOW(), '%Y/%m/%d'), TIME(NOW()), '" . $_SESSION['nomina'] . "', 0 , 'WEB', 'SIN VECINOS', 0, '" . $_POST[ 'ComentarioGestion' ] . "', '" . $jefe . "')";
                                            mysql_query( $strQuery ) or die( "ERROR ESTATUS CUENTA: " . mysql_error( ) );
                                    }
                            }
                            //final de negativa 2
                            //inicio defuncion
                            if ( $_POST[ 'resultado' ] == "defuncion" ) {
                                    $fecha = date( "Y/m/d" );
                                    $hora = date( "H:i:s" );
                                    $idProceso = $_POST[ 'intelisis' ] . $_POST[ 'cuenta' ] . $fecha . $hora;
                                    $sql = "
                                        SELECT * FROM bitacora_gestioncobranza_intelisis.cob_procesos_descripcion_finca_vec
                                        WHERE IDCLIENTE = '".$_POST['cuenta']."' AND IDPROCESO='TEMPORAL'";
                                    $exec = mysql_query( $sql ) or die( mysql_error( ) );
                                    $rows = mysql_num_rows( $exec );
                                    if( $rows > 0 ) {
                                            $numVecinos = $rows;
                                            $vecinos = 'CAPTURAR VECINOS';
                                    } else {
                                            $numVecinos = 0;
                                            $vecinos = 'SIN VECINOS';
                                            $descripcionAlrededor = $_POST[ 'comentSinVecins' ];
                                    }
                                    $sql = mysql_query( "SELECT Nomina FROM agentes WHERE Agente = ( SELECT Celula FROM agentes WHERE Agente = '" . $_POST[ 'intelisis' ] . "' )" );
                                    $exec = mysql_fetch_assoc( $sql );
                                    $jefe = $exec[ 'Nomina' ];

                                    $datos = array(
                                        'idProceso'         => strtoupper( $idProceso ) ,
                                        'idCliente'         => strtoupper( $_POST[ 'cuenta' ] ) ,
                                        'proceso'           => 'defuncion' ,
                                        'estatus'           => 3 ,
                                        'fecha'             => $fecha ,
                                        'hora'              => $hora ,
                                        'agente'            => strtoupper( $_POST[ 'intelisis' ] ) ,
                                        'jefe'              => strtoupper( $jefe ) ,
                                        'numVecinos'        => $numVecinos ,
                                        'vecinos'           => $vecinos ,
                                        'descripcionAlred'  => ( $descripcionAlrededor != "" ) ? strtoupper( $descripcionAlrededor ) : false ,
                                        'origen'            => 0 ,
                                        'empresa'           => strtoupper( $_POST[ 'empresa' ] ) ,
                                        'telefono'          => $_POST[ 'telefono' ] ,
                                        'comentarioEmpleo'  => strtoupper( $_POST[ 'comentarioEmpleo' ] ) ,
                                        'motivo'            => strtoupper( $_POST[ 'comentario' ] ) ,
                                        'nombreAval'        => strtoupper( $_POST[ 'nombreAval' ] ) ,
                                        'telAval'           => strtoupper( $_POST[ 'telAval' ] ) ,
                                        'domicilioAval'     => strtoupper( $_POST[ 'domicilioAval' ] )
                                    );
                                    $sql = "
                                        INSERT INTO cob_fichas_procesos_estcuenta(
                                                IDPROCESO, IDCLIENTE, PROCESO, ESTATUS, FECHA, HORA, AGENTE, JEFE, NUMERO_VECINOS,
                                                VECINOS, DESCRIPCION_ALREDEDOR, ORIGEN)
                                            VALUES(
                                                '" . $datos[ 'idProceso' ] . "',                      '" . $datos[ 'idCliente' ] . "',
                                                '" . $datos[ 'proceso' ] . "',                         " . $datos[ 'estatus' ] . ",
                                                '" . $datos[ 'fecha' ] . "',                          '" . $datos[ 'hora' ] . "',
                                                '" . $datos[ 'agente' ] . "',                         '" . $datos[ 'jefe' ] . "',
                                                 " . $datos[ 'numVecinos' ] . ",                      '" . $datos[ 'vecinos' ] . "',
                                                '" . utf8_decode( $datos[ 'descripcionAlred' ] ) . "', " . $datos[ 'origen' ] . "
                                            )";
                                    mysql_query( $sql ) or die( mysql_error( ) );
                                    $sql = "
                                        UPDATE cob_procesos_descripcion_finca_vec
                                        SET IDPROCESO = '" . $datos[ 'idProceso' ] . "'
                                        WHERE IDCLIENTE = '" . $datos[ 'idCliente' ] . "'
                                        AND IDPROCESO = 'TEMPORAL';
                                    ";
                                    mysql_query( $sql ) or die( mysql_error( ) );
                                    $sql = "
                                        UPDATE cob_procesos_descripcion_fisica_vec
                                        SET IDPROCESO = '" . $datos[ 'idProceso' ] . "'
                                        WHERE IDCLIENTE = '" . $datos[ 'idCliente' ] . "'
                                        AND IDPROCESO = 'TEMPORAL';
                                    ";
                                    mysql_query( $sql ) or die( mysql_error( ) );
                                    /*///////////////////////////////////IMAGES BEGIN*/
		                            $strQuery ="SELECT
		                            				*
		                            			FROM bitacora_gestioncobranza_intelisis.cob_procesos_catalogodoc
		                            			WHERE PROCESO = '" . $_POST[ 'resultado' ] . "' AND ACTIVO = 1";
		                            $answer = mysql_query( $strQuery );
		                            while ( $row = mysql_fetch_assoc( $answer ) ) {
		                                    $picture = fopen( $_FILES[ 'defuncion' . $row[ 'IDDOC' ] ][ 'tmp_name' ] , "r" );
		                                    $data = fread( $picture , filesize( $_FILES[ 'defuncion' . $row[ 'IDDOC' ] ][ 'tmp_name' ] ) );
		                                    $sqlpic =  mysql_real_escape_string( $data );
		                                    fclose( $picture );
		                                    $sql = "INSERT INTO cob_procesos_archivos( idarchivo, archivo )" .
		                                           "VALUE( '" . $idProceso . $row[ 'IDDOC' ] . "', '" . $sqlpic . "' )";
		                                    mysql_query( $sql ) or die( mysql_error( ) );
		                                    $sql = "INSERT INTO cob_procesos_indexarc( IDPROCESO, iddoc, idarchivo, agente, fecha, hora )" .
		                                           "VALUE( '" . $datos[ 'idProceso' ] . "',
		                                               " . $row[ 'IDDOC' ] . ",
		                                               '" . $datos[ 'idProceso' ] . $row[ 'IDDOC' ] . "',
		                                               '" . $datos[ 'agente' ] . "',
		                                               '" . $fecha . "',
		                                               '" . $hora . "' )";
		                                    mysql_query( $sql ) or die( mysql_error( ) );
		                            }
		                            /*///////////////////////////////////IMAGES END*/
                            }
                            //fin defuncion
                            //inicio insolvencia
                            if ( $_POST[ 'resultado' ] == "insolvenc" ) {
                                    $fecha = date( "Y/m/d" );
                                    $hora = date( "H:i:s" );
                                    $idProceso = $_POST[ 'intelisis' ] . $_POST[ 'cuenta' ] . $fecha . $hora;
                                    $sql = "
                                        SELECT * FROM bitacora_gestioncobranza_intelisis.cob_procesos_descripcion_finca_vec
                                        WHERE IDCLIENTE = '".$_POST['cuenta']."' AND IDPROCESO='TEMPORAL'";
                                    $exec = mysql_query( $sql ) or die( mysql_error( ) );
                                    $rows = mysql_num_rows( $exec );
                                    if( $rows > 0 ) {
                                            $numVecinos = $rows;
                                            $vecinos = 'CAPTURAR VECINOS';
                                    } else {
                                            $numVecinos = 0;
                                            $vecinos = 'SIN VECINOS';
                                            $descripcionAlrededor = $_POST[ 'comentSinVecins' ];
                                    }
                                    $sql = mysql_query( "SELECT Nomina FROM agentes WHERE Agente = ( SELECT Celula FROM agentes WHERE Agente = '" . $_POST[ 'intelisis' ] . "' )" );
                                    $exec = mysql_fetch_assoc( $sql );
                                    $jefe = $exec[ 'Nomina' ];

                                    $datos = array(
                                        'idProceso'         => strtoupper( $idProceso ) ,
                                        'idCliente'         => strtoupper( $_POST[ 'cuenta' ] ) ,
                                        'proceso'           => 'insolvenc' ,
                                        'estatus'           => 3 ,
                                        'fecha'             => $fecha ,
                                        'hora'              => $hora ,
                                        'agente'            => strtoupper( $_POST[ 'intelisis' ] ) ,
                                        'jefe'              => strtoupper( $jefe ) ,
                                        'numVecinos'        => $numVecinos ,
                                        'vecinos'           => $vecinos ,
                                        'descripcionAlred'  => ( $descripcionAlrededor != "" ) ? strtoupper( $descripcionAlrededor ) : false ,
                                        'origen'            => 0 ,
                                        'empresa'           => strtoupper( $_POST[ 'empresa' ] ) ,
                                        'telefono'          => $_POST[ 'telefono' ] ,
                                        'comentarioEmpleo'  => strtoupper( $_POST[ 'comentarioEmpleo' ] ) ,
                                        'motivo'            => strtoupper( $_POST[ 'comentario' ] ) ,
                                        'nombreAval'        => strtoupper( $_POST[ 'nombreAval' ] ) ,
                                        'telAval'           => strtoupper( $_POST[ 'telAval' ] ) ,
                                        'domicilioAval'     => strtoupper( $_POST[ 'domicilioAval' ] )
                                    );
                                    $sql = "
                                        INSERT INTO cob_fichas_procesos_estcuenta(
                                                IDPROCESO, IDCLIENTE, PROCESO, ESTATUS, FECHA, HORA, AGENTE, JEFE, NUMERO_VECINOS,
                                                VECINOS, DESCRIPCION_ALREDEDOR, ORIGEN)
                                            VALUES(
                                                '" . $datos[ 'idProceso' ] . "',                      '" . $datos[ 'idCliente' ] . "',
                                                '" . $datos[ 'proceso' ] . "',                         " . $datos[ 'estatus' ] . ",
                                                '" . $datos[ 'fecha' ] . "',                          '" . $datos[ 'hora' ] . "',
                                                '" . $datos[ 'agente' ] . "',                         '" . $datos[ 'jefe' ] . "',
                                                 " . $datos[ 'numVecinos' ] . ",                      '" . $datos[ 'vecinos' ] . "',
                                                '" . utf8_decode( $datos[ 'descripcionAlred' ] ) . "', " . $datos[ 'origen' ] . "
                                            )";
                                    mysql_query( $sql ) or die( mysql_error( ) );
                                    $sql = "
                                        UPDATE cob_procesos_descripcion_finca_vec
                                        SET IDPROCESO = '" . $datos[ 'idProceso' ] . "'
                                        WHERE IDCLIENTE = '" . $datos[ 'idCliente' ] . "'
                                        AND IDPROCESO = 'TEMPORAL';
                                    ";
                                    mysql_query( $sql ) or die( mysql_error( ) );
                                    $sql = "
                                        UPDATE cob_procesos_descripcion_fisica_vec
                                        SET IDPROCESO = '" . $datos[ 'idProceso' ] . "'
                                        WHERE IDCLIENTE = '" . $datos[ 'idCliente' ] . "'
                                        AND IDPROCESO = 'TEMPORAL';
                                    ";
                                    mysql_query( $sql ) or die( mysql_error( ) );
                                    /*///////////////////////////////////IMAGES BEGIN*/
		                            $strQuery ="SELECT
		                            				*
		                            			FROM bitacora_gestioncobranza_intelisis.cob_procesos_catalogodoc
		                            			WHERE PROCESO = '" . $_POST[ 'resultado' ] . "' AND ACTIVO = 1";
		                            $answer = mysql_query( $strQuery ) or die( mysql_error( ) );
		                            while ( $row = mysql_fetch_assoc( $answer ) ) {
		                                    $sql = "INSERT INTO cob_procesos_archivos( idarchivo, archivo )" .
		                                           "VALUE( '" . $idProceso . $row[ 'IDDOC' ] . "', '' )";
		                                    mysql_query( $sql ) or die( mysql_error( ) );
		                                    $sql = "INSERT INTO cob_procesos_indexarc( IDPROCESO, iddoc, idarchivo, agente, fecha, hora )" .
		                                           "VALUE( '" . $datos[ 'idProceso' ] . "',
		                                               " . $row[ 'IDDOC' ] . ",
		                                               '" . $datos[ 'idProceso' ] . $row[ 'IDDOC' ] . "',
		                                               '" . $datos[ 'agente' ] . "',
		                                               '" . $fecha . "',
		                                               '" . $hora . "' )";
		                                    mysql_query( $sql ) or die( mysql_error( ) );
		                            }
		                            /*///////////////////////////////////IMAGES END*/
		                            $strQuery ="SELECT
		                            		         *
		                            		    FROM bitacora_localizacion.catalogos
		                            		    WHERE campo = 'Articulo' AND estatus = 1";
		                            $answer = mysql_query( $strQuery );
		                            while ( $row = mysql_fetch_assoc( $answer ) ) {
                                        $strQuery = "INSERT INTO bitacora_gestioncobranza_intelisis.cob_procesos_solvencia( IDPROCESO, ARTICULO, CANTIDAD, ESTADO )
                                                     VALUE('" . $datos[ 'idProceso' ] . "', '" . $row[ 'valor' ] . "', 0, 'NINGUNO' )";
                                        mysql_query( $strQuery ) or die( mysql_error( ) );
		                            }
		                            $strQuery ="INSERT INTO cob_procesos_descripcion_finca_vec(
													IDPROCESO, IDAGENTE, IDCLIENTE, IDVECINO, FECHA, HORA, EJERCICIO, QUINCENA, TIPODEFINCA,
													COCHERA, CANCEL, NIVELES, COLOR, NUMERO_DE_CASA )
													VALUES(
														'" . $datos[ 'idProceso' ] . "',
														'" . $datos[ 'agente' ]."',
														'" . $datos[ 'idCliente' ] . "',
														'SOLVENCIA',
														'" . $datos[ 'fecha' ] . "',
														'" . $datos[ 'hora' ] . "',
														0, 0, '',	'', '',	'', '',	'')";
		                            mysql_query( $strQuery ) or die( mysql_error( ) );
									$strQuery ="INSERT INTO cob_procesos_descripcion_fisica_vec(
												IDPROCESO, IDCLIENTE, IDAGENTE, IDVECINO, FECHA, HORA, EJERCICIO, QUINCENA, SEXO, 
												ESTATURA, COMPLEXION, COLOR_PIEL, CABELLO, COLOR_CABELLO, NOMBRE_VECINO, COMENTARIO_VECINO )
												VALUES(
													'" . $datos['idProceso'] . "',
													'" . $datos['idCliente'] . "',
													'" . $datos['agente'] . "',
													'SOLVENCIA',
													'" . $datos['fecha'] . "',
													'" . $datos['hora'] . "',
													0, 0, '', '', '','', '', '', '', '')";
									mysql_query( $strQuery ) or die( mysql_error( ) );
                            }
                            //fin insolvencia
                            if( !empty( $folio_adjudicacion ) && $_POST[ 'capturista' ] != "webmaster" ) {
                                    for($i=0;$i<count($folio_adjudicacion);$i++){
                                            $sql = "INSERT INTO registros_adjudicaciones (id_registro,cuenta,folio,monto,factura,gastos)
                                                    VALUES ('".$id."','".$_POST['cuenta']."','".$folio_adjudicacion[$i]."','".$monto_adjudicacion[$i]."','".$factura_adjudicacion[$i]."','".$gastos_adjudicacion[$i]."')";
                                            $rq  =  mysql_query($sql);
                                    }
                            }
                            $this->tpl = "dialog.success.htm";
                            $this->msgaviso = "Captura registrada correctamente con el Folio: $id";//*/
                    }else{
                            $this->tpl = "dialog.success.htm";
                            $this->msgaviso = "Error #".mysql_errno()." : ".mysql_error();
                    }
            }else{
                    $this->tpl = "dialog.success.htm";
                    $this->msgaviso = "No se recibieron parametros";
            }
    }
	/*
     * Módulo de eliminación de registros.
     */
    function _cancelacion(){
            $this->tpl = "gestionescobranza/captura.bitacora.cancelacion.htm";
            $msg = array(
                    'filtro' => '',
                    'cancelados' => '',
                    'error' => ''
            );
            $cuenta     = $_POST['cuenta'];
            $factura    = $_POST['factura'];
            $usuario    = $_POST['usuario'];
            
            $sql = "SELECT
                    cuenta,
                    factura,
                    capturista,
                    fecha_captura,
                    hora_captura,
                    fecha_inicio,
                    hora_inicio,
                    hora_final,
                    UPPER(resultado) AS resultado,
                    UPPER(valor_resultado) AS valor_resultado,
                    CASE resultado WHEN 'adjudicac' THEN folio
                         WHEN 'pago' THEN recibo END AS extra1,
                    CASE WHEN resultado IN ('aviso', 'recado', 'negativa') THEN proximavisita
                         WHEN resultado = 'promesa' THEN pago_promesa END AS extra2,
                    pago_importe,
                    promotor,
                    folio,
                    comentario,
                    cancela,
                    FROM_UNIXTIME(fecha_cancelacion) AS fecha_cancelacion
                FROM registros_cancelados
                ORDER BY fecha_cancelacion DESC LIMIT 100";
            $msg['cancelados'] = 'Registros Cancelados';

            if($cuenta || $factura || $usuario){
                    if($cuenta != '')
                            $where .= "AND cuenta = '$cuenta' ";
                    if($factura != '')
                            $where .= "AND factura = '$factura' ";
                    if($usuario != '')
                            $where .= "AND capturista = '$usuario' ";
                    $sql = "SELECT
                            id,
                            cuenta,
                            factura,
                            capturista,
                            fecha_captura,
                            hora_captura,
                            fecha_inicio,
                            hora_inicio,
                            hora_final,
                            UPPER(resultado) AS resultado,
                            UPPER(valor_resultado) AS valor_resultado,
                            CASE resultado WHEN 'adjudicac' THEN folio
                                 WHEN 'pago' THEN recibo END AS extra1,
                            CASE WHEN resultado IN ('aviso', 'recado', 'negativa') THEN proximavisita
                                 WHEN resultado = 'promesa' THEN pago_promesa END AS extra2,
                            pago_importe,
                            promotor,
                            folio,
                            comentario
                        FROM registros WHERE 1 $where";
                    $msg['filtro'] = 'Registros Encontrados';
                    $msg['cancelados'] = '';
            } else if(!empty($_POST)){
                    $usuario = $_SESSION['user'];
                    foreach($_POST as $id => $value){
                            $sql = "INSERT INTO registros_cancelados
                                    SELECT r.*, '".$usuario."', UNIX_TIMESTAMP() FROM registros r WHERE id = ".$id;
                            $rq = mysql_query($sql);

                            if($rq && mysql_affected_rows() == 1){
                                    $sql = "DELETE FROM registros WHERE id = ".$id;
                                    mysql_query($sql);
                                    $registros .= $separador.$id;
                                    $separador = ', ';
                            }else{
                                    $error .= mysql_error()."\r\n";
                            }
                    }
                    if($registros){
                            $sql = "SELECT
                                    cuenta,
                                    factura,
                                    capturista,
                                    fecha_captura,
                                    hora_captura,
                                    fecha_inicio,
                                    hora_inicio,
                                    hora_final,
                                    UPPER(resultado) AS resultado,
                                    UPPER(valor_resultado) AS valor_resultado,
                                    CASE resultado WHEN 'adjudicac' THEN folio
                                         WHEN 'pago' THEN recibo END AS extra1,
                                    CASE WHEN resultado IN ('aviso', 'recado', 'negativa') THEN proximavisita
                                         WHEN resultado = 'promesa' THEN pago_promesa END AS extra2,
                                    pago_importe,
                                    promotor,
                                    folio,
                                    comentario,
                                    cancela,
                                    FROM_UNIXTIME(fecha_cancelacion) AS fecha_cancelacion
                                FROM registros_cancelados WHERE id IN (".$registros.")";
                            $msg['cancelados'] = 'Registros Cancelados';
                    }else{
                            $sql = "SELECT
                                    cuenta,
                                    factura,
                                    capturista,
                                    fecha_captura,
                                    hora_captura,
                                    fecha_inicio,
                                    hora_inicio,
                                    hora_final,
                                    UPPER(resultado) AS resultado,
                                    UPPER(valor_resultado) AS valor_resultado,
                                    CASE resultado WHEN 'adjudicac' THEN folio
                                         WHEN 'pago' THEN recibo END AS extra1,
                                    CASE WHEN resultado IN ('aviso', 'recado', 'negativa') THEN proximavisita
                                         WHEN resultado = 'promesa' THEN pago_promesa END AS extra2,
                                    pago_importe,
                                    promotor,
                                    folio,
                                    comentario,
                                    cancela,
                                    FROM_UNIXTIME(fecha_cancelacion) AS fecha_cancelacion
                                FROM registros_cancelados";
                            $msg['cancelados'] = 'Ocurrio un error al cancelar los registros';
                            $msg['error'] = $error;
                    }
                    $_POST = array();
            }
            $bloque = array(
                "blkForm"   => empty($_POST)?array():array($_POST),
                "blkReg"    => $sql,
                "msg"       => array($msg)
                );
            //print_r($bloque);
            return $bloque;
    }

    function _reporte_general(){
	
		
            $this->tpl = "gestionescobranza/reporte.general.htm";
           
			 $bloque = array(
                    "blkPost" => empty($_POST)?array():$_POST,
                    "blkGrupo" => "SELECT distinct TRIM(substring_index(MID(es, 5, 100), '0', 2)) as nombre FROM agentes where nomina=agente order by es",
                    "blkNivCob" => "SELECT distinct TRIM(substring_index(MID(es, 5, 100), '0', 1)) as id FROM agentes where nomina=agente order by es"
            );
							
            if(!empty($_POST)){
                    $rq = mysql_query("SELECT * FROM config ORDER BY id DESC LIMIT 1");
                    $config = mysql_fetch_assoc($rq);
            
                    if($_POST['nivcob'] != 'TODOS'){
                           // $nivcob = " and (nivel='".$_POST['nivcob']."' OR nivel in (SELECT niveles FROM relacion_niveles_especiales WHERE consolidado = '".$_POST['nivcob']."' ))";
							//$nivReg = " and (etapa='".$_POST['nivcob']."' OR etapa in (SELECT niveles FROM relacion_niveles_especiales WHERE consolidado = '".$_POST['nivcob']."' ))";
                           
                            $nivcob .= " AND nivel = '".$_POST['nivcob']."' ";
                            $nivReg .= " and etapa='".$_POST['nivcob']."'";
                    }
                    if($_POST['grupo'] != ''){
							$grupo =" and b.grupo='".$_POST['grupo']."'";
                           
                    }
					$CambiarIdioma="SET lc_time_names = 'es_MX'";
					mysql_query($CambiarIdioma);
					$fecha1="select quincena_cobranza1(".$_POST['quincena'].",".$_POST['ejercicio'].") as fecha";
					$res=mysql_fetch_assoc(mysql_query($fecha1));
					$desde=$res['fecha'];
					$fecha2="select quincena_cobranza2(".$_POST['quincena'].",".$_POST['ejercicio'].") as fecha";
					$res=mysql_fetch_assoc(mysql_query($fecha2));
					$hasta=$res['fecha'];
                    //mysql_result("DROP TEMPORARY TABLE IF EXISTS gest2_temp , asig2_temp");

					$ag=mysql_query("select distinct(agente_asociado) from agente_doble where and fecha_asigna between '".$desde."' and '".$hasta."'");					
					 if(mysql_num_rows($ag) == 1){
                            $this->msgaviso = "Reporte no diponible para este agente debido a que es suplente.";
                            mysql_free_result($ag);
							return $bloque;
                    }
                    $tabla1="CREATE TEMPORARY TABLE gest2_temp
        							 (
        								resultado varchar(20) NOT NULL,
        								promotor varchar(15),
        								hora_inicio varchar(15),
        								nomina varchar(15) NOT NULL,
        								cuenta varchar(15), 
        								movimiento varchar(20),
                                        factura varchar(15),
                                        etapa varchar(20)
                                        
        							  )
        							  ";
        			$tabla2="CREATE TEMPORARY TABLE asig2_temp
        				 (
        					nomina varchar(15) NOT NULL,
        					cuenta varchar(15) NOT NULL,
        					movimiento varchar(20), 
        					factura varchar(20),
        					nivel varchar(50),
        					grupo varchar(30)
        				  )
        							  ";
        			$tabla3="CREATE TEMPORARY TABLE reporte_temp
        					(
        					                           
                            nomina varchar(15) NOT NULL,
        					cuenta varchar(15) NOT NULL,
                            movimiento varchar(20), 
                            factura varchar(20),
                            nivel varchar(50),
                            grupo varchar(30),
                            resultado varchar(20) NOT NULL,
        					promotor varchar(15),
        					hora_inicio varchar(15)) ";
        			$index1="CREATE INDEX cuenta_index USING BTREE ON gest2_temp (cuenta)";
        			$index2="CREATE INDEX factura_index USING BTREE ON gest2_temp (factura)";
        			$index3="CREATE INDEX cuenta_index USING BTREE ON asig2_temp (cuenta)";
        			$index4="CREATE INDEX factura_index USING BTREE ON asig2_temp (factura)";
        			$index5="CREATE INDEX cuenta_index USING BTREE ON reporte_temp (cuenta)";
        			$index6="CREATE INDEX factura_index USING BTREE ON reporte_temp (factura)";
        			mysql_query($tabla1) or die(mysql_error());
        			mysql_query($index1) or die(mysql_error());
        			mysql_query($index2) or die(mysql_error());
        			mysql_query($tabla2) or die(mysql_error());
        			mysql_query($index3) or die(mysql_error());
        			mysql_query($index4) or die(mysql_error());
        			mysql_query($tabla3) or die(mysql_error());
        			mysql_query($index5) or die(mysql_error());
        			mysql_query($index6) or die(mysql_error());
        			$sqlReg = "
        					insert into gest2_temp 
        					select * 
        					from(	
        						select  
        						r.resultado,
        						r.intelisis, 
        						r.hora_inicio,
                                
        						REPLACE(CASE WHEN (r.intelisis IS NULL OR r.intelisis = 'SIN AGE' OR r.intelisis = ' ') THEN REPLACE(r.promotor,' ','') ELSE REPLACE(r.intelisis,' ','') END,' ','') as nomina, 
        						REPLACE(r.cuenta,' ','') as cuenta, 
        						REPLACE(r.mov,' ','') as movimiento, 
        						REPLACE(r.factura,' ','') as factura,
                                r.etapa
        						from bitacora_gestioncobranza_intcob.registros R 
        						where 1=1  and fecha_inicio BETWEEN '".$desde."' AND '".$hasta."'".$nivReg." and resultado <> ' ' 
        						union 
        						select 
        						r.resultado,
        						r.intelisis,
                                
        						r.hora_inicio, 
                                 
        						REPLACE(CASE WHEN (r.intelisis IS NULL OR r.intelisis = 'SIN AGE' OR r.intelisis = ' ') THEN REPLACE(r.promotor,' ','') ELSE REPLACE(r.intelisis,' ','') END,' ','') as nomina,
        						REPLACE(r.cuenta,' ','') as cuenta,
        						REPLACE(r.mov,' ','') as movimiento, 
        						REPLACE(r.factura,' ','') as factura,
                                r.etapa
        						from bitacora_gestioncobranza_intelisis.registros r 
        						where 1=1   and fecha_inicio BETWEEN '".$desde."' AND '".$hasta."'".$nivReg." and resultado <> ' ')as a"; 
                    // echo $sqlReg; exit();
        			mysql_query($sqlReg);
        					
        			$sqlValidarTabla=mysql_query("select 1 from android_reportes._Cuentas_GestionCobranzaHistorico
                                                  where  periodo=".$_POST['quincena']." and ejercicio=".$_POST['ejercicio']." limit 1");
        			
        			if (mysql_num_rows($sqlValidarTabla)==0)
        			    $consultaTabla="android_reportes._cuentas_gestioncobranza";
        			else
        			    $consultaTabla="android_reportes._Cuentas_GestionCobranzaHistorico";
        			
        			//echo $consultaTabla;
        				 
        					//$visitas = mysql_affected_rows();
        			$sqlandroid="insert into asig2_temp 
        						SELECT REPLACE(a.nomina,' ','') as nomina, 
        						REPLACE(a.cuenta,' ','') as cuenta, 
        						REPLACE(a.movimiento,' ','') as movimiento, 
        						REPLACE(a.factura,' ','') as factura, 
        						a.nivel, 
        						b.grupo
        						FROM ".$consultaTabla." a
        						left join (SELECT distinct TRIM(substring_index(MID(es, 5, 100), '0', 2)) as grupo,nomina FROM bitacora_gestioncobranza_intelisis.agentes where nomina=agente order by es)b on a.nomina=b.nomina 
        						where periodo=".$_POST['quincena']." and ejercicio=".$_POST['ejercicio'].$nivcob .$grupo."
        						union 
        						SELECT REPLACE(ad.agente_asociado,' ','') as nomina, 
        						REPLACE(a.cuenta,' ','') cuenta, 
        						REPLACE(a.movimiento,' ','') as movimiento, 
        						REPLACE(a.factura,' ','') as factura, 
        						a.nivel, 
        						b.grupo
        						FROM ( select agente_origen, agente_asociado from agente_doble where fecha_asigna  between '".$desde."' and '".$hasta."' group by agente_origen, agente_asociado ) as ad 
        						left join ".$consultaTabla." a on a.nomina=ad.agente_origen 
        						inner join (SELECT distinct TRIM(substring_index(MID(es, 5, 100), '0', 2)) as grupo,nomina FROM agentes where nomina=agente order by es)b on a.nomina=b.nomina 
        						where periodo=".$_POST['quincena']." and ejercicio=".$_POST['ejercicio'].$nivcob.$grupo;  	
        			
                    // echo $sqlandroid; exit();
        			mysql_query($sqlandroid) or die("Error 1  ".mysql_error());
        					//$visitas = mysql_affected_rows();
                    $sqlall = "	insert into reporte_temp
        					SELECT andr.*,reg.resultado, reg.promotor, reg.hora_inicio
        						FROM asig2_temp andr 
        						inner join gest2_temp reg on
        							reg.nomina=andr.nomina 
        							and reg.cuenta=andr.cuenta 
        							and andr.movimiento=reg.movimiento 
        							and andr.factura=reg.factura 
                                    LEFT JOIN `varios`.`empleados_rh201_intelisis` rh ON reg.nomina = rh.CLAVE
        							group by reg.cuenta , reg.promotor, reg.resultado , reg.hora_inicio

        							";

                                        //and reg.etapa = andr.nivel
                    //IMprimir
                                        
        			//echo($sqlall); exit();
        			mysql_query($sqlall) or die("Error 2 ".mysql_error());
        			

                   /* $sql = "SELECT *
                            FROM reporte_temp                             
                            where resultado like '%aviso%'
                            ";
                    //                  echo $sqlall;
                            $rq = mysql_query($sql) or die("Error 3  ".mysql_error());      
                            $num = mysql_num_rows($rq);
                            var_dump( $num); exit();
                    */
        			$visitas = mysql_affected_rows();





                    $sqlAux="
                        CREATE temporary table auxiliar Select 
                            r.factura,r.resultado, r.promotor, r.hora_inicio
                            FROM asig2_temp andr 
                            inner join gest2_temp r on r.nomina=andr.nomina and r.cuenta=andr.cuenta and andr.movimiento=r.movimiento
                             and andr.factura=r.factura  
                            LEFT JOIN `varios`.`empleados_rh201_intelisis` rh ON r.nomina = rh.CLAVE
                            WHERE r.resultado='pago'
                             GROUP BY r.factura , r.promotor, r.resultado , r.hora_inicio,r.movimiento             ;
                    ";//and reg.etapa = andr.nivel , reg.promotor, reg.resultado , reg.hora_inicio


                    /*SELECT andr.*,reg.resultado, reg.promotor, reg.hora_inicio
                                                    FROM asig2_temp andr 
                                                    inner join gest2_temp reg on
                                                        reg.nomina=andr.nomina 
                                                        and reg.cuenta=andr.cuenta 
                                                        and andr.movimiento=reg.movimiento 
                                                        and andr.factura=reg.factura and reg.etapa = andr.nivel
                                                        LEFT JOIN `varios`.`empleados_rh201_intelisis` rh ON reg.nomina = rh.CLAVE
                                                        WHERE reg.resultado='pago'
                    */

                    //echo($sqlAux);exit();
                    mysql_query($sqlAux) or die(mysql_error());
                    $sql = 
                      "(
                            select 1 as orden, count(factura) as val from auxiliar )
                            union all 
                            (select c.orden, COUNT(c.val) as val 
                            from( 
                                SELECT c.orden, COUNT(*) as val FROM reporte_temp reg 
                                inner join catalogo_resultado c ON c.id = reg.resultado 
                                where c.orden<>1 and reg.resultado<>'pago'
                                GROUP BY cuenta, promotor, resultado , hora_inicio )c 
                                
                            group by c. orden order by c.orden
                        )
                        ";






        					/*$sql = "SELECT orden, sum(a.val) as val
        							FROM (                
        							SELECT c.orden, COUNT(*) as val 
        							FROM reporte_temp r 
        							inner join catalogo_resultado c ON c.id = r.resultado 
        							GROUP BY cuenta , promotor, resultado , hora_inicio)a
        							GROUP BY a.orden
        							order by a.orden  ";*/
                    //					echo $sqlall;
                    $rq = mysql_query($sql) or die("Error 3  ".mysql_error());		
                    $num = mysql_num_rows($rq);
        			//echo $num;
        			$FechaTitulo="select DATE_FORMAT(quincena_cobranza1(".$_POST['quincena'].",".$_POST['ejercicio']."),'%Y %b %e') as pd";
        			$FechaTitulo2="select DATE_FORMAT(quincena_cobranza2(".$_POST['quincena'].",".$_POST['ejercicio']."),'%Y %b %e') as sd";
                    $fechas = array("inicio" => "".$desde, "fin" => "".$hasta, "Periodo" =>"".$_POST['quincena'], "Ejercicio" =>"".$_POST['ejercicio']);
                    $conteo=0;
                    $totales = array();
                    $porcent = array();
        			//$visitas = 0;
                    $numcol = mysql_num_rows(mysql_query("SELECT * FROM catalogo_resultado"));
                            
                    for($i=0,$j=0;$j<$numcol;$j++){
                            if($num>0 && $i<$num) $orden = mysql_result($rq, $i, 'orden');
                            else $orden = -99;
                            if($orden-1 == $j){
                                    $hist[$j] = mysql_result($rq, $i, 'val');
                                    //echo $hist[$j];
                                    
                                    $conteo+=$hist[$j];
                                    $i++;
                            }else{
                                    $hist[$j] = 0;
                                   
                            }
                    }




                    $visitas=$conteo;



                    for($i=0,$j=0;$j<$numcol;$j++){
                            if($num>0 && $i<$num) $orden = mysql_result($rq, $i, 'orden');
                            else $orden = -99;
                            if($orden-1 == $j){
                                    $hist[$j] = mysql_result($rq, $i, 'val');
        							//echo $hist[$j];
                                    $porc[$j] = $hist[$j] / $visitas;
                                    $i++;
                            }else{
                                    $hist[$j] = 0;
                                    $porc[$j] = 0;
                            }
                    }
                    //var_dump($hist);exit();
                    $sql_all = "SELECT count(factura) AS cuentas
                                FROM asig2_temp";
                            
                    $cuentas_all = mysql_result(mysql_query($sql_all),0);
        			//echo $cuentas_all;
                    if($cuentas_all == 0){
                            $cuentas_all = 1;
                    }
                    //echo $sql_all."<br>";
                    $sql = "SELECT count(DISTINCT cuenta) FROM reporte_temp";
                    $cuentas = mysql_result(mysql_query($sql),0);
        			//echo $cuentas;
                    $hist2 = array(
                            //array( "val" => $visitas, "frm" => "0,000."),
                            array( "val" => $cuentas, "frm" => "0,000."),
                            array( "val" => $cuentas / $cuentas_all, "frm" => "0.00%"),
                            array( "val" => $cuentas_all - $cuentas, "frm" => "0,000."),
                            array( "val" => ($cuentas_all - $cuentas) / $cuentas_all, "frm" => "0.00%")
                    );
            
                    $form = array(
                            "nivcob"    => $_POST['nivcob'],
                            "grupo"     => $_POST['grupo'],
                            "base"      => $config['base']
                    );
            

                    $this->tpl = "gestionescobranza/reporte.general.detalle.htm";
            
                    $bloque = array(
                            "blkFecha"      => array($fechas),
                            "blkResulv"     => "SELECT * FROM catalogo_resultado ORDER BY orden",
                            "blkHist"       => $hist,
                            "blkHist2"      => $hist2,
                            "blkPorc"       => $porc,
                            "totalVisitas"  => array($visitas),
                            "form"          => array($form),
        					"blkFecha1"      => $FechaTitulo,
        					"blkFecha2"      => $FechaTitulo2
                    );
        }
            return $bloque;
    } //fin _reporte_general

   function _reporte_gestor(){
           $this->tpl = "gestionescobranza/reporte.gestor.html";
            $bloque = array(
                    "blkPost" => empty($_POST)?array():$_POST,
                    "blkNivCob" => "SELECT DISTINCT nivel_cobranza AS id, nivel_cobranza AS nombre FROM catalogo_promotores WHERE nivel_cobranza <> '' ORDER BY nombre"
            );
			
            if(!empty($_POST)){
                    $rq = mysql_query("SELECT * FROM config ORDER BY id DESC LIMIT 1");
                    $config = mysql_fetch_assoc($rq);


                    $rq = mysql_query("SELECT * FROM(
									  SELECT factura 
										FROM bitacora_gestioncobranza_intcob.registros 
										WHERE (promotor = '".$_POST['nomina']."' OR intelisis = '".$_POST['nomina']."')
										UNION ALL
									   SELECT factura 
										FROM bitacora_gestioncobranza_intelisis.registros 
										WHERE (promotor = '".$_POST['nomina']."' OR intelisis = '".$_POST['nomina']."'))A LIMIT 1 ");
					

                    if((!$rq || mysql_num_rows($rq) == 0)){
                            $this->msgaviso = "No se encontro al Gestor";
                            mysql_free_result($rq);
                            mysql_free_result($rq2);
                            return $bloque;
                    }
                    mysql_free_result($rq);

					$CambiarIdioma="SET lc_time_names = 'es_MX'";
					mysql_query($CambiarIdioma);
					$fecha1="select bitacora_gestioncobranza_intelisis.quincena_cobranza1(".$_POST['quincena'].",".$_POST['ejercicio'].") as fecha";
					$res=mysql_fetch_assoc(mysql_query($fecha1));
					$desde=$res['fecha'];
					$fecha2="select bitacora_gestioncobranza_intelisis.quincena_cobranza2(".$_POST['quincena'].",".$_POST['ejercicio'].") as fecha";
					$res=mysql_fetch_assoc(mysql_query($fecha2));
					$hasta=$res['fecha'];
					
					
					$ag=mysql_query("select agente_asociado, agente_origen from bitacora_gestioncobranza_intelisis.agente_doble where agente_asociado='".$_POST['nomina']."' and fecha_asigna between '".$desde."' and '".$hasta."' limit 1");					
					
					 if(mysql_num_rows($ag) == 1){
                            
                           $res=mysql_fetch_assoc($ag);
						   $sp=1;
						   $agente_t=$res["agente_origen"];
						   $agente_d=$res["agente_asociado"];
							
                    }
					

					$tempges =
					"CREATE TEMPORARY TABLE gestiones_temp AS select * from(						
						select R.id, R.cuenta, R.mov, R.factura, R.fechaultimopago, R.fechafactura, R.saldocapital, R.druta, R.articulo, R.diasinactividad, 
                        R.diasvencidos, R.intereses, R.cuota, R.nombre, R.domicilio, R.entre, R.colonia, R.ciudad, R.estado, R.rutacobro, R.aval, 
                        R.aval_domicilio, R.aval_colonia, R.aval_ciudad, R.telefono, R.saldo, R.importe, R.fecha_captura, R.hora_captura, R.hora_captura_final, 
                        R.fecha_inicio, R.hora_inicio, R.fecha_final, R.hora_final, R.resultado, R.valor_resultado, R.proximavisita, R.comentario, 
                        R.pago_promesa, R.pago_importe, R.pago_quien, R.recibo, R.promotor, R.etapa, R.ruta, R.intelisis, R.saldovencido, R.cartera, 
                        R.folio, R.capturista, R.pzo, R.abonos, R.imp_convenio, R.imp_inicial, R.num_convenio, R.gps, R.numero, R.origen, R.estatus_cobro, '' as ZONA 
							from bitacora_gestioncobranza_intcob.registros R
						where (promotor='".$_POST['nomina']."' or intelisis='".$_POST['nomina']."') and fecha_inicio between '".$desde."' and '".$hasta."' and resultado<>''
						union all
						select R.id, R.cuenta, R.mov, R.factura, R.fechaultimopago, R.fechafactura, R.saldocapital, R.druta, R.articulo, R.diasinactividad, 
                        R.diasvencidos, R.intereses, R.cuota, R.nombre, R.domicilio, R.entre, R.colonia, R.ciudad, R.estado, R.rutacobro, R.aval, 
                        R.aval_domicilio, R.aval_colonia, R.aval_ciudad, R.telefono, R.saldo, R.importe, R.fecha_captura, R.hora_captura, R.hora_captura_final, 
                        R.fecha_inicio, R.hora_inicio, R.fecha_final, R.hora_final, R.resultado, R.valor_resultado, R.proximavisita, R.comentario, 
                        R.pago_promesa, R.pago_importe, R.pago_quien, R.recibo, R.promotor, R.etapa, R.ruta, R.intelisis, R.saldovencido, R.cartera, 
                        R.folio, R.capturista, R.pzo, R.abonos, R.imp_convenio, R.imp_inicial, R.num_convenio, R.gps, R.numero, R.origen, R.estatus_cobro, '' as ZONA 
							from bitacora_gestioncobranza_intelisis.registros R
						where (promotor='".$_POST['nomina']."' or intelisis='".$_POST['nomina']."') and fecha_inicio between '".$desde."' and '".$hasta."' and resultado<>''
					)a";	
					//echo $tempges ;	exit();
					mysql_query($tempges);	
				
				    $sqlValidarTabla=mysql_query("select 1 from android_reportes._Cuentas_GestionCobranzaHistorico
                                              where  periodo=".$_POST['quincena']." and ejercicio=".$_POST['ejercicio']." limit 1");
				
    				if (mysql_num_rows($sqlValidarTabla)==0)
    				    $consultaTabla="android_reportes._cuentas_gestioncobranza";
    				else
    				    $consultaTabla="android_reportes._Cuentas_GestionCobranzaHistorico";
    					
					//echo $consultaTabla;
					
					$tempasignacion ="CREATE TEMPORARY TABLE asignacion_temp AS SELECT * FROM( 
					SELECT id, Nomina ,cuenta,nivel,movimiento, factura,nombre,plazo,imp_comp,fup,  abonos, sdo_ven, sdo_tot, colonia, poblacion, 
					articulo, telefono, dias_inactivos, dias_vencidos,  ejercicio, periodo, empresa, origen
                    FROM ".$consultaTabla." where nomina='".$_POST['nomina']."' and periodo=".$_POST['quincena']." and ejercicio=".$_POST['ejercicio']."
				       UNION ALL
					   select agc.id, ad.agente_asociado as nomina, agc.cuenta, agc.nivel, agc.movimiento, agc.factura, agc.nombre, agc.plazo, 
						agc.imp_comp, agc.fup, agc.abonos, agc.sdo_ven, agc.sdo_tot, agc.colonia, agc.poblacion, agc.articulo, agc.telefono, agc.dias_inactivos,
						agc.dias_vencidos, agc.ejercicio, agc.periodo, agc.empresa, agc.origen from (

							select agente_origen, agente_asociado from bitacora_gestioncobranza_intelisis.agente_doble
							where fecha_asigna between '".$desde."' and '".$hasta."' 
							group by agente_origen, agente_asociado

						) as ad
						left join ".$consultaTabla." agc on agc.nomina=ad.agente_origen
						where agc.periodo=".$_POST['quincena']." and agc.ejercicio=".$_POST['ejercicio']." and ad.agente_asociado='".$_POST['nomina']."'
					) a";   
                    //echo $tempasignacion ;	exit();



					mysql_query($tempasignacion);


                    $reporte ="CREATE TEMPORARY TABLE reporte_temp AS SELECT
                                reg.id,
                                reg.factura,
                                reg.cuenta,
                                reg.fecha_inicio,
                                reg.nombre,
                                reg.pzo,
                                reg.importe,
                                reg.fechaultimopago,
                                reg.abonos,
                                reg.saldocapital,
                                reg.intereses,
                                CASE WHEN reg.saldovencido IS NULL THEN 0 ELSE reg.saldovencido END AS saldovencido,
                                reg.saldo,
                                reg.diasinactividad AS di,
                                reg.diasvencidos AS dv,
                                CASE WHEN reg.ruta = '' THEN CAST(reg.rutacobro AS UNSIGNED) ELSE CAST(reg.ruta AS UNSIGNED) END AS ruta,
                                reg.druta,
                                rp.Nombre AS agte,
                                reg.etapa,
                                reg.domicilio,
                                reg.colonia,
                                reg.ciudad,
                                reg.articulo,
                                reg.hora_inicio,
                                reg.hora_final,
                                UPPER(reg.resultado) AS resultado,
                                UPPER(reg.valor_resultado) AS valor_resultado,
                                reg.pago_importe,
                                TIMEDIFF(reg.hora_final, reg.hora_inicio) AS t_gestion,
                                'N/A' AS t_traslado,
                                reg.comentario,
                                reg.folio,
                                reg.cartera,
                                reg.imp_convenio,
                                reg.imp_inicial,
                                reg.num_convenio,
                    			andr.empresa
                           		FROM asignacion_temp andr
                        		LEFT JOIN gestiones_temp reg 
                        			ON CASE WHEN (reg.intelisis IS NULL OR reg.intelisis = 'SIN AGE' OR reg.intelisis = ' ') THEN REPLACE(andr.nomina,' ','') = REPLACE(reg.promotor,' ','') ELSE  		
                        			REPLACE(andr.nomina,' ','') = REPLACE(reg.intelisis,' ','') END
                        		and REPLACE(reg.cuenta,' ','')=REPLACE(andr.cuenta,' ','')
                        		and REPLACE(andr.movimiento,' ','')=REPLACE(reg.mov,' ','')
                        		and REPLACE(andr.factura,' ','')=REPLACE(reg.factura,' ','') 
                        		LEFT JOIN (SELECT nomina,nombre,es as grupo FROM agentes where nomina=agente) rp ON CASE WHEN (reg.intelisis IS NULL OR reg.intelisis = 'SIN AGE' OR reg.intelisis = ' ') THEN rp.nomina = reg.promotor ELSE  rp.nomina = reg.intelisis END
                        		ORDER BY reg.fecha_inicio, reg.hora_inicio,andr.empresa";   
                        					//echo $reporte ;exit();
                        					


                        					mysql_query($reporte);
                                            $sqlAux="
                                    CREATE temporary table auxiliar select distinct * from reporte_temp where resultado ='pago' and factura <> ' ';
                                    
                                    ";

                    mysql_query($sqlAux) or die(mysql_error());


                    $sql = 
                      "(
                            select 1 as orden, count(*) as val from auxiliar )
                            union all
                            (select c.orden, COUNT(c.val) as val 
                            from( 
                                SELECT c.orden, COUNT(*) as val FROM reporte_temp reg 
                                inner join catalogo_resultado c ON c.id = reg.resultado 
                                where c.orden<>1
                                GROUP BY cuenta, agte, resultado , hora_inicio )c 
                            group by c. orden order by c.orden
                        )
                        ";

                    /*"select (select c.orden, COUNT(c.val) as val  from(SELECT c.orden, COUNT(*) as val FROM reporte_temp reg  inner join catalogo_resultado c ON c.id = reg.resultado GROUP BY factura, cuenta , agte, resultado , hora_inicio  , fecha_inicio where reg.resultado <>'pago'
                                   )c  group by c. orden  order by c.orden)as b";*/
                    //echo($sql); exit();
                    $rq = mysql_query($sql);
                    ///mysql_result($rq, $i, 'orden')
                    //if(mysql_result($rq, 'orden'))

                    $num = mysql_num_rows($rq);
                    //$visitas = mysql_num_fields(result);
				    $sqlvist = "SELECT distinct reg.cuenta
						FROM asignacion_temp andr
    					INNER JOIN gestiones_temp reg ON CASE WHEN (reg.intelisis IS NULL OR reg.intelisis = 'SIN AGE' OR reg.intelisis = ' ') THEN REPLACE(andr.nomina,' ','') = REPLACE(reg.promotor,' ','') ELSE  REPLACE(andr.nomina,' ','') = REPLACE(reg.intelisis,' ','') END
    						and REPLACE(reg.cuenta,' ','')=REPLACE(andr.cuenta,' ','')
    						and REPLACE(andr.movimiento,' ','')=REPLACE(reg.mov,' ','')
    						and REPLACE(andr.factura,' ','')=REPLACE(reg.factura,' ','') 
    					    ";
                    mysql_query($sqlvist) or die ("Problemas al sacar numero de visitas".mysql_error());
					

                    //var_dump($visitas); exit();

					$conteo=0;
					$periodo=array("periodo"=>$_POST['quincena'],"ejercicio"=>$_POST['ejercicio']);	
					
                    $totales = array();
                    $porcent = array();
                    $numcol = mysql_num_rows(mysql_query("SELECT * FROM catalogo_resultado"));
					//$visitas=0;
                    //var_dump(mysql_result($rq, 2, 'val'));//exit();
                    for($i=0,$j=0;$j<$numcol;$j++){
                            if($num>0 && $i<$num){
                                    $orden = mysql_result($rq, $i, 'orden');
                            }
                            else{
                                    $orden = -99;
                            }
                            if($orden-1 == $j){
                                $hist[$j] = mysql_result($rq, $i, 'val');
                                //			$visitas=$visitas+$hist[$j];
                                //$porc[$j] = $hist[$j] / $visitas;
                                $conteo+=$hist[$j];
                                $i++;
                            }else{
                                    $hist[$j] = 0;
                             //       $porc[$j] = 0;
                            }
                    }
                    //var_dump($conteo); exit();
                    $visitas=$conteo;
                    for($i=0,$j=0;$j<$numcol;$j++){
                            if($num>0 && $i<$num){
                                    $orden = mysql_result($rq, $i, 'orden');
                            }
                            else{
                                    $orden = -99;
                            }
                            if($orden-1 == $j){
                                    $hist[$j] = mysql_result($rq, $i, 'val');
                            //          $visitas=$visitas+$hist[$j];
                                    $porc[$j] = $hist[$j] / $visitas;
                                    $i++;
                            }else{
                                    $hist[$j] = 0;
                                    $porc[$j] = 0;
                            }
                    }

					//var_dump($hist); exit();//print_r($hist);
                    //exit();
                    if($config['intelisis']==1){
                            $sql_all = "SELECT count(DISTINCT cuenta) 
										FROM asignacion_temp";

							 $histPromotor="SELECT CONCAT('Q',andr.periodo,'-',andr.ejercicio) AS 'Quincena',
									GROUP_CONCAT(DISTINCT reg.ruta ORDER BY ruta SEPARATOR ',') AS ruta,
									andr.nivel AS nivel,
									count(DISTINCT andr.factura) as Cartera,
									count(DISTINCT reg.cuenta) AS visitadas,
									count(DISTINCT andr.cuenta) AS Cuentas
									FROM asignacion_temp andr
									LEFT JOIN gestiones_temp reg ON CASE WHEN (reg.intelisis IS NULL OR reg.intelisis = 'SIN AGE' OR reg.intelisis = ' ') THEN REPLACE(andr.nomina,' ','') = REPLACE(reg.promotor,' ','') ELSE  REPLACE(andr.nomina,' ','') = REPLACE(reg.intelisis,' ','') END
									and REPLACE(reg.cuenta,' ','')=REPLACE(andr.cuenta,' ','')
									and REPLACE(andr.movimiento,' ','')=REPLACE(reg.mov,' ','')
									and REPLACE(andr.factura,' ','')=REPLACE(reg.factura,' ','')
									GROUP BY andr.periodo ORDER BY andr.nivel";
                    }
				
					

                    $cuentas_all = mysql_result(mysql_query($sql_all),0);
                    if($cuentas_all == 0){
                            $cuentas_all = 1;
                    }

                    $sql = "SELECT count(DISTINCT reg.cuenta) FROM 
							asignacion_temp andr
							INNER JOIN gestiones_temp reg ON CASE WHEN (reg.intelisis IS NULL OR reg.intelisis = 'SIN AGE' OR reg.intelisis = ' ') THEN REPLACE(andr.nomina,' ','') = REPLACE(reg.promotor,' ','') ELSE  REPLACE(andr.nomina,' ','') = REPLACE(reg.intelisis,' ','') END
							and REPLACE(reg.cuenta,' ','')=REPLACE(andr.cuenta,' ','')
							and REPLACE(andr.movimiento,' ','')=REPLACE(reg.mov,' ','')
							and REPLACE(andr.factura,' ','')=REPLACE(reg.factura,' ','')";
                    $cuentas = mysql_result(mysql_query($sql),0);
                    //echo $visitas;
                    $hist2 = array(
                            "registros"     => $visitas,
                            "ctasVis"       => $cuentas,
                            "porCtasVis"    => $cuentas / $cuentas_all,
                            "ctasSinVis"    => $cuentas_all - $cuentas,
                            "porCtasSinVis" => ($cuentas_all - $cuentas) / $cuentas_all
                    );
                    //var_dump($hist2);exit();

                    $promotor = "SELECT promotor AS nom_unicaja, intelisis AS nom_intelisis,  rp.nombre AS nombre, rp.grupo
                                 FROM gestiones_temp AS r
                                    LEFT JOIN (SELECT nomina,nombre,es as grupo FROM agentes where nomina=agente) AS rp ON CASE WHEN (r.intelisis IS NULL OR r.intelisis = 'SIN AGE' OR r.intelisis = ' ') THEN rp.nomina = r.promotor ELSE  rp.nomina = r.intelisis END 
                                 ORDER BY id DESC LIMIT 1";
                    $form = array(
                            "base"      => $config['base']
                    );
					//echo $promotor;
                    //					echo $hist;
					//echo $hist2
					//$Fechas =array("desde"=>$desde,"hasta"=>$hasta);
					//$date  = new DateTime($desde);
					//$date1 = new DateTime($hasta);
					$FechaTitulo="select DATE_FORMAT(quincena_cobranza1(".$_POST['quincena'].",".$_POST['ejercicio']."),'%Y %b %e') as pd";
					$FechaTitulo2="select DATE_FORMAT(quincena_cobranza2(".$_POST['quincena'].",".$_POST['ejercicio']."),'%Y %b %e') as sd";
					//$GLOBALS['desde']= $this ->FormatoFecha($desde);
					//$GLOBALS['hasta']= $this ->FormatoFecha($hasta);
                    $this->tpl = "gestionescobranza/reporte.gestor.detalle.html";
                    $bloque = array(
                            "blkResulv"     => "SELECT * FROM catalogo_resultado ORDER BY orden",
                            "blkHistProm"   => (!empty($_POST))?$histPromotor:array(),
                            "blkPromotor"   => $promotor,
                            "blkHist"       => $hist,
                            "blkPorc"       => $porc,
                            "blkFecha"      => array($periodo),
							"blkFechaVista" => array($periodo),
                            "blkHist2"      => array($hist2),
                            "nomina"        => array($_POST['nomina']),
                            "form"          => array($form),
							"blkzona"       => "select GROUP_CONCAT(DISTINCT zona ORDER BY zona SEPARATOR ',')  as Zona from bitacora_gestioncobranza_intelisis.agentes_zonas where agente ='".$_POST['nomina']."'",
							"fechaini"          => $FechaTitulo,
							"fechafin"          => $FechaTitulo2
				
                    );
            }
            return $bloque;
    } // fin _reporte_gestor

	function _rep_jefe(){
            $this->tpl = "gestionescobranza/reporte.jefe.html";
			
			
			 $bloque = array(
                    "blkPost" => empty($_POST)?array():$_POST,
                    "blkNivCob" => "SELECT DISTINCT nivel_cobranza AS id, nivel_cobranza AS nombre FROM catalogo_promotores WHERE nivel_cobranza <> '' ORDER BY nombre"
            );
            if(!empty($_POST)){
                    $rq = mysql_query("SELECT * FROM config ORDER BY id DESC LIMIT 1");
                    $config = mysql_fetch_assoc($rq);

                    $promotor = "SELECT * FROM catalogo_promotores WHERE (nom_unicaja = '".$_POST['nomina']."' OR nom_intelisis = '".$_POST['nomina']."') LIMIT 1";
                    $rq = mysql_query("SELECT factura FROM registros WHERE (promotor = '".$_POST['nomina']."' OR intelisis = '".$_POST['nomina']."') LIMIT 1");
                    $rq2 = mysql_query($promotor);
                    if((!$rq || mysql_num_rows($rq) == 0)&&(!$rq2 || mysql_num_rows($rq2) == 0)){
                            $this->msgaviso = "No se encontro al Jefe de Grupo";
                            mysql_free_result($rq);
                            mysql_free_result($rq2);
                            return $bloque;
                    }
                    mysql_free_result($rq);
                    mysql_free_result($rq2);
					$CambiarIdioma="SET lc_time_names = 'es_MX'";
					mysql_query($CambiarIdioma);
					$fecha1="select quincena_cobranza1(".$_POST['periodo'].",".$_POST['ejercicio'].") as fecha";
					$res=mysql_fetch_assoc(mysql_query($fecha1) );
					$desde=$res['fecha'];
					$fecha2="select quincena_cobranza2(".$_POST['periodo'].",".$_POST['ejercicio'].") as fecha";
					$res=mysql_fetch_assoc(mysql_query($fecha2) );
					$hasta=$res['fecha'];
                    //if($_POST['fecha_inicio'] != ''){
                            $fecha = " AND fecha_inicio BETWEEN '".$desde."' AND '".$hasta."' ";
                    //}
                    $sql = "CREATE TEMPORARY TABLE IF NOT EXISTS reporte_temp AS
							SELECT r.id, r.cuenta, r.mov, r.factura, r.fechaultimopago, r.fechafactura, r.saldocapital, r.druta, r.articulo, r.diasinactividad, 
                r.diasvencidos, r.intereses, r.cuota, r.nombre, r.domicilio, r.entre, r.colonia, r.ciudad, r.estado, r.rutacobro, r.aval, 
                r.aval_domicilio, r.aval_colonia, r.aval_ciudad, r.telefono, r.saldo, r.importe, r.fecha_captura, r.hora_captura, r.hora_captura_final, 
                r.fecha_inicio, r.hora_inicio, r.fecha_final, r.hora_final, r.resultado, r.valor_resultado, r.proximavisita, r.comentario, 
                r.pago_promesa, r.pago_importe, r.pago_quien, r.recibo, r.promotor, r.etapa, r.ruta, r.intelisis, r.saldovencido, r.cartera, 
                r.folio, r.capturista, r.pzo, r.abonos, r.imp_convenio, r.imp_inicial, r.num_convenio, r.gps, r.numero, r.origen,j.* FROM registros ra
							INNER JOIN (select distinct b.nomina,b.Nombre as Nombre_Jefe from agentes a
            				LEFT JOIN agentes b ON a.celula=b.agente
            				WHERE b.nomina is not null) j ON j.nomina=ra.promotor
							INNER JOIN registros r ON r.cuenta=ra.cuenta AND r.resultado=ra.resultado AND r.comentario=ra.comentario 
							AND (r.promotor='".$_POST['nomina']."' OR r.intelisis='".$_POST['nomina']."') AND r.fecha_inicio BETWEEN '".$desde."' AND '".$hasta."'  AND ra.resultado <>''  ORDER BY r.fecha_inicio";
							
				$sqlold="CREATE TEMPORARY TABLE IF NOT EXISTS reporte_temp AS
					Select r.id, r.cuenta, r.mov, r.factura, r.fechaultimopago, r.fechafactura, r.saldocapital, r.druta, r.articulo, r.diasinactividad, 
                    r.diasvencidos, r.intereses, r.cuota, r.nombre, r.domicilio, r.entre, r.colonia, r.ciudad, r.estado, r.rutacobro, r.aval, 
                    r.aval_domicilio, r.aval_colonia, r.aval_ciudad, r.telefono, r.saldo, r.importe, r.fecha_captura, r.hora_captura, r.hora_captura_final, 
                    r.fecha_inicio, r.hora_inicio, r.fecha_final, r.hora_final, r.resultado, r.valor_resultado, r.proximavisita, r.comentario, 
                    r.pago_promesa, r.pago_importe, r.pago_quien, r.recibo, r.promotor, r.etapa, r.ruta, r.intelisis, r.saldovencido, r.cartera, 
                    r.folio, r.capturista, r.pzo, r.abonos, r.imp_convenio, r.imp_inicial, r.num_convenio, r.gps, r.numero, r.origen 
					FROM registros AS r
					LEFT JOIN ruta_promotor AS rp ON CASE WHEN (r.intelisis IS NULL OR r.intelisis = 'SIN AGE' OR r.intelisis = ' ') THEN rp.nom_intelisis = r.promotor ELSE  rp.nom_intelisis = r.intelisis END
					WHERE r.intelisis = '".$_POST['nomina']."' ".$fecha." and r.resultado <> ' ' GROUP BY r.id ORDER BY r.fecha_inicio, r.hora_inicio";
                   //echo $sql;
                    mysql_query($sql);
                    $visitas = mysql_affected_rows();

                    $sql = "SELECT c.orden, COUNT(*) as val FROM reporte_temp r inner join catalogo_resultado c ON c.id = r.resultado GROUP BY resultado order by c.orden";
                    $rq = mysql_query($sql);
					//echo $sql;
                    $num = mysql_num_rows($rq);
					$mesini  = substr($desde,5,2);
					$mesfin  = substr($hasta,5,2);
					
					switch($mesini){
						case '01': $fini = str_replace("-01-","-ENE-",$desde);break;
						case '02': $fini = str_replace("-02-","-FEB-",$desde);break;
						case '03': $fini = str_replace("-03-","-MAR-",$desde);break;
						case '04': $fini = str_replace("-04-","-ABR-",$desde);break;
						case '05': $fini = str_replace("-05-","-MAY-",$desde);break;
						case '06': $fini = str_replace("-06-","-JUN-",$desde);break;
						case '07': $fini = str_replace("-07-","-JUL-",$desde);break;
						case '08': $fini = str_replace("-08-","-AGO-",$desde);break;
						case '09': $fini = str_replace("-09-","-SEP-",$desde);break;
						case '10': $fini = str_replace("-10-","-OCT-",$desde);break;
						case '11': $fini = str_replace("-11-","-NOV-",$desde);break;
						case '12': $fini = str_replace("-12-","-DIC-",$desde);break;
					}
					switch($mesfin){
						case '01': $ffin = str_replace("-01-","-ENE-",$hasta);break;
						case '02': $ffin = str_replace("-02-","-FEB-",$hasta);break;
						case '03': $ffin = str_replace("-03-","-MAR-",$hasta);break;
						case '04': $ffin = str_replace("-04-","-ABR-",$hasta);break;
						case '05': $ffin = str_replace("-05-","-MAY-",$hasta);break;
						case '06': $ffin = str_replace("-06-","-JUN-",$hasta);break;
						case '07': $ffin = str_replace("-07-","-JUL-",$hasta);break;
						case '08': $ffin = str_replace("-08-","-AGO-",$hasta);break;
						case '09': $ffin = str_replace("-09-","-SEP-",$hasta);break;
						case '10': $ffin = str_replace("-10-","-OCT-",$hasta);break;
						case '11': $ffin = str_replace("-11-","-NOV-",$hasta);break;
						case '12': $ffin = str_replace("-12-","-DIC-",$hasta);break;
					}
					$fechasvista = array("inicio" => "".$fini, "fin" => "".$ffin);
					$fechas = array("inicio" => "".$desde, "fin" => "".$hasta, "ejercicio" => "".$_POST['ejercicio'], "periodo" => "".$_POST['periodo']);
					

                    $totales = array();
                    $porcent = array();
                    $numcol = mysql_num_rows(mysql_query("SELECT * FROM catalogo_resultado"));
                    for($i=0,$j=0;$j<$numcol;$j++){
                            if($num>0 && $i<$num){
                                    $orden = mysql_result($rq, $i, 'orden');
                            }
                            else{
                                    $orden = -99;
                            }
                            if($orden-1 == $j){
                                    $hist[$j] = mysql_result($rq, $i, 'val');
                                    $porc[$j] = $hist[$j] / $visitas;
                                    $i++;
                            }else{
                                    $hist[$j] = 0;
                                    $porc[$j] = 0;
                            }
                    }
					//print_r($hist);
                    $rq = mysql_query("SELECT cartera FROM registros WHERE (promotor = '".$_POST['nomina']."' OR intelisis = '".$_POST['nomina']."') ".$fecha." ORDER BY fecha_inicio DESC LIMIT 1");
                    if($config['intelisis']==1){
                            $sql_all = "SELECT count(DISTINCT cuenta) FROM data_c086 WHERE agente = '".$_POST['nomina']."'";
                           /* $histPromotor = "SELECT CONCAT('Q',(MONTH(fecha_inicio) * 2 - (CASE WHEN DAYOFMONTH(fecha_inicio) < 16 THEN 1 ELSE 0 END)), ' - ', YEAR(fecha_inicio)) AS 'Quincena',
                                             GROUP_CONCAT(DISTINCT ruta ORDER BY ruta SEPARATOR ',') AS ruta,
                                             etapa AS nivel,
                                             CASE WHEN
                                                 BINARY(CONCAT('Q',(MONTH(fecha_inicio) * 2 - (CASE WHEN DAYOFMONTH(fecha_inicio) < 16 THEN 1 ELSE 0 END)), ' - ', YEAR(fecha_inicio))) =
                                                 BINARY(CONCAT('Q',(MONTH(NOW()) * 2 - (CASE WHEN DAYOFMONTH(NOW()) < 16 THEN 1 ELSE 0 END)), ' - ', YEAR(NOW())))
                                             THEN (SELECT count(DISTINCT cuenta) FROM data_c086 WHERE agente = '".$_POST['nomina']."' )
                                             ELSE cartera END AS Cartera,
                                                count(DISTINCT cuenta) AS visitadas
                                             FROM reporte_temp GROUP BY Quincena ORDER BY fecha_inicio";*/
							$histPromotor = "SELECT CONCAT('Q',".$_POST['periodo'].", ' - ', YEAR(fecha_inicio)) AS 'Quincena',
                                             GROUP_CONCAT(DISTINCT ruta ORDER BY ruta SEPARATOR ',') AS ruta,
                                             etapa AS nivel,
                                             CASE WHEN
                                                 BINARY(CONCAT('Q',(MONTH(fecha_inicio) * 2 - (CASE WHEN DAYOFMONTH(fecha_inicio) < 16 THEN 1 ELSE 0 END)), ' - ', YEAR(fecha_inicio))) =
                                                 BINARY(CONCAT('Q',(MONTH(NOW()) * 2 - (CASE WHEN DAYOFMONTH(NOW()) < 16 THEN 1 ELSE 0 END)), ' - ', YEAR(NOW())))
                                             THEN (SELECT count(DISTINCT cuenta) FROM data_c086 WHERE agente = '".$_POST['nomina']."' )
                                             ELSE cartera END AS Cartera,
                                                count(DISTINCT cuenta) AS visitadas
                                             FROM reporte_temp  ORDER BY fecha_inicio";
				     //echo  $histPromotor;
                    }else{
                            $sql_all = "SELECT count(DISTINCT cuenta) FROM data_c086 WHERE rutacobro IN (SELECT ruta FROM ruta_promotor WHERE nom_unicaja = '".$_POST['nomina']."')";
                           
                           /* $histPromotor = "SELECT CONCAT('Q',(MONTH(fecha_inicio) * 2 - (CASE WHEN DAYOFMONTH(fecha_inicio) < 16 THEN 1 ELSE 0 END)), ' - ', YEAR(fecha_inicio)) AS 'Quincena',
                                                ruta,
                                                etapa AS nivel,
                                                CASE WHEN
                                                    BINARY(CONCAT('Q',(MONTH(fecha_inicio) * 2 - (CASE WHEN DAYOFMONTH(fecha_inicio) < 16 THEN 1 ELSE 0 END)), ' - ', YEAR(fecha_inicio))) =
                                                    BINARY(CONCAT('Q',(MONTH(NOW()) * 2 - (CASE WHEN DAYOFMONTH(NOW()) < 16 THEN 1 ELSE 0 END)), ' - ', YEAR(NOW())))
                                                THEN (SELECT count(DISTINCT cuenta) FROM data_c086 WHERE rutacobro IN (SELECT ruta FROM ruta_promotor WHERE nom_unicaja = '".$_POST['nomina']."' ))
                                                ELSE cartera END AS Cartera,
                                                count(DISTINCT cuenta) AS visitadas
                                            FROM reporte_temp GROUP BY Quincena ORDER BY fecha_inicio";*/
							 $histPromotor = "SELECT CONCAT('Q',".$_POST['periodo'].", ' - ', YEAR(fecha_inicio)) AS 'Quincena',
                                                ruta,
                                                etapa AS nivel,
                                                CASE WHEN
                                                    BINARY(CONCAT('Q',(MONTH(fecha_inicio) * 2 - (CASE WHEN DAYOFMONTH(fecha_inicio) < 16 THEN 1 ELSE 0 END)), ' - ', YEAR(fecha_inicio))) =
                                                    BINARY(CONCAT('Q',(MONTH(NOW()) * 2 - (CASE WHEN DAYOFMONTH(NOW()) < 16 THEN 1 ELSE 0 END)), ' - ', YEAR(NOW())))
                                                THEN (SELECT count(DISTINCT cuenta) FROM data_c086 WHERE rutacobro IN (SELECT ruta FROM ruta_promotor WHERE nom_unicaja = '".$_POST['nomina']."' ))
                                                ELSE cartera END AS Cartera,
                                                count(DISTINCT cuenta) AS visitadas
                                            FROM reporte_temp ORDER BY fecha_inicio";											
                            //echo $histPromotor;
                    }

                    $cuentas_all = mysql_result(mysql_query($sql_all),0);
                    if($cuentas_all == 0){
                            $cuentas_all = 1;
                    }

                    $sql = "SELECT count(DISTINCT cuenta) FROM reporte_temp";
                    $cuentas = mysql_result(mysql_query($sql),0);

                    $hist2 = array(
                            "registros"     => $visitas,
                            "ctasVis"       => $cuentas,
                            "porCtasVis"    => $cuentas / $cuentas_all,
                            "ctasSinVis"    => $cuentas_all - $cuentas,
                            "porCtasSinVis" => ($cuentas_all - $cuentas) / $cuentas_all
                    );

                    $promotor = "SELECT Nomina as nom_intelisis, Nombre AS nombre, Es AS grupo
                                 FROM agentes 
                                    
                                 WHERE Nomina = '".$_POST['nomina']."'  LIMIT 1";
                    $form = array(
                            "base"      => $config['base']
                    );
					//echo $promotor;
            //					echo $hist;
                    $this->tpl = "gestionescobranza/reporte.jefe.detalle.html";
                    $bloque = array(
                            "blkResulv"     => "SELECT * FROM catalogo_resultado ORDER BY orden",
                            "blkHistProm"   => (!empty($_POST))?$histPromotor:array(),
                            "blkPromotor"   => $promotor,
                            "blkHist"       => $hist,
                            "blkPorc"       => $porc,
                            "blkFecha"      => array($fechas),
							"blkFechaVista" => array($fechasvista),
                            "blkHist2"      => array($hist2),
                            "nomina"        => array($_POST['nomina']),
                            "form"          => array($form)
                    );
            }
            return $bloque;
			
          
    }

    function _reporte_cliente(){
       
            $this->tpl = "gestionescobranza/reporte.cliente.html";
            $bloque = array(
                    "blkPost" => empty($_POST)?array():$_POST,
                    "blkNivCob" => "SELECT DISTINCT nivel_cobranza AS id, nivel_cobranza AS nombre FROM catalogo_promotores WHERE nivel_cobranza <> '' ORDER BY nombre"
            );
			date_default_timezone_set("America/Mexico_City");
			
            
            if(!empty($_POST)){
                    $rq = mysql_query("SELECT * FROM config ORDER BY id DESC LIMIT 1");
                    $config = mysql_fetch_assoc($rq);
            
                    $cuenta = $_POST['cuenta'];

                    $CambiarIdioma="SET lc_time_names = 'es_MX'";
                    mysql_query($CambiarIdioma);
                    $fecha1="select quincena_cobranza1(".$_POST['quincena'].",".$_POST['ejercicio'].") as fecha";
                    $res=mysql_fetch_assoc(mysql_query($fecha1));
                    $desde=$res['fecha'];
                    $fecha2="select quincena_cobranza2(".$_POST['quincena'].",".$_POST['ejercicio'].") as fecha";
                    $res=mysql_fetch_assoc(mysql_query($fecha2));
                    $hasta=$res['fecha'];

                    $fecha_inicio = str_replace('/', '-', $desde);
                    $fecha_fin = str_replace('/', '-', $hasta);
            
                    if($_POST['quincena'] != ''){
                            $fecha = " AND fecha_inicio BETWEEN '".$fecha_inicio."' AND '".$fecha_fin."' ";
                    }
                    $cliente = "SELECT 
										*
									FROM
										(select 
											cuenta,
												nombre,
												domicilio,
												ruta,
												druta,
												CASE
													WHEN
														(intelisis IS NULL
															OR intelisis = 'SIN AGE'
															OR intelisis = ' ')
													THEN
														promotor
													ELSE intelisis
												END as promo,
												fecha_inicio
										from
											registros
										where
											BINARY( cuenta) = BINARY( '".$cuenta."' ) UNION ALL select 
											cuenta,
												nombre,
												domicilio,
												ruta,
												druta,
												CASE
													WHEN
														(intelisis IS NULL
															OR intelisis = 'SIN AGE'
															OR intelisis = ' ')
													THEN
														promotor
													ELSE intelisis
												END as promo,
												fecha_inicio
										from
											bitacora_gestioncobranza_intcob.registros
										where
											BINARY( cuenta) = BINARY( '".$cuenta."')) A
									order by fecha_inicio desc
									limit 1";
                    $rq = mysql_query($cliente);
				//	echo $cliente;
                    if(!$rq || mysql_num_rows($rq) == 0){
                            $cliente = "SELECT * FROM (
											SELECT * FROM cliente_registros WHERE BINARY(cuenta) = BINARY('".$cuenta."') 
											UNION
											SELECT * FROM bitacora_gestioncobranza_intcob.cliente_registro WHERE BINARY(cuenta) = BINARY('".$cuenta."') 
										) A
										ORDER BY dv DESC LIMIT 1;";
							//echo $cliente;
                            $rq = mysql_query($cliente);
                            if(!$rq || mysql_num_rows($rq) == 0){
                                    $this->msgaviso = "El Cliente no tiene adeudos vencidos o no ha tenido registro de visitas, pruebe de nuevo con otro periodo.";
                                    return $bloque;
                            }
                    }
         
				    $sql= " CREATE TEMPORARY TABLE IF NOT EXISTS reporte_temp AS
							Select 
								promotor,
								intelisis,
								rutacobro,
								ruta,
								druta,
								comentario,
								cuenta,
								factura,
								fecha_inicio,
								hora_inicio,
								resultado,
								mov
							FROM
								registros
							WHERE
								BINARY( cuenta) = BINARY( '".$cuenta."')
									and resultado <> '' ".$fecha."	
							GROUP BY cuenta, promotor, resultado, hora_inicio 						
							UNION ALL

							 Select 
								promotor,
								intelisis,
								rutacobro,
								ruta,
								druta,
								comentario,
								cuenta,
								factura,
								fecha_inicio,
								hora_inicio,
								resultado,
								mov
							FROM
								bitacora_gestioncobranza_intcob.registros
							WHERE
								BINARY( cuenta) = BINARY( '".$cuenta."')
									and resultado <> ''
									".$fecha."
							GROUP BY cuenta, promotor, resultado, hora_inicio" ;
					//echo $sql;	
                    mysql_query($sql);
				 $visitas = mysql_affected_rows();
					
				$sqlValidarTabla=mysql_query("select 1 from android_reportes._Cuentas_GestionCobranzaHistorico
                                              where  periodo=".$_POST['quincena']." and ejercicio=".$_POST['ejercicio']." limit 1");
				
				if (mysql_num_rows($sqlValidarTabla)==0)
				    $consultaTabla="android_reportes._cuentas_gestioncobranza";
				else
				    $consultaTabla="android_reportes._Cuentas_GestionCobranzaHistorico";
				
				//echo $consultaTabla;
					
					  $tempasignacion ="CREATE TEMPORARY TABLE asignacion_temp AS SELECT * FROM( 
					  SELECT id, nomina, cuenta, nivel, movimiento, factura, nombre, plazo, imp_comp, fup, abonos, sdo_ven, sdo_tot, colonia, poblacion,
	                         articulo, telefono, dias_inactivos, dias_vencidos, ejercicio, periodo, empresa, origen 
					  FROM ".$consultaTabla." where cuenta='".$cuenta."' and periodo=".$_POST['quincena']." and ejercicio=".$_POST['ejercicio']."
					  UNION ALL
					  select agc.id, ad.agente_asociado as nomina, agc.cuenta, agc.nivel, agc.movimiento, agc.factura, agc.nombre, agc.plazo, 
						agc.imp_comp, agc.fup, agc.abonos, agc.sdo_ven, agc.sdo_tot, agc.colonia, agc.poblacion, agc.articulo, agc.telefono, agc.dias_inactivos,
						agc.dias_vencidos, agc.ejercicio, agc.periodo, agc.empresa, agc.origen from (

							select agente_origen, agente_asociado from bitacora_gestioncobranza_intelisis.agente_doble
							where fecha_asigna between '".$desde."' and '".$hasta."'
							group by agente_origen, agente_asociado

						) as ad
						left join ".$consultaTabla." agc on agc.nomina=ad.agente_origen
						where agc.periodo=".$_POST['quincena']." and agc.ejercicio=".$_POST['ejercicio']." and agc.cuenta='".$cuenta."'
						) a";     
                    //echo $tempasignacion ;
                    mysql_query($tempasignacion) or die(mysql_error());                          
					
					$sqlvist = "SELECT reg.cuenta
						FROM asignacion_temp andr
						INNER JOIN reporte_temp reg ON CASE WHEN (reg.intelisis IS NULL OR reg.intelisis = 'SIN AGE' OR reg.intelisis = ' ') THEN REPLACE(andr.nomina,' ','') = REPLACE(reg.promotor,' ','') ELSE  REPLACE(andr.nomina,' ','') = REPLACE(reg.intelisis,' ','') END
						and REPLACE(reg.cuenta,' ','')=REPLACE(andr.cuenta,' ','')
						and REPLACE(andr.movimiento,' ','')=REPLACE(reg.mov,' ','')
						and REPLACE(andr.factura,' ','')=REPLACE(reg.factura,' ','') 
						";
					//echo $sqlvist;
					mysql_query($sqlvist) or die(mysql_error());
                   
					//echo $visitas;

                   // $sql = "SELECT c.orden, COUNT(*) as val FROM reporte_temp r inner join catalogo_resultado c ON c.id = r.resultado GROUP BY resultado order by c.orden";
             /*     $sql = "SELECT c.orden, COUNT(*) as val 
                        FROM asignacion_temp andr
                        INNER JOIN reporte_temp reg ON CASE WHEN (reg.intelisis IS NULL OR reg.intelisis = 'SIN AGE' OR reg.intelisis = ' ') THEN REPLACE(andr.nomina,' ','') = REPLACE(reg.promotor,' ','') ELSE  REPLACE(andr.nomina,' ','') = REPLACE(reg.intelisis,' ','') END
                        and REPLACE(reg.cuenta,' ','')=REPLACE(andr.cuenta,' ','')
                        and REPLACE(andr.movimiento,' ','')=REPLACE(reg.mov,' ','')
                        and REPLACE(andr.factura,' ','')=REPLACE(reg.factura,' ','') 
                        inner join catalogo_resultado c ON c.id = reg.resultado 
                        GROUP BY orden
						order by c.orden";*/
						
						$sql="select c.orden, COUNT(c.val) as val
							  from(
									SELECT c.orden, COUNT(*) as val 
									FROM reporte_temp reg  
									inner join catalogo_resultado c ON c.id = reg.resultado 
									GROUP BY cuenta , promotor, resultado , hora_inicio
									)c
							  group by c. orden 
							  order by c.orden";
						
                    $rq = mysql_query($sql);
                    $num = mysql_num_rows($rq);
					
					$meses = array("ENE","FEB","MAR","ABR","MAY","JUN","JUL","AGO","SEP","OCT","NOV","DIC");
					$xpld  = explode("-",$fecha_inicio);
					$xpld1 = explode("-",$fecha_fin);
                    $fechas  = array("inicio" => "".$fecha_inicio, "fin" => "".date($fecha_fin));
				    $fechas1 = array("inicio" => "".$xpld[0]."-".$meses[$xpld[1]-1]."-".$xpld[2], "fin" => "".$xpld1[0]."-".$meses[$xpld1[1]-1]."-".$xpld1[2]);
            

					$facturas="SELECT 
									d.factura,
									d.pzo,
									concat('$', d.saldocapital) AS sc,
									concat('$', d.intereses) as intereses,
									concat('$', d.saldovencido) AS sv,
									d.diasinactividad AS di,
									d.diasvencidos AS dv
								FROM
									data_c086 AS d
								WHERE
									BINARY( d.cuenta) = BINARY( '".$cuenta."')
								UNION
								SELECT 
									d.factura,
									d.pzo,
									concat('$', d.saldocapital) AS sc,
									concat('$', d.intereses) as intereses,
									concat('$', d.saldovencido) AS sv,
									d.diasinactividad AS di,
									d.diasvencidos AS dv
								FROM
									bitacora_gestioncobranza_intcob.data_c086 AS d
								WHERE
									BINARY( d.cuenta) = BINARY( '".$cuenta."')";
            		//echo $facturas;                   
					$totales = array();
                    $porcent = array();
                    $numcol = mysql_num_rows(mysql_query("SELECT * FROM catalogo_resultado"));
                    //$visitas=0;
                    for($i=0,$j=0;$j<$numcol;$j++){
                            if($num>0 && $i<$num){
                                    $orden = mysql_result($rq, $i, 'orden');
                            }
                            else{
                                    $orden = -99;
                            }
                            if($orden-1 == $j){
                                    $hist[$j] = mysql_result($rq, $i, 'val');

                        //          $visitas=$visitas+$hist[$j];
                                    $porc[$j] = $hist[$j] / $visitas;
                                    $i++;
                            }else{
                                    $hist[$j] = 0;
                                    $porc[$j] = 0;
                            }
                    }
					//print_r($hist);
					//exit();
            
                    $promotor = "SELECT B.*, cg.comentario_credito FROM (
								SELECT 
										(MONTH(fecha_inicio) * 2 - (CASE
												WHEN DAYOFMONTH(fecha_inicio) < 17 THEN 1
												ELSE 0
											END)) as Periodo,
										YEAR(fecha_inicio) as ejercicio,
										CONCAT('Q', (MONTH(fecha_inicio) * 2 - (CASE
												WHEN DAYOFMONTH(fecha_inicio) < 17 THEN 1
												ELSE 0
											END)), ' - ', YEAR(fecha_inicio)) AS 'Quincena',
											r.promotor AS nomina,
											r.factura,
											r.intelisis AS intelisis,
											CASE
												WHEN CAST(r.rutacobro AS UNSIGNED) = 0 THEN rh.NOMBRE
												ELSE r.rutacobro
											END AS nombre,
											r.ruta,
											r.druta AS diaruta,
											r.comentario AS comm,
											fecha_inicio
									FROM
										registros AS r
									LEFT JOIN `varios`.`empleados_rh201_intelisis` rh ON r.intelisis = rh.CLAVE
									WHERE
										BINARY( r.cuenta) = BINARY('".$cuenta."')
								
								UNION 
								SELECT 
										(MONTH(fecha_inicio) * 2 - (CASE
												WHEN DAYOFMONTH(fecha_inicio) < 17 THEN 1
												ELSE 0
											END)) as Periodo,
										YEAR(fecha_inicio) as ejercicio,
										CONCAT('Q', (MONTH(fecha_inicio) * 2 - (CASE
												WHEN DAYOFMONTH(fecha_inicio) < 17 THEN 1
												ELSE 0
											END)), ' - ', YEAR(fecha_inicio)) AS 'Quincena',
											r.promotor AS nomina,
											r.factura,
											r.intelisis AS intelisis,
											CASE
												WHEN CAST(r.rutacobro AS UNSIGNED) = 0 THEN rh.NOMBRE
												ELSE r.rutacobro
											END AS nombre,
											r.ruta,
											r.druta AS diaruta,
											r.comentario AS comm,
											fecha_inicio
									FROM
										bitacora_gestioncobranza_intcob.registros AS r
									LEFT JOIN `varios`.`empleados_rh201_intelisis` rh ON r.intelisis = rh.CLAVE
									WHERE
										BINARY( r.cuenta) = BINARY( '".$cuenta."')
									) B  LEFT JOIN comentarios_gestiones cg ON cg.cuenta = '".$cuenta."' and cg.factura = B.factura and cg.quinsena = B.periodo and cg.ejercicio = B.ejercicio
                                    GROUP BY Quincena,nomina
									ORDER BY fecha_inicio DESC";
					//echo $promotor;			 
            
                    $form = array(
                            "base"      => $config['base']
                    );

                    $this->tpl = "gestionescobranza/reporte.cliente.detalle.html";
                    $bloque = array(
				                "blkResulv"         => "SELECT * FROM catalogo_resultado ORDER BY orden",
                                "ultimoResultado"   => "Select cr.nombre AS 'rus' FROM reporte_temp LEFT JOIN catalogo_resultado AS cr ON cr.id = resultado ORDER BY fecha_inicio DESC LIMIT 1",
                                "ultimaFecha"       => "Select fecha_inicio AS 'fus' FROM reporte_temp ORDER BY fecha_inicio DESC LIMIT 1",
				                "blkHist"           => $hist,
				                "blkHist2"          => $hist2,
				                "blkPorc"           => $porc,
                                "blkCliente"        => $cliente,
                                "blkFac"            => $facturas,
                                "blkProm"           => $promotor,
                                "totalVisitas"      => array($visitas),
				                "blkFecha"          => array($fechas),
								"blkFecha1"			=> array($fechas1),
                                "form"              => array($form)
                    );
            }
            return $bloque;
    } //fin _reporte_cliente

    function _reporte_ctas(){
            $this->tpl = "gestionescobranza/reporte.visitas.html";
			$bloque = array(
                    "blkPost" => empty($_POST)?array():$_POST,
                    "blkGrupo" => "SELECT distinct TRIM(substring_index(MID(es, 5, 100), '0', 2)) as nombre FROM agentes where nomina=agente order by es",
                    "blkNivCob" => "SELECT distinct TRIM(substring_index(MID(es, 5, 100), '0', 1)) as id FROM agentes where nomina=agente order by es"
            );

            if(!empty($_POST)){
                
				$periodo        = $_POST['periodo'];
				$ejercicio      = $_POST['ejercicio'];
                $niv_cob        = $_POST['nivcob'];
                $promotor       = $_POST['nomina'];
                $grupo          = $_POST['grupo'];
            
				if($niv_cob != "TODOS"){
					$nivcob   = " and (nivel='".$niv_cob."' OR nivel in (SELECT niveles FROM bitacora_gestioncobranza_intelisis.relacion_niveles_especiales WHERE consolidado = '".$niv_cob."' ))";
					$nivReg   = " and (etapa='".$niv_cob."' OR etapa in (SELECT niveles FROM bitacora_gestioncobranza_intelisis.relacion_niveles_especiales WHERE consolidado = '".$niv_cob."' ))";
					$concepto = $niv_cob;
				}else{
					$concepto = 'Cuenta sin visita';
				}
					
                if($grupo != ''){
					$grupo =" and b.grupo='".$grupo."'";
				}
                    
                if($promotor != ""){
    				$promo    = " AND  a.nomina = '".$promotor."'";
    				$promoad    = " AND  ad.agente_asociado = '".$promotor."'";
    				$promoreg = " and (CASE
    								WHEN (R.intelisis IS NULL OR R.intelisis = 'SIN AGE' OR R.intelisis = ' ') THEN REPLACE(R.promotor, ' ', '')
    								ELSE REPLACE(R.intelisis, ' ', '')
    							END)='".$promotor."'";
    			}
			
                $CambiarIdioma="SET lc_time_names = 'es_MX'";
				mysql_query($CambiarIdioma);
				$fecha1="select bitacora_gestioncobranza_intelisis.quincena_cobranza1(".$periodo.",". $ejercicio.") as fecha";
				$res=mysql_fetch_assoc(mysql_query($fecha1));
				$desde=$res['fecha'];
				$fecha2="select bitacora_gestioncobranza_intelisis.quincena_cobranza2(".$periodo.",". $ejercicio.") as fecha";
				$res=mysql_fetch_assoc(mysql_query($fecha2));
				$hasta=$res['fecha'];
				
				$tabla1="CREATE TEMPORARY TABLE gest2_temp
    			(
    				nomina varchar(15) not null,
    				factura varchar(20) not null,
    				mov varchar(20) not null,
    				cuenta varchar(15) not null
    			)
    			";
    				  
    			$tabla2="CREATE TEMPORARY TABLE asig2_temp
    			(
    				nomina varchar(15) NOT NULL,
    				cuenta varchar(15) NOT NULL,
    				movimiento varchar(20), 
    				factura varchar(20),
    				nivel varchar(50),
    				Nombre varchar(50),
    				Pzo int,
    				Importe float,
    				fup date,
    				Abonos float,
    				Saldo_Vencido float,
    				Saldo_Total float,
    				D_I int,
    				D_V int ,
    				Articulo varchar (50),
    				grupo varchar(30)
    			)
				  ";
				  
				$tabla3="CREATE TEMPORARY TABLE reporte_temp
			    (
    				nomina varchar(15) NOT NULL,
    				cuenta varchar(15) NOT NULL,
    				movimiento varchar(20), 
    				factura varchar(20),
    				nivel varchar(50),
    				Nombre varchar(50),
    				Pzo int,
    				Importe float,
    				fup date,
    				Abonos float,
    				Saldo_Vencido float,
    				Saldo_Total float,
    				D_I int,
    				D_V int ,
    				Articulo varchar (50),
    				grupo varchar(30)
    			)
				  ";
				  
				$index1="CREATE INDEX cuenta_index USING BTREE ON gest2_temp (cuenta)";
				$index2="CREATE INDEX factura_index USING BTREE ON gest2_temp (factura)";
				$index3="CREATE INDEX cuenta_index USING BTREE ON asig2_temp (cuenta)";
				$index4="CREATE INDEX factura_index USING BTREE ON asig2_temp (factura)";
				$index5="CREATE INDEX cuenta_index USING BTREE ON reporte_temp (cuenta)";
				$index6="CREATE INDEX factura_index USING BTREE ON reporte_temp (factura)";
				mysql_query($tabla1) or die(mysql_error());
				mysql_query($index1) or die(mysql_error());
				mysql_query($index2) or die(mysql_error());
				mysql_query($tabla2) or die(mysql_error());
				mysql_query($index3) or die(mysql_error());
				mysql_query($index4) or die(mysql_error());
				mysql_query($tabla3) or die(mysql_error());
				mysql_query($index5) or die(mysql_error());
				mysql_query($index6) or die(mysql_error());
                $sqlReg = "insert into gest2_temp 
					select 
							*
						from
							(select
									CASE WHEN (R.intelisis IS NULL OR R.intelisis = 'SIN AGE' OR R.intelisis = ' ') THEN 
									REPLACE(R.promotor, ' ', '') ELSE REPLACE(R.intelisis, ' ', '') END as nomina, 
									REPLACE(R.factura, ' ', '') as factura, 
									REPLACE(R.mov, ' ', '') as mov, 
									REPLACE(R.cuenta, ' ', '') as cuenta 
									from bitacora_gestioncobranza_intcob.registros R
							where
								fecha_inicio BETWEEN '".$desde."' AND '".$hasta."'".$nivReg." and resultado <> ' ' ".$promoreg." 
						union all 
							select 
							CASE WHEN (R.intelisis IS NULL OR R.intelisis = 'SIN AGE' OR R.intelisis = ' ') THEN 
							REPLACE(R.promotor, ' ', '') ELSE REPLACE(R.intelisis, ' ', '') END as nomina, 
							REPLACE(R.factura, ' ', '') as factura, 
							REPLACE(R.mov, ' ', '') as mov, 
							REPLACE(R.cuenta, ' ', '') as cuenta 
							from bitacora_gestioncobranza_intelisis.registros R 
							where
								fecha_inicio BETWEEN '".$desde."' AND '".$hasta."'".$nivReg." and resultado <> ' ' ".$promoreg." 
				
				) as a";
                //                    echo $sqlReg;
                //					exit();
                //					mysql_query($sqlReg);
				mysql_query($sqlReg)or die ('problemas al llenar la tabla gest2_temp'. mysql_error());
	   		    //$regis = mysql_affected_rows();
				//echo $regis."<br/>";
				$sqlandroid="insert into asig2_temp  SELECT * FROM (
					SELECT REPLACE(a.nomina,' ','') as nomina,
							   REPLACE(a.cuenta,' ','') as cuenta,
							   REPLACE(a.movimiento,' ','') as movimiento,
							   REPLACE(a.factura,' ','') as factura,
							   a.nivel,
							   a.nombre ,
							   a.plazo,
							   a.imp_comp,
							   a.fup ,
								a.abonos,
								a.sdo_ven,
								a.sdo_tot,
								a.dias_inactivos ,
								a.dias_vencidos,
								a.articulo,
							   b.grupo
					FROM android_reportes._cuentas_gestioncobranza a 
					inner join (SELECT distinct TRIM(substring_index(MID(es, 5, 100), '0', 2)) as grupo,nomina 
						FROM bitacora_gestioncobranza_intelisis.agentes where nomina=agente order by es)b on a.nomina=b.nomina 
				    where periodo=".$periodo." and ejercicio=".$ejercicio.$nivcob.$grupo.$promo."
						union all
							SELECT REPLACE(a.nomina,' ','') as nomina, 
							REPLACE(a.cuenta,' ','') as cuenta, 
							REPLACE(a.movimiento,' ','') as movimiento, 
							REPLACE(a.factura,' ','') as factura, 
							a.nivel, 
							a.nombre , 
							a.plazo, 
							a.imp_comp, 
							a.fup , 
							a.abonos, 
							a.sdo_ven, 
							a.sdo_tot, 
							a.dias_inactivos , 
							a.dias_vencidos, 
							a.articulo, 
							b.grupo 
							FROM android_reportes._cuentas_gestioncobranza a 
							inner join (SELECT distinct TRIM(substring_index(MID(es, 5, 100), '0', 2)) as grupo,nomina 
							FROM bitacora_gestioncobranza_intelisis.agentes order by es)b on a.nomina=b.nomina 
							where periodo=".$periodo." and ejercicio=".$ejercicio.$nivcob.$grupo.$promo." AND a.origen = 'OPERATIVO'
						UNION ALL 
						SELECT REPLACE(ad.agente_asociado,' ','') as nomina,
									   REPLACE(a.cuenta,' ','') as cuenta,
									   REPLACE(a.movimiento,' ','') as movimiento,
									   REPLACE(a.factura,' ','') as factura,
									   a.nivel,
									   a.nombre ,
									   a.plazo,
									   a.imp_comp,
								       a.fup ,
										a.abonos,
										a.sdo_ven,
										a.sdo_tot,
										a.dias_inactivos ,
										a.dias_vencidos,
										a.articulo,
									   b.grupo
							FROM (

							select agente_origen, agente_asociado from bitacora_gestioncobranza_intelisis.agente_doble
							where fecha_asigna between '".$desde."' and '".$hasta."'
							group by agente_origen, agente_asociado

						) as ad
						left join android_reportes._cuentas_gestioncobranza a on a.nomina=ad.agente_origen
						inner join (SELECT distinct TRIM(substring_index(MID(es, 5, 100), '0', 2)) as grupo,nomina 
						FROM bitacora_gestioncobranza_intelisis.agentes where nomina=agente order by es)b on a.nomina=b.nomina
						where a.periodo=".$periodo." and a.ejercicio=".$ejercicio.$nivcob.$grupo.$promoad."
				) AS at";
						
				//echo $sqlandroid;
				//exit();
				mysql_query($sqlandroid)or die ('problemas al llenar la tabla asig2_temp'. mysql_error());
				//mysql_query($sqlandroid);
				//$registros = mysql_affected_rows();
				
				$sqlvisitas = "SELECT 	count(DISTINCT andr.factura) as Cartera
							FROM asig2_temp andr
							LEFT JOIN gest2_temp reg ON CASE WHEN (reg.nomina IS NULL OR reg.nomina = 'SIN AGE' OR reg.nomina = ' ') 
							THEN REPLACE(andr.nomina,' ','') = REPLACE(reg.nomina,' ','') 
							ELSE  REPLACE(andr.nomina,' ','') = REPLACE(reg.nomina,' ','') END
							and REPLACE(reg.cuenta,' ','')=REPLACE(andr.cuenta,' ','')
							and REPLACE(andr.movimiento,' ','')=REPLACE(reg.mov,' ','')
							and REPLACE(andr.factura,' ','')=REPLACE(reg.factura,' ','')
							 ORDER BY andr.nivel";
				//echo $sqlvisitas;
				//exit();

				//$res=mysql_query($sqlvisitas) or die (mysql_error());
				$res=mysql_query($sqlvisitas)or die ('problemas al ejecutar el conteo cartera'. mysql_error());
				$dat=mysql_fetch_array($res);
				$registros= $dat["Cartera"];


				//$registros = mysql_affected_rows();	
				//var_dump($registros);		 



                //					echo $registros;
				//	exit();
				
                $sqlall = "insert into reporte_temp
					select 
						a.nomina,
						a.cuenta,
						a.movimiento , 
						a.factura ,
						a.nivel ,
						a.Nombre ,
						a.Pzo ,
						a.Importe ,
						a.fup ,
						a.Abonos,
						a.Saldo_Vencido,
						a.Saldo_Total ,
						a.D_I ,
						a.D_V ,
						a.Articulo,
						a.grupo 
				from asig2_temp a 
				WHERE a.cuenta IN (SELECT cuenta FROM gest2_temp)
				GROUP BY a.cuenta";
                //	echo $sqlall;
                //	exit();				
							
    			mysql_query($sqlall)or die ('problemas al llenar la tabla reporte_temp'. mysql_error());
                //  mysql_query($sqlall);
                //	$registro= mysql_affected_rows();
				// echo $registro;
				$FechaTitulo="select DATE_FORMAT(quincena_cobranza1(".$_POST['periodo'].",".$_POST['ejercicio']."),'%Y %b %e') as pd";
				$FechaTitulo2="select DATE_FORMAT(quincena_cobranza2(".$_POST['periodo'].",".$_POST['ejercicio']."),'%Y %b %e') as sd";
                $sql_all = "SELECT count(distinct cuenta) AS cuentas
                            FROM asig2_temp";
                //					echo $sql_all;
                //					exit();
                $cuentas_all = mysql_result(mysql_query($sql_all),0);
                
				$sql = "SELECT count(DISTINCT cuenta) FROM reporte_temp";
                $cuentas = mysql_result(mysql_query($sql),0);
				//echo $cuentas_all;
				//$GLOBALS['NumDatos'] = $cuentas_all-$cuentas;
				if($cuentas_all == 0){
                        $por_sin_vis =0;
				}else{
					$por_sin_vis = (($cuentas_all-$cuentas) / $cuentas_all);
				}
				
				$fechas = array("inicio" => "".$desde, "fin" => "".$hasta, "Periodo" =>"".$periodo, "Ejercicio" =>"".$ejercicio , "Total" =>($cuentas_all-$cuentas));
			
                $hist2 = array(
                        array( "val" => $registros, "frm" => "0,000."),
						array( "val" => $cuentas, "frm" => "0,000."),
                        array( "val" => $cuentas_all-$cuentas, "frm" => "0,000."),
                        array( "val" => $cuentas_all, "frm" => "0,000."),
                        array( "val" => $por_sin_vis , "frm" => "0.00%")
                );
            
			    //var_dump($hist2);
			
                $form = array(
                        "nivcob"    => $niv_cob,
                        "nomina"    => $promotor,
                        "grupo"     => $grupo,
                        "base"      => $config['base']
                );
					
					
                $this->tpl = "gestionescobranza/reporte.visitas.detalle.html";
                $bloque = array(
                        "blkFecha"  => array($fechas),
                        "registros" => array($registros),
                        "form"      => array($form),
                        "blkHist2"  => $hist2,
						"fechaini"  => $FechaTitulo,
						"fechafin"  => $FechaTitulo2,
						"blknum"    => $DatosNum
                );
            }
            return $bloque;
    } //fin _reporte_ctas

    function query_cuota($nivcob, $concepto){
            $rq = mysql_query("SELECT * FROM config ORDER BY id DESC LIMIT 1");
            $config = mysql_fetch_assoc($rq);

            if($config['intelisis']==1){
                    $factura = "r.movi like 'Factura%' ";
                    $cargo = "r.movi = 'Credilana' ";
                    $credilana = "r.movi = 'Nota Cargo' ";
            }else{
                    $factura = "r.factura like 'F%' ";
                    $cargo = "r.factura like 'C%' ";
                    $credilana = "r.factura like 'N%' ";
            }
            $totales = array();
            if($concepto != ''){
                    $where = "AND resultado = '".$concepto."'";
            }
            if($nivcob == 'PREINTERMEDIA' || $nivcob == 'TODOS'){
                    $sql = "SELECT
                                SUM(CASE WHEN (diasvencidos BETWEEN 90 AND 9999) AND (diasinactividad BETWEEN 90 AND 9999) AND ($credilana) THEN 1 ELSE 0 END) AS cuota_c,
                                SUM(CASE WHEN (diasvencidos BETWEEN 1 AND 9999) AND (diasinactividad BETWEEN 1 AND 9999) AND ($cargo) THEN 1 ELSE 0 END) AS cuota_g,
                                SUM(CASE WHEN (diasvencidos BETWEEN 90 AND 9999) AND (diasinactividad BETWEEN 90 AND 9999) AND ($credilana OR $factura) THEN 1 ELSE 0 END) AS determi
                            FROM (SELECT resultado, factura, diasvencidos, diasinactividad, fecha_inicio, etapa, movi FROM registros_temp ) AS r WHERE etapa = 'PREINTERMEDIA' ".$where;
                    $result = mysql_query($sql);
                    if($result && mysql_num_rows($result)>0){
                            $row = mysql_fetch_assoc($result);
                            $totales['cuota_c'] = $totales['cuota_c'] + $row['cuota_c'];
                            $totales['cuota_g'] = $totales['cuota_g'] + $row['cuota_g'];
                            $totales['determi'] = $totales['determi'] + $row['determi'];
                            //$totales['liquidi'] = $totales['liquidi'] + $row['liquidi'];
                            //echo "- PREINTERMEDIA -".$concepto." - ".print_r($row)."<br>";
                    }
            }

            if($nivcob == 'INTERMEDIA' || $nivcob == 'TODOS'){
                    $sql = "SELECT
                                (SUM(CASE WHEN (diasvencidos BETWEEN 180 AND 9999) AND (diasinactividad BETWEEN 180 AND 9999) AND ($credilana) THEN 1 ELSE 0 END)) AS cuota_c,
                                (SUM(CASE WHEN (diasvencidos BETWEEN 1 AND 9999) AND (diasinactividad BETWEEN 1 AND 9999) AND ($cargo) THEN 1 ELSE 0 END)) AS cuota_g,
                                (SUM(CASE WHEN (diasvencidos BETWEEN 180 AND 9999) AND (diasinactividad BETWEEN 180 AND 9999) AND ($credilana OR $factura) THEN 1 ELSE 0 END)) AS determi
                            FROM (SELECT resultado, factura, diasvencidos, diasinactividad, fecha_inicio, etapa, movi FROM registros_temp ) AS r WHERE etapa = 'INTERMEDIA' ".$where;            //echo $sql . "<br />";
                    $result = mysql_query($sql);
                    //echo $result . "<br />";
                    if($result && mysql_num_rows($result)>0){
                            //echo "hello";
                            $row = mysql_fetch_assoc($result);
                            $totales['cuota_c'] = $totales['cuota_c'] + $row['cuota_c'];
                            $totales['cuota_g'] = $totales['cuota_g'] + $row['cuota_g'];
                            $totales['determi'] = $totales['determi'] + $row['determi'];
                            //$totales['liquidi'] = $totales['liquidi'] + $row['liquidi'];
                            //echo "- INTERMEDIA - ".$concepto." - ".print_r($row)."<br>";
                    }
            }

            if($nivcob == 'FORANEO01' || $nivcob == 'FORANEAS' || $nivcob == 'TODOS'){
                    $sql = "SELECT
                                (SUM(CASE WHEN (diasvencidos BETWEEN 90 AND 9999) AND (diasinactividad BETWEEN 90 AND 9999) AND ($credilana) THEN 1 ELSE 0 END)) AS cuota_c,
                                (SUM(CASE WHEN (diasvencidos BETWEEN 1 AND 9999) AND (diasinactividad BETWEEN 1 AND 9999) AND ($cargo) THEN 1 ELSE 0 END)) AS cuota_g,
                                (SUM(CASE WHEN (diasvencidos BETWEEN 90 AND 9999) AND (diasinactividad BETWEEN 90 AND 9999) AND ($credilana OR $factura) THEN 1 ELSE 0 END)) AS determi
                            FROM (SELECT resultado, factura, diasvencidos, diasinactividad, fecha_inicio, etapa, movi FROM registros_temp ) AS r WHERE etapa IN ('FORANEO01', 'FORANEAS') ".$where;
                    $result = mysql_query($sql);
                    if($result && mysql_num_rows($result)>0){
                            $row = mysql_fetch_assoc($result);
                            $totales['cuota_c'] = $totales['cuota_c'] + $row['cuota_c'];
                            $totales['cuota_g'] = $totales['cuota_g'] + $row['cuota_g'];
                            $totales['determi'] = $totales['determi'] + $row['determi'];
                            //$totales['liquidi'] = $totales['liquidi'] + $row['liquidi'];
                            //echo "- FORANEO01 - ".$concepto." - ".print_r($row)."<br>";
                    }
            }

            if($nivcob == 'LOCAL09' || $nivcob == 'INSTITUCIONES LOCAL' || $nivcob == 'TODOS'){
                    $sql = "SELECT
                                (SUM(CASE WHEN (diasvencidos BETWEEN 360 AND 9999) AND (diasinactividad BETWEEN 180 AND 9999) AND ($credilana) THEN 1 ELSE 0 END)) AS cuota_c,
                                (SUM(CASE WHEN (diasvencidos BETWEEN 1 AND 9999) AND (diasinactividad BETWEEN 1 AND 9999) AND ($cargo) THEN 1 ELSE 0 END)) AS cuota_g,
                                (SUM(CASE WHEN (diasvencidos BETWEEN 90 AND 9999) AND (diasinactividad BETWEEN 90 AND 9999) AND ($credilana OR $factura) THEN 1 ELSE 0 END)) AS determi
                            FROM (SELECT resultado, factura, diasvencidos, diasinactividad, fecha_inicio, etapa, movi FROM registros_temp ) AS r WHERE etapa IN ('LOCAL09', 'INSTITUCIONES LOCAL') ".$where;
                    $result = mysql_query($sql);
                    if($result && mysql_num_rows($result)>0){
                            $row = mysql_fetch_assoc($result);
                            $totales['cuota_c'] = $totales['cuota_c'] + $row['cuota_c'];
                            $totales['cuota_g'] = $totales['cuota_g'] + $row['cuota_g'];
                            $totales['determi'] = $totales['determi'] + $row['determi'];
                            //$totales['liquidi'] = $totales['liquidi'] + $row['liquidi'];
                            //echo "- INST09 - ".$concepto." - ".print_r($row)."<br>";
                    }
            }

            if($nivcob == 'FORANEO09' || $nivcob == 'INSTITUCIONES FORANEO' || $nivcob == 'TODOS'){
                    $sql = "SELECT
                                (SUM(CASE WHEN (diasvencidos BETWEEN 360 AND 9999) AND (diasinactividad BETWEEN 180 AND 9999) AND ($credilana) THEN 1 ELSE 0 END)) AS cuota_c,
                                (SUM(CASE WHEN (diasvencidos BETWEEN 1 AND 9999) AND (diasinactividad BETWEEN 1 AND 9999) AND ($cargo) THEN 1 ELSE 0 END)) AS cuota_g,
                                (SUM(CASE WHEN (diasvencidos BETWEEN 90 AND 9999) AND (diasinactividad BETWEEN 90 AND 9999) AND ($credilana OR $factura) THEN 1 ELSE 0 END)) AS determi
                            FROM (SELECT resultado, factura, diasvencidos, diasinactividad, fecha_inicio, etapa, movi FROM registros_temp ) AS r WHERE etapa IN ('FORANEO09', 'INSTITUCIONES FORANEO') ".$where;
                    $result = mysql_query($sql);
                    if($result && mysql_num_rows($result)>0){
                            $row = mysql_fetch_assoc($result);
                            $totales['cuota_c'] = $totales['cuota_c'] + $row['cuota_c'];
                            $totales['cuota_g'] = $totales['cuota_g'] + $row['cuota_g'];
                            $totales['determi'] = $totales['determi'] + $row['determi'];
                            //$totales['liquidi'] = $totales['liquidi'] + $row['liquidi'];
                            //echo "- INST09 - ".$concepto." - ".print_r($row)."<br>";
                    }
            }

            if($nivcob == 'PRE-ESPECIAL' || $nivcob == 'PREESPECIAL' || $nivcob == 'TODOS'){
                    $sql = "SELECT
                                (SUM(CASE WHEN (diasvencidos BETWEEN 360 AND 9999) AND (diasinactividad BETWEEN 180 AND 9999) AND ($credilana) THEN 1 ELSE 0 END)) AS cuota_c,
                                (SUM(CASE WHEN (diasvencidos BETWEEN 1 AND 9999) AND (diasinactividad BETWEEN 1 AND 9999) AND ($cargo) THEN 1 ELSE 0 END)) AS cuota_g,
                                (SUM(CASE WHEN (diasvencidos BETWEEN 360 AND 9999) AND (diasinactividad BETWEEN 180 AND 9999) AND ($credilana OR $factura) THEN 1 ELSE 0 END)) AS determi
                            FROM (SELECT resultado, factura, diasvencidos, diasinactividad, fecha_inicio, etapa, movi FROM registros_temp ) AS r WHERE etapa IN ('PRE-ESPECIAL', 'PREESPECIAL') ".$where;
                    $result = mysql_query($sql);
                    if($result && mysql_num_rows($result)>0){
                            $row = mysql_fetch_assoc($result);
                            $totales['cuota_c'] = $totales['cuota_c'] + $row['cuota_c'];
                            $totales['cuota_g'] = $totales['cuota_g'] + $row['cuota_g'];
                            $totales['determi'] = $totales['determi'] + $row['determi'];
                            //$totales['liquidi'] = $totales['liquidi'] + $row['liquidi'];
                            //echo "- PRE-ESPECIAL - ".$concepto." - ".print_r($row)."<br>";
                    }
            }

            if($nivcob == 'ESPECIAL' || $nivcob == 'TODOS'){
                    $sql = "SELECT
                                (SUM(CASE WHEN (diasvencidos BETWEEN 900 AND 9999) AND (diasinactividad BETWEEN 180 AND 9999) AND ($credilana OR $factura) THEN 1 ELSE 0 END)) AS liquidi,
                                (SUM(CASE WHEN (diasvencidos BETWEEN 900 AND 9999) AND (diasinactividad BETWEEN 180 AND 9999) AND ($credilana OR $factura) THEN 1 ELSE 0 END)) AS determi
                            FROM (SELECT resultado, factura, diasvencidos, diasinactividad, fecha_inicio, etapa, movi FROM registros_temp ) AS r WHERE etapa = 'ESPECIAL' ".$where;
                    $result = mysql_query($sql);
                    if($result && mysql_num_rows($result)>0){
                            $row = mysql_fetch_assoc($result);
                            //$totales['cuota_c'] = $totales['cuota_c'] + $row['cuota_c'];
                            //$totales['cuota_g'] = $totales['cuota_g'] + $row['cuota_g'];
                            $totales['determi'] = $totales['determi'] + $row['determi'];
                            $totales['liquidi'] = $totales['liquidi'] + $row['liquidi'];
                            //echo "- ESPECIAL -".$concepto." - ".print_r($row)."<br>";
                    }
            }
            //print_r($tototales);
            //print $sql."<br />";

            return $totales;
    }

    function _reporte_prod(){
            $this->tpl = "gestionescobranza/reporte.productividad.html";
			$bloque = array(
                    "blkPost" => empty($_POST)?array():$_POST,
                    "blkGrupo" => "SELECT distinct TRIM(substring_index(MID(es, 5, 100), '0', 2)) as nombre FROM agentes where nomina=agente order by es",
                    "blkNivCob" => "SELECT distinct TRIM(substring_index(MID(es, 5, 100), '0', 1)) as id FROM agentes where nomina=agente order by es"
            );
            if(!empty($_POST)){
					$periodo        = $_POST['periodo'];
					$ejercicio      = $_POST['ejercicio'];
                    if($_POST['nivcob'] != 'TODOS'){
                            $nivcob = " and (nivel='".$_POST['nivcob']."' OR nivel in (SELECT niveles FROM relacion_niveles_especiales WHERE consolidado = '".$_POST['nivcob']."' ))";
							$nivReg = " and (etapa='".$_POST['nivcob']."' OR etapa in (SELECT niveles FROM relacion_niveles_especiales WHERE consolidado = '".$_POST['nivcob']."' ))";
                    }
                    if($_POST['nomina'] != ''){
                            $promo    = " AND  a.nomina = '".$_POST['nomina'] ."' ";
							$promoreg = " and (CASE
											WHEN (R.intelisis IS NULL OR R.intelisis = 'SIN AGE' OR R.intelisis = ' ') THEN REPLACE(R.promotor, ' ', '')
											ELSE REPLACE(R.intelisis, ' ', '')
										END)='".$_POST['nomina'] ."'";
                    }
                    if($_POST['grupo'] != ''){
                           $grupoand =" and b.grupo='".$_POST['grupo']."'";
                    }
					$CambiarIdioma="SET lc_time_names = 'es_MX'";
					mysql_query($CambiarIdioma);
					$fecha1="select quincena_cobranza1(".$periodo.",".$ejercicio.") as fecha";
					$res=mysql_fetch_assoc(mysql_query($fecha1));
					$desde=$res['fecha'];
					$fecha2="select quincena_cobranza2(".$periodo.",".$ejercicio.") as fecha";
					$res=mysql_fetch_assoc(mysql_query($fecha2));
					$hasta=$res['fecha'];
					
					
					$ag=mysql_query("select distinct(agente_asociado) from agente_doble where agente_asociado='".$_POST['nomina']."' and fecha_asigna between '".$desde."' and '".$hasta."'");					
					 if(mysql_num_rows($ag) == 1){
                            $this->msgaviso = "Reporte no diponible para este agente debido a que es suplente.";
                            mysql_free_result($ag);
							return $bloque;
                    }
					
					
					$tabla1="CREATE TEMPORARY TABLE gest2_temp
							 (
								resultado varchar(20) NOT NULL,
								factura varchar(15),
								nomina varchar(15) NOT NULL,
								cuenta varchar(15),
								movimiento varchar(20)
								
							  )
							  ";
					$index1="CREATE INDEX cuenta_index USING BTREE ON gest2_temp (cuenta)";
					$index2="CREATE INDEX factura_index USING BTREE ON gest2_temp (factura)";
					mysql_query($tabla1) or die(mysql_error());
					mysql_query($index1) or die(mysql_error());
					mysql_query($index2) or die(mysql_error());
					
					$tabla2="CREATE TEMPORARY TABLE asig2_temp
							 (
								nomina varchar(15) NOT NULL,
								cuenta varchar(15) NOT NULL,
								movimiento varchar(20), 
								factura varchar(20),
								nivel varchar(50),
								grupo varchar(30)
							  )
							  ";
					$index3="CREATE INDEX cuenta_index USING BTREE ON asig2_temp (cuenta)";
					$index4="CREATE INDEX factura_index USING BTREE ON asig2_temp (factura)";
					mysql_query($tabla2) or die(mysql_error());
					mysql_query($index3) or die(mysql_error());
					mysql_query($index4) or die(mysql_error());
					
					
					$tabla4="CREATE TEMPORARY TABLE cuotas_temp
							 (
								agente varchar(15) NOT NULL,
								cuenta varchar(15) NOT NULL,
								ejercicio int,
								quincena int,
								movid varchar(15) NOT NULL,
								mov varchar(15) NOT NULL,
								mayor_vencimiento int,
								prevencion int,
								prestamos int,
								liquidacion int,
								contencion int,
								abono_cero int,
								gastos int,
								inactividad int,
								facts_problema int
							  )
							  ";
					$index7="CREATE INDEX cuenta_index USING BTREE ON cuotas_temp (cuenta)";
					$index8="CREATE INDEX movid_index USING BTREE ON cuotas_temp (movid)";
					mysql_query($tabla4) or die(mysql_error());
					mysql_query($index7) or die(mysql_error());
					mysql_query($index8) or die(mysql_error());					
					
					$tabla3="CREATE TEMPORARY TABLE asignacion_temp
							 (
							 	resultado varchar(15),
								vencimiento int,
								prevencion int,
								prestamos int,
								liquidacion int,
								contencion int,
								abono int,
								gastos int,
								inactividad int,
								problema int,
								total_cuota int,
								total_cuenta int
							  )
							  ";
					$index5="CREATE INDEX cuenta_index USING BTREE ON asignacion_temp (resultado)";
					mysql_query($tabla3) or die(mysql_error());
					mysql_query($index5) or die(mysql_error());
	
					 $sqlReg = "insert into gest2_temp 
								select * from(	
								select R.resultado,
										REPLACE(R.factura,' ','') as factura,
										CASE WHEN (R.intelisis IS NULL OR R.intelisis = 'SIN AGE' OR R.intelisis = ' ') THEN REPLACE(R.promotor,' ','') ELSE  REPLACE(R.intelisis,' ','') END as nomina,
										REPLACE(R.cuenta,' ','') as cuenta,
										REPLACE(R.mov,' ','') as mov
								from bitacora_gestioncobranza_intcob.registros R 
									where 1=1  and fecha_inicio BETWEEN '".$desde."' AND '".$hasta."'".$nivReg." and resultado <> ' ' ".$promoreg." 
								union all 
								select R.resultado,
										REPLACE(R.factura,' ','') as factura ,
										CASE WHEN (R.intelisis IS NULL OR R.intelisis = 'SIN AGE' OR R.intelisis = ' ') THEN REPLACE(R.promotor,' ','') ELSE  REPLACE(R.intelisis,' ','') END as nomina,
										REPLACE(R.cuenta,' ','') as cuenta,
										REPLACE(R.mov,' ','') as mov
								from bitacora_gestioncobranza_intelisis.registros R
									where 1=1   and fecha_inicio BETWEEN '".$desde."' AND '".$hasta."'".$nivReg." and resultado <> ' ' ".$promoreg."
								)as a";
                    //echo $sqlReg;
					mysql_query($sqlReg);
					//$registros = mysql_affected_rows();
					$sqlandroid="insert into asig2_temp  
							SELECT REPLACE(a.nomina,' ','') as nomina,
								   REPLACE(a.cuenta,' ','') as cuenta,
								   REPLACE(a.movimiento,' ','') as movimiento,
								   REPLACE(a.factura,' ','') as factura,
								   a.nivel,
								   b.grupo
							FROM android_reportes._cuentas_gestioncobranza a 
							inner join (SELECT distinct TRIM(substring_index(MID(es, 5, 100), '0', 2)) as grupo,nomina 
								FROM agentes where nomina=agente order by es)b on a.nomina=b.nomina 
					where periodo=".$periodo." and ejercicio=".$ejercicio.$nivcob.$grupoand.$promo;
					//echo $sqlandroid;
					mysql_query($sqlandroid);
					$registros = mysql_affected_rows();
					
					
					$sqlcuota = "insert into cuotas_temp 
								  SELECT    REPLACE(agente,' ','') as agente,
											REPLACE(cuenta,' ','') as cuenta,
											ejercicio,
											quincena,
											REPLACE(movid,' ','') as movid,
											REPLACE(mov,' ','') as mov,
											mayor_vencimiento,
											prevencion,
											prestamos,
											liquidacion,
											contencion,
											abono_cero,
											gastos,
											inactividad,
											facts_problema 
								  FROM cuotas_facturas 
								  where ejercicio=".$ejercicio." and quincena=".$periodo;
                    //echo $sqlcuota;
					mysql_query($sqlcuota);					
					
					
                    $tempasignacion ="insert into asignacion_temp 
    					SELECT c.nombre,
    						case when (a.vencimiento='' or a.vencimiento is null) then 0 else a.vencimiento end as Ven,
    						case when (a.prevencion='' or a.prevencion is null)   then 0 else a.prevencion end as prev,
    						case when (a.prestamos='' or prestamos is null)       then 0 else a.prestamos end as pres,
    						case when (a.liquidacion='' or a.liquidacion is null) then 0 else a.liquidacion end  as liqui,
    						case when (a.contencion='' or a.contencion is null)   then 0 else a.contencion end as cont,
    						case when (a.abono='' or a.abono is null)             then 0 else a.abono end as abonos,
    						case when (a.gastos='' or a.gastos is null)           then 0 else a.gastos end as gas,
    						case when (a.inactividad='' or a.inactividad is null) then 0 else a.inactividad end as ina,
    						case when (a.problema='' or a.problema is null)       then 0 else a.problema end as pro,
    						case when (a.total_cuota='' or a.total_cuota is null) then 0 else a.total_cuota end as total_cuota,
    						case when (a.total_cuenta='' or a.total_cuenta is null)then 0 else a.total_cuenta end as total_cuenta
    					FROM catalogo_resultado c
						left join (SELECT reg.resultado,
								sum(c.mayor_vencimiento) as vencimiento,
								sum(c.prevencion) as prevencion,
								sum(c.prestamos) as prestamos,
								sum(c.liquidacion) as liquidacion,
								sum(c.contencion) as contencion,
								sum(c.abono_cero) as abono,
								sum(c.gastos) as gastos,
								sum(c.inactividad) as inactividad,
								sum(c.facts_problema) as problema,
								(sum(c.mayor_vencimiento)+
								sum(c.prevencion)+
								sum(c.prestamos)+
								sum(c.liquidacion)+
								sum(c.contencion)+
								sum(c.abono_cero)+
								sum(c.gastos)+
								sum(c.inactividad)+
								sum(c.facts_problema)
								) as total_cuota,
								count(reg.cuenta
								) as total_cuenta
							FROM asig2_temp andr
							inner join gest2_temp reg on 
											reg.nomina=andr.nomina 
										and reg.cuenta=andr.cuenta 
										and andr.movimiento=reg.movimiento 
										and andr.factura=reg.factura
							inner join cuotas_temp c on  
											reg.nomina      = c.agente 
										and reg.cuenta      = c.cuenta
										and reg.movimiento  = c.mov
										and reg.factura     = c.movid
							group by reg.resultado ) a on a.resultado=c.id
							group by c.nombre
							ORDER BY c.orden
				    ";	
				   //echo $tempasignacion."<br>";
					mysql_query($tempasignacion) or die ("Porblemas al insertatar en la tabla asignacion_temp" .mysql_error());
					//$registros = mysql_affected_rows();
					//echo $registros ;
                    $fechas = array(
                            "inicio" => "".$desde,
                            "fin" => "".$hasta,
							"periodo" => "".$periodo,
							"ejercicio" => "".$ejercicio
                    );
					
					$FechaTitulo="select DATE_FORMAT(quincena_cobranza1(".$_POST['periodo'].",".$_POST['ejercicio']."),'%Y %b %e') as pd";
					$FechaTitulo2="select DATE_FORMAT(quincena_cobranza2(".$_POST['periodo'].",".$_POST['ejercicio']."),'%Y %b %e') as sd";
					
                    $encabezados = array("VENCIMIENTO", "PREVENCION","PRESTAMOS","LIQUIDACION","CONTENCION","ABONO","GASTOS","INACTIVIDAD","FACTURA PROBLEMA","TOTAL COUTA","TOTAL CUENTA");
					$TotalEncabezados ="Select 
											sum(c.vencimiento) as vencimineto,
											sum(c.prevencion) as prevencion,
											sum(c.prestamos) as prestamos,
											sum(c.liquidacion) as liquidacion,
											sum(c.contencion) as contencion,
											sum(c.abono) as abonos,
											sum(c.gastos) as gastos,
											sum(c.inactividad) as inactividad,
											sum(c.problema) as problema, 
											sum(total_cuota) cuota,
											sum(total_cuenta) cuenta_fact
										 from asignacion_temp c";
					$EncabezadosTotal = mysql_query($TotalEncabezados);
					$TotalEncabezado  = mysql_fetch_assoc($EncabezadosTotal);
					$Detalle="Select
									vencimiento,
									prevencion,
									prestamos,
									liquidacion,
									contencion,
									abono,
									gastos,
									inactividad,
									problema,
									total_cuota,
									total_cuenta
								 from asignacion_temp ";
					$Detallet=mysql_query($Detalle);
					$Detalleto=mysql_fetch_assoc($Detallet);
                    
                    $form = array(
                            "nivcob" => $_POST['nivcob'],
                            "nomina" => $_POST['nomina'],
                            "grupo"  => $_POST['grupo'],
                            "base"   => $config['base']
                    );

                    $this->tpl = "gestionescobranza/reporte.productividad.detalle.html";
                    $bloque = array(
                            "blkFecha"      => array($fechas),
                            "blkCuotaEnc"   => $encabezados,
                            "blkCuota"      => $Detalle,
							"blkCuotaTotal" => $TotalEncabezado,
                            "blkResulv"     => "SELECT * FROM catalogo_resultado ORDER BY orden",
                            "form"          => array($form),
							"fechaini"      => $FechaTitulo,
							"fechafin"      => $FechaTitulo2
                    );
            }
        return $bloque;
    }

    function _reporte_gest(){
            $this->tpl = "gestionescobranza/reporte.gestiones.html";
           /* $bloque = array(
                    "blkPost" => empty($_POST)?array():$_POST,
                    "blkGrupo" => "SELECT DISTINCT grupo AS id, grupo AS nombre FROM catalogo_promotores WHERE nivel_cobranza <> '' ORDER BY nombre",
                    "blkNivCob" => "SELECT DISTINCT nivel_cobranza AS id, nivel_cobranza AS nombre FROM catalogo_promotores WHERE nivel_cobranza <> '' ORDER BY nombre"
            );*/
			$bloque = array(
                    "blkPost" => empty($_POST)?array():$_POST,
                    "blkGrupo" => "SELECT distinct TRIM(substring_index(MID(es, 5, 100), '0', 2)) as nombre FROM agentes where nomina=agente order by es",
                    "blkNivCob" => "SELECT distinct TRIM(substring_index(MID(es, 5, 100), '0', 1)) as id FROM agentes where nomina=agente order by es"
            );
        
            if(!empty($_POST)){
                    /* $rq = mysql_query("SELECT * FROM config ORDER BY id DESC LIMIT 1");
                    $config = mysql_fetch_assoc($rq);

                    if($_POST['nivcob'] != 'TODOS'){
                            $nivcob = "AND etapa = '".$_POST['nivcob']."' ";
                    }
                    if($_POST['nomina'] != ''){
                            //$nomina = "AND (promotor = '".$_POST['nomina']."' OR intelisis = '".$_POST['nomina']."') ";
							$nomina = "AND  intelisis = '".$_POST['nomina']."' ";
                    }
                    if($_POST['grupo'] != ''){
                            $grupo  = "AND grupo = '".$_POST['grupo']."' ";
                            //$grupo  = "AND agente IN (SELECT nom_intelisis FROM catalogo_promotores WHERE grupo = '".$_POST['grupo']."') ";
                    }*/
					$periodo        = $_POST['quincena'];
					$ejercicio      = $_POST['ejercicio'];
                    $niv_cob        = $_POST['nivcob'];
                    $promotor       = $_POST['nomina'];
                    $grupo          = $_POST['grupo'];
            
					if($niv_cob != 'TODOS'){
							$nivcob = " and (nivel='".$_POST['nivcob']."' OR nivel in (SELECT niveles FROM relacion_niveles_especiales WHERE consolidado = '".$_POST['nivcob']."' ))";
							$nivReg = " and (etapa='".$_POST['nivcob']."' OR etapa in (SELECT niveles FROM relacion_niveles_especiales WHERE consolidado = '".$_POST['nivcob']."' ))";
                    }
                    if($grupo != ''){
							$grupoand =" and b.grupo='".$_POST['grupo']."'";
                    }
                    
                    if($promotor != ""){
                            $promo    = " AND  a.nomina = '".$promotor."' ";
							$promoad    = " AND  ad.agente_asociado = '".$promotor."' ";
							$promoreg = " and (CASE
											WHEN (R.intelisis IS NULL OR R.intelisis = 'SIN AGE' OR R.intelisis = ' ') THEN REPLACE(R.promotor, ' ', '')
											ELSE REPLACE(R.intelisis, ' ', '')
										END)='".$promotor."'";
                    }
                    $CambiarIdioma="SET lc_time_names = 'es_MX'";
					mysql_query($CambiarIdioma);
					$fecha1="select quincena_cobranza1(".$periodo.",".$ejercicio.") as fecha";
					$res=mysql_fetch_assoc(mysql_query($fecha1));
					$desde=$res['fecha'];
					$fecha2="select quincena_cobranza2(".$periodo.",".$ejercicio.") as fecha";
					$res=mysql_fetch_assoc(mysql_query($fecha2));
					$hasta=$res['fecha'];
					
					$tabla1="CREATE TEMPORARY TABLE gest2_temp
							 (
								resultado varchar(20) NOT NULL,
								nomina varchar(15) NOT NULL,
								cuenta varchar(15), 
								movimiento varchar(20),
								factura varchar(15),                          
                                fecha_inicio date,
                                valor_resultado varchar(15)
							  )
							  ";
					$tabla2="CREATE TEMPORARY TABLE asig2_temp
							 (
								nomina varchar(15) NOT NULL,
								cuenta varchar(15) NOT NULL,
								movimiento varchar(20), 
								factura varchar(20),
								nivel varchar(50),
								grupo varchar(30)
							  )
							  ";
					$tabla3="CREATE TEMPORARY TABLE reporte_temp
							 (
								cuenta varchar(15) NOT NULL,
								factura varchar(20),
								resultado varchar(20) NOT NULL,
                                valor_resultado varchar(15) NOT NULL,
								fecha_inicio date
							  )
							  ";
					$index1="CREATE INDEX cuenta_index USING BTREE ON gest2_temp (cuenta)";
					$index2="CREATE INDEX factura_index USING BTREE ON gest2_temp (factura)";
					$index3="CREATE INDEX cuenta_index USING BTREE ON asig2_temp (cuenta)";
					$index4="CREATE INDEX factura_index USING BTREE ON asig2_temp (factura)";
					$index5="CREATE INDEX cuenta_index USING BTREE ON reporte_temp (cuenta)";
					$index6="CREATE INDEX factura_index USING BTREE ON reporte_temp (factura)";
					mysql_query($tabla1) or die(mysql_error());
					mysql_query($index1) or die(mysql_error());
					mysql_query($index2) or die(mysql_error());
					mysql_query($tabla2) or die(mysql_error());
					mysql_query($index3) or die(mysql_error());
					mysql_query($index4) or die(mysql_error());
					mysql_query($tabla3) or die(mysql_error());
					mysql_query($index5) or die(mysql_error());
					mysql_query($index6) or die(mysql_error());
                    $sqlReg = "insert into gest2_temp 
								select * from(	
								select R.resultado,
										CASE WHEN (R.intelisis IS NULL OR R.intelisis = 'SIN AGE' OR R.intelisis = ' ') THEN REPLACE(R.promotor,' ','') ELSE  REPLACE(R.intelisis,' ','') END as nomina,
										REPLACE(R.cuenta,' ','') as cuenta,
										REPLACE(R.mov,' ','') as mov,
										REPLACE(R.factura,' ','') as factura,
										R.valor_resultado,
										R.fecha_inicio 
								from bitacora_gestioncobranza_intcob.registros R 
									where 1=1  and fecha_inicio BETWEEN '".$desde."' AND '".$hasta."'".$nivReg." and resultado <> ' ' ".$promoreg." 
								union all 
								select R.resultado,
										CASE WHEN (R.intelisis IS NULL OR R.intelisis = 'SIN AGE' OR R.intelisis = ' ') THEN REPLACE(R.promotor,' ','') ELSE  REPLACE(R.intelisis,' ','') END as nomina,
										REPLACE(R.cuenta,' ','') as cuenta,
										REPLACE(R.mov,' ','') as mov,
										REPLACE(R.factura,' ','') as factura,
										R.valor_resultado,
										R.fecha_inicio 
								from bitacora_gestioncobranza_intelisis.registros R
									where 1=1   and fecha_inicio BETWEEN '".$desde."' AND '".$hasta."'".$nivReg." and resultado <> ' ' ".$promoreg."
								)as a";
                    //echo $sqlReg ."<br/>";
					mysql_query($sqlReg) or die ("Problema al llenar la tabla de gest2_temp...".mysql_error());;
					//$regis = mysql_affected_rows();
					//echo $regis."<br/>";
					$sqlandroid="insert into asig2_temp  
						SELECT REPLACE(a.nomina,' ','') as nomina,
							   REPLACE(a.cuenta,' ','') as cuenta,
							   REPLACE(a.movimiento,' ','') as movimiento,
							   REPLACE(a.factura,' ','') as factura,
							   a.nivel,
							   b.grupo
						FROM android_reportes._cuentas_gestioncobranza a 
						inner join (SELECT distinct TRIM(substring_index(MID(es, 5, 100), '0', 2)) as grupo,nomina 
							FROM bitacora_gestioncobranza_intelisis.agentes where nomina=agente order by es)b on a.nomina=b.nomina 
						where periodo=".$periodo." and ejercicio=".$ejercicio.$nivcob.$grupoand.$promo."
						union all
						SELECT REPLACE(ad.agente_asociado,' ','') as nomina, 
							REPLACE(a.cuenta,' ','') cuenta, 
							REPLACE(a.movimiento,' ','') as movimiento, 
							REPLACE(a.factura,' ','') as factura, 
							a.nivel,
							b.grupo
							FROM (
							select agente_origen, agente_asociado from bitacora_gestioncobranza_intelisis.agente_doble
							where fecha_asigna between '".$desde."' and '".$hasta."'
							group by agente_origen, agente_asociado

						) as ad
						left join android_reportes._cuentas_gestioncobranza a on a.nomina=ad.agente_origen
						inner join (SELECT distinct TRIM(substring_index(MID(es, 5, 100), '0', 2)) as grupo,nomina 
								FROM agentes where nomina=agente order by es)b on a.nomina=b.nomina
						where a.periodo=".$periodo." and a.ejercicio=".$ejercicio.$nivcob.$grupoand.$promoad;
					//echo $sqlandroid;
					mysql_query($sqlandroid) or die ("Problema al llenar la tabla de asig2_temp...".mysql_error());
					$registros = mysql_affected_rows();
					//echo $registros. "<br/>";
                    $sql = "insert into  reporte_temp 
								Select
									reg.cuenta,
									reg.factura,
									reg.resultado,
									reg.valor_resultado,
									reg.fecha_inicio
								FROM asig2_temp andr
								inner join gest2_temp reg on 
										reg.nomina=andr.nomina 
									and reg.cuenta=andr.cuenta 
									and andr.movimiento=reg.movimiento 
									and andr.factura=reg.factura
                        ";
                    //echo $sql;
                    mysql_query($sql) or die ("Error al insertar en la tabla de reporte_temp...".mysql_error());
                    $visitas = mysql_affected_rows();
					//echo $visitas;
                    $sql = "SELECT c.orden, COUNT(*) as val FROM reporte_temp r inner join catalogo_resultado c ON c.id = r.resultado GROUP BY resultado order by c.orden";
                    $rq = mysql_query($sql);
                    $num = mysql_num_rows($rq);
					$sqlcuentas=" select distinct cuenta from asig2_temp ";
					mysql_query($sqlcuentas);
                    $Cuenta = mysql_affected_rows();
					
                    $form = array(
                        "nivcob"    => $_POST['nivcob'],
                        "nomina"    => $_POST['nomina'],
                        "grupo"     => $_POST['grupo'],
                        "regist"    => $visitas,
                        "base"      => $config['base'],
						"asignadas" => $registros,
						"cuenta"    => $Cuenta
                    );
                    $fechas = array("periodo"   => "".$periodo,
									"ejercicio" => "".$ejercicio,
									"inicio"    => "".$desde,
									"fin"       => "".$hasta);
					$FechaTitulo="select DATE_FORMAT(quincena_cobranza1(".$_POST['quincena'].",".$_POST['ejercicio']."),'%Y %b %e') as pd";
					$FechaTitulo2="select DATE_FORMAT(quincena_cobranza2(".$_POST['quincena'].",".$_POST['ejercicio']."),'%Y %b %e') as sd";
                    $totales = array();
                    $porcent = array();
                    $numcol = mysql_num_rows(mysql_query("SELECT * FROM catalogo_resultado"));
                    for($i=0,$j=0;$j<$numcol;$j++){
                            if($num>0 && $i<$num) $orden = mysql_result($rq, $i, 'orden');
                            else $orden = -99;
                            if($orden-1 == $j){
                                    $hist[$j] = mysql_result($rq, $i, 'val');
                                    $porc[$j] = $hist[$j] / $visitas;
                                    $i++;
                            } else {
                                    $hist[$j] = 0;
                                    $porc[$j] = 0;
                            }
                    }
                   // mysql_query("DROP TEMPORARY TABLE IF EXISTS reporte_temp");
                    $this->tpl = "gestionescobranza/reporte.gestiones.detalle.html";
                    $bloque = array(
                            "blkFecha"    => array($fechas),
                            "blkResulv"   => "SELECT * FROM catalogo_resultado ORDER BY orden",
                            "blkHist"     => $hist,
                            "form"        => array($form),
							"blkFechaini" => $FechaTitulo,
							"blkFechafin" => $FechaTitulo2
                    );
            }
            return $bloque;
    }

    function _reporte_capt(){
            $this->tpl = "gestionescobranza/reporte.capturista.html";
            $bloque = array(
                    "blkPost" => empty($_POST)?array():$_POST,
            );


            if(!empty($_POST)){
                    $rq = mysql_query("SELECT * FROM config ORDER BY id DESC LIMIT 1");
                    $config = mysql_fetch_assoc($rq);
            
                    $fecha_inicio   = $_POST['fecha_inicio'];
                    $fecha_fin      = $_POST['fecha_fin'];
                    $hora_inicio    = $_POST['hora_inicio'];
                    $hora_fin       = $_POST['hora_fin'];
                    if($fecha_inicio != ''){
                            $fecha = "AND fecha_captura BETWEEN '$fecha_inicio' AND '$fecha_fin' ";
                    }
                    if(isset ($_POST['check_horas'])){
                            $hora = "AND hora_captura BETWEEN '$hora_inicio:00' AND '$hora_fin:00' ";
                    }
                    $sql = "CREATE TEMPORARY TABLE IF NOT EXISTS reporte_temp AS
                                Select
                                    capturista,
                                    cuenta,
                                    factura,
                                    fecha_captura,
                                    hora_captura,
                                    hora_captura_final,
                                    TIMEDIFF(hora_captura_final, hora_captura) AS tiempo_captura
                                FROM registros
                                WHERE capturista IS NOT NULL ".$fecha.$hora;
                    mysql_query($sql);
                    $registros = mysql_affected_rows();
			
                    $extra = "SELECT
                                 sec_to_time(SUM(tiempo_captura)) as tiempo_total,
                                 sec_to_time(AVG(tiempo_captura)) as promedio,
                                 MIN(tiempo_captura) as minimo,
                                 MAX(tiempo_captura) as maximo
                             FROM reporte_temp";
                    $sql = "SELECT
                                capturista,
                                COUNT(*) AS registros,
                                sec_to_time(SUM(tiempo_captura)) as tiempo_total,
                                sec_to_time(AVG(tiempo_captura)) as promedio,
                                MIN(tiempo_captura) as minimo,
                                MAX(tiempo_captura) as maximo
                            FROM reporte_temp GROUP BY capturista";
            
                    $form = array(
                            "fecha_inicio"  => $fecha_inicio,
                            "fecha_fin"     => $fecha_fin,
                            "hora_inicio"   => $hora_inicio,
                            "hora_fin"      => $hora_fin,
                            "base"          => $config['base']
                    );
            
                    $this->tpl = "gestionescobranza/reporte.capturista.detalle.html";
                    $bloque = array(
                            "total"         => $extra,
                            "registros"     => array($registros),
                            "form"          => array($form),
                            "blkReg"        => $sql
                    );
                    //print_r($bloque);

            }
            return $bloque;
    }

    function _preguntas(){
            $this->tpl="gestionescobranza/preguntas_seguridad.html";
		
            $f_nomina = $_POST['nomina'];
            $f_cliente = $_POST['cliente'];
            $f_fecha_1 = $_POST['fecha1'];
            $f_fecha_2 = $_POST['fecha2'];
			
			$limit=' ';
			
			if(empty($_POST))
			{
			$limit=' Limit 0';
			//echo 'uno';
			}
			else
			{
			$limit=' ';
			//echo 'dos';
			}
			
			
            if($f_nomina != ''){
                    $where .= "AND Nomina_Agente LIKE '%".$f_nomina."%' ";
            }else{
                     $where.='';
            }
            if($f_cliente != ''){
                    $where .= "AND Cuenta_Cliente LIKE '%".$f_cliente."%' ";
            }else{
                     $where.='';
            }
            if($f_fecha_1 != '' && $f_fecha_2 !=''){
                    $where .= "AND fecha BETWEEN '".$f_fecha_1."' AND '".$f_fecha_2."' ";
            }else{
                     $where.='';
            }
		
            $sql1="SELECT
                        id,
                        Nomina_Agente AS nomina,
                        Nombre_Agente AS nombrea,
                        Cuenta_Cliente AS cuenta,
                        Nombre_Cliente AS nombrec,
                        Domicilio_Cliente AS domicilio,
                        Colonia_Cliente AS colonia,
                        Poblacion_Cliente AS poblacion,
                        Pregunta as pregunta,
                        Respuesta as respuesta,
                        Fecha as fecha
                    FROM android_reportes._reporte_preguntas WHERE 1 ".$where.$limit;
            //echo $sql1;
            $bloq = array(
                    "post" => empty($_POST)?array():array($_POST),
                    "blk1" => $sql1
            );
            //print_r($bloq);
            return $bloq;
    }

    function _reportenegativa(){

            $rq = mysql_query("SELECT * FROM config ORDER BY id DESC LIMIT 1");
            $config = mysql_fetch_assoc($rq);
            $nDataBase = $config['intelisis'];
            if($nDataBase == 1){
                    $strDataBase = "bitacora_gestioncobranza_intelisis";
                    $nDataBase = 0;
            }else{
                    $strDataBase = "bitacora_gestioncobranza_intcob";
                    $nDataBase = 1;
            }
			
			
            //echo $ndatabase;
            $strWhere = "";
            $arrayObject = array();
            $arrayObject2 = array();
            $arrayHistory = array();
            $arrayAux = array();
            $arrayGestor = array();
			
			
			
			/*
			if($_POST["fecha1"]!="" && $_POST["fecha2"]!="" && $_POST["nomina"]!="")
			{
				
				$ag=mysql_query("select distinct(agente_asociado) from agente_doble where agente_asociado='".$_POST['nomina']."' 
				and (fecha_asigna >= '".$_POST["fecha1"]."' or fecha_desasigna <= '".$_POST["fecha2"]."')");	
				
				echo "select distinct(agente_asociado) from agente_doble where agente_asociado='".$_POST['nomina']."' 
				and (fecha_asigna >= '".$_POST["fecha1"]."' or fecha_desasigna <= '".$_POST["fecha2"]."')";
									
				 if(mysql_num_rows($ag) == 1){
					 
						$this->msgaviso = "Reporte no diponible para este agente debido a que es suplente.";
						
						mysql_free_result($ag);
						
						return $bloq;
				}
				
				
				
			}
			*/
			
			

            if(isset($_GET['reportenega'])){
                    $strWhere = $strWhere . ($_GET["reportenega"]!="" ? " and registros.Cuenta='".$_GET["reportenega"]."' " : " ");
                    /*$strQuery1 ="select
                                    registros.*,
                                    emp.nombre as Agente
                                 from
                                     ("."(select
                                          reg.intelisis as Nomina,
                                          reg.cuenta as Cuenta,
                                          '' as NegativaPor,
                                          reg.valor_resultado as TipoNegativa,
                                          reg.nombre as NombreCliente,
                                          reg.domicilio as DomicilioCliente,
                                          reg.colonia as ColoniaCliente,
                                          reg.entre as CruceCliente,
                                          reg.ciudad as PoblacionCliente,
                                          reg.aval as NombreAval,
                                          reg.aval_domicilio as DomicilioAval,
                                          reg.aval_colonia as ColoniaAval,
                                          '' as CruceAval,
                                          reg.aval_ciudad as PoblacionAval,

                                          reg.factura as Factura,
                                          reg.articulo as Articulo,
                                          '' as Condicion,
                                          ROUND(reg.importe, 3) as Importe,
                                          ROUND(reg.saldocapital, 3) as SaldoCapital,
                                          '0' as IM,
                                          ROUND(reg.abonos, 3) as Abonos,
                                          ROUND(reg.saldovencido, 3) as SaldoVencido,

                                          ROUND(reg.saldo, 3) as SaldoTotal,
                                          reg.fecha_inicio as FechaUltimo,
                                          reg.diasvencidos as DV,
                                          reg.diasinactividad as DI,
                                          reg.fecha_inicio as Fecha,
                                          reg.hora_inicio as Hora,

                                          reg.resultado as Resultado,
                                          '0' as MAVI
                                        from
                                          ".$strDataBase.".registros reg
                                        where capturista <> 'Android'
                                        and (valor_resultado = 'NEGATIVA DE PAGO EXPRESA (MANIFESTADA)' or
                                          valor_resultado = 'AGRESION FISICA' or
                                          valor_resultado = 'CTE Y/O AVAL SE ESCONDE O LO NIEGAN' or
                                          valor_resultado = 'CTE Y/O AVAL NIEGA IDENTIDAD' or
                                          valor_resultado = 'CTE Y/O AVAL ASESORADO POR UN ABOGADO' or
                                          valor_resultado = 'CTE SOLICITA FUERZA PÚBLICA'))
                                         union all"."
                                         (select
                                          reg.agente as Nomina,
                                          reg.cta as Cuenta,
                                          com.valor_resultado as NegativaPor,
                                          com.comentario as TipoNegativa,
                                          reg.nombre as NombreCliente,
                                          reg.domicilio as DomicilioCliente,
                                          reg.colonia as ColoniaCliente,
                                          reg.entrecalles as CruceCliente,
                                          reg.poblacion as PoblacionCliente,
                                          reg.nombre_aval as NombreAval,
                                          reg.domicilio_aval as DomicilioAval,
                                          reg.colonia_aval as ColoniaAval,
                                          '' as CruceAval,
                                          reg.delegacion_aval as PoblacionAval,

                                          reg.movimiento as Factura,
                                          reg.articulo as Articulo,
                                          '' as Condicion,
                                          ROUND(reg.importe, 3) as Importe,
                                          ROUND(his.saldocap, 3) as SaldoCapital,
                                          '0' as IM,
                                          ROUND(reg.abonos, 3) as Abonos,
                                          ROUND(reg.saldov, 3) as SaldoVencido,
                                          ROUND(reg.saldototal, 3) as SaldoTotal,
                                          reg.fechaultimopago as FechaUltimo,
                                          reg.dv as DV,
                                          reg.di as DI,
                                          com.fecha_inicio as Fecha,
                                          com.hora_inicio as Hora,

                                          com.resultado as Resultado,
                                          com.mavicob as MAVI
                                        from
                                          android_reportes.historial_cuentas reg
                                          inner join android_reportes.comentarios_cob com on com.idcomentario = reg.IDCOMENTARIO
                                          inner join android_reportes.cob_negativahistoria his on his.IDCOMENTARIO = com.idcomentario)
                                         union all
                                        (select
                                          com.promotor as Nomina,
                                          com.cuenta as Cuenta,
                                          com.valor_resultado as NegativaPor,
                                          com.comentario as TipoNegativa,
                                          his.nombrecte as NombreCliente,
                                          his.domiciliocte as DomicilioCliente,
                                          his.coloniacte as ColoniaCliente,
                                          his.crucescte as CruceCliente,
                                          his.poblacioncte as PoblacionCliente,
                                          his.nombreavl as NombreAval,
                                          his.domicilioavl as DomicilioAval,
                                          his.coloniaavl as ColoniaAval,
                                          '' as CruceAval,
                                          his.poblacionavl as PoblacionAval,

                                          com.factura as Factura,
                                          his.articulo as Articulo,
                                          his.condicion as Condicion,
                                          ROUND(his.importe, 3) as Importe,
                                          ROUND(his.saldocap, 3) as SaldoCapital,
                                          his.IM as IM,
                                          ROUND(his.abonos, 3) as Abonos,
                                          ROUND(his.saldoven, 3) as SaldoVencido,
                                          ROUND(his.saldotot, 3) as SaldoTotal,
                                          his.fechultabn as FechaUltimo,
                                          his.dv as DV,
                                          his.di as DI,
                                          com.fecha_inicio as Fecha,
                                          com.hora_inicio as Hora,

                                          com.resultado as Resultado,
                                          com.mavicob as MAVI
                                        from
                                          android_reportes.comentarios_cob com
                                          inner join android_reportes.cob_negativahistoria his on his.IDCOMENTARIO = com.idcomentario)
                                          ) registros
                                          left join varios.empleados_rh201_intelisis emp on emp.Clave = registros.Nomina
                                          where 1
                                          and registros.MAVI = ".$nDataBase." " . $strWhere . "
                                          order by registros.Fecha desc,registros.Hora desc";
													  
                    */					
					$strQuery1 ="select
                                registros.*,
                                emp.nombre as Agente
                                from
							    ("."(select
								  reg.intelisis as Nomina,
								  reg.cuenta as Cuenta,
								  '' as NegativaPor,
								  reg.valor_resultado as TipoNegativa,
								  reg.nombre as NombreCliente,
								  reg.domicilio as DomicilioCliente,
								  reg.colonia as ColoniaCliente,
								  reg.entre as CruceCliente,
								  reg.ciudad as PoblacionCliente,
								  reg.aval as NombreAval,
								  reg.aval_domicilio as DomicilioAval,
								  reg.aval_colonia as ColoniaAval,
								  '' as CruceAval,
								  reg.aval_ciudad as PoblacionAval,

								  reg.factura as Factura,
								  reg.articulo as Articulo,
								  '' as Condicion,
								  ROUND(reg.importe, 3) as Importe,
								  ROUND(reg.saldocapital, 3) as SaldoCapital,
								  '0' as IM,
								  ROUND(reg.abonos, 3) as Abonos,
								  ROUND(reg.saldovencido, 3) as SaldoVencido,

								  ROUND(reg.saldo, 3) as SaldoTotal,
								  reg.fecha_inicio as FechaUltimo,
								  reg.diasvencidos as DV,
								  reg.diasinactividad as DI,
								  reg.fecha_inicio as Fecha,
								  reg.hora_inicio as Hora,

								  reg.resultado as Resultado,
								  '0' as MAVI
								from
								  bitacora_gestioncobranza_intelisis.registros reg
								where capturista <> 'Android'
								and (valor_resultado = 'NEGATIVA DE PAGO EXPRESA (MANIFESTADA)' or
								  valor_resultado = 'AGRESION FISICA' or
								  valor_resultado = 'CTE Y/O AVAL SE ESCONDE O LO NIEGAN' or
								  valor_resultado = 'CTE Y/O AVAL NIEGA IDENTIDAD' or
								  valor_resultado = 'CTE Y/O AVAL ASESORADO POR UN ABOGADO' or
								  valor_resultado = 'CTE SOLICITA FUERZA PÚBLICA'))
								 union all"."
								 (select
								  reg.agente as Nomina,
								  reg.cta as Cuenta,
								  com.valor_resultado as NegativaPor,
								  com.comentario as TipoNegativa,
								  reg.nombre as NombreCliente,
								  reg.domicilio as DomicilioCliente,
								  reg.colonia as ColoniaCliente,
								  reg.entrecalles as CruceCliente,
								  reg.poblacion as PoblacionCliente,
								  reg.nombre_aval as NombreAval,
								  reg.domicilio_aval as DomicilioAval,
								  reg.colonia_aval as ColoniaAval,
								  '' as CruceAval,
								  reg.delegacion_aval as PoblacionAval,

								  reg.movimiento as Factura,
								  reg.articulo as Articulo,
								  '' as Condicion,
								  ROUND(reg.importe, 3) as Importe,
								  ROUND(his.saldocap, 3) as SaldoCapital,
								  '0' as IM,
								  ROUND(reg.abonos, 3) as Abonos,
								  ROUND(reg.saldov, 3) as SaldoVencido,
								  ROUND(reg.saldototal, 3) as SaldoTotal,
								  reg.fechaultimopago as FechaUltimo,
								  reg.dv as DV,
								  reg.di as DI,
								  com.fecha_inicio as Fecha,
								  com.hora_inicio as Hora,

								  com.resultado as Resultado,
								  com.mavicob as MAVI
								from
								  android_reportes.historial_cuentas reg
								  inner join android_reportes.comentarios_cob com on com.idcomentario = reg.IDCOMENTARIO
								  inner join android_reportes.cob_negativahistoria his on his.IDCOMENTARIO = com.idcomentario)
								 union all
								(select
								  com.promotor as Nomina,
								  com.cuenta as Cuenta,
								  com.valor_resultado as NegativaPor,
								  com.comentario as TipoNegativa,
								  his.nombrecte as NombreCliente,
								  his.domiciliocte as DomicilioCliente,
								  his.coloniacte as ColoniaCliente,
								  his.crucescte as CruceCliente,
								  his.poblacioncte as PoblacionCliente,
								  his.nombreavl as NombreAval,
								  his.domicilioavl as DomicilioAval,
								  his.coloniaavl as ColoniaAval,
								  '' as CruceAval,
								  his.poblacionavl as PoblacionAval,

								  com.factura as Factura,
								  his.articulo as Articulo,
								  his.condicion as Condicion,
								  ROUND(his.importe, 3) as Importe,
								  ROUND(his.saldocap, 3) as SaldoCapital,
								  his.IM as IM,
								  ROUND(his.abonos, 3) as Abonos,
								  ROUND(his.saldoven, 3) as SaldoVencido,
								  ROUND(his.saldotot, 3) as SaldoTotal,
								  his.fechultabn as FechaUltimo,
								  his.dv as DV,
								  his.di as DI,
								  com.fecha_inicio as Fecha,
								  com.hora_inicio as Hora,

								  com.resultado as Resultado,
								  com.mavicob as MAVI
								from
								  android_reportes.comentarios_cob com
								  inner join android_reportes.cob_negativahistoria his on his.IDCOMENTARIO = com.idcomentario)
								  ) registros
								  left join varios.empleados_rh201_intelisis emp on emp.Clave = registros.Nomina
								  where 1
								  and registros.MAVI = 0 " . $strWhere . "
								  
								  UNION ALL
								  
								  select
                                    registros.*,
                                    emp.nombre as Agente
                                 from
							 ("."(select
								  reg.intelisis as Nomina,
								  reg.cuenta as Cuenta,
								  '' as NegativaPor,
								  reg.valor_resultado as TipoNegativa,
								  reg.nombre as NombreCliente,
								  reg.domicilio as DomicilioCliente,
								  reg.colonia as ColoniaCliente,
								  reg.entre as CruceCliente,
								  reg.ciudad as PoblacionCliente,
								  reg.aval as NombreAval,
								  reg.aval_domicilio as DomicilioAval,
								  reg.aval_colonia as ColoniaAval,
								  '' as CruceAval,
								  reg.aval_ciudad as PoblacionAval,

								  reg.factura as Factura,
								  reg.articulo as Articulo,
								  '' as Condicion,
								  ROUND(reg.importe, 3) as Importe,
								  ROUND(reg.saldocapital, 3) as SaldoCapital,
								  '0' as IM,
								  ROUND(reg.abonos, 3) as Abonos,
								  ROUND(reg.saldovencido, 3) as SaldoVencido,

								  ROUND(reg.saldo, 3) as SaldoTotal,
								  reg.fecha_inicio as FechaUltimo,
								  reg.diasvencidos as DV,
								  reg.diasinactividad as DI,
								  reg.fecha_inicio as Fecha,
								  reg.hora_inicio as Hora,

								  reg.resultado as Resultado,
								  '0' as MAVI
								from
								  bitacora_gestioncobranza_intcob.registros reg
								where capturista <> 'Android'
								and (valor_resultado = 'NEGATIVA DE PAGO EXPRESA (MANIFESTADA)' or
								  valor_resultado = 'AGRESION FISICA' or
								  valor_resultado = 'CTE Y/O AVAL SE ESCONDE O LO NIEGAN' or
								  valor_resultado = 'CTE Y/O AVAL NIEGA IDENTIDAD' or
								  valor_resultado = 'CTE Y/O AVAL ASESORADO POR UN ABOGADO' or
								  valor_resultado = 'CTE SOLICITA FUERZA PÚBLICA'))
								 union all"."
								 (select
								  reg.agente as Nomina,
								  reg.cta as Cuenta,
								  com.valor_resultado as NegativaPor,
								  com.comentario as TipoNegativa,
								  reg.nombre as NombreCliente,
								  reg.domicilio as DomicilioCliente,
								  reg.colonia as ColoniaCliente,
								  reg.entrecalles as CruceCliente,
								  reg.poblacion as PoblacionCliente,
								  reg.nombre_aval as NombreAval,
								  reg.domicilio_aval as DomicilioAval,
								  reg.colonia_aval as ColoniaAval,
								  '' as CruceAval,
								  reg.delegacion_aval as PoblacionAval,

								  reg.movimiento as Factura,
								  reg.articulo as Articulo,
								  '' as Condicion,
								  ROUND(reg.importe, 3) as Importe,
								  ROUND(his.saldocap, 3) as SaldoCapital,
								  '0' as IM,
								  ROUND(reg.abonos, 3) as Abonos,
								  ROUND(reg.saldov, 3) as SaldoVencido,
								  ROUND(reg.saldototal, 3) as SaldoTotal,
								  reg.fechaultimopago as FechaUltimo,
								  reg.dv as DV,
								  reg.di as DI,
								  com.fecha_inicio as Fecha,
								  com.hora_inicio as Hora,

								  com.resultado as Resultado,
								  com.mavicob as MAVI
								from
								  android_reportes.historial_cuentas reg
								  inner join android_reportes.comentarios_cob com on com.idcomentario = reg.IDCOMENTARIO
								  inner join android_reportes.cob_negativahistoria his on his.IDCOMENTARIO = com.idcomentario)
								 union all
								(select
								  com.promotor as Nomina,
								  com.cuenta as Cuenta,
								  com.valor_resultado as NegativaPor,
								  com.comentario as TipoNegativa,
								  his.nombrecte as NombreCliente,
								  his.domiciliocte as DomicilioCliente,
								  his.coloniacte as ColoniaCliente,
								  his.crucescte as CruceCliente,
								  his.poblacioncte as PoblacionCliente,
								  his.nombreavl as NombreAval,
								  his.domicilioavl as DomicilioAval,
								  his.coloniaavl as ColoniaAval,
								  '' as CruceAval,
								  his.poblacionavl as PoblacionAval,

								  com.factura as Factura,
								  his.articulo as Articulo,
								  his.condicion as Condicion,
								  ROUND(his.importe, 3) as Importe,
								  ROUND(his.saldocap, 3) as SaldoCapital,
								  his.IM as IM,
								  ROUND(his.abonos, 3) as Abonos,
								  ROUND(his.saldoven, 3) as SaldoVencido,
								  ROUND(his.saldotot, 3) as SaldoTotal,
								  his.fechultabn as FechaUltimo,
								  his.dv as DV,
								  his.di as DI,
								  com.fecha_inicio as Fecha,
								  com.hora_inicio as Hora,

								  com.resultado as Resultado,
								  com.mavicob as MAVI
								from
								  android_reportes.comentarios_cob com
								  inner join android_reportes.cob_negativahistoria his on his.IDCOMENTARIO = com.idcomentario)
								  ) registros
								  left join varios.empleados_rh201_intelisis emp on emp.Clave = registros.Nomina
								  where 1
								  and registros.MAVI = 1 " . $strWhere . "
								  order by Fecha desc,Hora desc"
								  
								  ;
					//echo $strQuery1;
                    $answer = mysql_query($strQuery1) or die(mysql_error());
                    $strTables = "";
                    $strInner = "";
                    $strDisplay1 = "none";
                    $strDisplay2 = "none";
                    $strDisplay3 = "inline";
                    $strDisplay4 = "inline";
                    $strDisplay5 = "none";

                    $strComentario = "";
                    while($row = mysql_fetch_assoc($answer)){
                            $arrayObject[] = $row;
                    }
                    switch($_GET['idnega2']){
                        case "NEGATIVA DE PAGO EXPRESA (MANIFESTADA)":
                            $strTables = " ,fin.TIPOFINCA,fin.COCHERA,fin.NIVELES,fin.COLOR,fin.CANCEL
                                           ,sol.ARTICULO,sol.CANTIDAD,sol.ESTADO,sol.COMENTARIO
                                           ,qui.QUIEN,qui.NOMBRE
                                           ,des.SEXO,des.ESTATURA,des.COMPLEXION,des.COLOR_PIEL,des.CABELLO,des.COLOR_CABELLO ";
                            $strTables2 =" ,fin.tipo as TIPOFINCA,fin.COCHERA,fin.NIVELES,fin.COLOR,fin.CANCEL
                                           ,sol.ARTICULO,sol.CANTIDAD,sol.ESTADO, '' as COMENTARIO
                                           ,qui.QUIEN,qui.valor_quien as NOMBRE
                                           ,des.SEXO,des.ESTATURA,des.COMPLEXION,des.COLORPIEL as COLOR_PIEL,des.CABELLO,des.COLORCABELLO as COLOR_CABELLO ";
                            $strInner ="left join android_reportes.cob_negativafinca fin on fin.IDCOMENTARIO = com.idcomentario
                                        left join android_reportes.cob_negativasolvencia sol on sol.IDCOMENTARIO = com.idcomentario
                                        left join android_reportes.cob_negativaquien qui on qui.IDCOMENTARIO = com.idcomentario
                                        left join android_reportes.cob_negativadescripcion des on des.IDCOMENTARIO = com.idcomentario";
                            $strInner2 ="inner join bitacora_gestioncobranza_intelisis.tipo_finca fin on fin.id_registro = com.id
                                         left join bitacora_gestioncobranza_intelisis.solvencia sol on sol.id_registro = com.id
                                         left join bitacora_gestioncobranza_intelisis.quien qui on qui.id_registro = com.id
                                         left join bitacora_gestioncobranza_intelisis.descripcion_fisica des on des.id_registro = com.id";
										 
						  $strInner2_ ="inner join bitacora_gestioncobranza_intcob.tipo_finca fin on fin.id_registro = com.id
										 left join bitacora_gestioncobranza_intcob.solvencia sol on sol.id_registro = com.id
										 left join bitacora_gestioncobranza_intcob.quien qui on qui.id_registro = com.id
										 left join bitacora_gestioncobranza_intcob.descripcion_fisica des on des.id_registro = com.id";
                            break;
                        case "AGRESION FISICA":
                            $strTables = " ,fin.TIPOFINCA,fin.COCHERA,fin.NIVELES,fin.COLOR,fin.CANCEL
                                           ,sol.ARTICULO,sol.CANTIDAD,sol.ESTADO,sol.COMENTARIO
                                           ,qui.QUIEN,qui.NOMBRE
                                           ,des.SEXO,des.ESTATURA,des.COMPLEXION,des.COLOR_PIEL,des.CABELLO,des.COLOR_CABELLO ";
                            $strTables2 =" ,fin.tipo as TIPOFINCA,fin.COCHERA,fin.NIVELES,fin.COLOR,fin.CANCEL
                                           ,sol.ARTICULO,sol.CANTIDAD,sol.ESTADO, '' as COMENTARIO
                                           ,qui.QUIEN,qui.valor_quien as NOMBRE
                                           ,des.SEXO,des.ESTATURA,des.COMPLEXION,des.COLORPIEL as COLOR_PIEL,des.CABELLO,des.COLORCABELLO as COLOR_CABELLO ";
                            $strInner ="left join android_reportes.cob_negativafinca fin on fin.IDCOMENTARIO = com.idcomentario
                                        left join android_reportes.cob_negativasolvencia sol on sol.IDCOMENTARIO = com.idcomentario
                                        left join android_reportes.cob_negativaquien qui on qui.IDCOMENTARIO = com.idcomentario
                                        left join android_reportes.cob_negativadescripcion des on des.IDCOMENTARIO = com.idcomentario";
                            $strInner2 ="inner join bitacora_gestioncobranza_intelisis.tipo_finca fin on fin.id_registro = com.id
                                         left join bitacora_gestioncobranza_intelisis.solvencia sol on sol.id_registro = com.id
                                         left join bitacora_gestioncobranza_intelisis.quien qui on qui.id_registro = com.id
                                         left join bitacora_gestioncobranza_intelisis.descripcion_fisica des on des.id_registro = com.id";
							
							$strInner2_ ="inner join bitacora_gestioncobranza_intcob.tipo_finca fin on fin.id_registro = com.id
                                         left join bitacora_gestioncobranza_intcob.solvencia sol on sol.id_registro = com.id
                                         left join bitacora_gestioncobranza_intcob.quien qui on qui.id_registro = com.id
                                         left join bitacora_gestioncobranza_intcob.descripcion_fisica des on des.id_registro = com.id";										 
                            break;
                        case "CTE Y/O AVAL SE ESCONDE O LO NIEGAN":
                            $strTables = " ,fin.TIPOFINCA,fin.COCHERA,fin.NIVELES,fin.COLOR,fin.CANCEL
                                           ,sol.ARTICULO,sol.CANTIDAD,sol.ESTADO,sol.COMENTARIO
                                           ,qui.QUIEN,qui.NOMBRE
                                           ,des.SEXO,des.ESTATURA,des.COMPLEXION,des.COLOR_PIEL,des.CABELLO,des.COLOR_CABELLO,
                                vec.TIPOFINCA as VecinoTipoFinca,
                                vec.COCHERA as VecinoCochera,
                                vec.CANCEL as VecinoCancel,
                                vec.NIVELES as VecinoNivel,
                                vec.COLOR as VecinoColor,
                                vec.SEXO as VecinoSexo,
                                vec.ESTATURA as VecinoEstatura,
                                vec.COMPLEXION as VecinoComplexion,
                                vec.COLOR_PIEL as VecinoColorPiel,
                                vec.CABELLO as VecinoCabello,
                                vec.COLOR_CABELLO as VecinoColorCabello,
                                vec.QUIEN as VecinoQuien,
                                vec.NOMBRE as VecinoNombre,
                                vec.NUMEROCASA as VecinoNumeroCasa ";
                            $strTables2 =" ,fin.tipo as TIPOFINCA,fin.COCHERA,fin.NIVELES,fin.COLOR,fin.CANCEL
                                           ,sol.ARTICULO,sol.CANTIDAD,sol.ESTADO, '' as COMENTARIO
                                           ,qui.QUIEN,qui.valor_quien as NOMBRE
                                           ,des.SEXO,des.ESTATURA,des.COMPLEXION,des.COLORPIEL as COLOR_PIEL,des.CABELLO,des.COLORCABELLO as COLOR_CABELLO
                                           ,vecfis.tipo as VecinoTipoFinca, vecfis.cochera as VecinoCochera, vecfis.cancel as VecinoCancel, vecfis.niveles as VecinoNivel, vecfis.color as VecinoColor
                                           ,vecfis.sexo as VecinoSexo, vecfis.estatura as VecinoEstatura, vecfis.complexion as VecinoComplexion, vecfis.colorpiel as VecinoColorPiel, vecfis.cabello as VecinoCabello, vecfis.colorcabello as VecinoColorCabello
                                           ,'' as VecioQuien
                                           ,vecfis.nombre as VecinoNombre
                                           ,vecfis.domicilio as VecinoNumeroCasa ";
                            $strInner ="left join android_reportes.cob_negativafinca fin on fin.IDCOMENTARIO = com.idcomentario
                                        left join android_reportes.cob_negativasolvencia sol on sol.IDCOMENTARIO = com.idcomentario
                                        left join android_reportes.cob_negativaquien qui on qui.IDCOMENTARIO = com.idcomentario
                                        left join android_reportes.cob_negativadescripcion des on des.IDCOMENTARIO = com.idcomentario
                                        left join android_reportes.cob_negativavecinos vec on vec.IDCOMENTARIO = com.idcomentario";
                            $strInner2 ="inner join bitacora_gestioncobranza_intelisis.tipo_finca fin on fin.id_registro = com.id
                                         left join bitacora_gestioncobranza_intelisis.solvencia sol on sol.id_registro = com.id
                                         left join bitacora_gestioncobranza_intelisis.quien qui on qui.id_registro = com.id
                                         left join bitacora_gestioncobranza_intelisis.descripcion_fisica des on des.id_registro = com.id
                                         left join bitacora_gestioncobranza_intelisis.descripcion_vecino vecfis on vecfis.id_registro = com.id";

							$strInner2_ ="inner join bitacora_gestioncobranza_intcob.tipo_finca fin on fin.id_registro = com.id
                                         left join bitacora_gestioncobranza_intcob.solvencia sol on sol.id_registro = com.id
                                         left join bitacora_gestioncobranza_intcob.quien qui on qui.id_registro = com.id
                                         left join bitacora_gestioncobranza_intcob.descripcion_fisica des on des.id_registro = com.id
                                         left join bitacora_gestioncobranza_intcob.descripcion_vecino vecfis on vecfis.id_registro = com.id";										 
                            break;
                        case "CTE Y/O AVAL NIEGA IDENTIDAD":
                            $strTables = " ,fin.TIPOFINCA,fin.COCHERA,fin.NIVELES,fin.COLOR,fin.CANCEL
                                           ,sol.ARTICULO,sol.CANTIDAD,sol.ESTADO,sol.COMENTARIO
                                           ,qui.QUIEN,qui.NOMBRE
                                           ,des.SEXO,des.ESTATURA,des.COMPLEXION,des.COLOR_PIEL,des.CABELLO,des.COLOR_CABELLO,
                                vec.TIPOFINCA as VecinoTipoFinca,
                                vec.COCHERA as VecinoCochera,
                                vec.CANCEL as VecinoCancel,
                                vec.NIVELES as VecinoNivel,
                                vec.COLOR as VecinoColor,
                                vec.SEXO as VecinoSexo,
                                vec.ESTATURA as VecinoEstatura,
                                vec.COMPLEXION as VecinoComplexion,
                                vec.COLOR_PIEL as VecinoColorPiel,
                                vec.CABELLO as VecinoCabello,
                                vec.COLOR_CABELLO as VecinoColorCabello,
                                vec.QUIEN as VecinoQuien,
                                vec.NOMBRE as VecinoNombre,
                                vec.NUMEROCASA as VecinoNumeroCasa ";
                            $strTables2 =" ,fin.tipo as TIPOFINCA,fin.COCHERA,fin.NIVELES,fin.COLOR,fin.CANCEL
                                           ,sol.ARTICULO,sol.CANTIDAD,sol.ESTADO, '' as COMENTARIO
                                           ,qui.QUIEN,qui.valor_quien as NOMBRE
                                           ,des.SEXO,des.ESTATURA,des.COMPLEXION,des.COLORPIEL as COLOR_PIEL,des.CABELLO,des.COLORCABELLO as COLOR_CABELLO
                                           ,vecfis.tipo as VecinoTipoFinca, vecfis.cochera as VecinoCochera, vecfis.cancel as VecinoCancel, vecfis.niveles as VecinoNivel, vecfis.color as VecinoColor
                                           ,vecfis.sexo as VecinoSexo, vecfis.estatura as VecinoEstatura, vecfis.complexion as VecinoComplexion, vecfis.colorpiel as VecinoColorPiel, vecfis.cabello as VecinoCabello, vecfis.colorcabello as VecinoColorCabello
                                           ,'' as VecioQuien
                                           ,vecfis.nombre as VecinoNombre
                                           ,vecfis.domicilio as VecinoNumeroCasa ";
                            $strInner ="left join android_reportes.cob_negativafinca fin on fin.IDCOMENTARIO = com.idcomentario
                                        left join android_reportes.cob_negativasolvencia sol on sol.IDCOMENTARIO = com.idcomentario
                                        left join android_reportes.cob_negativaquien qui on qui.IDCOMENTARIO = com.idcomentario
                                        left join android_reportes.cob_negativadescripcion des on des.IDCOMENTARIO = com.idcomentario
                                        left join android_reportes.cob_negativavecinos vec on vec.IDCOMENTARIO = com.idcomentario";
                            $strInner2 ="inner join bitacora_gestioncobranza_intelisis.tipo_finca fin on fin.id_registro = com.id
                                         left join bitacora_gestioncobranza_intelisis.solvencia sol on sol.id_registro = com.id
                                         left join bitacora_gestioncobranza_intelisis.quien qui on qui.id_registro = com.id
                                         left join bitacora_gestioncobranza_intelisis.descripcion_fisica des on des.id_registro = com.id
                                         left join bitacora_gestioncobranza_intelisis.descripcion_vecino vecfis on vecfis.id_registro = com.id";
							
							$strInner2_ ="inner join bitacora_gestioncobranza_intcob.tipo_finca fin on fin.id_registro = com.id
                                         left join bitacora_gestioncobranza_intcob.solvencia sol on sol.id_registro = com.id
                                         left join bitacora_gestioncobranza_intcob.quien qui on qui.id_registro = com.id
                                         left join bitacora_gestioncobranza_intcob.descripcion_fisica des on des.id_registro = com.id
                                         left join bitacora_gestioncobranza_intcob.descripcion_vecino vecfis on vecfis.id_registro = com.id";
							
                            break;
                        case "CTE Y/O AVAL ASESORADO POR UN ABOGADO":
                            $strTables = " ,fin.TIPOFINCA,fin.COCHERA,fin.NIVELES,fin.COLOR,fin.CANCEL
                                           ,sol.ARTICULO,sol.CANTIDAD,sol.ESTADO,sol.COMENTARIO
                                           ,qui.QUIEN,qui.NOMBRE
                                           ,des.SEXO,des.ESTATURA,des.COMPLEXION,des.COLOR_PIEL,des.CABELLO,des.COLOR_CABELLO ";
                            $strTables2 =" ,fin.tipo as TIPOFINCA,fin.COCHERA,fin.NIVELES,fin.COLOR,fin.CANCEL
                                           ,sol.ARTICULO,sol.CANTIDAD,sol.ESTADO, '' as COMENTARIO
                                           ,qui.QUIEN,qui.valor_quien as NOMBRE
                                           ,des.SEXO,des.ESTATURA,des.COMPLEXION,des.COLORPIEL as COLOR_PIEL,des.CABELLO,des.COLORCABELLO as COLOR_CABELLO ";
                            $strInner ="left join android_reportes.cob_negativafinca fin on fin.IDCOMENTARIO = com.idcomentario
                                        left join android_reportes.cob_negativasolvencia sol on sol.IDCOMENTARIO = com.idcomentario
                                        left join android_reportes.cob_negativaquien qui on qui.IDCOMENTARIO = com.idcomentario
                                        left join android_reportes.cob_negativadescripcion des on des.IDCOMENTARIO = com.idcomentario";
                            $strInner2 ="inner join bitacora_gestioncobranza_intelisis.tipo_finca fin on fin.id_registro = com.id
                                         left join bitacora_gestioncobranza_intelisis.solvencia sol on sol.id_registro = com.id
                                         left join bitacora_gestioncobranza_intelisis.quien qui on qui.id_registro = com.id
                                         left join bitacora_gestioncobranza_intelisis.descripcion_fisica des on des.id_registro = com.id";
							
							$strInner2_ ="inner join bitacora_gestioncobranza_intcob.tipo_finca fin on fin.id_registro = com.id
                                         left join bitacora_gestioncobranza_intcob.solvencia sol on sol.id_registro = com.id
                                         left join bitacora_gestioncobranza_intcob.quien qui on qui.id_registro = com.id
                                         left join bitacora_gestioncobranza_intcob.descripcion_fisica des on des.id_registro = com.id";
                            break;
                        case "CTE SOLICITA FUERZA PÚBLICA":
                            $strTables = " ,fin.TIPOFINCA,fin.COCHERA,fin.NIVELES,fin.COLOR,fin.CANCEL
                                           ,sol.ARTICULO,sol.CANTIDAD,sol.ESTADO,sol.COMENTARIO
                                           ,qui.QUIEN,qui.NOMBRE
                                           ,des.SEXO,des.ESTATURA,des.COMPLEXION,des.COLOR_PIEL,des.CABELLO,des.COLOR_CABELLO ";
                            $strTables2 =" ,fin.tipo as TIPOFINCA,fin.COCHERA,fin.NIVELES,fin.COLOR,fin.CANCEL
                                           ,sol.ARTICULO,sol.CANTIDAD,sol.ESTADO, '' as COMENTARIO
                                           ,qui.QUIEN,qui.valor_quien as NOMBRE
                                           ,des.SEXO,des.ESTATURA,des.COMPLEXION,des.COLORPIEL as COLOR_PIEL,des.CABELLO,des.COLORCABELLO as COLOR_CABELLO ";
                            $strInner ="left join android_reportes.cob_negativafinca fin on fin.IDCOMENTARIO = com.idcomentario
                                        left join android_reportes.cob_negativasolvencia sol on sol.IDCOMENTARIO = com.idcomentario
                                        left join android_reportes.cob_negativaquien qui on qui.IDCOMENTARIO = com.idcomentario
                                        left join android_reportes.cob_negativadescripcion des on des.IDCOMENTARIO = com.idcomentario";
                            $strInner2 ="inner join bitacora_gestioncobranza_intelisis.tipo_finca fin on fin.id_registro = com.id
                                         left join bitacora_gestioncobranza_intelisis.solvencia sol on sol.id_registro = com.id
                                         left join bitacora_gestioncobranza_intelisis.quien qui on qui.id_registro = com.id
                                         left join bitacora_gestioncobranza_intelisis.descripcion_fisica des on des.id_registro = com.id";
							
							$strInner2_ ="inner join bitacora_gestioncobranza_intcob.tipo_finca fin on fin.id_registro = com.id
                                         left join bitacora_gestioncobranza_intcob.solvencia sol on sol.id_registro = com.id
                                         left join bitacora_gestioncobranza_intcob.quien qui on qui.id_registro = com.id
                                         left join bitacora_gestioncobranza_intcob.descripcion_fisica des on des.id_registro = com.id";										 
                            break;
                    }
                    if(isset($_GET['idnega'])){
                            $strQuery2="SELECT registros.* FROM((select
                                                                    com.idcomentario,
                                                                    com.mavicob,
                                                                    com.cuenta,
                                                                    his.historia
                                                                  ".$strTables."
                                                                 from
                                                                  android_reportes.comentarios_cob com
                                                                  inner join android_reportes.cob_negativahistoria his on his.IDCOMENTARIO = com.idcomentario
                                                                  ".$strInner.")union all
                                                                 (select
                                                                  com.id as idcomentario
                                                                  ,0 as mavicob
                                                                  ,com.cuenta
                                                                  ,com.comentario as historia
                                                                  ".$strTables2."
                                                                 from bitacora_gestioncobranza_intelisis.registros com
                                                                  ".$strInner2.")) registros
                                              where 1 and registros.mavicob = 0 ".(isset($_GET['idnega']) ? " and registros.idcomentario = '".$_GET['idnega']."' " : " ")."
                                        
											UNION ALL
											
											SELECT registros.* FROM((select
                                                                    com.idcomentario,
                                                                    com.mavicob,
                                                                    com.cuenta,
                                                                    his.historia
                                                                  ".$strTables."
                                                                 from
                                                                  android_reportes.comentarios_cob com
                                                                  inner join android_reportes.cob_negativahistoria his on his.IDCOMENTARIO = com.idcomentario
                                                                  ".$strInner.")union all
                                                                 (select
                                                                  com.id as idcomentario
                                                                  ,1 as mavicob
                                                                  ,com.cuenta
                                                                  ,com.comentario as historia
                                                                  ".$strTables2."
                                                                 from bitacora_gestioncobranza_intcob.registros com
                                                                  ".$strInner2_.")) registros
                                              where 1 and registros.mavicob = 1 ".(isset($_GET['idnega']) ? " and registros.idcomentario = '".$_GET['idnega']."' " : " ")."
                                        order by cuenta
										";
										
										//echo $strQuery2;

                            $answer = mysql_query($strQuery2) or die("ERROR: " . mysql_error());
                            if($answer){
                                    $strDisplay1 = "inline";
                                    while($row = mysql_fetch_assoc($answer)){
                                            if($row['ARTICULO'] != ""){
                                                    $strDisplay2 = "inline";
                                                    $strDisplay3 = "none";
                                            }
                                            if($row['VecinoTipoFinca'] != ""){
                                                    $strDisplay4 = "none";
                                                    $strDisplay5 = "inline";
                                            }
                                            $strComentario = $row['historia'];
                                            $arrayObject2[] = $row;
                                    }
                            }
                    }
                    $aux['stylemain'] = $strDisplay1;
                    $aux['styleSolvencia1'] = $strDisplay2;
                    $aux['styleSolvencia2'] = $strDisplay3;
                    $aux['styleQuien1'] = $strDisplay4;
                    $aux['styleQuien2'] = $strDisplay5;
                    $styleDisplay[] = $aux;
                    $aux = array();
                    $aux['comentario'] = $strComentario;
                    $message[] = $aux;
                    $this->tpl = "gestionescobranza/reporte.negativa.detalle.html";
                    $bloq = array(
                              "blk1" => $arrayObject,
                              "blk2" => $arrayObject,
                              "finca" => $arrayObject2,
                              "solvencia" => $arrayObject2,
                              "quien" => $arrayObject2,
                              "descripcion" => $arrayObject2,
                              "vecino" => $arrayObject2,
                              "styleMain" => $styleDisplay,
                              "messageMain" => $message
                    );
                    return $bloq;
            }


            $_SESSION['WhereNegativa'] = "       ";
            if(!empty($_POST)){
                    $strWhere = $strWhere . ($_POST["nomina"]!="" ? " AND registros.Nomina='".$_POST["nomina"]."' " : " ");
                    $strWhere = $strWhere . ($_POST["cuenta"]!="" ? " AND registros.Cuenta='".$_POST["cuenta"]."' " : " ");
                    $strWhere = $strWhere . ($_POST["nivelcobranza"]!="" ? " AND registros.Resultado='".$_POST["nivelcobranza"]."' " : " ");
                    $strWhere = $strWhere . ($_POST["fecha1"]!="" ? " AND DATE(registros.Fecha) BETWEEN '".$_POST["fecha1"]."' " : " ");
                    $strWhere = $strWhere . ($_POST["fecha2"]!="" ? " AND '".$_POST["fecha2"]."' " : " ");
                    $strWhere = $strWhere . ($_POST["dv"]!="" ? " AND registros.DV='".$_POST["dv"]."' " : " ");
                    $strWhere = $strWhere . ($_POST["di"]!="" ? " AND registros.DI='".$_POST["di"]."' " : " ");
                    $_SESSION['WhereNegativa'] = $strWhere;
            }

            if($_POST["nomina"]!="" || $_POST["cuenta"]!="" || $_POST["nivelcobranza"]!="" ||
               $_POST["dv"]!="" || $_POST["di"]!="" || ($_POST["fecha1"]!="" && $_POST["fecha2"]!="") || ($_SESSION['WhereNegativa'] != "       ")){
				   
				   
				   
                    /*$strQuery1 ="select
                                    registros.*
                                 from
                                     ((select
                                          reg.id as Id,
                                          reg.intelisis as Nombre,
                                          reg.cuenta as Cuenta,
                                          reg.factura as Factura,
                                          reg.nombre as NombreCliente,
                                          reg.pzo as Plazo,
                                          ROUND(reg.importe, 3) as Importe,
                                          reg.fechaultimopago as FUP,
                                          ROUND(reg.abonos, 3) as Abonos,
                                          ROUND(reg.saldocapital, 3) as SaldoCapital,
                                          ROUND(reg.intereses, 3) as Intereses,
                                          ROUND(reg.saldovencido, 3) as SaldoVencido,
                                          ROUND(reg.saldo, 3) as SaldoTotal,
                                          reg.diasinactividad as DI,
                                          reg.diasvencidos as DV,
                                          reg.ruta as Ruta,

                                          reg.intelisis as Nomina,
                                          reg.resultado as Resultado,
                                          reg.valor_resultado as TipoNegativa,
                                          reg.fecha_captura as Fecha,
                                          reg.hora_inicio as Hora
                                        from
                                          ".$strDataBase.".registros reg
                                          where capturista <> 'Android'
                                          and (valor_resultado = 'NEGATIVA DE PAGO EXPRESA (MANIFESTADA)' or
                                          valor_resultado = 'AGRESION FISICA' or
                                          valor_resultado = 'CTE Y/O AVAL SE ESCONDE O LO NIEGAN' or
                                          valor_resultado = 'CTE Y/O AVAL NIEGA IDENTIDAD' or
                                          valor_resultado = 'CTE Y/O AVAL ASESORADO POR UN ABOGADO' or
                                          valor_resultado = 'CTE SOLICITA FUERZA PÚBLICA'))
                                         union all
                                        (select
                                          com.idcomentario as Id,
                                          com.promotor as Nombre,
                                          com.cuenta as Cuenta,
                                          com.factura as Factura,
                                          his.nombrecte as NombreCliente,
                                          '' as Plazo,
                                          ROUND(his.importe, 3) as Importe,
                                          his.fechultabn as FUP,
                                          ROUND(his.abonos, 3) as Abonos,
                                          ROUND(his.saldocap, 3) as SaldoCapital,
                                          '' as Intereses,
                                          ROUND(his.saldoven, 3) as SaldoVencido,
                                          ROUND(his.saldotot, 3) as SaldoTotal,
                                          his.di as DI,
                                          his.dv as DV,
                                          '' as Ruta,

                                          com.promotor as Nomina,
                                          com.resultado as Resultado,
                                          com.comentario as TipoNegativa,
                                          com.fecha_inicio as Fecha,
                                          com.hora_inicio as Hora
                                        from
                                          android_reportes.comentarios_cob com
                                          left join android_reportes.cob_negativahistoria his on his.IDCOMENTARIO = com.idcomentario
                                        where com.mavicob = ".$nDataBase.")
                                          ) registros
                                          where 1 " . ($_SESSION['WhereNegativa'] != "" ? $_SESSION['WhereNegativa'] : "") . " order by registros.Cuenta,registros.Fecha desc,registros.Hora desc";*/
										  
					$strQuery1="
								select 
									registros . *
								from
									((select 
										reg.id as Id,
											reg.intelisis as Nombre,
											reg.cuenta as Cuenta,
											reg.factura as Factura,
											reg.nombre as NombreCliente,
											reg.pzo as Plazo,
											ROUND(reg.importe, 3) as Importe,
											reg.fechaultimopago as FUP,
											ROUND(reg.abonos, 3) as Abonos,
											ROUND(reg.saldocapital, 3) as SaldoCapital,
											ROUND(reg.intereses, 3) as Intereses,
											ROUND(reg.saldovencido, 3) as SaldoVencido,
											ROUND(reg.saldo, 3) as SaldoTotal,
											reg.diasinactividad as DI,
											reg.diasvencidos as DV,
											reg.ruta as Ruta,
											reg.intelisis as Nomina,
											reg.resultado as Resultado,
											reg.valor_resultado as TipoNegativa,
											reg.fecha_captura as Fecha,
											reg.hora_inicio as Hora
									from
										bitacora_gestioncobranza_intelisis.registros reg
									where
										capturista <> 'Android'
											and (valor_resultado = 'NEGATIVA DE PAGO EXPRESA (MANIFESTADA)'
											or valor_resultado = 'AGRESION FISICA'
											or valor_resultado = 'CTE Y/O AVAL SE ESCONDE O LO NIEGAN'
											or valor_resultado = 'CTE Y/O AVAL NIEGA IDENTIDAD'
											or valor_resultado = 'CTE Y/O AVAL ASESORADO POR UN ABOGADO'
											or valor_resultado = 'CTE SOLICITA FUERZA PÚBLICA')) union all (select 
										com.idcomentario as Id,
											com.promotor as Nombre,
											com.cuenta as Cuenta,
											com.factura as Factura,
											his.nombrecte as NombreCliente,
											'' as Plazo,
											ROUND(his.importe, 3) as Importe,
											his.fechultabn as FUP,
											ROUND(his.abonos, 3) as Abonos,
											ROUND(his.saldocap, 3) as SaldoCapital,
											'' as Intereses,
											ROUND(his.saldoven, 3) as SaldoVencido,
											ROUND(his.saldotot, 3) as SaldoTotal,
											his.di as DI,
											his.dv as DV,
											'' as Ruta,
											com.promotor as Nomina,
											com.resultado as Resultado,
											com.comentario as TipoNegativa,
											com.fecha_inicio as Fecha,
											com.hora_inicio as Hora
									from
										android_reportes.comentarios_cob com
									left join android_reportes.cob_negativahistoria his ON his.IDCOMENTARIO = com.idcomentario
									where
										com.mavicob = 0)) registros
								where
									1
										" . ($_SESSION['WhereNegativa'] != "" ? $_SESSION['WhereNegativa'] : "") . "
								
								UNION ALL
								
								select 
									registros . *
								from
									((select 
										reg.id as Id,
											reg.intelisis as Nombre,
											reg.cuenta as Cuenta,
											reg.factura as Factura,
											reg.nombre as NombreCliente,
											reg.pzo as Plazo,
											ROUND(reg.importe, 3) as Importe,
											reg.fechaultimopago as FUP,
											ROUND(reg.abonos, 3) as Abonos,
											ROUND(reg.saldocapital, 3) as SaldoCapital,
											ROUND(reg.intereses, 3) as Intereses,
											ROUND(reg.saldovencido, 3) as SaldoVencido,
											ROUND(reg.saldo, 3) as SaldoTotal,
											reg.diasinactividad as DI,
											reg.diasvencidos as DV,
											reg.ruta as Ruta,
											reg.intelisis as Nomina,
											reg.resultado as Resultado,
											reg.valor_resultado as TipoNegativa,
											reg.fecha_captura as Fecha,
											reg.hora_inicio as Hora
									from
										bitacora_gestioncobranza_intcob.registros reg
									where
										capturista <> 'Android'
											and (valor_resultado = 'NEGATIVA DE PAGO EXPRESA (MANIFESTADA)'
											or valor_resultado = 'AGRESION FISICA'
											or valor_resultado = 'CTE Y/O AVAL SE ESCONDE O LO NIEGAN'
											or valor_resultado = 'CTE Y/O AVAL NIEGA IDENTIDAD'
											or valor_resultado = 'CTE Y/O AVAL ASESORADO POR UN ABOGADO'
											or valor_resultado = 'CTE SOLICITA FUERZA PÚBLICA')) union all (select 
										com.idcomentario as Id,
											com.promotor as Nombre,
											com.cuenta as Cuenta,
											com.factura as Factura,
											his.nombrecte as NombreCliente,
											'' as Plazo,
											ROUND(his.importe, 3) as Importe,
											his.fechultabn as FUP,
											ROUND(his.abonos, 3) as Abonos,
											ROUND(his.saldocap, 3) as SaldoCapital,
											'' as Intereses,
											ROUND(his.saldoven, 3) as SaldoVencido,
											ROUND(his.saldotot, 3) as SaldoTotal,
											his.di as DI,
											his.dv as DV,
											'' as Ruta,
											com.promotor as Nomina,
											com.resultado as Resultado,
											com.comentario as TipoNegativa,
											com.fecha_inicio as Fecha,
											com.hora_inicio as Hora
									from
										android_reportes.comentarios_cob com
									left join android_reportes.cob_negativahistoria his ON his.IDCOMENTARIO = com.idcomentario
									where
										com.mavicob = 1)) registros
								where
									1
										" . ($_SESSION['WhereNegativa'] != "" ? $_SESSION['WhereNegativa'] : "") . "
								order by Cuenta , Fecha desc , Hora desc
					";
					//echo $strQuery1;
                    $answer = mysql_query($strQuery1);
                    $strAux1 = "";
                    $arrayRow = array();
                    $nuevo = 0;
                    $negativa = 0;
                    $i = 1;
                    $auxiliar = 0;
                    while($row = mysql_fetch_assoc($answer)){
                            $arrayRow = $row;
                            //print_r($row);
                            if($row['Cuenta'] != $strAux1 && $strAux1 != ""){
                                    $nuevo = 1;
                            }
                            if($nuevo == 1){
                                    if($negativa > 0){
                                            $arrayObject[] = $arrayHistory + $arrayAux;
                                    }
                                    $negativa = 0;
                                    $nuevo = 0;
                                    $i = 1;
                                    for($n=0; $n<11; $n++){
                                            $arrayAux['R'.$n.'_Resultado'] = "";
                                            $arrayAux['R'.$n.'_Fecha'] = "";
                                            $arrayAux['R'.$n.'_Hora'] = "";
                                            $arrayAux['R'.$n.'_Link'] = "";
                                            $arrayAux['R'.$n.'_Link2'] = "";
                                            $arrayAux['R'.$n.'_Link3'] = "";
                                    }
                            }
                            if($row['Cuenta'] == $strAux1){
                                    $arrayAux['R'.$i.'_Resultado'] = $row['Resultado'];
                                    $arrayAux['R'.$i.'_Fecha'] = $row['Fecha'];
                                    $arrayAux['R'.$i.'_Hora'] = $row['Hora'];
                                    if($row['Resultado'] == "negativa"){
                                            $negativa++;
                                    }
                                    if($row['Resultado'] == "negativa"){
                                            $arrayAux['R'.$i.'_Link'] = $row['Cuenta'];
                                            $arrayAux['R'.$i.'_Link2'] = $row['Id'];
                                            $arrayAux['R'.$i.'_Link3'] = $row['TipoNegativa'];
                                    }else{
                                            $arrayAux['R'.$i.'_Link'] = "0";
                                            $arrayAux['R'.$i.'_Link2'] = "0";
                                            $arrayAux['R'.$i.'_Link3'] = "0";
                                    }
                                    $i++;
                            }else{
                                    $arrayHistory = $row;
                                    $arrayAux['R'.$i.'_Resultado'] = $row['Resultado'];
                                    $arrayAux['R'.$i.'_Fecha'] = $row['Fecha'];
                                    $arrayAux['R'.$i.'_Hora'] = $row['Hora'];
                                    $strAux1 = $row['Cuenta'];
                                    if($row['Resultado'] == "negativa"){
                                            $negativa++;
                                    }
                                    if($row['Resultado'] == "negativa"){
                                            $arrayAux['R'.$i.'_Link'] = $row['Cuenta'];
                                            $arrayAux['R'.$i.'_Link2'] = $row['Id'];
                                            $arrayAux['R'.$i.'_Link3'] = $row['TipoNegativa'];
                                    }else{
                                            $arrayAux['R'.$i.'_Link'] = "0";
                                            $arrayAux['R'.$i.'_Link2'] = "0";
                                            $arrayAux['R'.$i.'_Link3'] = "0";
                                    }
                                    $i++;
                            }
                    }
                    if($arrayRow['Resultado'] == "negativa" || $negativa > 0){
                            $arrayObject[] = $arrayHistory + $arrayAux;
                    }
            }

            if($_POST["nomina"]!=""){
                    $strQuery2="SELECT
                                    emp.CLAVE AS nominaPromotor,
                                    emp.NOMBRE AS nombrePromotor
                                FROM
                                    varios.empleados_rh201_intelisis emp
                                WHERE
                                    emp.CLAVE = '" . $_POST["nomina"] . "'";
                    $answer = mysql_query($strQuery2);
                    while($row = mysql_fetch_assoc($answer)){
                            $arrayGestor[] = $row;
                    }
            }//*/
            $this->tpl = "gestionescobranza/reporte.negativa_disenio.html";
            $bloq = array(
                    "blk1" => $arrayObject,
                    "blk2" => $arrayGestor
            );

            return $bloq;
    }

    function _repEncFisics(){
            $this->tpl="gestionescobranza/reporteEncuentrosFisicos.htm";
            $GLOBALS['var1'] = 0;
            if( !empty( $_POST ) ){
                    $GLOBALS['var1'] = 1;
                    $nomina    = $_POST['nomina'];
                    $ejercicio = $_POST['ejercicio'];
                    $periodo   = $_POST['quincena'];
					$fecha1="select quincena_cobranza1(".$periodo.",".$ejercicio.") as fecha";
					$res=mysql_fetch_assoc(mysql_query($fecha1));
					$desde=$res['fecha'];
					$fecha2="select quincena_cobranza2(".$periodo.",".$ejercicio.") as fecha";
					$res=mysql_fetch_assoc(mysql_query($fecha2));
					$hasta=$res['fecha'];
					$CambiarIdioma="SET lc_time_names = 'es_MX'";
					mysql_query($CambiarIdioma);
                    $where     =  " where 1=1";
                    if( $nomina != "" ) {
                            $where.= " and intelisis = '".$nomina."'";
                            
                    }
                    if( $periodo != "" && $ejercicio != "" ) {
                            $where.= " and  fecha_captura BETWEEN '".$desde."' and '".$hasta."'";
                    }
					$sqlasig=" CREATE TEMPORARY TABLE gest2_temp AS 
						SELECT * FROM(
							SELECT 
								CASE
									WHEN (R.intelisis IS NULL OR R.intelisis = 'SIN AGE' OR R.intelisis = ' ') THEN REPLACE(R.promotor, ' ', '')
									ELSE REPLACE(R.intelisis, ' ', '')
								END as intelisis,
								REPLACE(R.cuenta, ' ', '') as cuenta,
								REPLACE(R.mov, ' ', '') as mov,
								REPLACE(R.factura, ' ', '') as factura,
								DATE_FORMAT(r.fecha_inicio, '%Y %b %e ') as fecha_inicio,
								r.nombre,
								r.pzo,
								concat('$', r.importe) as importe,
								DATE_FORMAT(r.fechaultimopago, '%Y %b %e ') as fechaultimopago,
								concat('$', r.abonos) as abonos,
								concat('$', r.saldocapital) as saldocapital,
								concat('$', r.intereses) as intereses,
								concat('$', r.saldovencido) as saldovencido,
								concat('$', r.saldo) as saldo,
								r.diasinactividad,
								r.diasvencidos,
								r.ruta,
								r.domicilio,
								r.colonia,
								r.ciudad,
								r.articulo,
								r.valor_resultado,
								r.hora_final,
								r.hora_inicio,
								r.comentario,
								r.folio,
								r.recibo,
								concat('$', r.pago_importe) as pago_importe,
								R.resultado,
								R.fecha_captura
							FROM
								bitacora_gestioncobranza_intelisis.registros R
							".$where." and intelisis <> ''
						union 
							SELECT 
								CASE
									WHEN (R.intelisis IS NULL OR R.intelisis = 'SIN AGE' OR R.intelisis = ' ') THEN REPLACE(R.promotor, ' ', '')
									ELSE REPLACE(R.intelisis, ' ', '')
								END as intelisis,
								REPLACE(R.cuenta, ' ', '') as cuenta,
								REPLACE(R.mov, ' ', '') as mov,
								REPLACE(R.factura, ' ', '') as factura,
								DATE_FORMAT(r.fecha_inicio, '%Y %b %e ') as fecha_inicio,
								r.nombre,
								r.pzo,
								concat('$', r.importe) as importe,
								DATE_FORMAT(r.fechaultimopago, '%Y %b %e ') as fechaultimopago,
								concat('$', r.abonos) as abonos,
								concat('$', r.saldocapital) as saldocapital,
								concat('$', r.intereses) as intereses,
								concat('$', r.saldovencido) as saldovencido,
								concat('$', r.saldo) as saldo,
								r.diasinactividad,
								r.diasvencidos,
								r.ruta,
								r.domicilio,
								r.colonia,
								r.ciudad,
								r.articulo,
								r.valor_resultado,
								r.hora_final,
								r.hora_inicio,
								r.comentario,
								r.folio,
								r.recibo,
								concat('$', r.pago_importe) as pago_importe,
								R.resultado,
								R.fecha_captura
							FROM
								bitacora_gestioncobranza_intcob.registros R
							".$where." and intelisis <> ''
								)AS A
					";
					//echo $sqlasig;
					mysql_query($sqlasig) or die("Problemas la insertar en la tabla gestiones_temp...".mysql_error());
					//$registros = mysql_affected_rows();
					$sqlandroid="CREATE TEMPORARY TABLE asig2_temp  as SELECT * FROM(
						SELECT REPLACE(a.nomina,' ','') as nomina,
								   REPLACE(a.cuenta,' ','') as cuenta,
								   REPLACE(a.movimiento,' ','') as movimiento,
								   REPLACE(a.factura,' ','') as factura
						FROM android_reportes._cuentas_gestioncobranza a 
    					where periodo=".$periodo." and ejercicio=".$ejercicio." and a.nomina='".$nomina."'
    					UNION ALL
    					select REPLACE(ad.agente_asociado,' ','') as nomina, 
    					REPLACE(agc.cuenta,' ','') as cuenta, 
    					REPLACE(agc.movimiento,' ','') as movimiento, 
    					REPLACE(agc.factura,' ','') as factura
    					FROM (
							select agente_origen, agente_asociado from bitacora_gestioncobranza_intelisis.agente_doble
							where fecha_asigna between '".$desde."' and '".$hasta."' 
							group by agente_origen, agente_asociado

						) as ad
						left join android_reportes._cuentas_gestioncobranza agc on agc.nomina=ad.agente_origen
						where agc.periodo=".$periodo." and agc.ejercicio=".$ejercicio." and ad.agente_asociado='".$nomina."'
					) b";
					//echo $sqlandroid;
					mysql_query($sqlandroid) or die ("Problema al llenar la tabla de asig2_temp...".mysql_error());
					//$registros = mysql_affected_rows();
					//echo $registros;
					$sqlasignacion="CREATE TEMPORARY TABLE gestiones_temp  as
										Select
											reg.intelisis,
											reg.resultado,
											reg.fecha_captura
										FROM asig2_temp andr
										inner join gest2_temp reg on 
												reg.intelisis   = andr.nomina 
											and reg.cuenta      = andr.cuenta 
											and andr.movimiento = reg.mov 
											and andr.factura    = reg.factura";
					mysql_query($sqlasignacion) or die ("Problema al llenar la tabla de gestiones_temp...".mysql_error());
					//$registros = mysql_affected_rows();
					//echo $registros;
                    $sql = "SELECT DATE_FORMAT(Fecha,'%Y %b %e ') as Fechaform, Fecha,Agente,b.Nombre,Total,concontacto,round(concontacto*100/total,0) as Porc, allTotal, allConCto
                    FROM(
                        SELECT
                          intelisis as Agente,
                          count(intelisis) as total,
                          '' as allTotal, '' as allConCto,
                          sum(case when p.condir='ON' then 1 else 0 end) as concontacto,
                          fecha_captura as Fecha
                        FROM gestiones_temp g
						left join bitacora_gestioncobranza_intelisis.parametros_gestiones p on g.resultado=p.resultado
                        group by intelisis,fecha_captura)a
                    inner join intranet_home.directoriogral_intelisis b on b.clave=a.Agente
                        order by fecha ASC";

					//echo $sql;
                    $allTotal = 0;
                    $allConCto = 0;
					$FechaTitulo  = " select DATE_FORMAT(quincena_cobranza1(".$periodo.",".$ejercicio."),'%Y %b %e') as pd";
					$FechaTitulo2 = " select DATE_FORMAT(quincena_cobranza2(".$periodo.",".$ejercicio."),'%Y %b %e') as sd";
					$GLOBALS['periodo']   = $periodo;
					$GLOBALS['ejercicio'] = $ejercicio;
					$GLOBALS['inicio']    = $FechaTitulo;
					$GLOBALS['fin']       = $FechaTitulo2;
					$GLOBALS['nomina__']          = $nomina;
                    $exec = mysql_query( $sql );
                    if( mysql_num_rows( $exec ) > 0 ) {
                            while( $rows = mysql_fetch_assoc( $exec ) ) {
                                    $GLOBALS['allTotal'] += $rows['Total'];
                                    $GLOBALS['allConCto'] += $rows['concontacto'];
                            }
                    }

                    if( $GLOBALS['allTotal'] != 0 ) {
                            $GLOBALS['totalPorc'] = $GLOBALS['allConCto'] * 100 / $GLOBALS['allTotal'];
                            $GLOBALS['totalPorc'] = ( int ) $GLOBALS['totalPorc'];
                    }
						
                    $bloq = array( 
								"blk"    => $sql,
								"inicio" => $FechaTitulo,
								"fin"    => $FechaTitulo2
								);
                    return $bloq;
            }
    }

    function _telconfigura() {
            $this->tpl = "gestionescobranza/telefonica/configuracion.calificacion.telefonica.htm";
            if ( empty( $_POST ) ) {
                    $bloq = array(
                            "blk1" => "SELECT tel.tel_calificacion_valor AS VALOR, tel.tel_calificacion_orden AS ORDEN FROM tel_calificacion tel WHERE tel.tel_calificacion_visible = 1 ORDER BY tel.tel_calificacion_orden" ,
                            "blk2" => "SELECT tel.tel_calificacion_id AS ID, tel.tel_calificacion_valor AS VALOR, tel.tel_calificacion_orden AS ORDEN FROM tel_calificacion tel WHERE tel.tel_calificacion_visible = 1 ORDER BY tel.tel_calificacion_orden" ,
                            "db" => "SELECT intelisis AS DB FROM config LIMIT 1"
                    );
            } else {
                    $strQuery = "SELECT ( CASE WHEN (SELECT COUNT(tel_calificacion_id) FROM tel_calificacion WHERE tel_calificacion_valor = '" . $_POST[ 'valor' ] . "') = 0
                                 THEN 'NO'
                                 ELSE 'SI'
                                 END) AS Existe";
                    $answer = mysql_query( $strQuery ) or die( "ERROR CASE : " . mysql_error() );
                    $comparacion = mysql_fetch_assoc( $answer );
                    if ( $comparacion [ 'Existe' ] == "NO" ) {
                            $strQuery = "INSERT INTO tel_calificacion( tel_calificacion_valor, tel_calificacion_orden )
                                         VALUE( '" . $_POST[ 'valor' ] . "', " . $_POST[ 'orden' ] . " )";
                            mysql_query( $strQuery ) or die( "ERROR INSERT : " . mysql_error() );
                    } else {
                            if ( $_POST[ 'del' ] == 1 ) {
                                    $strQuery = "UPDATE tel_calificacion
                                                 SET tel_calificacion_visible = 0
                                                 WHERE tel_calificacion_valor = '" . $_POST[ 'valor' ] . "'";
                            } else {
                                    $strQuery = "UPDATE tel_calificacion
                                                 SET tel_calificacion_valor = '" . $_POST[ 'valor' ] . "', tel_calificacion_orden = " . $_POST[ 'orden' ] . "
                                                 WHERE tel_calificacion_valor = '" . $_POST[ 'valor' ] . "'";
                            }
                            mysql_query( $strQuery ) or die( "ERROR UPDATE : " . mysql_error() );
                    }
                    $this->tpl = "dialog.success.htm";
                    $this->msgaviso = "Se ha cargado la informaci&oacute;n";
            }
            return $bloq;
    }

    function _telcaptura() {
            $this->tpl = "gestionescobranza/telefonica/captura.telefonica.htm";
            $bloq = array();

            if ( isset( $_POST[ 'cuenta' ] ) ) {
                    $objects = array();
                    if ( !empty( $_POST[ 'cuenta' ] ) && empty( $_POST[ 'calificacion' ] ) && empty( $_POST[ 'comentario' ] ) ) {

                                $strQuery = "SELECT reg.* FROM (  (SELECT
                                                                        tel.tel_registro_fecha AS FECHA,
                                                                        tel.tel_registro_hora AS HORA,
                                                                        aux.tel_calificacion_valor as CALIFICACION,
                                                                        tel.tel_registro_comentario AS COMENTARIO,
                                                                        tel.tel_registro_cuenta AS CUENTA,
                                                                        n.nombre AS GESTOR
                                                                    FROM tel_registro tel
                                                                    left join tel_calificacion aux on aux.tel_calificacion_id = tel.tel_registro_calificacion
                                                                    left join usuarios a on a.user = tel.tel_registro_usuario
                                                                    left join varios.empleados_rh201_intelisis n on n.clave=a.nomina
																	WHERE tel.tel_registro_cuenta = '" . $_POST[ 'cuenta' ] . "')
                                                                    UNION ALL
                                                                   (SELECT
                                                                        r.fecha_captura AS FECHA,
                                                                        r.hora_captura AS HORA,
                                                                        r.resultado AS CALIFICACION,
                                                                        r.comentario AS COMENTARIO,
                                                                        r.cuenta AS CUENTA,
                                                                        u.nombre AS GESTOR
                                                                    FROM registros r
                                                                    left join varios.empleados_rh201_intelisis u on u.CLAVE = r.intelisis
																	WHERE R.COMENTARIO<>'' AND  R.CUENTA = '" . $_POST[ 'cuenta' ] . "')
                                                             ) reg
                                         WHERE reg.CUENTA = '" . $_POST[ 'cuenta' ] . "'
                                         ORDER BY reg.FECHA DESC, reg.HORA DESC";

										 
                            $answer = mysql_query( $strQuery ) or die( "ERROR SELECT : " . mysql_error() );
                            while ( $row = mysql_fetch_assoc( $answer ) ) {
                                    $objects[] = $row;
                            }
                            $aux[ 'CUENTA' ] = ( $_POST[ 'cuenta' ] == "" ? $_POST[ 'existe' ] : $_POST[ 'cuenta' ] );
                            $arryAux[] = $aux;
                            $bloq = array(
                                    "blk1" => $objects ,
                                    "blk2" => "SELECT c.tel_calificacion_id AS ID, c.tel_calificacion_valor AS CALIFICACION FROM tel_calificacion c WHERE c.tel_calificacion_visible = 1 ORDER BY c.tel_calificacion_orden" ,
                                    "blk3" => $arryAux ,//"SELECT tel.cuenta AS CUENTA FROM registros tel WHERE tel.cuenta = '" . $_POST[ 'cuenta' ] . "' LIMIT 1" ,
                                    "db" => "SELECT intelisis AS DB FROM config LIMIT 1"
                            );
                    } else {
                            $strQuery = "INSERT INTO tel_registro(tel_registro_fecha, tel_registro_hora, tel_registro_calificacion, tel_registro_comentario, tel_registro_cuenta, tel_registro_usuario)
                                         VALUE('" . date( 'Y' ) . "-" . date( 'm' ) . "-" . date( 'd' ) . "', '" . date( " H:i:s" , time() ) . "', " . $_POST[ 'calificacion' ] . ", '" . $_POST[ 'comentario' ] . "', '" . ( $_POST[ 'existe' ] == "" ? $_POST[ 'cuenta' ] : $_POST[ 'existe' ] ) . "', '" . $_SESSION[ 'user' ] . "')";
                            mysql_query( $strQuery );

                            $strQuery = "SELECT reg.* FROM (  (SELECT
                                                                    tel.tel_registro_fecha AS FECHA,
                                                                    tel.tel_registro_hora AS HORA,
                                                                    aux.tel_calificacion_valor as CALIFICACION,
                                                                    tel.tel_registro_comentario AS COMENTARIO,
                                                                    tel.tel_registro_cuenta AS CUENTA,
                                                                    n.nombre AS GESTOR
                                                                FROM tel_registro tel
                                                                left join tel_calificacion aux on aux.tel_calificacion_id = tel.tel_registro_calificacion
                                                                left join usuarios a on a.user = tel.tel_registro_usuario
                                                                left join varios.empleados_rh201_intelisis n on n.clave=a.nomina
																WHERE tel.tel_registro_cuenta = '" . $_POST[ 'cuenta' ] . "')
                                                                UNION ALL
                                                               (SELECT
                                                                    r.fecha_captura AS FECHA,
                                                                    r.hora_captura AS HORA,
                                                                    r.resultado AS CALIFICACION,
                                                                    r.comentario AS COMENTARIO,
                                                                    r.cuenta AS CUENTA,
                                                                    u.nombre AS GESTOR
                                                                FROM registros r
                                                                left join varios.empleados_rh201_intelisis u on u.CLAVE = r.intelisis
																WHERE R.COMENTARIO<>'' AND R.CUENTA = '" . $_POST[ 'cuenta' ] . "')
                                                         ) reg
                                     WHERE reg.CUENTA = '" . $_POST[ 'existe' ] . "'
                                     ORDER BY reg.FECHA DESC, reg.HORA DESC";									 
                            $answer = mysql_query( $strQuery ) or die( "ERROR SELECT : " . mysql_error() );
                            while ( $row = mysql_fetch_assoc( $answer ) ) {
                                    $objects[] = $row;
                            }
                            $aux[ 'CUENTA' ] = ( $_POST[ 'cuenta' ] == "" ? $_POST[ 'existe' ] : $_POST[ 'cuenta' ] );
                            $arryAux[] = $aux;
                            $bloq = array(
                                    "blk1" => $objects ,
                                    "blk2" => "SELECT c.tel_calificacion_id AS ID, c.tel_calificacion_valor AS CALIFICACION FROM tel_calificacion c WHERE c.tel_calificacion_visible = 1 ORDER BY c.tel_calificacion_orden" ,
                                    "blk3" => $arryAux ,//"SELECT tel.cuenta AS CUENTA FROM registros tel WHERE tel.cuenta = '" . $_POST[ 'existe' ] . "' LIMIT 1" ,
                                    "db" => "SELECT intelisis AS DB FROM config LIMIT 1"
                            );
                    }
            } else {
                    $bloq = array(
                            "blk1" => array() ,
                            "blk2" => array() ,
                            "blk3" => array() ,
                            "db" => "SELECT intelisis AS DB FROM config LIMIT 1"
                    );
            }
            return $bloq;
    }

    function _telreporte() {
            $this->tpl = "gestionescobranza/telefonica/reporte.telefonica.htm";
            $bloq = array();
            $objects = array();
            if ( !empty( $_POST ) ) {
                    $arrayPost[] = $_POST;
                    if ( $_POST[ 'cuenta' ] != "" ) {
                            $strWhere .= " AND t.tel_registro_cuenta = '" . $_POST[ 'cuenta' ] . "' ";
                    }
                    if ( $_POST[ 'gestor' ] != "" ) {
                            $strWhere .= " AND u.nomina = '" . $_POST[ 'gestor' ] . "' ";
                    }
                    if ( ( $_POST[ 'fecha1' ] != "" ) && ( $_POST[ 'fecha2' ] != "" ) ) {
                            $strWhere .= " AND t.tel_registro_fecha BETWEEN '" . $_POST[ 'fecha1' ] . "' AND  '" . $_POST[ 'fecha2' ] . "' ";
                    }
                    $strQuery = "SELECT
                                    t.tel_registro_usuario AS USUARIO,
                                    t.tel_registro_cuenta AS CUENTA,
                                    t.tel_registro_hora AS HORA,
                                    t.tel_registro_fecha AS FECHA,
                                    (SELECT tel_calificacion_valor FROM tel_calificacion WHERE tel_calificacion_id = t.tel_registro_calificacion) AS CALIFICACION,
                                    t.tel_registro_comentario AS COMENTARIO
                                 FROM
                                    tel_registro t
                                    LEFT JOIN usuarios u ON u.user = t.tel_registro_usuario
                                 WHERE 1 " . $strWhere;
                    //echo $strQuery;
                    $answer = mysql_query( $strQuery ) or die(mysql_error());
                    while( $row = mysql_fetch_assoc( $answer ) ) {
                            $objects[] = $row;
                    }
					
					
                    $bloq = array(
                            "blk1" => $objects ,
                            "blk2" => $arrayPost ,
                            "db" => "SELECT intelisis AS DB FROM config LIMIT 1"
                    );
            } else {
				
				
                    $bloq = array(
                            "blk1" => $objects ,
                            "blk2" => array() ,
                            "db" => "SELECT intelisis AS DB FROM config LIMIT 1"
                    );
            }
            return $bloq;
    }

    function _repCorreoTel() {
		$this->tpl = "gestionescobranza/reporte.correotel.htm";
		$bloq = array();
		$objects = array();
		$strWhere = '';
		$m = date('m');
		$dia = date('d') < 16 ? '01' : '16';
		$quincena = ( ( $m - 1 ) * 2 ) + (date('d') < 16 ? 1 : 2);
		$ejercicio = date('Y');
		$fechaIni = date('Y').'-'.$m.'-'.$dia ;
		$fechaFin = date('Y').'-'.($m + 1).'-'.$dia ;
		
		if ( !empty( $_POST ) ) {
			$arrayPost[] = $_POST;
			if ( $_POST[ 'agente' ] != "" ) {
				$strWhere .= " AND com.promotor = '" . $_POST[ 'agente' ] . "' ";
				}
			if ( $_POST[ 'cuenta' ] != "" ) {
				$strWhere .= " AND com.cuenta = '" . $_POST[ 'cuenta' ] . "' ";
				}
			if ( $_POST[ 'nivel' ] != "" ) {
				$strWhere .= " AND c.etapa = '" . $_POST[ 'nivel' ] . "' ";
				}
			if ( $_POST[ 'division' ] != "" ) {
				$strWhere .= " AND divi.division = '" . $_POST[ 'division' ] . "' ";
				}
			if ( ( $_POST[ 'fecha1' ] != "" ) && ( $_POST[ 'fecha2' ] != "" ) ) {
				$strWhere .= " AND c.fecha_captura BETWEEN '" . $_POST[ 'fecha1' ] . "' AND  '" . $_POST[ 'fecha2' ] . "' ";
				}
			else{
				$strWhere .= " AND c.fecha_captura >= '" . $fechaIni . "' ";
				}
			}
		else{
			$strWhere = " AND c.fecha_captura >= '" . $fechaIni . "' ";
			}
			
			
		
			
			
			
		$strQuery = "SELECT icom.agente AS AGENTE,	a.NOMBRE AS NOMBREA,	c.etapa AS NIVEL,	IFNULL(divi.division,'Sin Division') AS DIVISION,
						c.ciudad AS POBLACION,		icom.cuenta AS CUENTA,		c.nombre AS NOMBREC,
						((MONTH(c.fecha_captura) - 1) * 2) + IF(DAY(c.fecha_captura) < 16, 1 , 2) AS PERIODO,
						YEAR(c.fecha_captura) AS EJERCICIO,		c.fecha_captura AS FECHA,		icom.gestion AS GESTION,
						icom.contacto AS TIPOCONT,			c.saldovencido AS SALDOV,			
						CASE WHEN (c1.tipo='correo1' or c1.tipo='correo 1')	THEN c1.valor ELSE '' END as MAILUNO,
						CASE WHEN (c1.tipo='correo2' or c1.tipo='correo 2')	THEN c1.valor ELSE '' END as MAILDOS,
						CASE WHEN c1.tipo='telef1' THEN c1.valor ELSE '' END as TELUNO,
						CASE WHEN c1.tipo='telef2' THEN c1.valor ELSE '' END as TELDOS,
						icom.idcomentario AS ID,			com.fecha_inicio AS CAPTURA
					FROM android_reportes.comentarios_cob com
					JOIN(SELECT distinct cuenta, gestion, fecha, agente, contacto, idcomentario
						FROM android_reportes.cob_gestiones_correo_tel )icom ON icom.idcomentario = com.idcomentario
					JOIN varios.empleados_rh201_intelisis a ON a.CLAVE = icom.agente
					JOIN bitacora_gestioncobranza_intelisis.registros c ON c.cuenta = icom.cuenta AND c.promotor = icom.agente
																			AND com.fecha_inicio = c.fecha_inicio
					LEFT JOIN android_reportes.cob_gestiones_correo_tel c1 ON c1.idcomentario = icom.idcomentario 
					LEFT JOIN bitacora_interes.datos_info_c086_intel divi ON divi.cuenta = icom.cuenta and divi.division is not null
					WHERE 1 ".$strWhere."
					GROUP BY icom.agente,a.NOMBRE,c.etapa,c.ciudad,icom.cuenta,c.nombre,icom.fecha,icom.gestion,
						icom.contacto, c1.valor, c.saldovencido, c.fecha_captura";

		//echo $strQuery;
		$bloq = array(
				"blk1" => $strQuery ,
				"blk2" => empty($arrayPost)? array():$arrayPost,
				"db" => "SELECT intelisis AS DB FROM config LIMIT 1"
				);
		return $bloq;
    	}

    function _repHitsCorreoTel() {
		$this->tpl = "gestionescobranza/reporte.hitscorreotel.htm";
		$bloq = array();

		$m = date('m');
		$dia = date('d') < 16 ? '01' : '16';
		$quincena = ( ( $m - 1 ) * 2 ) + (date('d') < 16 ? 1 : 2);
		$ejercicio = date('Y');

		$mes = ($mes > 9) ? $mes : '0'.$mes ;
		$mes1 = ($m > 8) ? ($m + 1) : '0'.($m + 1) ;
		$mes12 = ($m == 12) ? (date('Y') + 1).'/01/01' : date('Y').'/'.$mes1.'/01' ;
		$fechaIni = date('Y').'/'.$m.'/'.$dia ;
		$fechaFin = date('d') < 16 ? date('Y').'/'.$m.'/16' : $mes12 ;

		if ( !empty( $_POST ) ) {
			$arrayPost[] = $_POST;
			if ( $_POST[ 'agente' ] != "" ) {
				$strWhere .= " AND mov.promotor = '" . $_POST[ 'agente' ] . "' ";
				}
			if ( $_POST[ 'periodo' ] != "" && $_POST[ 'ejercicio' ] != "" ) {
				$mes = ($_POST[ 'periodo' ] % 2) == 0 ? ( $_POST[ 'periodo' ] / 2 ) : ( ( $_POST[ 'periodo' ]+1 ) / 2 ) ;
				$mes = ($mes > 9) ? $mes : '0'.$mes ;
				$mes1 = ($mes > 8) ? ($mes + 1) : '0'.($mes + 1);
				$mes12 = ($mes == 12) ? ($_POST[ 'ejercicio' ] + 1).'/01/01' : $_POST[ 'ejercicio' ].'/'.$mes1.'/01' ;
				$dia = ($_POST[ 'periodo' ] % 2) == 0 ? '16' : '01' ;
				
				$fechaIni = $_POST[ 'ejercicio' ].'/'.$mes.'/'.$dia ;
				$fechaFin = ($_POST[ 'periodo' ] % 2) != 0 ? $_POST[ 'ejercicio' ].'/'.$mes.'/16' : $mes12 ;
				
				$strWhere .= " AND mov.fecha_inicio between '" . $fechaIni . "' and '" . $fechaFin . "' ";
				$whereFecha=" and (fecha between '".$fechaIni."' and '".$fechaFin."' or fecha between '".str_ireplace('/','-', $fechaIni)."' and '".str_ireplace('/','-', $fechaFin)."')";
				}
			else{
				$strWhere = " AND mov.fecha_inicio >= '" . $fechaIni . "' ";
				}
			}
		else{
			$strWhere = " AND mov.fecha_inicio >= '" . $fechaIni . "' ";
			}
		/*	
		$strQuery = "SELECT mov.promotor AS AGENTE, a.NOMBRE AS NOMBREA,'' AS NIVEL, '' AS DIVISION,
						((MONTH('" . $fechaIni ."') - 1) * 2) + IF(DAY('" . $fechaIni ."') < 16, 1 , 2) AS PERIODO,
						YEAR('". $fechaFin . "') AS EJERCICIO, count(distinct mov.idcomentario) AS CUENTAS,
						IFNULL(capt.capturas,0) AS SIDATOS, count(distinct mov.idcomentario) - IFNULL(capt.capturas,0) AS NODATOS,
						IFNULL(tel.capturas,0) AS TEL, (IFNULL(tel.capturas,0) / count(distinct mov.idcomentario)) * 100 AS EFECTITEL,
						IFNULL(corr.capturas,0) AS CORR, (IFNULL(corr.capturas,0) / count(distinct mov.idcomentario)) * 100 AS EFECTIMAIL
					FROM android_reportes.comentarios_cob mov
					JOIN varios.empleados_rh201_intelisis a ON a.CLAVE = mov.promotor
					JOIN(SELECT fecha, agente, count(distinct idcomentario)capturas
						FROM android_reportes.cob_gestiones_correo_tel group by fecha, agente)capt ON capt.agente = mov.promotor
					LEFT JOIN(SELECT fecha, agente, count(distinct idcomentario)capturas
								FROM android_reportes.cob_gestiones_correo_tel
								where tipo like '%tel%' group by fecha, agente)tel ON tel.agente = mov.promotor
					LEFT JOIN(SELECT fecha, agente, count(distinct idcomentario)capturas
								FROM android_reportes.cob_gestiones_correo_tel
								where tipo like '%correo%' group by fecha, agente)corr ON corr.agente = mov.promotor
					WHERE mov.resultado in('aclaraciones','adjudicac ','adjudicac','adjuducac','LCT ','LCT','insolvenc','pago ','pago','promesa ','promesa','promesaAdj')
							".$strWhere."
					GROUP BY mov.promotor, a.NOMBRE, capt.capturas, tel.capturas, corr.capturas";*/
					
					
		$strQuery = "SELECT mov.promotor AS AGENTE, a.NOMBRE AS NOMBREA,'' AS NIVEL, '' AS DIVISION,
						((MONTH('" . $fechaIni ."') - 1) * 2) + IF(DAY('" . $fechaIni ."') < 16, 1 , 2) AS PERIODO,
						YEAR('". $fechaFin . "') AS EJERCICIO, count(distinct mov.idcomentario) AS CUENTAS,
						IFNULL(capt.capturas,0) AS SIDATOS, count(distinct mov.idcomentario) - IFNULL(capt.capturas,0) AS NODATOS,
						IFNULL(tel.capturas,0) AS TEL, (IFNULL(tel.capturas,0) / count(distinct mov.idcomentario)) * 100 AS EFECTITEL,
						IFNULL(corr.capturas,0) AS CORR, (IFNULL(corr.capturas,0) / count(distinct mov.idcomentario)) * 100 AS EFECTIMAIL
					FROM android_reportes.comentarios_cob mov
					Left JOIN varios.empleados_rh201_intelisis a ON a.CLAVE = mov.promotor
					JOIN(SELECT agente, count(distinct idcomentario)capturas
						FROM android_reportes.cob_gestiones_correo_tel where 1=1 ".$whereFecha." group by agente)capt ON capt.agente = mov.promotor
					LEFT JOIN(SELECT  agente, count(distinct idcomentario)capturas
								FROM android_reportes.cob_gestiones_correo_tel
								where tipo like '%tel%' ".$whereFecha." group by agente)tel ON tel.agente = mov.promotor
					LEFT JOIN(SELECT agente, count(distinct idcomentario)capturas
								FROM android_reportes.cob_gestiones_correo_tel
								where tipo like '%correo%' ".$whereFecha." group by agente)corr ON corr.agente = mov.promotor
					WHERE mov.resultado in('aclaraciones','adjudicac ','adjudicac','adjuducac','LCT ','LCT','insolvenc','pago ','pago','promesa ','promesa','promesaAdj')
							".$strWhere."
					GROUP BY mov.promotor, a.NOMBRE";
		
		//echo $strQuery;
		
		$bloq = array(
				"blk1" => $strQuery ,
				"blk2" => empty($arrayPost)? array():$arrayPost,
				"db" => "SELECT intelisis AS DB FROM config LIMIT 1"
				);

		return $bloq;
    	}

    function _repSegVidaCobros() {
		$this->tpl = "gestionescobranza/reporte.segvidacobros.htm";
		$bloq = array();
		$objects = array();
		
		$m = date('m');
		$dia = date('d') < 16 ? '01' : '16';
		$quincena = ( ( $m - 1 ) * 2 ) + (date('d') < 16 ? 1 : 2);
		$ejercicio = date('Y');
		$fechaIni = date('Y').'-'.$m.'-'.$dia ;
		
		$strWhere = " and mov.ejercicio = ".$ejercicio." and mov.periodo = ".$quincena;
		
		if ( !empty( $_POST ) ) {
			$strWhere = "";
			$arrayPost[] = $_POST;
			if ( $_POST[ 'agente' ] != "" ) {
				$strWhere .= " AND mov.nomina = '" . $_POST[ 'agente' ] . "' ";
				}
			if ( $_POST[ 'nivel' ] != "" ) {
				$strWhere .= " AND mov.nivel = '" . $_POST[ 'nivel' ] . "' ";
				}
			if ( $_POST[ 'division' ] != "" ) {
				$strWhere .= " AND divi.division = '" . $_POST[ 'division' ] . "' ";
				}
			if ( $_POST[ 'periodo' ] != "" ) {
				$quincena = $_POST[ 'periodo' ];
				$strWhere .= " AND mov.periodo = '" . $_POST[ 'periodo' ] . "' ";
				}
			if ( $_POST[ 'ejercicio' ] != "" ) {
				$ejercicio = $_POST[ 'ejercicio' ];
				$strWhere .= " AND mov.ejercicio = '" . $_POST[ 'ejercicio' ] . "' ";
				}
			}

		$strQuery = "SELECT mov.nomina AS AGENTE, a.nombre AS NOMBREA, mov.nivel AS NIVEL, IFNULL(divi.division,'Sin Division') AS DIVISION,
					mov.periodo AS PERIODO, mov.ejercicio AS EJERCICIO, count(distinct mov.cuenta) AS CUENTAS, ifnull(cob.cobros,0) AS COBROS,
						round ((( ifnull(cob.cobros,0) / count(distinct mov.cuenta) ) * 100),2) AS EFECTIV
					FROM android_reportes._cuentas_gestioncobranza mov
					LEFT JOIN(SELECT count(cliente) cobros, agente, ejercicio, quincena, nivel_cobranza
							FROM bitacora_gestioncobranza_intelisis.abonos_497d
							WHERE mov LIKE '%seguro%' and nivel_cobranza <> 'AVAL'
							GROUP BY agente, ejercicio, quincena
							)cob ON cob.agente = mov.nomina and cob.ejercicio = mov.ejercicio
								and cob.quincena = mov.periodo and cob.nivel_cobranza = mov.nivel
					LEFT JOIN varios.empleados_rh201_intelisis a ON a.CLAVE = mov.nomina
					LEFT JOIN bitacora_interes.datos_info_c086_intel divi ON divi.cuenta = mov.cuenta and divi.division is not null
					WHERE mov.movimiento LIKE '%seguro%' ".$strWhere."
					GROUP BY mov.nomina, a.nombre, cob.cobros, mov.ejercicio, mov.periodo, mov.nivel, divi.division
					ORDER BY mov.nomina, mov.ejercicio DESC, mov.periodo DESC";

		$bloq = array(
				"blk1" => $strQuery ,
				"blk2" => empty($arrayPost)? array():$arrayPost,
				"db" => "SELECT intelisis AS DB FROM config LIMIT 1"
				);

		return $bloq;
    	}
    //// Parametros Sanciones (Visitas)////
    function _configsansion(){

            //// Envio de Datos ////
            if(!empty($_POST)){

                    //// Modificacion de datos ////
                    $sql1="UPDATE config_sanciones SET ".
                                              ($_POST['visitas']!=''?"visitas='".$_POST['visitas']."',":'').
                                              ($_POST['monto']!=''?"monto='".$_POST['monto']."',":'').
                                              " nivel='".$_POST['nivel']."' WHERE nivel='".$_POST['nivel']."'";

                    //// Ejecucion de query ////
                    mysql_query($sql1);
                    $this->msgaviso="Modificaci&oacute;n realizada con &eacute;xito";
            }

            //// Template ////
            $this->tpl = "gestionescobranza/sanciones/config_sancion.htm";

            //// Consulta de datos ////
            $sql="SELECT puesto, nivel, visitas, monto FROM config_sanciones";
            $select="SELECT nivel FROM config_sanciones";

            //// Consulta TinyButStrong ////
            $bloq = array
                            (
                "post" => empty($_POST)?array():array($_POST),
                "sel"  => $select,
                "blkc" => $sql
            );
    return $bloq;
    }
    //// Reporte Sanciones ////
    function _repsancion(){

            //// Template ////
            $this->tpl = "gestionescobranza/sanciones/reporte_sancion.htm";

            //// Filtros ////

            $fil_quincena  = $_POST['quincena'];
			$fil_nomina    = $_POST['nomina'];
            $fil_nombre    = $_POST['nombre'];
            $fil_nivel     = $_POST['nivel'];

            //// Consulta valor actual /////
            $quincena = "SELECT quincena FROM registros_sanciones";
            $rq_quincena = mysql_query ($quincena);
            $val_quincena = mysql_fetch_assoc($rq_quincena);

            //// Control de Quincena ////
            if($_POST['quincena'] == ''){
                    $val = $val_quincena['quincena'];
					
            }else{
                    $val = $_POST['quincena'];
					
            }
			if($fil_nomina != ''){
                    $where .= " AND reg.nomina = '".$fil_nomina."'";
            }
            if($fil_nombre != ''){
                    $where .= " AND reg.nombre LIKE '%".$fil_nombre."%'";
            }
            if($fil_nivel != ''){
                    $where .= " AND nivel = '".$fil_nivel."'";
            }
			if ($_POST['ejercicio']!=''){
				$ejercicio = $_POST['ejercicio'];
			}else{
				$ejercicio = date ("Y");
			}
			//echo $ejercicio."<br>";
            //// Consulta General ////
            $sql =
                            "SELECT * FROM(
                                    SELECT
                                      reg.id
                                    #, reg.fecha_ingreso AS fecha
                                    , reg.nomina
                                    , reg.nombre
                                    , reg.nivel
                                    , reg.cantidad
                                    , reg.total
                                    , reg.quincena
                                    , reg.historial
									, reg.fecha
                            FROM bitacora_gestioncobranza_intelisis.registros_sanciones reg
                            UNION ALL
                                    SELECT
                                      reg.id
                                    #, reg.fecha_ingreso AS fecha
                                    , reg.nomina
                                    , reg.nombre
                                    , reg.nivel
                                    , reg.cantidad
                                    , reg.total
                                    , reg.quincena
                                    , reg.historial
									, reg.fecha
                            FROM bitacora_gestioncobranza_intelisis.registros_sanciones_respaldo reg) reg
                            LEFT JOIN agentes cat ON cat.nomina=reg.nomina
                            WHERE 1 AND quincena=".$val." ".$where." and SUBSTRING(fecha,1,4)=".$ejercicio." GROUP BY reg.nomina ORDER BY reg.nivel";

            //// Consulta TinyButStrong ////
			//echo $sql;
            $bloq = array
                    (
                    "post" => empty($_POST)?array():array($_POST),
                    "Blk1" => $sql,
                    "sel1" => "SELECT * FROM (SELECT DISTINCT quincena FROM registros_sanciones UNION ALL
                                       SELECT DISTINCT quincena FROM registros_sanciones_respaldo) reg ORDER BY quincena DESC",
                    "sel2" => "SELECT * FROM (SELECT DISTINCT nivel FROM registros_sanciones UNION ALL
                                       SELECT DISTINCT nivel FROM registros_sanciones_respaldo) reg"
                    );
            return $bloq;
    }
    //// Top Sanciones ////
    function _topsancion(){

            //// Template ////
            $this->tpl = "gestionescobranza/sanciones/top_sancion.htm";

            //// Filtros ////
            $fil_nivel   = $_POST['nivel'];
            $fil_nomina  = $_POST['nomina'];
            $fil_nombre  = $_POST['nombre'];

            if($fil_nivel != ''){
                    $where .= "AND age.es LIKE '%".$fil_nivel."%' ";
                    $rango ="";
            }
            if($fil_nomina != ''){
                    $where .= "AND age.nomina = '".$fil_nomina."' ";
                    $rango ="";
            }
            if($fil_nombre != ''){
                    $where .= "AND age.nombre LIKE '%".$fil_nombre."%' ";
                    $rango ="";
            }

            //// Rango Dinamico ////

            $dinamico = $_POST['rango'];

            if($dinamico !=''){
                    $rango = "LIMIT ".$_POST['rango']."";
            }
            else $rango = 'LIMIT 10';

            //// Consulta General ////
            $sql = "SELECT
                                    age.nomina,
                                    age.nombre,
                                    nom.`fec ingreso` AS ingreso,
                                    CASE
                                            WHEN age.celula LIKE '%EQILO%' THEN 'INSTITUCIONES'
                                            WHEN age.celula = 'EQESP05' THEN 'ESPECIAL FORANEO'
                                            WHEN age.celula = 'EQESP06' THEN 'ESPECIAL FORANEO'
                                            WHEN age.celula = 'EQINT05' THEN 'INTERMEDIA FORANEO'
                                            WHEN age.celula = 'EQINT06' THEN 'INTERMEDIA FORANEO'
                                            WHEN age.celula = 'EQPIN05' THEN 'PREINTERMEDIA FORANEO'
                                            WHEN age.celula = 'EQPIN06' THEN 'PREINTERMEDIA FORANEO'
                                            ELSE age.es
                                    END AS nivel,
                                    nom.puesto,
                                    FORMAT(
										CASE
                                            WHEN aut.historial IS NULL
                                            THEN
                                                    SUM(san.sancion)
                                            WHEN san.sancion IS NULL
                                            THEN
                                                    aut.total
                                            ELSE
                                                    SUM(san.sancion) + aut.total
                                    END,2) AS monto,
                                    CASE
                                            WHEN aut.historial IS NULL
                                            THEN
                                                    COUNT(san.sancion)
                                            WHEN san.sancion IS NULL
                                            THEN
                                                    aut.cantidad
                                            ELSE
                                                    COUNT(san.sancion) + aut.cantidad
                                            END AS sanciones,
                                    CASE
                                            WHEN aut.historial IS NULL
                                            THEN
                                                    GROUP_CONCAT(san.fecha, '|',san.categoria,'|S/V|0|', san.sancion, '|')
                                            WHEN san.sancion IS NULL
                                            THEN
                                                    CONCAT(aut.historial)
                                            ELSE
                                                    CONCAT(GROUP_CONCAT(san.fecha,'|',san.categoria,'|S/V|0|', san.sancion, '|'), aut.historial)
                                    END AS historial
                            FROM
                                     (
                                       SELECT * FROM
                                            (SELECT nomina, nombre, es, celula FROM bitacora_gestioncobranza_intelisis.agentes age
                                             UNION ALL
                                             SELECT nomina, nombre, nivel AS es, '' AS celula FROM bitacora_gestioncobranza_intelisis.registros_sanciones aut
                                             ) a GROUP BY nomina
                                     ) age
                            LEFT JOIN
                                     (
                                       SELECT nomina, SUM(cantidad) AS cantidad, SUM(total) AS total, GROUP_CONCAT(historial) AS historial, fecha
                                       FROM (
                                              SELECT * FROM bitacora_gestioncobranza_intelisis.registros_sanciones_respaldo
                                                      UNION ALL
                                              SELECT * FROM bitacora_gestioncobranza_intelisis.registros_sanciones
                                             ) a GROUP BY nomina
                                     ) aut ON age.nomina = aut.nomina
                            LEFT JOIN aplicaciones_web._sanciones_registro san
                            ON age.nomina = san.nomina


                            LEFT JOIN varios.empleados_rh201_intelisis nom
                            ON age.nomina = nom.clave
                            WHERE
                                    CASE
                                            WHEN san.sancion IS NULL
                                            THEN
                                                    aut.fecha BETWEEN ADDDATE(DATE(NOW()), INTERVAL -90 DAY) AND DATE(NOW())
                                            ELSE
                                                    san.fecha BETWEEN ADDDATE(DATE(NOW()), INTERVAL -90 DAY) AND DATE(NOW())
                                            END
                            AND puesto <> 'GERENTE' AND puesto <> 'SUB GERENTE' ".$where."
                            GROUP BY nomina ORDER BY sanciones DESC ".$rango."";
			//echo $sql;

            //// Consulta TinyButStrong ////
            $bloq = array
                    (
                    "post" => empty($_POST)?array():array($_POST),
                    "Blk2" => $sql
                    );
            return $bloq;
    }

	function _repDesagCancelRechaza(){
		$this->tpl = "gestionescobranza/rep.desasignacion.rechazados.cancelados.htm";
		$GLOBALS['path'] = "../template_htm/gestionescobranza";
        $GLOBALS['ev'] = "desag";
	}

	function _repVisitProgram(){
		$this->tpl ="gestionescobranza/reporte.visitas.programadas.htm";
		$GLOBALS['path'] = "../template_htm/gestionescobranza";
	}
    /*Cuentas en Proceso Desasignación*/
    function _desasig(){
        $this->tpl = "gestionescobranza/desasignacion.htm";
        $GLOBALS['path'] = "../template_htm/gestionescobranza";
        $GLOBALS['ev'] = "desag";

        $agente = $_POST['txtAgente'];
        $cuenta = $_POST['txtCuenta'];
        $proceso = $_REQUEST['ddlProceso'];
        $fecha = $_REQUEST['ddlFecha'];

        $where = "";

        if($agente != ''){ $where .= " AND A.AGENTE = '".$agente."' "; }
        if($cuenta != ''){ $where .= " AND A.IDCLIENTE = '".$cuenta."' "; }
        if($proceso != ''){ $where .= " AND A.PROCESO = '".$proceso."' "; }
        if($fecha != ''){ $where .= " AND SUBSTRING(SUBSTRING(A.IDPROCESO, - 18),1,10)  = '".$fecha."' "; }

        //verificamos si se hizo click en el submit mostrar
        if(isset($_POST['btn_mostrar']) || isset($_GET["pg"])){
            //consulta de las cuentas
            $sql = "SELECT 
                    CASE WHEN r2.cuenta is null THEN 
                        CASE 
                            WHEN a.proceso IN('negativa','pago','recado', 'insolvenc', 'verificacion', 'defuncion',
                                'localizcampo' 'A_VERIFICAR', 'VERIFICACION', 'ILOCALIZABLE') THEN rc2.valor_resultado 
                            WHEN a.proceso IN('localizac','promesa','promesaAdj') THEN rc2.pago_quien 
                            WHEN a.proceso IN('Localiz.Campo', 'Loc Insolvencia/Ilocalizable',
                                'Loc Verif1', 'Loc Verif Admin') THEN gs.quien
                            WHEN a.proceso IN('cobroaval') THEN 'AVAL' 
                        ELSE 'N/A' END 
                    ELSE 
                        CASE 
                            WHEN a.proceso IN('negativa','pago','recado', 'insolvenc', 'verificacion', 'defuncion',
                                'localizcampo', 'A_VERIFICAR', 'VERIFICACION', 'ILOCALIZABLE') THEN r2.valor_resultado 
                            WHEN a.proceso IN('localizac','promesa','promesaAdj') THEN r2.pago_quien 
                            WHEN a.proceso IN('Localiz.Campo', 'Loc Insolvencia/Ilocalizable',
                                'Loc Verif1', 'Loc Verif Admin') THEN gs.quien
                            WHEN a.proceso IN('cobroaval') THEN 'AVAL'
                        ELSE 'N/A' END 
                    END AS valor_resultado, 
                    CASE WHEN r2.comentario IS NULL THEN rc2.comentario ELSE r2.comentario END AS comentario ,

                    a.* 

                FROM( 
                    SELECT P.*, MAX(r.id) AS ultimoId, MAX(rc.id) AS ultimoIdmavicob FROM (
                        SELECT A.IDCLIENTE, A.IDPROCESO, A.AGENTE, A.FECHA, A.PROCESO, di.nombre 
                        FROM bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta A 
                            LEFT JOIN varios.empleados_rh201_intelisis di ON di.clave = A.AGENTE 
                        WHERE (A.estatus = 3 or A.estatus =100 or A.estatus =101 ) {$where}
                    ) P 
                        LEFT JOIN bitacora_gestioncobranza_intelisis.registros r ON r.cuenta = P.IDCLIENTE AND r.resultado=P.proceso 
                        LEFT JOIN bitacora_gestioncobranza_intcob.registros rc ON rc.cuenta = P.IDCLIENTE AND rc.resultado=P.proceso 
                    GROUP BY P.IDCLIENTE, P.IDPROCESO,P.AGENTE, P.FECHA ,P.PROCESO ,P.nombre 
                )a 
                    LEFT JOIN bitacora_gestioncobranza_intelisis.registros r2 ON r2.id = a.ultimoId
                    LEFT JOIN bitacora_gestioncobranza_intelisis.cob_desasig_gestion gs ON gs.id_comentario = a.IDPROCESO  
                    LEFT JOIN bitacora_gestioncobranza_intcob.registros rc2 ON rc2.id = a.ultimoIdmavicob 
                ORDER BY a.fecha DESC , a.agente";
        } else{
            $sql = "SELECT '' AS valor_resultado, '' as comentario, '' AS nombre, '' AS FECHA, 
                '' as IDCLIENTE, '' as AGENTE, '' as PROCESO, '' as IDPROCESO";
        }

        $GLOBALS['fecha'] = $fecha;
        $GLOBALS['proceso'] = $proceso;

        $sqlProcesos = "SELECT DISTINCT (PROCESO) AS resultado 
            FROM bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta 
            WHERE estatus = 3  or estatus =100 or estatus =101 ";

        $sqlFechas = "SELECT SUBSTRING(SUBSTRING(IDPROCESO, - 18), 1, 10) AS fecha_inicio
            FROM bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta A
                INNER JOIN (
                    SELECT MAX(replace(replace(substring(IDPROCESO, (select length(IDPROCESO) - 18 + 1), 18), '/', ''), ':', '')) AS divide,
                        idcliente
                    FROM bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta
                    GROUP BY idcliente
                ) B ON B.idcliente = A.idcliente
                    AND B.divide = (
                        SELECT replace(replace(substring(A.IDPROCESO, (SELECT length(A.IDPROCESO) - 18 + 1), 18), '/', ''), ':', '')
                    )
            WHERE A.estatus = 3  or A.estatus =100 or A.estatus =101 GROUP BY fecha_inicio ORDER BY fecha_inicio";

        $this->dts = 30;

        $data = array();
        $aux["nomina"] = $_SESSION["nomina"];
        $aux["user"] = $_SESSION['user'];
        $data[] = $aux;

        $bloque = array(
            'blkProcesos' => $sqlProcesos,
            'blkFechas'   => $sqlFechas,
            'blkUsuario'  => $data,
            'blk'         => $sql
        );

        return $bloque;
    } //fin _desasig()

    function _sancion_desasig_tiempo(){
        $GLOBALS['user'] = $_SESSION['user'];

        $this->tpl = "gestionescobranza/sancion_desasignacion_tiempo.htm";
        $sql = "SELECT SANCION FROM parametro_sancion_desasignacion LIMIT 1";
        $bloque = array('blk' => $sql,);

        return $bloque;
    } //fin _sancion_desasig_tiempo()

	function _desasig_tiempo() {
        $this->tpl = "gestionescobranza/desasignacion_tiempo.htm";
        $GLOBALS['path'] = "../template_htm/gestionescobranza";
        $GLOBALS['ev'] = "desag_tiempo";        

		$agente = $_POST['txtAgente'];
		$cuenta = $_POST['txtCuenta'];		
		$where = "";
		
		if($agente != '')
			$where.=" AND A.AGENTE = '".$agente."' ";
			
		if($cuenta != '')
			$where.= " AND A.IDCLIENTE = '".$cuenta."' ";
				
	
		

        $sql = "
		SELECT *,  CAST(SUBSTRING_INDEX(HORAS, ':', 1) AS DECIMAL) AS ENTERO FROM(
			SELECT 
				CASE WHEN registros.cuenta is null
				THEN
					 CASE
					 WHEN a.proceso in ('negativa','pago','recado') THEN registrosintcob.valor_resultado
					 WHEN a.proceso in ('localizac','promesa','promesaAdj') THEN registrosintcob.pago_quien
					 WHEN a.proceso in ('A_VERIFICAR', 'VERIFICACION', 'ILOCALIZABLE') THEN registrosintcob.valor_resultado
					 WHEN a.proceso in ('cobroaval') THEN 'AVAL'
					 ELSE 'N/A' 
					 END
				ELSE
					 CASE
					 WHEN a.proceso in ('negativa','pago','recado') THEN registros.valor_resultado
					 WHEN a.proceso in ('localizac','promesa','promesaAdj') THEN registros.pago_quien
					 WHEN a.proceso in ('A_VERIFICAR', 'VERIFICACION', 'ILOCALIZABLE') THEN registros.valor_resultado
					 WHEN a.proceso in ('cobroaval') THEN 'AVAL'
					 ELSE 'N/A' 
					 END				
				END AS valor_resultado,
				CASE
					WHEN registros.comentario IS NULL THEN registrosintcob.comentario
					else registros.comentario
				end as comentario,
				di.nombre,
				A . *,
				 CONCAT(A.FECHA, ' ', A.HORA) AS FECHA1,
				CAST(CONCAT(A.FECHA, ' ', A.HORA) AS DATETIME) AS FECHA2,
				NOW(),
				SEC_TO_TIME(TIMESTAMPDIFF(SECOND,
							CAST(CONCAT(A.FECHA, ' ', A.HORA) AS DATETIME),
							NOW())) HORAS
			FROM bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta A
			LEFT JOIN varios.empleados_rh201_intelisis di ON di.clave = A.AGENTE
			INNER JOIN (SELECT 
							MAX(replace(replace(substring(IDPROCESO, (select length(IDPROCESO) - 18 + 1), 18), '/', ''), ':', '')) AS divide,
								idcliente
						 FROM bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta
						 group by idcliente) B ON B.idcliente = A.idcliente
						  and B.divide = (SELECT 
								          replace(replace(substring(A.IDPROCESO,
												(SELECT length(A.IDPROCESO) - 18 + 1),
												18),
											'/',
											''),
										':',
										'')
							)
				LEFT JOIN
				(SELECT id, cuenta, valor_resultado, comentario, pago_quien
				 FROM bitacora_gestioncobranza_intelisis.registros
				 INNER JOIN (select MAX(id) as ultimoId
							from bitacora_gestioncobranza_intelisis.registros
							group by cuenta) ultimoRegistro ON registros.id = ultimoRegistro.ultimoId) registros ON registros.cuenta = A.IDCLIENTE
				LEFT JOIN
				(SELECT id, cuenta, valor_resultado, comentario, pago_quien
				 FROM bitacora_gestioncobranza_intcob.registros
				  INNER JOIN (select MAX(id) as ultimoId
							  from bitacora_gestioncobranza_intcob.registros
							   group by cuenta) ultimoRegistro ON registros.id = ultimoRegistro.ultimoId) registrosintcob ON registrosintcob.cuenta = A.IDCLIENTE
			WHERE
				A.estatus NOT IN (2,100,101) ".$where."			
			ORDER BY fecha DESC , agente
            ) Q WHERE CAST(SUBSTRING_INDEX(Q.HORAS, ':', 1) AS DECIMAL) > 72
        ";
		//echo $sql;
		
		//$fecha = str_replace('/','-',$fecha);	

		$data=array();
		$aux["nomina"]=$_SESSION["nomina"];
		$aux["user"]=$_SESSION['user'];
		
		$data[]=$aux;		
			
        $this->dts = 30;
        $bloque = array( 										
					'blk' => $sql, 
					'blkUsuario'=>$data
				  );
        return $bloque;
    } // fin _desasig_tiempo()

	function _rechazo() {
        $this->tpl = "gestionescobranza/rechazo.htm";
        $GLOBALS['path'] = "../template_htm/gestionescobranza";
        $GLOBALS['ev'] = "rechz";
/*        
        $sql = "SELECT pec.* 
                FROM bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta pec
                INNER JOIN(
                    SELECT MAX( CONCAT( FECHA, ' ', HORA ) ) AS ULTIMO_REG
                    FROM bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta pec
                    GROUP BY IDCLIENTE
                ) B ON B.ULTIMO_REG=CONCAT(pec.FECHA,' ', pec.HORA)  
                WHERE (PROCESO = 'localizac' OR PROCESO = 'cobroaval' OR PROCESO = 'negativa' OR PROCESO = 'defuncion' OR PROCESO = 'insolvenc') AND ESTATUS = 2
                ORDER BY fecha";
*/

		$agente = $_POST['txtAgente'];
		$cuenta = $_POST['txtCuenta'];
		$proceso = $_REQUEST['ddlProceso'];
		$fecha = $_REQUEST['ddlFecha'];
		$where = "";
		
		if($agente != '')
			$where.=" AND A.AGENTE = '".$agente."' ";
			
		if($cuenta != '')
			$where.= " AND A.IDCLIENTE = '".$cuenta."' ";
		
		if($proceso != '')
			$where.="  AND A.PROCESO = '".$proceso."' ";
	  
	    if($fecha != ''){
			
			$where.=" AND SUBSTRING(SUBSTRING(A.IDPROCESO, - 18),1,10)  = '".$fecha."' ";	
			
		}

        $sql = "
            SELECT 
                registros.valor_resultado, A.* 
            FROM bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta A
            INNER JOIN( 
                SELECT 
                    MAX( replace( 
                            replace( 
                                substring( IDPROCESO, 
                                    ( select length( IDPROCESO )-18+1 ), 
                                    18 
                                ), '/', '' 
                            ), ':', ''
                        ) 
                    ) AS divide, idcliente
                FROM bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta
                group by idcliente 
            ) B ON B.idcliente=A.idcliente and B.divide=(
                SELECT replace( 
                            replace( 
                                substring( A.IDPROCESO, 
                                    ( SELECT length( A.IDPROCESO )-18+1 ), 
                                    18 
                                ), '/', '' 
                            ), ':', ''
                        )  
            ) 
            LEFT JOIN(
                SELECT id, cuenta, valor_resultado
                FROM registros 
                INNER JOIN(
                    select MAX(id) as ultimoId from registros
                    group by cuenta
                ) ultimoRegistro ON registros.id = ultimoRegistro.ultimoId
            ) registros ON registros.cuenta=A.IDCLIENTE
            WHERE ( A.PROCESO = 'localizac' OR A.PROCESO = 'cobroaval' OR A.PROCESO = 'negativa' OR A.PROCESO = 'defuncion' OR A.PROCESO = 'insolvenc' or A.PROCESO='VERIFICACION' or A.PROCESO='ILOCALIZABLE' or A.PROCESO='A_VERIFICAR') 
            AND A.estatus = 2  ".$where."          
            ORDER BY fecha DESC, agente       
        ";              
		
		//echo $sql;

		$GLOBALS['fecha'] = $fecha;
		$GLOBALS['proceso']= $proceso;
		
		$sqlProcesos="SELECT DISTINCT
						(PROCESO) AS resultado
					FROM
						bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta
					where
						estatus = 2";

		$sqlFechas=" SELECT 
					  SUBSTRING(SUBSTRING(IDPROCESO, - 18),
												1,
												10) as fecha_inicio
								  
					FROM
						bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta A
							INNER JOIN
						(SELECT 
							MAX(replace(replace(substring(IDPROCESO, (select length(IDPROCESO) - 18 + 1), 18), '/', ''), ':', '')) AS divide,
								idcliente
						FROM
							bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta
						group by idcliente) B ON B.idcliente = A.idcliente
							and B.divide = (SELECT 
								replace(replace(substring(A.IDPROCESO,
												(SELECT length(A.IDPROCESO) - 18 + 1),
												18),
											'/',
											''),
										':',
										'')
							)
							LEFT JOIN
						(SELECT 
							id, cuenta, valor_resultado
						FROM
							registros
						INNER JOIN (select 
							MAX(id) as ultimoId
						from
							registros
						group by cuenta) ultimoRegistro ON registros.id = ultimoRegistro.ultimoId) registros ON registros.cuenta = A.IDCLIENTE
					WHERE
						(A.PROCESO = 'localizac'
							OR A.PROCESO = 'cobroaval'
							OR A.PROCESO = 'negativa'
							OR A.PROCESO = 'defuncion'
							OR A.PROCESO = 'insolvenc' 
							or A.PROCESO='VERIFICACION' 
							or A.PROCESO='ILOCALIZABLE' 
							or A.PROCESO='A_VERIFICAR')
							AND A.estatus = 2
					GROUP BY fecha_inicio
					ORDER BY fecha_inicio";		
					
		//echo "<br><br>".$sqlFechas;
		
		$data=array();
		$aux["nomina"]=$_SESSION["nomina"];
		$aux["user"]=$_SESSION['user'];		
		$data[]=$aux;

        $this->dts = 30;
        $bloque = array( 
					'blkProcesos' => $sqlProcesos,
					'blkFechas' => $sqlFechas,
					'blkUsuario'=>$data,
					'blk' => $sql, 
				  );
        return $bloque;
    } //fin _rechazo()

    function _determinacion(){ 
        $this->tpl = "gestionescobranza/determinacion.htm";
        $GLOBALS['path'] = "../template_htm/gestionescobranza";
        $GLOBALS['ev'] = "deter";

        $agente = $_POST['txtAgente'];
        $cuenta = $_POST['txtCuenta'];
        $proceso = $_REQUEST['ddlProceso'];
        $fecha = $_REQUEST['ddlFecha'];
        $where = "";

        if($agente != ''){ $where .= " AND A.AGENTE = '".$agente."' "; }
        if($cuenta != ''){ $where .= " AND A.IDCLIENTE = '".$cuenta."' "; }
        if($proceso != ''){ $where .= "  AND A.PROCESO = '".$proceso."' "; }
        if($fecha != ''){ $where .= " AND SUBSTRING(SUBSTRING(A.IDPROCESO, - 18),1,10)  = '".$fecha."' "; }

        $sql = "SELECT 
                CASE 
                    WHEN A.proceso IN('Localiz.Campo', 'Loc Insolvencia/Ilocalizable',
                        'Loc Verif1', 'Loc Verif Admin') THEN gs.quien
                ELSE registros.valor_resultado END AS valor_resultado,
                di.nombre, A.*
            FROM bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta A
                LEFT JOIN varios.empleados_rh201_intelisis di on di.clave = A.AGENTE
                LEFT JOIN(
                    SELECT id, cuenta, valor_resultado FROM registros 
                        INNER JOIN(
                            SELECT MAX(id) as ultimoId FROM registros
					        WHERE cuenta IN(SELECT idcliente FROM cob_fichas_procesos_estcuenta WHERE estatus=4)
                            GROUP BY cuenta
                        ) ultimoRegistro ON registros.id = ultimoRegistro.ultimoId
                ) registros ON registros.cuenta=A.IDCLIENTE
                LEFT JOIN bitacora_gestioncobranza_intelisis.cob_desasig_gestion gs ON gs.id_comentario = A.IDPROCESO
            WHERE A.estatus = 4 ".$where." ORDER BY fecha DESC, agente";

        $GLOBALS['fecha'] = $fecha;
        $GLOBALS['proceso'] = $proceso;

        $sqlProcesos = "SELECT DISTINCT (PROCESO) AS resultado 
            FROM bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta 
            WHERE estatus = 4";

        $sqlFechas = "SELECT SUBSTRING(SUBSTRING(IDPROCESO, - 18), 1, 10) AS fecha_inicio 
            FROM bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta A            
                LEFT JOIN(
                    SELECT id, cuenta, valor_resultado FROM registros 
                        INNER JOIN(
                            SELECT MAX(id) AS ultimoId FROM registros
                            WHERE cuenta IN(SELECT idcliente FROM cob_fichas_procesos_estcuenta WHERE estatus=4)
                            GROUP BY cuenta
                        ) ultimoRegistro ON registros.id = ultimoRegistro.ultimoId
                ) registros ON registros.cuenta=A.IDCLIENTE
            WHERE A.estatus = 4
            GROUP BY fecha_inicio ORDER BY fecha_inicio";
        //echo $sql;

        $data = array();
        $aux["nomina"] = $_SESSION["nomina"];
        $aux["user"] = $_SESSION['user'];
        $data[] = $aux;

        $bloque = array(
            'blkProcesos' => $sqlProcesos,
            'blkFechas'   => $sqlFechas,
            'blkUsuario'  => $data,
            'blk'         => $sql
        ); 

        return $bloque;
    }//_determinacion()
    //// Parametros Sanciones ////
    function _configsanctas(){

            //// Envio de Datos ////
            if(!empty($_POST)){

                    //// Modificacion de datos ////
                    $sql1 = "UPDATE config_sanciones SET ".
                                              ($_POST['porcentaje']!=''?"porcentaje='".$_POST['porcentaje']."',":'').
                                              ($_POST['montoc']!=''?"montoc='".$_POST['montoc']."',":'').
                                              " nivel='".$_POST['nivel']."' WHERE nivel='".$_POST['nivel']."'";

                    //// Ejecucion de query ////
                    mysql_query($sql1);

                    //// MEnsaje ////
                    $this->msgaviso="Modificaci&oacute;n realizada con &eacute;xito";
            }

            //// Template ////
            $this->tpl = "gestionescobranza/sanciones/config_sancion_cuentas.htm";

            //// Consulta de datos ////
            $sql="SELECT puesto, nivel, porcentaje, montoc FROM config_sanciones";
            $select="select distinct nivel from
					(
						SELECT distinct TRIM(substring_index(MID(es, 5, 100), '0', 1)) as nivel FROM agentes where nomina=agente 
					union all
						SELECT nivel FROM bitacora_gestioncobranza_intelisis.config_sanciones 
					) as a
					where nivel NOT LIKE 'LOC%' AND nivel NOT LIKE 'LEG%'
					order by nivel";

            //// Consulta TinyButStrong ////
            $bloq = array
                            (
                "post" => empty($_POST)?array():array($_POST),
                "sel"  => $select,
                "blkc" => $sql
				
            );
    return $bloq;
    }
    //// Reporte Sanciones ////
    function _sancioncuentas(){
			
            //// Template ////
            $this->tpl = "gestionescobranza/sanciones/reporte_sancion_cuentas.htm";

            //// Filtros ////

            $fil_nivel     = $_POST['nivel'];
            $fil_nomina     = $_POST['nomina'];
            $fil_nombre    = $_POST['nombre'];
            $fil_quincena  = $_POST['quincena'];
			$fil_ejercicio = $_POST['ejercicio'];


            if($fil_nivel != ''){
                    $where .= " AND nivel = '".$fil_nivel."'";
            }

            if($fil_nomina != ''){
                    $where .= " AND reg.nomina = '".$fil_nomina."'";
            }

            if($fil_nombre != ''){
                    $where .= " AND reg.nombre LIKE '%".$fil_nombre."%'";
            }
			if ($fil_ejercicio!= ''){
				 	$where .= " AND substring(reg.fecha,1,4) = ".$fil_ejercicio;
			}

            //// Consulta valor actual /////
            $quincena = "SELECT quincena FROM registros_sanciones";
            $rq_quincena = mysql_query ($quincena);
            $val_quincena = mysql_fetch_assoc($rq_quincena);

            //// Control de Quincena ////
            if($fil_quincena != ''){
                    $where .=  " and quincena=".$fil_quincena;
            }
			
            //// Consulta General ////
            $sql =
                            "SELECT * FROM(
                                    SELECT
                                      reg.nivel
                                    , reg.nomina
                                    , reg.nombre
                                    , reg.asignadas
                                    , reg.x_visitar
                                    , reg.monto
                                    , reg.quincena
                                    , reg.historial
									, reg.fecha
                            FROM bitacora_gestioncobranza_intelisis.registros_sanciones_cuentas reg
                            UNION ALL
                                    SELECT
                                      reg.nivel
                                    , reg.nomina
                                    , reg.nombre
                                    , reg.asignadas
                                    , reg.x_visitar
                                    , reg.monto
                                    , reg.quincena
                                    , reg.historial
									, reg.fecha
                            FROM bitacora_gestioncobranza_intelisis.registros_sanciones_cuentas_respaldo reg
							UNION ALL
									SELECT
									  reg.nivel
									, agente_asociado as nomina
									, concat('Agente Doble de ', reg.nombre) as nombre
									, reg.asignadas
									, reg.x_visitar
									, reg.monto
									, reg.quincena
									, reg.historial
									, reg.fecha
							FROM  (
							select agente_origen, agente_asociado from bitacora_gestioncobranza_intelisis.agente_doble
							group by agente_origen, agente_asociado) as ad
							left join bitacora_gestioncobranza_intelisis.registros_sanciones_cuentas reg on reg.nomina=ad.agente_origen
							UNION ALL
									SELECT
									  reg.nivel
									, agente_asociado as nomina
									, concat('Agente Doble de ', reg.nombre) as nombre
									, reg.asignadas
									, reg.x_visitar
									, reg.monto
									, reg.quincena
									, reg.historial
									, reg.fecha
							FROM  (
							select agente_origen, agente_asociado from bitacora_gestioncobranza_intelisis.agente_doble
							group by agente_origen, agente_asociado) as ad
							left join bitacora_gestioncobranza_intelisis.registros_sanciones_cuentas_respaldo reg on reg.nomina=ad.agente_origen
							
							) reg
                            LEFT JOIN agentes cat ON cat.nomina=reg.nomina
                            WHERE 1 ".$where." GROUP BY reg.nomina ORDER BY reg.nivel";
			//echo $sql;
							
            //// Consulta TinyButStrong ////
            $bloq = array
                    (
                    "post" => empty($_POST)?array():array($_POST),
                    "Blk1" => $sql,
                    "sel1" => "SELECT * FROM (SELECT DISTINCT quincena FROM registros_sanciones_cuentas UNION ALL
                                       SELECT DISTINCT quincena FROM registros_sanciones_cuentas_respaldo) reg ORDER BY quincena DESC",
                    /*"sel2" => "SELECT * FROM (SELECT DISTINCT nivel FROM registros_sanciones_cuentas UNION ALL
                                       SELECT DISTINCT nivel FROM registros_sanciones_cuentas_respaldo) reg"*/
					"sel2" => "select distinct nivel from
								(
									SELECT distinct TRIM(substring_index(MID(es, 5, 100), '0', 1)) as nivel FROM agentes where nomina=agente 
								union all
									SELECT nivel FROM bitacora_gestioncobranza_intelisis.config_sanciones 
								) as a
								where nivel NOT LIKE 'LOC%' AND nivel NOT LIKE 'LEG%'
								order by nivel",
					 "sel3" => "SELECT distinct reg.fec FROM(
											SELECT
											substring(reg.fecha,1,4) as fec
									FROM bitacora_gestioncobranza_intelisis.registros_sanciones_cuentas reg
									UNION ALL
											SELECT
											 substring(reg.fecha,1,4)as fec
									FROM bitacora_gestioncobranza_intelisis.registros_sanciones_cuentas_respaldo reg) reg
								order by reg.fec"
                    );
            return $bloq;
			
    }

    function _corrsDsag() {
            $this->tpl = "gestionescobranza/sanciones/config_correos.htm";
			
			$sql = 
				"
					SELECT
						correo
					FROM
					directorio_correos
				";
				
			if($_POST['correo']!=''){
				$agregar = 
					"
						INSERT INTO
							directorio_correos
							(correo)
						VALUES
							('".$_POST['correo']."')
					";
				mysql_query($agregar);
			}
			if($_GET['correo']!=''){
				$eliminar = 
					"
						DELETE 
						FROM
							directorio_correos
						WHERE
							correo = '".$_GET['correo']."'
					";
				mysql_query($eliminar);
			}
			
			$bloq = array
                    (
                    "post" => empty($_POST)?array():array($_POST),
                    "Blk1" => $sql
                    );
            return $bloq;
    }

    function _ReciboEnt(){
        $this->tpl = "recibofolioconvenio/entregar.htm";
        $data = array();
        $aux['date'] = date('Y')."-".date('m')."-".date('d');
        $aux['nomina'] = $_SESSION['nomina'];
        $data[] = $aux;
        $arrayBlock = array("blk1" => $data);

        return $arrayBlock;
    }

    function _ConvEnt(){
        $this->tpl = "recibofolioconvenio/entregac.htm";
        $data = array();
        $aux['date'] = date('Y')."-".date('m')."-".date('d');
        $aux['nomina'] = $_SESSION['nomina'];
        $data[] = $aux;
        $arrayBlock = array("blk1" => $data);

        return $arrayBlock;
    }

    function _ReciboRev(){
        $this->tpl = "recibofolioconvenio/revisionr.htm";
        $data = array();
        $aux['date'] = date('Y' )."-".date( 'm' )."-".date( 'd' );
        $aux['nomina'] = $_SESSION['nomina'];
        $data[] = $aux;
        $GLOBALS['nomina_recibe'] = $_SESSION['nomina'];
        $GLOBALS['nombre_recibe']=  $_SESSION['nombre'];
        $arrayBlock = array ("blk1" => $data );

        return $arrayBlock;
    }

    function _ConvRev(){
        $this->tpl = "recibofolioconvenio/revisionc.htm";
        $data = array();
        $aux['date'] = date('Y' )."-".date( 'm' )."-".date( 'd' );
        $aux['nomina'] = $_SESSION[ 'nomina' ];
        $data[] = $aux;
        $GLOBALS['nomina_recibe'] = $_SESSION['nomina'];
        $GLOBALS['nombre_recibe']=  $_SESSION['nombre'];
        $arrayBlock = array ("blk1" => $data );

        return $arrayBlock;
    }

    function _ReciboCap(){
        $this->tpl = "recibofolioconvenio/capturar.htm";
        $data = array();
        $aux['date'] = date('Y')."-".date('m')."-".date('d');
        $aux['nomina'] = $_SESSION['nomina'];
        $data[] = $aux;
        $arrayBlock = array("blk1" => $data);

        return $arrayBlock;
    }

    function _ConvCap(){
        $this->tpl = "recibofolioconvenio/capturac.htm";
        $GLOBALS['nomina'] = $_SESSION['nomina'];
        $data = array();
        $aux['date'] = date('Y')."-".date('m')."-".date('d');
        $aux['nomina'] = $_SESSION['nomina'];
        $data[] = $aux;
        $convenios = "SELECT * FROM folios.folios_convenios 
            WHERE (F_entrega IS NOT NULL OR F_entrega <> '') AND Entregado = 'SI' AND Capturado = 'NO' AND Reasignar = 'NO'";
        $arrayBlock = array("blk1" => $data, "conv" => $convenios);

        return $arrayBlock;
    }

    function _ReciboRea(){
        $this->tpl = "recibofolioconvenio/reasignacionr.htm";
        $data = array();
        $aux['date'] = date('Y') . "-" . date('m') . "-" . date('d');
        $aux['date2'] = $this->myWeek(date('D')).", ".date('d')." de ".$this->myMonth(date('m'))." de ".date('Y');
        $aux['nomina'] = $_SESSION['nomina'];
        $data[] = $aux;
        $arrayBlock = array("blk1" => $data);

        return $arrayBlock;
    }

    function _ConvRea(){
        $this->tpl = "recibofolioconvenio/reasignacionc.htm";
        $data = array();
        $aux['date'] = date('Y') . "-" . date('m') . "-" . date('d');
        $aux['date2'] = $this->myWeek(date('D')).", ".date('d')." de ".$this->myMonth(date('m'))." de ".date('Y');
        $aux['nomina'] = $_SESSION['nomina'];
        $data[] = $aux;
        $arrayBlock = array("blk1" => $data);

        return $arrayBlock;
    }

    function _ReciboCon( ) {
            $this->tpl = "recibofolioconvenio/consultar.htm";
            $data = array( );
            $aux[ 'date' ] = date( 'Y' ) . "-" . date( 'm' ) . "-" . date( 'd' );
            $aux[ 'date2' ] = $this->myWeek( date( 'D' ) ) . ", " . date( 'd' ) . " de " . $this->myMonth( date( 'm' ) ) . " de " . date( 'Y' );
            $aux[ 'nomina' ] = $_SESSION[ 'nomina' ];
            $data[ ] = $aux;
            $arrayBlock = array ( "blk1"     => $data );
            return $arrayBlock;
    }

    function _ConvCon( ) {
            $this->tpl = "recibofolioconvenio/consultac.htm";
            $data = array( );
            $aux[ 'date' ] = date( 'Y' ) . "-" . date( 'm' ) . "-" . date( 'd' );
            $aux[ 'date2' ] = $this->myWeek( date( 'D' ) ) . ", " . date( 'd' ) . " de " . $this->myMonth( date( 'm' ) ) . " de " . date( 'Y' );
            $aux[ 'nomina' ] = $_SESSION[ 'nomina' ];
            $data[ ] = $aux;
            $arrayBlock = array ( "blk1"     => $data );
            return $arrayBlock;
    }

    function _ReciboCor( ) {
            $this->tpl = "recibofolioconvenio/correccionr.htm";
            $data = array( );
            $aux[ 'date' ] = date( 'Y' ) . "-" . date( 'm' ) . "-" . date( 'd' );
            $aux[ 'date2' ] = $this->myWeek( date( 'D' ) ) . ", " . date( 'd' ) . " de " . $this->myMonth( date( 'm' ) ) . " de " . date( 'Y' );
            $aux[ 'nomina' ] = $_SESSION[ 'nomina' ];
            $data[ ] = $aux;
            $arrayBlock = array ( "blk1"     => $data );
            return $arrayBlock;
    }

    function _ConvCor( ) {
            $this->tpl = "recibofolioconvenio/correccionc.htm";
            $data = array( );
            $aux[ 'date' ] = date( 'Y' ) . "-" . date( 'm' ) . "-" . date( 'd' );
            $aux[ 'date2' ] = $this->myWeek( date( 'D' ) ) . ", " . date( 'd' ) . " de " . $this->myMonth( date( 'm' ) ) . " de " . date( 'Y' );
            $aux[ 'nomina' ] = $_SESSION[ 'nomina' ];
            $data[ ] = $aux;
            $arrayBlock = array ( "blk1"     => $data );
            return $arrayBlock;
    }

    function _ReciboHis( ) {
            $this->tpl = "recibofolioconvenio/historicar.htm";
            $data = array( );
            $aux[ 'date' ] = date( 'Y' ) . "-" . date( 'm' ) . "-" . date( 'd' );
            $aux[ 'date2' ] = $this->myWeek( date( 'D' ) ) . ", " . date( 'd' ) . " de " . $this->myMonth( date( 'm' ) ) . " de " . date( 'Y' );
            $aux[ 'nomina' ] = $_SESSION[ 'nomina' ];
            $data[ ] = $aux;
            $arrayBlock = array ( "blk1"     => $data );
            return $arrayBlock;
    }

    function _ConvHis( ) {
            $this->tpl = "recibofolioconvenio/historicac.htm";
            $data = array( );
            $aux[ 'date' ] = date( 'Y' ) . "-" . date( 'm' ) . "-" . date( 'd' );
            $aux[ 'date2' ] = $this->myWeek( date( 'D' ) ) . ", " . date( 'd' ) . " de " . $this->myMonth( date( 'm' ) ) . " de " . date( 'Y' );
            $aux[ 'nomina' ] = $_SESSION[ 'nomina' ];
            $data[ ] = $aux;
            $arrayBlock = array ( "blk1"     => $data );
            return $arrayBlock;
    }

	function _BuscadorDocumentos( ) {
            $this->tpl = "buscador.documentos.htm";
	}

	function _DocumentosActivos( ) {
            $this->tpl = "documentos.activos.htm";
	}

	function _ReportePagoGestiones( ) {
            $this->tpl = "reporte.pago.gestiones.htm";
	}

	function _ReporteCuentasProceso( ) {
            $this->tpl = "reporte.cuentas.proceso.htm";
	}

    function myWeek( $day ) {
            $strDay = "";
            switch ( $day ) {
                case "Sun":$strDay = " Domingo ";break;
                case "Tue":$strDay = " Martes ";break;
                case "Thu":$strDay = " Jueves ";break;
                case "Sat":$strDay = " Sabado ";break;
                case "Mon":$strDay = " Lunes ";break;
                case "Wed":$strDay = " Miercoles ";break;
                case "Fri":$strDay = " Viernes ";break;
            }
            return $strDay;
    }

    function myMonth( $month ) {
            $strMonth = "";
            switch ( $month ) {
                case 1:$strMonth = " Enero ";break;
                case 2:$strMonth = " Febrero ";break;
                case 3:$strMonth = " Marzo ";break;
                case 4:$strMonth = " Abril ";break;
                case 5:$strMonth = " Mayo ";break;
                case 6:$strMonth = " Junio ";break;
                case 7:$strMonth = " Julio ";break;
                case 8:$strMonth = " Agosto ";break;
                case 9:$strMonth = " Septiembre ";break;
                case 10:$strMonth = " Octubre ";break;
                case 11:$strMonth = " Noviembre ";break;
                case 12:$strMonth = " Diciembre ";break;
            }
            return $strMonth;
    }
    //// Reporte Sanciones ////
    function _ctasdesag(){

            //// Template ////
            $this->tpl = "gestionescobranza/sanciones/reporte_cuentas_desasignar.htm";
            //// Filtros ////
            $fil_proceso  = $_POST['proceso'];
            if($fil_proceso != ''){
                    //$where .= "AND proceso = '".$fil_proceso."'";
					       
            }
			
			$agente = $_POST['txtAgente'];
			$cuenta = $_POST['txtCuenta'];
			$proceso = $_REQUEST['ddlProceso'];
			$fecha = $_REQUEST['ddlFecha'];
			$where = "";
			
			if($agente != '')
				$where.=" AND est.AGENTE = '".$agente."' ";
				
			if($cuenta != '')
				$where.= " AND est.IDCLIENTE = '".$cuenta."' ";
			
			if($proceso != '')
				$where.="  AND est.PROCESO = '".$proceso."' ";
		  
			if($fecha != ''){
				
				$where.=" AND SUBSTRING(SUBSTRING(est.IDPROCESO, - 18),1,10)  = '".$fecha."' ";	
			}

			
            //// Consulta General ////
            
           	$sql = "
					SELECT 
						est.idproceso AS idproceso,
						est.idcliente AS codigo,
						reg.nombre AS cliente,
						reg.domicilio AS direccion,
						reg.colonia AS colonia,
						reg.ciudad AS delegacion,
						est.proceso AS proceso,
						reg.valor_resultado
					FROM cob_fichas_procesos_estcuenta est
					left JOIN registros reg ON est.idcliente = reg.cuenta and reg.resultado=est.proceso
					WHERE estatus = 5 ".$where." GROUP BY idcliente 
					";	
			//echo $sql;
                   
			$total =
					"
					SELECT 
						   SUM(localizac) AS localizacion,
						   SUM(cobroaval) AS cobroaval,
						   SUM(legal) AS legal,
						   SUM(defuncion) AS defuncion,
						   SUM(insolvencia) AS insolvencia,
						   SUM(negativa) AS negativa,
						   SUM(localizac) + SUM(cobroaval) + SUM(legal) + SUM(defuncion) + SUM(insolvencia)+SUM(negativa) AS total
					FROM
						(SELECT
								CASE WHEN proceso = 'localizac' THEN 1 ELSE 0 END AS localizac,
								CASE WHEN proceso = 'cobroaval' THEN 1 ELSE 0 END AS cobroaval,
								CASE WHEN proceso = 'legal' THEN 1 ELSE 0 END AS legal,
								CASE WHEN proceso = 'defuncion' THEN 1 ELSE 0 END AS defuncion,
								CASE WHEN proceso = 'insolvenc' THEN 1 ELSE 0 END AS insolvencia,
								CASE WHEN proceso = 'negativa' THEN 1 ELSE 0 END AS negativa
					FROM cob_fichas_procesos_estcuenta WHERE estatus = 5 ) reg
					";


			$GLOBALS['fecha'] = $fecha;
			$GLOBALS['proceso']= $proceso;

			$sqlProcesos="SELECT DISTINCT
							(proceso) as resultado
						FROM
							cob_fichas_procesos_estcuenta
						WHERE
							estatus = 5";
							
			$sqlFechas=" SELECT 
							SUBSTRING(SUBSTRING(IDPROCESO, - 18),
								1,
								10) AS fecha_inicio
						FROM
							cob_fichas_procesos_estcuenta
						WHERE
							estatus = 5
						group by fecha_inicio order by fecha_inicio; ";
            //// Consulta TinyButStrong ////
			
			$data=array();
			$aux["nomina"]=$_SESSION["nomina"];
			$aux["user"]=$_SESSION['user'];
			
			$data[]=$aux;
			
			
            $bloq = array
                    (
                    "post"  => empty($_POST)?array():array($_POST),
                    "blk1"  => $sql,
					'blkProcesos' => $sqlProcesos,
					'blkFechas' => $sqlFechas,
					'blkUsuario'=>$data,
					"total" => $total,
                    "sel1"  => "SELECT DISTINCT proceso FROM cob_fichas_procesos_estcuenta WHERE estatus = 5"
                    );
		
            return $bloq;
			
    } // Fin _ctasdesag()
    /*Reporte Cuentas Desasignadas*/
    function _desasignadas(){
		$this->tpl = "gestionescobranza/sanciones/reporte_cuentas_desasignadas.htm";
		$Limit=" Limit 0";
		if(!empty($_POST))
			$Limit="";
		
		$fil_fecha  = $_POST['fecha'];

		if($fil_fecha != ''){
				$where .= "AND pe.fecha = '".$fil_fecha."'";
		}
		
		//// Consulta General ////
		
		$sql="Select 
    		ps.idcliente as codigo,
    		case when (reg.nombre!='') then reg.nombre else re.nombre end AS cliente,
    		case when (reg.domicilio!='') then	reg.domicilio else re.domicilio end AS direccion,
    		case when (reg.colonia!='') then reg.colonia else re.colonia end AS colonia,
    		case when (reg.ciudad!='') then reg.ciudad else re.ciudad end AS delegacion,
    				CASE
    					 WHEN ce.proceso in ('negativa','pago','recado') THEN case when (reg.valor_resultado!='') then reg.valor_resultado 
    					     else re.valor_resultado end
    					 WHEN ce.proceso in ('localizac','promesa','promesaAdj') THEN case when (reg.pago_quien!='') then reg.pago_quien 
    					     else 	re.pago_quien end 
    					 WHEN ce.proceso in ('cobroaval') THEN 'AVAL'
    					 ELSE 'N/A' 
    					 END as	valor_resultado,
    					ce.proceso, ce.fecha, cd.Nivel
    		From
    		(
    		    Select pe.idcliente  , Max(d.id) id, Max(r.id) idgest, r.nombre, r.domicilio, r.colonia, r.ciudad
        		from cob_fichas_procesos_estcuenta pe
        		JOIN cuentas_desasig_procesonivel d ON d.idproceso = pe.idproceso
        		LEFT JOIN registros r on r.cuenta = pe.idcliente and r.resultado in('localizac')
        		Where pe.estatus = 101 ".$where."
        		Group by pe.idcliente
    		)ps
    		JOIN cuentas_desasig_procesonivel cd on cd.id = ps.id
    		JOIN cob_fichas_procesos_estcuenta ce ON cd.idproceso = ce.idproceso
    		left join registros reg on ps.idcliente = reg.cuenta  and  
						cd.idproceso = concat(reg.promotor,reg.cuenta,replace(fecha_inicio,'-','/'), reg.hora_final) 
		left join registros re on ps.idcliente = re.cuenta and ps.idgest=re.id";

			

				
		//// Consulta TinyButStrong ////
		$GLOBALS['fecha']=$fil_fecha;
		$bloq = array
				(
				"post" => empty($_POST)?array():array($_POST),
				"blk1" => $sql,
				"sel1" => "SELECT DISTINCT fecha FROM cob_fichas_procesos_estcuenta WHERE estatus = 101 order by fecha desc"
				);
		
			return $bloq;
    }

    function _catalogodoc( ) {
            $this->tpl = "gestionescobranza/catalogo_documentos.htm";
            $object = array( );
    		$strQuery = "SELECT * FROM bitacora_gestioncobranza_intelisis.cob_procesos_catalogodoc WHERE ACTIVO = 1";
    		$answer = mysql_query( $strQuery );
    		while ( $row = mysql_fetch_assoc( $answer ) ) {
    				$object[ ] = $row;
    		}
            $bloq = array(
            			'blk1' => $object
            );
		
            	return $bloq;
			
    }
    //// Parametros ////
    function _parametros(){
	
		//// Template ////
		$this->tpl="gestionescobranza/administrador_cartera/parametros.htm";		
		
		$sql_parametros =
			"
			SELECT
				id,
				concepto,
				criterio,
				sancion,
				base,
				nivel,
				puntos 
			FROM 
			parametros
			";

		//// Consulta TinyButStrong ////
        $bloq = array
				(
                    "post"  => empty($_POST)?array():array($_POST),
                    "blk1"  => $sql_parametros
                );
        return $bloq;		
	}

    function _editar_parametro(){
		//// Template ////
		$this->tpl="gestionescobranza/administrador_cartera/editar.htm";
		
		if($_POST['id_edit']!=''){
			$sql_update = 
			"
			UPDATE parametros
			SET
			concepto = '".$_POST['concepto_edit']."',
			criterio = '".$_POST['criterio_edit']."',
			sancion  = ".$_POST['sancion_edit'].",
			base     = '".$_POST['base_edit']."',
			nivel    = '".$_POST['nivel_edit']."',
			puntos   = ".$_POST['puntos_edit']."
			WHERE id = ".$_POST['id_edit']."
			";
			mysql_query($sql_update);
			
			//// Template ////
			$this->tpl="gestionescobranza/administrador_cartera/parametros.htm";		
			
			$sql_parametros =
				"
				SELECT
					id,
					concepto,
					criterio,
					sancion,
					base,
					nivel,
					puntos 
				FROM 
				parametros
				";
	
			//// Consulta TinyButStrong ////
			$bloq = array
					(
						"post"  => empty($_POST)?array():array($_POST),
						"blk1"  => $sql_parametros
					);
			return $bloq;			
		}
		
		$sql_editar =
			"
			SELECT * FROM parametros WHERE id = ".$_GET['id']."
			";

		//// Consulta TinyButStrong ////
		$bloq = array
			(
				"post"  => empty($_POST)?array():array($_POST),
				"blkc"  => $sql_editar
			);
		return $bloq;
	}

    function _borrar_parametro(){
		if($_GET['id']!=''){			
			$borrar =
				"
				DELETE FROM parametros WHERE id = ".$_GET['id']."
				";
			mysql_query($borrar);
			
			//// Template ////
			$this->tpl="gestionescobranza/administrador_cartera/parametros.htm";
			
			$sql_parametros =
			"
			SELECT
				id,
				concepto,
				criterio,
				sancion,
				base,
				nivel,
				puntos 
			FROM 
			parametros
			";

			//// Consulta TinyButStrong ////
			$bloq = array
				(
                    "post"  => empty($_POST)?array():array($_POST),
                    "blk1"  => $sql_parametros
                );
       		return $bloq;
			
		}
	}

    function _registrar_parametro(){
		$registrar =
			"
			INSERT INTO parametros  
			(
				concepto,
				criterio,
				sancion,
				base,
				nivel,
				puntos
			)
			VALUES
			(
				'".$_POST['concepto']."',
				'".$_POST['criterio']."',
				 ".$_POST['sancion']." ,
				'".$_POST['base']."',
				'".$_POST['nivel']."',
				 ".$_POST['puntos']." 
			)
			";
		mysql_query($registrar);
		
		//// Template ////
		$this->tpl="gestionescobranza/administrador_cartera/parametros.htm";
		
		$sql_parametros =
		"
		SELECT
			id,
			concepto,
			criterio,
			sancion,
			base,
			nivel,
			puntos 
		FROM 
		parametros
		";

		//// Consulta TinyButStrong ////
		$bloq = array
			(
				"post"  => empty($_POST)?array():array($_POST),
				"blk1"  => $sql_parametros
			);
		return $bloq;
	}

    function _revision_cartera(){
		//// Template ////
		$this->tpl="gestionescobranza/administrador_cartera/revision_cartera.htm";	
			
		$selxx =	
			"
				SELECT
					id,
					nombre_tabla
				FROM tbls_creadas_bgc where id!=1
			";

		$bloq = array
				(
					"selx" => $selxx
				);
		return $bloq;	
	}

    function _creacion_tbl_sanciones(){
		$this->tpl="gestionescobranza/administrador_cartera/configuracion_de_conceptos_abc.php";
		$tabla=trim($_POST['crear_tbl']);
		
				
		$checar=
			"
			Select 
				distinct liga
			From 
				parametros where liga='".$tabla."'
			";
			
		$res=mysql_query($checar);
		$row=mysql_num_rows($res);
		
		if($row==0)
		{
		
		
		if(isset($_POST["agregar"]) && !empty($tabla)) //////////////////////////////////creacion tabla
		{
//			$tabla=$_POST['crear_tbl'];
			
			$tot_reg =
				"
				Select count(id)+1 reg_new From parametros
				";
				
			$res=mysql_query($tot_reg);
			$data=mysql_fetch_array($res);	
			$registro=$data["reg_new"];
			
			$agregar_01 =
				"
				Insert Into parametros values (1,'ACTUALIZACION DE GESTIONES','SE DEBE TENER ACTUALIZADO LAS GESTIONES QUE SE CAPTURAN EN TABLET,DE NO TENER TABLET DEBEN CAPTURARSE EN LA WEB.',0,'CUENTA','GRAVE',0,'GESTOR',0,0,0,'".$tabla."');
				";
				mysql_query($agregar_01) or die(mysql_error());
				$registro=$registro +1;
			$agregar_02 =
				"
				Insert Into parametros values (2,'CONOCIMIENTO DE CUOTAS','DEBERA CONOCER A DETALLE LAS CUOTAS QUE TENGA DE ACUERDO AL NIVEL DE COBRANZA CORRESPONDIENTE',0,'CUENTA','LEVE',0,'GESTOR',0,0,0,'".$tabla."');
				";
				mysql_query($agregar_02) or die(mysql_error());
				$registro=$registro +1;
			$agregar_03 =
				"				
				Insert Into parametros values (3,'VISITAS DIARIAS','EL NUMERO DE CUENTAS VISITADAS DEBE SER UN MINIMO DE XX POR DIA.',0,'CUENTA','LEVE',0,'GESTOR',0,0,0,'".$tabla."');
				";
				mysql_query($agregar_03) or die(mysql_error());
				$registro=$registro +1;
			$agregar_04 =
				"				
				Insert Into parametros values (4,'CUENTAS CONTACTO DIRECTO','EL PORCENTAJE DE CONTACTO DIRECTO DEBE SER MINIMO DEL 50%.',0,'CUENTA','LEVE',0,'GESTOR',0,0,0,'".$tabla."');
				";
				mysql_query($agregar_04) or die(mysql_error());
				$registro=$registro +1;
			$agregar_05 =
				"				
				Insert Into parametros values (5,'INVESTIGACION EN RESULTADOS SIN CONTACTO','INVESTIGACION VECINAL PARA IDENTIFICAR SI CLIENTE VIVE AUN EN DOMICILIO O EL POSIBLE HORARIO DE CONTACTO',0,'CUENTA','LEVE',0,'GESTOR',0,0,0,'".$tabla."');
				";
				mysql_query($agregar_05) or die(mysql_error());
				$registro=$registro +1;
			$agregar_06 =
				"				
				Insert Into parametros values (6,'PROMESAS DE PAGO','DEBE CUMPLIRSE CON PAGO EN EFECTIVO EL 50% DE LAS PROMESAS REALIZADAS EN LA QUINCENA INMEDIATA ANTERIOR.',0,'CUENTA','LEVE',0,'GESTOR',0,0,0,'".$tabla."');
				";
				mysql_query($agregar_06) or die(mysql_error());
				$registro=$registro +1;
			$agregar_07 =
				"				
				Insert Into parametros values (7,'INCONGRUENCIA EN RESULTADO DE GESTION','INFORMACION OTORGADA POR EL GESTOR NO COINCIDE CON EL RESULTADO DE GESTION ACTUAL O CON ANTERIORES INJUSTIFICADAMENTE.',0,'CUENTA','GRAVE',0,'GESTOR',0,0,0,'".$tabla."');
				";
				mysql_query($agregar_07) or die(mysql_error());
				$registro=$registro +1;
			$agregar_08 =
				"				
				Insert Into parametros values (8,'CUENTAS A DESASIGNAR','CUANDO RESULTADO DE GESTION SEA UN PROCESO ADMINISTRATIVO Y NO SE HAYA REALIZADO (A LOCALIZAR, NEGATIVA DE PAGO, COBRO AL AVAL, CANCELACION DE SEGURO Y/O ACLARACIONES).',0,'CUENTA','LEVE',0,'GESTOR',0,0,0,'".$tabla."');
				";
				mysql_query($agregar_08) or die(mysql_error());
				$registro=$registro +1;
			$agregar_09 =
				"				
				Insert Into parametros values (9,'ENRUTADO','LA RUTA TRAZADA EN GPS DEBE SER SE SECUENCIAL Y LOGICA DE ACUERDO A LAS DISTANCIAS ENTRE LOS DOMICILIOS DE LAS CUENTAS.',0,'CUENTA','LEVE',0,'GESTOR',0,0,0,'".$tabla."');
				";
				mysql_query($agregar_09) or die(mysql_error());
				$registro=$registro +1;
			$agregar_10 =
				"				
				Insert Into parametros values (10,'GPS','LA RUTA TRAZADA EN GPS DEBE COINCIDIR CON LOS DOMICILIOS DE LAS CUENTAS REPORTADAS COMO VISITADAS.',0,'CUENTA','LEVE',0,'GESTOR',0,0,0,'".$tabla."');
				";
				mysql_query($agregar_10) or die(mysql_error());
				$registro=$registro +1;
			$agregar_11 =
				"				
				Insert Into parametros values (11,'CUENTAS PRIORIDAD','VALIDACION DE VISITAS Y COBROS A LAS CUENTAS PRIORIDAD, ASIGNADAS AL GESTOR. SE DEBE VISITAR EL TOTAL DE ESTAS CUENTAS.',0,'CUENTA','LEVE',0,'GESTOR',0,0,0,'".$tabla."');
				";
				mysql_query($agregar_11) or die(mysql_error());
				$registro=$registro +1;
			$agregar_12 =
				"				
				Insert Into parametros values (12,'CUENTAS ASIGNADAS CON MAS DE 30 DIAS SIN GESTION','REVISION DE CUENTAS CON MAS DE 30 DIAS SIN GESTION. LAS CUALES SE SANCIONARAN SI EL GESTOR NO A REALIZADO VISITA.',0,'CUENTA','LEVE',0,'GESTOR',0,0,0,'".$tabla."');
				";
				mysql_query($agregar_12) or die(mysql_error());
				$registro=$registro +1;
			$agregar_13 =
				"				
				Insert Into parametros values (13,'COBERTURA DE CARTERA','DE LAS CUENTAS ASIGNADAS AL INICIO DEL PERIODO, SE DEBERA TENER UNA COBERTURA AL RESPECTO AL TOTAL, DE LO CONTRARIO SE APLICARA SANCION.',0,'CUENTA','LEVE',0,'GESTOR',0,0,0,'".$tabla."');
				";
				mysql_query($agregar_13) or die(mysql_error());
				$registro=$registro +1;
			$agregar_14 =
				"				
				Insert Into parametros values (14,'INASISTENCIA A REVISION','EL GESTOR DEBERA ASISTIR A REVISION DE CARTERA PARA LA EVALUACION CORRESPONDIENTE.',0,'CUENTA','LEVE',0,'GESTOR',0,0,0,'".$tabla."');					
				";
				mysql_query($agregar_14) or die(mysql_error());	
				
			$tabla1=$_POST['crear_tbl'];
			
			$tot_regts =
				"
				Select count(id)+1 reg_new1 From tbls_creadas_bgc
				";
				
			$res1=mysql_query($tot_regts);
			$data1=mysql_fetch_array($res1);	
			$registro1=$data1["reg_new1"];
			
		$agregar_indice =
			"
			Insert Into tbls_creadas_bgc values (".$registro1.",'".$tabla1."')
			";				
			mysql_query($agregar_indice) or die(mysql_error());		
			
			
			
			$this->tpl="gestionescobranza/administrador_cartera/configuracion_conceptos_add.php";
			
		$nombre=$_POST['crear_tbl'];		
		$reforzar=
			"
			Update marcada
			Set
				nombre = '".$nombre."'
			";
		mysql_query($reforzar);	
		
		$nom_tabla="select * from marcada";
			
		$agregar =
		"
		INSERT INTO parametros  
		(
			id,
			concepto,
			criterio,
			sancion,
			base,
			nivel,
			puntos,
			tipo,
			cantidad_requerida,
			p_req_cobertura,
			s_p_no_cubrir
		)
		VALUES
		(
			".$_POST['id'].",
			'".$_POST['concepto']."',
			'".$_POST['criterio']."',
			".$_POST['sancion'].",
			'".$_POST['base']."',
			'".$_POST['nivel']."',
			".$_POST['puntos'].",
			'".$_POST['tipo']."',
			".$_POST['cantidad_requerida'].",	
			".$_POST['p_req_cobertura'].",
			".$_POST['s_p_no_cubrir']."
		)
		";
		
		mysql_query($agregar);
			
		$consultar =
		"
		SELECT   
			id,
			concepto,
			criterio,
			sancion,
			base,
			nivel,
			puntos,
			tipo,
			cantidad_requerida,
			p_req_cobertura,
			s_p_no_cubrir,
			liga
		FROM
			parametros 
		WHERE 
			liga = '".$nombre."'
		";
		
		$bloq = array
		(
			"post" => empty($_POST)?array():array($_POST),
			"blk1" => $consultar,
			"blk2" => $nom_tabla
		);
		
		return $bloq;						
			
		}/////////////////////////////////////////////////////////////////// fin crear tabla
		
		
		}/////fin del if
		
		
		if(isset($_POST["Guardar"]))
		{
			$liga=$_POST["liga_mod"];	
			$ids=$_POST["id_mod"];
			foreach($ids as $i => $id)
			{
				$almacenar=
					"
					Update parametros
					Set
						p_req_cobertura = ".$_POST["p_req_cobertura_mod"][$i].",
						s_p_no_cubrir = ".$_POST["s_p_no_cubrir_mod"][$i].",
						cantidad_requerida = ".$_POST["cantidad_requerida_mod"][$i].",
						sancion = ".$_POST["sancion_mod"][$i].",
						puntos = ".$_POST["puntos_mod"][$i]."
					Where id = ".$id." and liga = '".$liga."'
					";	
				mysql_query($almacenar) or die (mysql_error());
			}	
		}
		
		if(isset($_POST["eliminar"]))
		{
		
			$borrar=$_POST['crear_tbl'];
					
			$eliminar =
				"
				delete from parametros where liga = '".$borrar."'
				";
			mysql_query($eliminar)  or die(mysql_error());	
			
			$eliminar_tbl =
				"
				Delete from tbls_creadas_bgc Where nombre_tabla = '".$borrar."'
				";
			mysql_query($eliminar_tbl)  or die(mysql_error());				
		}
		$selxx =	
			"
				SELECT
					id,
					nombre_tabla
				FROM tbls_creadas_bgc where id!=1
			";
		$reforzarII=
			"
			Update marcada
			Set
				nombre = '".$tabla."'
			";
		mysql_query($reforzarII);				

		$bloq = array
				(
					"selx" => $selxx,
					"blk2" => $reforzarII
				);
		return $bloq;
	}

    function _creacion_tbl_sanciones_del(){
		$this->tpl="gestionescobranza/administrador_cartera/configuracion_de_conceptos_abc.php";
		
		if(isset($_GET["eliminar"]))
		{

			$borrar=$_GET['crear_tbl'];
					
			$eliminar =
				"
				delete from parametros where liga = '".$borrar."'
				";
			mysql_query($eliminar)  or die(mysql_error());	
			
			$eliminar_tbl =
				"
				Delete from tbls_creadas_bgc Where nombre_tabla = '".$borrar."'
				";
			mysql_query($eliminar_tbl)  or die(mysql_error());				
		}
		$selxx =	
			"
				SELECT
					id,
					nombre_tabla
				FROM tbls_creadas_bgc where id!=1
			";

		$bloq = array
				(
					"selx" => $selxx
				);
		return $bloq;
	}

    function _configuracion_de_conceptos_abc(){
		$this->tpl="gestionescobranza/administrador_cartera/configuracion_de_conceptos_abc.php";
		
		$selxx =	
			"
				SELECT
					id,
					nombre_tabla
				FROM tbls_creadas_bgc where id!=1;
			";		
		$bloq = array
				(
					"selx" => $selxx
				);
		return $bloq;
	}

	function _configuracion_conceptos_add(){	
	
		//// Template ////
		$this->tpl="gestionescobranza/administrador_cartera/configuracion_conceptos_add.php";

		if(isset($_POST["Guardar"]))
		{
			$liga=$_POST["liga_mod"];	
			$ids=$_POST["id_mod"];
			
			foreach($ids as $i => $id)
			{
				$almacenar=
					"
					Update parametros
					Set
						p_req_cobertura = ".$_POST["p_req_cobertura_mod"][$i].",
						s_p_no_cubrir = ".$_POST["s_p_no_cubrir_mod"][$i].",
						cantidad_requerida = ".$_POST["cantidad_requerida_mod"][$i].",
						sancion = ".$_POST["sancion_mod"][$i].",
						puntos = ".$_POST["puntos_mod"][$i].",
						rango_medio = ".$_POST["rango_medio_mod"][$i]." 
					Where id = ".$id." and liga = '".$liga."'
					";	
				mysql_query($almacenar);
				
			}
		
	
		}
		
		$nombre=$_GET['modif_tbl'];		
		$reforzar=
			"
			Update marcada
			Set
				nombre = '".$nombre."'
			";
		mysql_query($reforzar);
		
		$nom_tabla="select * from marcada";		
		$agregar =
		"
		INSERT INTO parametros  
		(
			id,
			concepto,
			criterio,
			sancion,
			base,
			nivel,
			puntos,
			tipo,
			cantidad_requerida,
			p_req_cobertura,
			s_p_no_cubrir,
			rango_medio
		)
		VALUES
		(
			".$_POST['id'].",
			'".$_POST['concepto']."',
			'".$_POST['criterio']."',
			".$_POST['sancion'].",
			'".$_POST['base']."',
			'".$_POST['nivel']."',
			".$_POST['puntos'].",
			'".$_POST['tipo']."',
			".$_POST['cantidad_requerida'].",	
			".$_POST['p_req_cobertura'].",
			".$_POST['s_p_no_cubrir'].",
			".$_POST['rango_medio']."
			
		)
		";
		
		mysql_query($agregar);
			
		$consultar =
		"
		SELECT   
			id,
			concepto,
			criterio,
			sancion,
			base,
			nivel,
			puntos,
			tipo,
			cantidad_requerida,
			p_req_cobertura,
			s_p_no_cubrir,
			liga,
			rango_medio
		FROM
			parametros 
		WHERE 
			liga = '".$nombre."'
		";
		
		$bloq = array
		(
			"post" => empty($_POST)?array():array($_POST),
			"blk1" => $consultar,
			"blk2" => $nom_tabla
		);
		
		return $bloq;						
	}

	function _configuracion_conceptos_mod(){	
	
		//// Template ////
		$this->tpl="gestionescobranza/administrador_cartera/configuracion_conceptos_mod.php";

			$var_id = $_GET['id_mod'];
			$var_concepto = $_GET['concepto_mod'];
			$var_criterio = $_GET['criterio_mod'];
			$var_sancion = $_GET['sancion_mod'];
			$var_base = $_GET['base_mod'];
			$var_nivel = $_GET['nivel_mod'];
			$var_puntos = $_GET['puntos_mod'];
			$var_tipo = $_GET['tipo_mod'];
			$var_cantidad_requerida = $_GET['cantidad_requerida_mod'];
			$var_p_req_cobertura = $_GET['p_req_cobertura_mod'];
			$var_s_p_no_cubrir = $_GET['s_p_no_cubrir_mod'];
			$var_liga = $_GET['liga_mod'];
			
			echo $var_id;
			echo $var_concepto;
			echo $var_criterio;
			echo $var_sancion;
			echo $var_base;
			echo $var_nivel;
			echo $var_puntos;
			echo $var_tipo;
			echo $var_cantidad_requerida;
			echo $var_p_req_cobertura;
			echo $var_s_p_no_cubrir;
			echo $var_liga;
				
			$modificar = 
			"
			UPDATE parametros
			SET
			id = ".$_POST['id_mod'].",
			criterio = '".$_POST['criterio_mod']."',
			sancion = ".$_POST['sancion_mod'].",
			base = '".$_POST['base_mod']."',
			nivel = '".$_POST['nivel_mod']."',
			puntos = ".$_POST['puntos_mod'].",
			tipo = '".$_POST['tipo_mod']."',
			cantidad_requerida = ".$_POST['cantidad_requerida_mod'].",
			p_req_cobertura = ".$_POST['p_req_cobertura_mod'].", 
			s_p_no_cubrir =	".$_POST['s_p_no_cubrir_mod']."	
			WHERE concepto = '".$_POST['concepto_mod']."' and liga = '".$_POST['liga_mod']."'
			";
			
			mysql_query($modificar);
		
		$consultar =
			"		
			SELECT
				id,
				concepto,
				criterio,
				sancion,
				base,
				nivel,
				puntos,
				tipo,
				cantidad_requerida,
				p_req_cobertura,
				s_p_no_cubrir,
				liga
			FROM
				parametros		
			WHERE concepto = '".$var_concepto."' and liga = '".$var_liga."'
				";
		
		$bloq = array
		(
			"post"  => empty($_POST)?array():array($_POST),
			"blk1"  => $consultar
		);
		
		return $bloq;
			
	}	
    ////////////////////////////////////////////////////////////////////////////////////////////////
	function _configuracion_conceptos_del(){
	
		//// Template ////
		$this->tpl="gestionescobranza/administrador_cartera/configuracion_conceptos_del.php";
			
		$eliminar =
				"
				DELETE FROM conceptos WHERE concepto = '".$_POST['del_concepto']."'
				";
			mysql_query($eliminar);
		
		$consultar =
		"
		SELECT   
			concepto,
			p_req_cobertura,
			s_p_no_cubrir,
			cantidad_requerida,
			s_x_n_cobertura
		FROM
			conceptos						
		";
		
		$bloq = array
		(
			"post"  => empty($_POST)?array():array($_POST),
			"blk1"  => $consultar
		);
		
		return $bloq;						
	}

	function _configuracion_de_cuotas_add(){	
	
		//// Template ////
		$this->tpl="gestionescobranza/administrador_cartera/configuracion_de_cuotas_add.htm";	
		  
		  if ($_POST['nombre'] != "")
		  {
			  $agregar =
				"
				INSERT INTO cuotas  
				(
					nombre,
					activada
				)
				VALUES
				(
					'".$_POST['nombre']."', 1												
				)
				";
				
				mysql_query($agregar);
			
			}	
			$consultar =
			"
			SELECT   
				nombre,
				case when activada = 1 then 'checked' else '' end activada
			FROM
				cuotas						
			";
			
			$bloq = array
			(
				"post"  => empty($_POST)?array():array($_POST),
				"blk1"  => $consultar
			);
			
			return $bloq;						
	}
    //////////////////////////////////////////////////////////////////////////////////////////////
	function _revision_cartera_param(){
	
		//// Template ////
		$this->tpl="gestionescobranza/administrador_cartera/revision_cartera_param.php";

			$consultar = 
				"		
				SELECT
					id,
					zona,
					e_nomina,
					e_nombre,
					j_nomina,
					j_nombre,
					dias_empresa,
					ultima_revision,
					quincena,
					anio,
					concepto1,
					puntos1,
					sancion1,
					concepto2,
					puntos2,
					sancion2,
					concepto3,
					puntos3,
					sancion3,
					concepto4,
					puntos4,
					sancion4,
					concepto5,
					puntos5,
					sancion5,
					concepto6,
					puntos6,
					sancion6,
					concepto7,
					puntos7,
					sancion7,
					concepto8,
					puntos8,
					sancion8,
					concepto9,
					puntos9,
					sancion9,
					concepto10,
					puntos10,
					sancion10,
					concepto11,
					puntos11,
					sancion11,
					concepto12,
					puntos12,
					sancion12,
					concepto13,
					puntos13,
					sancion13,
					concepto14,
					puntos14,
					sancion14,
					total_puntos,
					total_sancion,
					calificacion,
					estatus,
					san_final_01,
					san_coment01,
					san_final_02,
					san_coment02,
					san_final_03,
					san_coment03,
					san_final_04,
					san_coment04,
					san_final_05,
					san_coment05,
					san_final_06,
					san_coment06,
					san_final_07,
					san_coment07,
					san_final_08,
					san_coment08,
					san_final_09,
					san_coment09,
					san_final_10,
					san_coment10,
					san_final_11,
					san_coment11,
					san_final_12,
					san_coment12,
					san_final_13,
					san_coment13,
					san_final_14,
					san_coment14																																																																																															
				FROM
					estadisticas_cartera
				";	

		$bloq = array
		(
			"post"  => empty($_POST)?array():array($_POST),
			"blk1"  => $consultar
		);
		
		return $bloq;		
			
	}		
    ///////////////////////////////////////////////////////////////////////////////////////////////
    function _reporte_cartera_param_x(){
	
		//// Template ////
		$this->tpl="gestionescobranza/administrador_cartera/reporte_cartera_param_x.html";
			
			$v_n1 = $_GET['txt_nomina'];
			$v_q1 = $_GET['txt_quincena'];
			$v_e1 = $_GET['txt_anio'];
			$v_id = $_GET['txt_id'];
			
			echo $v_id;
			
			if(isset($_POST["eliminar"]))
			{	
					
			$eliminar =
				"
				DELETE FROM estadisticas_cartera WHERE id = '".$_POST['txt_id']."'
				";
			mysql_query($eliminar);
			}	
							
			$consultar = 
				"		
				SELECT
					id,
					zona,
					e_nomina,
					e_nombre,
					j_nomina,
					j_nombre,
					dias_empresa,
					ultima_revision,
					quincena,
					anio,
					cerrado																																																																																										
				FROM
					estadisticas_cartera
				WHERE e_nomina = '".$_POST['txt_nomina']."' and quincena = '".$_POST['txt_quincena']."' and anio = '".$_POST['txt_anio']."'
				";	
				
		$bloq = array
		(
			"post"  => empty($_POST)?array():array($_POST),
			"blk1"  => $consultar
		);
		
		return $bloq;
			
	}
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function _configuracion_de_cuotas_mod(){
	
		//// Template ////
		$this->tpl="gestionescobranza/administrador_cartera/configuracion_de_cuotas_mod.php";
		
		   $v_nombre = $_GET['nombre_mod'];
		  // $v_activada = $_GET['activada_mod'];	
		 
				if (isset( $_POST['activada_mod']) )
				{
				 $var_activada = 1 ;
				}
				else
				{ $var_activada = 0 ;
				 }
			//echo $_GET['activada_mod'];
			$modificar = 
				"
				UPDATE cuotas
				SET
				activada =   ".$var_activada." 
				WHERE nombre = '".$_POST['nombre_mod']."'
				";
			mysql_query($modificar);

			$consultar = 
				"		
				SELECT
					nombre,
					case when activada = 1 then 'checked' else '' end activada
				FROM
					cuotas		
				WHERE nombre = '".$v_nombre."'
				";				
              
		$bloq = array
		(
			"post"  => empty($_POST)?array():array($_POST),
			"blk1"  => $consultar
		);
		
		return $bloq;
			
	}	
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function _configuracion_de_cuotas_del(){
	
		$v_nombre = $_GET['nombre_del'];
		//// Template ////
		$this->tpl="gestionescobranza/administrador_cartera/configuracion_de_cuotas_del.php";
			
		$eliminar =
				"
				DELETE FROM cuotas WHERE nombre = '".$_POST['nombre_del']."'
				";
		mysql_query($eliminar);
		
		$consultar =
		"
		SELECT   
			nombre
		FROM
			cuotas
		WHERE nombre = '".$v_nombre."'								
		";
		
		$bloq = array
		(
			"post"  => empty($_POST)?array():array($_POST),
			"blk1"  => $consultar
		);
		
		return $bloq;							
	}
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function _configuracion_de_categorias(){
	
		//// Template ////
		$this->tpl="gestionescobranza/administrador_cartera/configuracion_de_categorias.htm";	
		  $agregar =
				"
				INSERT INTO categorias  
				(
					id,
					categoria
				)
				VALUES
				(
					'".$_POST['id']."',
					'".$_POST['categoria']."'							
				)
				";
			
			mysql_query($agregar);
				
			$consultar =
				"
				SELECT   
					id,
					categoria
				FROM
					categorias						
				";
			
			$bloq = array
			(
				"post"  => empty($_POST)?array():array($_POST),
				"blk1"  => $consultar
			);
			
			return $bloq;						
	}

	function _configuracion_de_categorias_mod(){	
	
		//// Template ////
		$this->tpl="gestionescobranza/administrador_cartera/configuracion_de_categorias_mod.htm";

				$var_id = $_GET['id_mod'];
				$var_categoria = $_GET['categoria_mod'];
				
				echo $var_id;
				echo $var_categoria;
			
			$modificar = 
				"
					UPDATE categorias
					SET
					id = ".$_POST['id_x']."	
					WHERE categoria = '".$_POST['categoria_x']."'
				";
				
				mysql_query($modificar);
				
			$consultar = 
				"		
					SELECT
						id,
						categoria
					FROM
						categorias		
					WHERE categoria = '".$var_categoria."'
				";
					
				echo $consultar;

		$bloq = array
		(
			"post"  => empty($_POST)?array():array($_POST),
			"blk1"  => $consultar
		);
		
		return $bloq;
			
	}

	function _configuracion_de_categorias_del(){	
	
		//// Template ////
		$this->tpl="gestionescobranza/administrador_cartera/configuracion_de_categorias_del.htm";
		
		$var_id = $_GET['id_del'];
		$var_categoria = $_GET['categoria_del'];
		
		echo $var_id;
		echo $var_categoria;
							
		$eliminar =
			"
				DELETE FROM categorias WHERE categoria = '".$_POST['categoria_del']."'
			";
			mysql_query($eliminar);
		
		$consultar =
		"
			SELECT   
				id,
				categoria
			FROM
				categorias 
			WHERE categoria = '".$var_categoria."'					
		";
		
		$bloq = array
		(
			"post"  => empty($_POST)?array():array($_POST),
			"blk1"  => $consultar
		);
		
		return $bloq;	
								
	}			

	function _configuracion_de_actividades(){	
	
		//// Template ////
		$this->tpl="gestionescobranza/administrador_cartera/configuracion_de_actividades.html";	
		
		$revision =
				"
				SELECT 
					id,
					categoria
				FROM
					categorias
				";
				
		  $agregar =
				"
				INSERT INTO actividades  
				(
					id,
					actividad,
					categoria
				)
				VALUES
				(
					'".$_POST['id']."',
					'".$_POST['actividad']."',
					'".$_POST['categoria']."'							
				)
				";
			
			mysql_query($agregar);
				
			$consultar =
				"
				SELECT   
					id,
					actividad,
					categoria
				FROM
					actividades						
				";
			
			$bloq = array
			(
				"post"  => empty($_POST)?array():array($_POST),
				"blk1"  => $consultar,
				"blk2"  => $revision
			);
			
			return $bloq;						
	}

	function _configuracion_de_actividades_mod(){	
	
		//// Template ////
		$this->tpl="gestionescobranza/administrador_cartera/configuracion_de_actividades_mod.html";

			$var_id = $_GET['id_mod'];
			$var_actividad = $_GET['actividad_mod'];
			$var_categoria = $_GET['categoria_mod'];
			
			echo $var_id;
			echo $var_actividad;
			echo $var_categoria;
				
			$revision =
					"
					SELECT 
						id,
						categoria
					FROM
						categorias
					";				
			
			$modificar = 
				"
					UPDATE actividades
					SET
					id = ".$_POST['id_mod'].",
					categoria = '".$_POST['categoria_mod']."'
					WHERE actividad = '".$_POST['actividad_mod']."'
				";
				
				mysql_query($modificar);
				
			$consultar = 
				"		
					SELECT
						id,
						actividad,
						categoria
					FROM
						actividades		
					WHERE actividad = '".$var_actividad."'
				";
					
				echo $consultar;

			$bloq = array
			(
				"post"  => empty($_POST)?array():array($_POST),
				"blk1"  => $consultar,
				"blk2"  => $revision
			);
		
		return $bloq;
			
	}

	function _configuracion_de_actividades_del(){	
	
		//// Template ////
		$this->tpl="gestionescobranza/administrador_cartera/configuracion_de_actividades_del.html";
		
		$var_id = $_GET['id_del'];
		$var_actividad = $_GET['actividad_del'];
		$var_categoria = $_GET['categoria_del'];
		
		echo $var_id;
		echo $var_actividad;
		echo $var_categoria;
							
		$eliminar =
			"
				DELETE FROM actividades WHERE actividad = '".$_POST['actividad_del']."'
			";
			mysql_query($eliminar);
		
		$consultar =
		"
			SELECT   
				id,
				actividad,
				categoria
			FROM
				actividades 
			WHERE actividad = '".$var_actividad."'					
		";
		
		$bloq = array
		(
			"post"  => empty($_POST)?array():array($_POST),
			"blk1"  => $consultar
		);
		
		return $bloq;	
								
	}	

	function _reporte_adminc(){
		
		$GLOBALS['quincena'] = $_POST['quincena'];
		$GLOBALS['u_nomina'] = $_SESSION['nomina'];
		$GLOBALS['u_nombre'] = $_SESSION['nombre'];
		$GLOBALS['nano'] = $_POST['ano'];	
		$GLOBALS['concepto_san_tbl'] = $_POST["concepto_san_tbl"];
		
		//echo "Configuracion: ";
		//echo $_POST["concepto_san_tbl"];
		//echo "  Periodo: ";
		//echo $_POST['quincena'];
		//echo "  Ejercicio: ";
		//echo $_POST['ano'];
		
		$nombre_tabla = $_POST["concepto_san_tbl"];
		
		$m1_fecha = mysql_query("Select quincena_cobranza1(".$_POST['quincena'].", ".$_POST['ano'].")");
		$m2_fecha = mysql_query("Select quincena_cobranza2(".$_POST['quincena'].", ".$_POST['ano'].")");	
		
		$ano_vi = $_POST['ano'];
		$quincena_vi = $_POST['quincena'];
		$v_qui = $_POST['quincena'];
		$v_ano = $_POST['ano'];
		 
		$ano = $_POST['ano'];
		$ano1 = $_POST['ano']+1;			
		$sql = "SELECT YEAR ('".$ano."-12-31')+1 AS nano";
		$rq_sql = mysql_query($sql);
		$vano = mysql_fetch_assoc($rq_sql);
		$nano = $vano['nano'];
		
		
		
		$fecha1="select bitacora_gestioncobranza_intelisis.quincena_cobranza1(".$_POST['quincena'].",".$_POST['ano'].") as fecha";
		$res=mysql_fetch_assoc(mysql_query($fecha1));
		$desde=$res['fecha'];
		$fecha2="select bitacora_gestioncobranza_intelisis.quincena_cobranza2(".$_POST['quincena'].",".$_POST['ano'].") as fecha";
		$res=mysql_fetch_assoc(mysql_query($fecha2));
		$hasta=$res['fecha'];

		switch($_POST['quincena']){
			case 1: 
				$periodo = "BETWEEN '".$ano."-01-02' AND '".$ano."-01-16'";
			break;
			case 2: 
				$periodo = "BETWEEN '".$ano."-01-17' AND '".$ano."-02-01'";
			break;
			case 3: 
				$periodo = "BETWEEN '".$ano."-02-02' AND '".$ano."-02-16'";
			break;
			case 4: 
				$periodo = "BETWEEN '".$ano."-02-17' AND '".$ano."-03-01'";
			break;
			case 5: 
				$periodo = "BETWEEN '".$ano."-03-02' AND '".$ano."-03-16'";
			break;
			case 6: 
				$periodo = "BETWEEN '".$ano."-03-17' AND '".$ano."-04-01'";
			break;
			case 7: 
				$periodo = "BETWEEN '".$ano."-04-02' AND '".$ano."-04-16'";
			break;
			case 8: 
				$periodo = "BETWEEN '".$ano."-04-17' AND '".$ano."-05-01'";
			break;
			case 9: 
				$periodo = "BETWEEN '".$ano."-05-02' AND '".$ano."-05-16'";
			break;
			case 10: 
				$periodo = "BETWEEN '".$ano."-05-17' AND '".$ano."-06-01'";
			break;
			case 11: 
				$periodo = "BETWEEN '".$ano."-06-02' AND '".$ano."-06-16'";
			break;
			case 12: 
				$periodo = "BETWEEN '".$ano."-06-17' AND '".$ano."-07-01'";
			break;
			case 13: 
				$periodo = "BETWEEN '".$ano."-07-02' AND '".$ano."-07-16'";
			break;
			case 14: 
				$periodo = "BETWEEN '".$ano."-07-17' AND '".$ano."-08-01'";
			break;
			case 15: 
				$periodo = "BETWEEN '".$ano."-08-02' AND '".$ano."-08-16'";
			break;
			case 16: 
				$periodo = "BETWEEN '".$ano."-08-17' AND '".$ano."-09-01'";
			break;
			case 17: 
				$periodo = "BETWEEN '".$ano."-09-02' AND '".$ano."-09-16'";
			break;
			case 18: 
				$periodo = "BETWEEN '".$ano."-09-17' AND '".$ano."-10-01'";
			break;
			case 19: 
				$periodo = "BETWEEN '".$ano."-10-02' AND '".$ano."-10-16'";
			break;
			case 20: 
				$periodo = "BETWEEN '".$ano."-10-17' AND '".$ano."-11-01'";
			break;
			case 21: 
				$periodo = "BETWEEN '".$ano."-11-02' AND '".$ano."-11-16'";
			break;
			case 22: 
				$periodo = "BETWEEN '".$ano."-11-17' AND '".$ano."-12-01'";
			break;
			case 23: 
				$periodo = "BETWEEN '".$ano."-12-02' AND '".$ano."-12-16'";
			break;
			case 24: 
				$periodo = "BETWEEN '".$ano."-12-17' AND '".$ano1."-01-01'";
			break;
		}
		
		 mysql_query("create temporary table dias (dia date)"); 
		 
		 $x1_fecha = mysql_query("Select quincena_cobranza1(".$_POST['quincena'].", ".$_POST['ano']."), '%d-%m-%Y' campo_dias");
		 $x2_fecha = mysql_query("Select quincena_cobranza2(".$_POST['quincena'].", ".$_POST['ano']."), '%d-%m-%Y' campo_dias");
		 
		 $v_mesdia = mysql_fetch_assoc($x1_fecha);
		 
		 $v_mesdia['campo_dias'];
		 $v_ddd = substr($v_mesdia['campo_dias'],0,2); 
		 $v_mmm = substr($v_mesdia['campo_dias'],3,2);
		 $v_aaa = substr($v_mesdia['campo_dias'],6,4);
		  
		 if ( $ddd == 2 ){
			$ultdiaquincena = 16 ;
		}else{ 
			$ultdiaquincena = 28;  
		}//ultimo día del mes
					
		$dia= $ddd;
		 
		
		 //Saca el numero de dias entre la fecha_inicio y fecha_fin
		$f_1= new DateTime(str_ireplace('/','-',$desde));
		$f_2= new DateTime(str_ireplace('/','-',$hasta));		
		$intervalo=$f_1->diff($f_2);		
		$numero_dias=$intervalo->format("%a");
		
		
		//POne las fechas en la tabla
		for($i=0; $i<=$numero_dias ;$i++)
		{
			$temp=strtotime($desde." +".$i." days");
			$timestrap= date("Y-m-d",$temp);
			mysql_query("insert into dias (dia) values ('".$timestrap."')");
			
		}
		
		//para los cobros si es agente doble
		$sql="select agente_origen, agente_asociado from bitacora_gestioncobranza_intelisis.agente_doble
							where fecha_asigna between '".$desde."' and '".$hasta."' and agente_asociado='".$_POST['nomina']."'
							group by agente_origen, agente_asociado";
		$re=mysql_query($sql);
		$num=mysql_num_rows($re);
		
		if($num>0)
		{
			$dat=mysql_fetch_assoc($re);
			$nomina=$dat["agente_origen"];
			
		}else{
			$nomina=$_POST['nomina'];
		}
		
		$elimina =
			"
			Delete From marcada Where nombre != null
			";
		$actualiza =
			"
			UPDATE marcada
			SET nombre = '".$nombre_tabla."'
			";		
		mysql_query($actualiza) or die(mysql_error());
        $temp =
     	"CREATE TEMPORARY TABLE registros_tem AS select * from(
			select id, cuenta, mov, factura, fechaultimopago, fechafactura, saldocapital, druta, articulo, diasinactividad, 
diasvencidos, intereses, cuota, nombre, domicilio, entre, colonia, ciudad, estado, rutacobro, aval, 
aval_domicilio, aval_colonia, aval_ciudad, telefono, saldo, importe, fecha_captura, hora_captura, hora_captura_final, 
fecha_inicio, hora_inicio, fecha_final, hora_final, resultado, valor_resultado, proximavisita, comentario, 
pago_promesa, pago_importe, pago_quien, recibo, promotor, etapa, ruta, intelisis, saldovencido, cartera, 
folio, capturista, pzo, abonos, imp_convenio, imp_inicial, num_convenio, gps, numero, origen from bitacora_gestioncobranza_intcob.registros
			where (promotor='".$_POST['nomina']."' or intelisis='".$_POST['nomina']."') and fecha_inicio ".$periodo." and resultado<>''
			union all
			select id, cuenta, mov, factura, fechaultimopago, fechafactura, saldocapital, druta, articulo, diasinactividad, diasvencidos, intereses, 
				cuota, nombre, domicilio, entre, colonia, ciudad, estado, rutacobro, aval, aval_domicilio, aval_colonia, aval_ciudad, telefono, saldo, 
				importe, fecha_captura, hora_captura, hora_captura_final, fecha_inicio, hora_inicio, fecha_final, hora_final, resultado, valor_resultado, 
				proximavisita, comentario, pago_promesa, pago_importe, pago_quien, recibo, promotor, etapa, ruta, intelisis, saldovencido, cartera, folio, 
				capturista, pzo, abonos, imp_convenio, imp_inicial, num_convenio, gps, numero, origen 
			from bitacora_gestioncobranza_intelisis.registros
			where (promotor='".$_POST['nomina']."' or intelisis='".$_POST['nomina']."') and fecha_inicio ".$periodo." and resultado<>''
		)a";
				
	    mysql_query($temp);	
		$temp1= 
			"CREATE TEMPORARY TABLE sancion_actgest as
			Select d.dia, case when ctasxdia = 0 then p.sancion else  0 end sancion
			From
			(
			Select d.dia, count(r.cuenta) ctasxdia
			-- , case when r.fecha_inicio is null then 100 else 0 end sancion 
			-- , r.cuenta
			from 
			dias d
			LEFT JOIN registros_tem r on r.fecha_inicio =  d.dia
			group by d.dia, r.fecha_inicio
			)d
			LEFT join parametros p on p.concepto = 'ACTUALIZACION DE GESTIONES' and p.liga = '".$nombre_tabla."' 
			";
			
			 mysql_query($temp1) or die (mysql_error());
		
        $temp2 =
        "CREATE TEMPORARY TABLE cuentas_temp AS SELECT * FROM( 
					  SELECT id, nomina, cuenta, nivel, movimiento, factura, nombre, plazo, imp_comp, fup, abonos, sdo_ven, sdo_tot, colonia, poblacion, articulo, telefono, dias_inactivos, dias_vencidos, ejercicio, periodo, empresa FROM android_reportes._cuentas_gestioncobranza where nomina='".$nomina."' and periodo=".$_POST['quincena']." and ejercicio=".$_POST['ano']."
					 UNION ALL
					  select agc.id, ad.agente_asociado as nomina, agc.cuenta, agc.nivel, agc.movimiento, agc.factura, agc.nombre, agc.plazo, 
						agc.imp_comp, agc.fup, agc.abonos, agc.sdo_ven, agc.sdo_tot, agc.colonia, agc.poblacion, agc.articulo, agc.telefono, agc.dias_inactivos,
						agc.dias_vencidos, agc.ejercicio, agc.periodo, agc.empresa from (

							select agente_origen, agente_asociado from bitacora_gestioncobranza_intelisis.agente_doble
							where fecha_asigna between '".$desde."' and '".$hasta."' 
							group by agente_origen, agente_asociado

						) as ad
						left join android_reportes._cuentas_gestioncobranza agc on agc.nomina=ad.agente_origen
						where agc.periodo=".$_POST['quincena']." and agc.ejercicio=".$_POST['ano']." and ad.agente_asociado='".$_POST['nomina']."'
						) a";		//echo $temp2;
		mysql_query($temp2);
		
//-- tabla 2
		$temp_vis2 =	
			"
			 CREATE TEMPORARY TABLE ctas_ejerc_ant  AS SELECT a.* FROM( 
								  SELECT c.* FROM android_reportes._cuentas_gestioncobranza c
								  where c.nomina= '".$_POST['nomina']."' and c.periodo= ".$quincena_vi."  and c.ejercicio= ".$ano_vi."
								 UNION ALL
								  select agc.id, ad.agente_asociado as nomina, agc.cuenta, agc.nivel, agc.movimiento, agc.factura, agc.nombre, agc.plazo, 
									agc.imp_comp, agc.fup, agc.abonos, agc.sdo_ven, agc.sdo_tot, agc.colonia, agc.poblacion, agc.articulo, agc.telefono, agc.dias_inactivos,
									agc.dias_vencidos, agc.ejercicio, agc.periodo, agc.empresa from (
			
										select agente_origen, agente_asociado from bitacora_gestioncobranza_intelisis.agente_doble
										where fecha_asigna between '".$m3_fecha."' and '".$m4_fecha."' 
										group by agente_origen, agente_asociado
			
									) as ad
									left join android_reportes._cuentas_gestioncobranza agc on agc.nomina=ad.agente_origen
									where agc.periodo= ".$quincena_vi." and agc.ejercicio= ".$ano_vi." and ad.agente_asociado='".$_POST['nomina']."'
									) a 
									Join ctas_sinvisita v on  v.nomina = a.nomina  and
										REPLACE(a.cuenta,' ','')=REPLACE(v.cuenta,' ','')
											and REPLACE(v.movimiento,' ','')=REPLACE(a.movimiento,' ','')
											and REPLACE(v.factura,' ','')=REPLACE(a.factura,' ','')
				";
 				mysql_query($temp_vis2);
 //-- tabal 3
 		$temp_vis3 =	
			"
			 CREATE TEMPORARY TABLE registros_tem_ant  AS select a.* from(
						select id, cuenta, mov, factura, fechaultimopago, fechafactura, saldocapital, druta, articulo, diasinactividad, 
			diasvencidos, intereses, cuota, nombre, domicilio, entre, colonia, ciudad, estado, rutacobro, aval, 
			aval_domicilio, aval_colonia, aval_ciudad, telefono, saldo, importe, fecha_captura, hora_captura, hora_captura_final, 
			fecha_inicio, hora_inicio, fecha_final, hora_final, resultado, valor_resultado, proximavisita, comentario, 
			pago_promesa, pago_importe, pago_quien, recibo, promotor, etapa, ruta, intelisis, saldovencido, cartera, 
			folio, capturista, pzo, abonos, imp_convenio, imp_inicial, num_convenio, gps, numero, origen from bitacora_gestioncobranza_intcob.registros
						where ( promotor='".$_POST['nomina']."' or intelisis='".$_POST['nomina']."' ) and fecha_inicio between '".$m3_fecha."' and '".$m4_fecha."' and resultado <> ''
						union all
						select id, cuenta, mov, factura, fechaultimopago, fechafactura, saldocapital, druta, articulo, diasinactividad, diasvencidos, intereses, 
							cuota, nombre, domicilio, entre, colonia, ciudad, estado, rutacobro, aval, aval_domicilio, aval_colonia, aval_ciudad, telefono, saldo, 
							importe, fecha_captura, hora_captura, hora_captura_final, fecha_inicio, hora_inicio, fecha_final, hora_final, resultado, valor_resultado, 
							proximavisita, comentario, pago_promesa, pago_importe, pago_quien, recibo, promotor, etapa, ruta, intelisis, saldovencido, cartera, folio, 
							capturista, pzo, abonos, imp_convenio, imp_inicial, num_convenio, gps, numero, origen 
						from bitacora_gestioncobranza_intelisis.registros
						where (promotor= '".$_POST['nomina']."' or intelisis='".$_POST['nomina']."') and fecha_inicio between '".$m3_fecha."' and '".$m4_fecha."' and resultado <> ''
						)a
						JOIN ctas_ejerc_ant ca ON ca.nomina = a.promotor  and REPLACE(a.cuenta,' ','')=REPLACE(ca.cuenta,' ','')
											and REPLACE(ca.movimiento,' ','')=REPLACE(a.mov,' ','')
											and REPLACE(ca.factura,' ','')=REPLACE(a.factura,' ','')
			";
			mysql_query($temp_vis3);		
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$temp_ccd =
			"
			CREATE TEMPORARY TABLE ctas_contacto_directo AS
				Select f.* , p.p_req_cobertura , p.s_p_no_cubrir
               , Case when f.porcentaje < p.p_req_cobertura  then  p.s_p_no_cubrir else 0 end  as sancion
               From
               (
                SELECT
					uno.fecha,
					COUNT(*) AS total,
					SUM(contacto) AS contacto,
					ROUND(SUM(contacto)/Count(contacto)*100, 2) AS porcentaje
                  
					FROM
					(
					  SELECT
							CASE WHEN (reg.intelisis IS NULL OR reg.intelisis = 'SIN AGE' OR reg.intelisis = ' ') THEN reg.promotor ELSE reg.intelisis END AS nomina,
							reg.fecha_inicio AS fecha,
							CASE WHEN reg.resultado = par.resultado AND condir = 'ON' THEN 1 ELSE 0 END AS contacto
					FROM cuentas_temp andr
					LEFT JOIN registros_tem reg ON CASE WHEN (reg.intelisis IS NULL OR reg.intelisis = 'SIN AGE' OR reg.intelisis = ' ') THEN REPLACE(andr.nomina,' ','') = REPLACE(reg.promotor,' ','') ELSE  REPLACE(andr.nomina,' ','') = REPLACE(reg.intelisis,' ','') END
					and REPLACE(reg.cuenta,' ','')=REPLACE(andr.cuenta,' ','')
					and REPLACE(andr.movimiento,' ','')=REPLACE(reg.mov,' ','')
					and REPLACE(andr.factura,' ','')=REPLACE(reg.factura,' ','')
					LEFT JOIN parametros_gestiones par ON reg.resultado = par.resultado			
					where reg.fecha_inicio is not null
					)uno 
				GROUP BY fecha	
                ) F
                LEFT JOIN parametros p on id = 4 and liga = '".$nombre_tabla."'				
			";
			mysql_query($temp_ccd);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		
		$sqlp="select * from varios.empleados_rh201_intelisis where clave='".$_POST['nomina']."'";					
		
		$sql1 =
				"SELECT
								age.nomina,
								age.nombre,
								jef.nomina AS jnomina,
								jef.nombre AS jnombre,
								ABS(DATEDIFF(NOW(),DATE(`fec ingreso`))) AS dias_empresa,
								CASE WHEN tab.mac_address IS NULL THEN 'NO' ELSE 'SI' END AS mac_address,
								CASE WHEN cel.celulares IS NULL THEN 'SIN CELULAR' ELSE celulares END AS celulares							
						    FROM bitacora_gestioncobranza_intelisis.agentes age
							LEFT JOIN bitacora_gestioncobranza_intelisis.agentes jef ON age.celula = jef.agente
							LEFT JOIN intranet_home.directoriogral_intelisis cel ON age.nombre = cel.nombre
							LEFT JOIN mailing.tablet_ma tab ON age.nomina = tab.nomina
							LEFT JOIN varios.empleados_rh201_intelisis emp ON age.nomina = emp.clave
						    WHERE age.es <> 'TELEFONICA'
						    AND age.es NOT LIKE '%JEFE%'
						    AND age.es NOT LIKE '%GERENTE%'
						    AND age.nomina = '".$_POST['nomina']."'
					  	    GROUP BY nomina
				";
		$sql11 ="
				SELECT *,       (Aclaraciones / total)*100 as porc_aclar,
								(pago / total)*100 as porc_pago,
								(LCT / total)*100 as porc_LCT,								
								(adjudicac / total)*100 as porc_adjudicac,																
								(promesa / total)*100 as porc_promesa,																								
								(aviso / total)*100 as porc_aviso,																
								(recado / total)*100 as porc_recado,																
								(cobroaval / total)*100 as porc_cobroaval,																
								(negativa / total)*100 as porc_negativa,																
								(insolvenc / total)*100 as porc_insolvenc,																
								(localizac / total)*100 as porc_localizac,																
								(legal / total)*100 as porc_legal,																																																																								
								(defuncion / total)*100 as porc_defuncion,
								(total / total)*100 as porc_total					 
				FROM(
					SELECT
						sum(CASE WHEN reg.resultado = 'aclaraciones' THEN 1 ELSE 0 END) AS Aclaraciones,
						sum(CASE WHEN reg.resultado = 'pago' 	  THEN 1 ELSE 0 END) AS pago,
						sum(CASE WHEN reg.resultado = 'LCT'       THEN 1 ELSE 0 END) AS LCT,
						sum(CASE WHEN reg.resultado = 'adjudicac' THEN 1 ELSE 0 END) AS adjudicac,
						sum(CASE WHEN reg.resultado = 'promesa'   THEN 1 ELSE 0 END) AS promesa,
						sum(CASE WHEN reg.resultado = 'aviso'     THEN 1 ELSE 0 END) AS aviso,
						sum(CASE WHEN reg.resultado = 'recado'    THEN 1 ELSE 0 END) AS recado,
						sum(CASE WHEN reg.resultado = 'cobroaval'  THEN 1 ELSE 0 END) AS cobroaval,
						sum(CASE WHEN reg.resultado = 'negativa'  THEN 1 ELSE 0 END) AS negativa,
						sum(CASE WHEN reg.resultado = 'insolvenc' THEN 1 ELSE 0 END) AS insolvenc,
						sum(CASE WHEN reg.resultado = 'localizac' THEN 1 ELSE 0 END) AS localizac,
						sum(CASE WHEN reg.resultado = 'legal'     THEN 1 ELSE 0 END) AS legal,
						sum(CASE WHEN reg.resultado = 'defuncion' THEN 1 ELSE 0 END) AS defuncion,
						sum(CASE WHEN reg.resultado in('Aclaraciones','pago','defuncion','legal','localizac','insolvenc','legal','LCT','adjudicac','promesa','aviso','recado','cobroaval','negativa') THEN 1 ELSE 0 END) as total 
					FROM cuentas_temp andr
					INNER JOIN registros_tem reg ON CASE WHEN (reg.intelisis IS NULL OR reg.intelisis = 'SIN AGE' OR reg.intelisis = ' ') THEN REPLACE(andr.nomina,' ','') = REPLACE(reg.promotor,' ','') ELSE  REPLACE(andr.nomina,' ','') = REPLACE(reg.intelisis,' ','') END
					and REPLACE(reg.cuenta,' ','')=REPLACE(andr.cuenta,' ','')
					and REPLACE(andr.movimiento,' ','')=REPLACE(reg.mov,' ','')
					and REPLACE(andr.factura,' ','')=REPLACE(reg.factura,' ','')
				)a
				";

		$temp_vicitadas =
			" CREATE TEMPORARY TABLE ctas_visitadas_tot AS 
			SELECT   MAX(ifnull(asignadas,0)) asignadas, MAX(ifnull(facturas,0)) facturas, max(ifnull(visitadas,0)) visitadas, max(ifnull(restantes,0)) restantes,
          MAX(ifnull(asignadasmavicob,0)) asignadasmavicob, MAX(ifnull(facturasmavicob,0)) facturasmavicob, max(ifnull(visitadasmavicob,0)) visitadasmavicob, max(ifnull(restantesmavicob,0)) restantesmavicob	,	case when MAX(ifnull(asignadas,0)) >0 then max(ifnull(visitadas,0)) / MAX(ifnull(asignadas,0)) else 0 end porcmavi  
		  ,   case when   MAX(ifnull(asignadasmavicob,0)) >0 then max(ifnull(visitadasmavicob,0))/ MAX(ifnull(asignadasmavicob,0))  else 0 end porcmavicob     
				
                 FROM
               (
				 SELECT
					Case when empresa = 'mavi' then COUNT(DISTINCT(andr.cuenta)) end AS asignadas ,
			        Case when empresa = 'mavi' then COUNT(distinct(andr.factura)) end   AS facturas,
					Case when empresa = 'mavi' then COUNT(DISTINCT(reg.cuenta)) end AS visitadas,
					Case when empresa = 'mavi' then COUNT(DISTINCT(andr.cuenta))- COUNT(DISTINCT(reg.cuenta)) end AS restantes,
					Case when empresa = 'mavicob' then COUNT(DISTINCT(andr.cuenta)) end AS asignadasmavicob ,
			        Case when empresa = 'mavicob' then COUNT(distinct(andr.factura)) end   AS facturasmavicob,
					Case when empresa = 'mavicob' then COUNT(DISTINCT(reg.cuenta)) end AS visitadasmavicob,
					Case when empresa = 'mavicob' then COUNT(DISTINCT(andr.cuenta))- COUNT(DISTINCT(reg.cuenta)) end AS restantesmavicob	  
				FROM cuentas_temp andr
				LEFT JOIN registros_tem reg ON CASE WHEN (reg.intelisis IS NULL OR reg.intelisis = 'SIN AGE' OR reg.intelisis = ' ') THEN REPLACE(andr.nomina,' ','') =                 REPLACE(reg.promotor,' ','') ELSE  REPLACE(andr.nomina,' ','') = REPLACE(reg.intelisis,' ','') END
				and REPLACE(reg.cuenta,' ','')=REPLACE(andr.cuenta,' ','')
				and REPLACE(andr.movimiento,' ','')=REPLACE(reg.mov,' ','')
				and REPLACE(andr.factura,' ','')=REPLACE(reg.factura,' ','')			
			    group by empresa  )ctas
			";
			mysql_query($temp_vicitadas);
//		echo $sql2;
		$sql2 = "	SELECT asignadas, facturas,visitadas,restantes,asignadasmavicob ,facturasmavicob ,visitadasmavicob ,restantesmavicob,totporc,porcmavicob,totasig,totvisit,porcmavi
				,(sancionmavi+sancionmavicob) as totsancion
				FROM
				( 
				  Select asignadas, facturas,visitadas,restantes,asignadasmavicob ,facturasmavicob ,visitadasmavicob ,restantesmavicob 
		          ,round(porcmavi*100,2) porcmavi ,round(porcmavicob *100,2) porcmavicob , asignadas + asignadasmavicob totasig , visitadas + visitadasmavicob totvisit ,round(((visitadas + visitadasmavicob)/(asignadas + asignadasmavicob)) * 100,2) totporc
				  ,CASE WHEN p.p_req_cobertura> round((visitadas/asignadas ) * 100,2) then p.s_p_no_cubrir ELSE 0 END as sancionmavi
                  ,CASE WHEN p.p_req_cobertura> round((visitadasmavicob/asignadasmavicob) * 100,2) then p.s_p_no_cubrir ELSE 0 END as sancionmavicob
		          from ctas_visitadas_tot
				  INNER JOIN Parametros p ON p.id=13 and p.liga= '".$nombre_tabla."' 
				  )fin
		  
		   " ;
		
		$sql3 =
			"
				SELECT
				  case when SUM(mayor_vencimiento) is null then 0 else SUM(mayor_vencimiento) end AS mayor_total,
				  case when SUM(prevencion) is null then 0 else SUM(prevencion) end AS preven_total,
				  case when SUM(liquidacion) is null then 0 else SUM(liquidacion) end AS liqui_total,
				  case when SUM(contencion) is null then 0 else SUM(contencion) end AS conten_total,
				  case when SUM(abono_cero) is null then 0 else SUM(abono_cero) end AS abono_total,
				  case when SUM(gastos) is null then 0 else SUM(gastos) end AS gastos_total,
				  case when SUM(inactividad) is null then 0 else SUM(inactividad) end AS inac_total,
				  case when SUM(regularizacion) is null then 0 else SUM(regularizacion) end AS regul_total,
				  case when SUM(contencion_dirigida) is null then 0 else SUM(contencion_dirigida) end AS contdir_total
				FROM
				  (
					SELECT
					  CASE WHEN mayor_vencimiento IS NULL THEN 0 ELSE mayor_vencimiento END AS mayor_vencimiento,
					  CASE WHEN prevencion IS NULL THEN 0 ELSE prevencion END AS prevencion,
					  CASE WHEN liquidacion IS NULL THEN 0 ELSE liquidacion END AS liquidacion,
					  CASE WHEN contencion IS NULL THEN 0 ELSE contencion END AS contencion,
					  CASE WHEN abono_cero IS NULL THEN 0 ELSE abono_cero END AS abono_cero,
					  CASE WHEN gastos IS NULL THEN 0 ELSE gastos END AS gastos,
					  CASE WHEN inactividad IS NULL THEN 0 ELSE inactividad END AS inactividad,
					  CASE WHEN regularizacion IS NULL THEN 0 ELSE regularizacion END AS regularizacion, 
					  CASE WHEN contencion_dirigida IS NULL THEN 0 ELSE contencion_dirigida END AS contencion_dirigida
					FROM cuotas_facturas
				WHERE agente='".$_POST['nomina']."'
				AND quincena='".$_POST['quincena']."' AND ejercicio = '".$ano."' )a";  
				//checar con TOM porque en este query no especifican el ejercicio , lo chuequé con Tom y ya se lo agregué

		$sqll4 = "
		
		Select MAX(montomavi) montomavi , MAX(Ctasasignadasmavi) Ctasasignadasmavi ,  MAX(Cuentas_pagomavi) Cuentas_pagomavi
 ,  MAX( porccoberturamavi) porccoberturamavi
 , MAX(montomcob) montomcob , MAX(Ctasasignadasmcob) Ctasasignadasmcob ,  MAX(Cuentas_pagomcob) Cuentas_pagomcob
 ,  MAX( porccoberturamcob) porccoberturamcob
 , MAX(montoLCT) montoLCT , MAX(CtasasignadasLCT) CtasasignadasLCT ,  MAX(Cuentas_pagoLCT) Cuentas_pagoLCT
 ,  MAX( porccoberturaLCT) porccoberturaLCT
 , MAX(montoaval)  montoaval ,  MAX(Ctasasignadasaval) Ctasasignadasaval,  MAX(Cuentas_pagoaval) Cuentas_pagoaval
 ,  MAX( porccoberturaaval) porccoberturaaval
,MAX(montomavi) + MAX(montomcob) +  MAX(montoLCT) + MAX(montoaval)  totMonto,MAX(Ctasasignadasmavi) + MAX(Ctasasignadasmcob) + MAX(CtasasignadasLCT)+MAX(CtasasignadasAval)  totCtasasignadas
, MAX(Cuentas_pagomavi) + MAX(Cuentas_pagomcob) + MAX(Cuentas_pagoLCT) +  MAX(Cuentas_pagoaval) totCuentas_pago ,( ( MAX(Cuentas_pagomavi) + MAX(Cuentas_pagomcob) + MAX(Cuentas_pagoLCT) +  MAX(Cuentas_pagoaval) ) / ( MAX(Ctasasignadasmavi) + MAX(Ctasasignadasmcob) + MAX(CtasasignadasLCT)+ MAX(CtasasignadasAval) )  ) * 100 totporccobertura

From 
(
		 			Select nivelrecup ,Case when nivelrecup = 'MAVI' then SUM(cobrado) else 0 end Montomavi,
					 Case when nivelrecup = 'MAVI' then Count(cuenta) else 0 end  Ctasasignadasmavi  ,  Case when nivelrecup = 'MAVI' then  SUM(concobro) else 0 end 			Cuentas_pagomavi, Case when nivelrecup = 'MAVI' then ( SUM(concobro)/Count(cuenta) ) * 100 else 0 end  porccoberturamavi,
					 Case when nivelrecup = 'MAVICOB' then SUM(cobrado) else 0 end Montomcob, Case when nivelrecup = 'MAVICOB' then Count(cuenta) else 0 end  Ctasasignadasmcob  ,  Case when nivelrecup = 'MAVICOB' then  SUM(concobro) else 0 end Cuentas_pagomcob
 , Case when nivelrecup = 'MAVICOB' then ( SUM(concobro)/Count(cuenta) ) * 100 else 0 end  porccoberturamcob
 ,Case when nivelrecup = 'LCT' then SUM(cobrado) else 0 end MontoLCT, Case when nivelrecup = 'LCT' then Count(cuenta) else 0 end  CtasasignadasLCT  ,  Case when nivelrecup = 'LCT' then  SUM(concobro) else 0 end Cuentas_pagoLCT
 , Case when nivelrecup = 'LCT' then   ( SUM(concobro)/Count(cuenta) ) * 100 else 0 end  porccoberturaLCT 
  ,Case when nivelrecup = 'Aval' then SUM(cobrado) else 0 end Montoaval, Case when nivelrecup = 'Aval' then Count(cuenta) else 0 end  Ctasasignadasaval, Case when nivelrecup = 'Aval' then  SUM(concobro) else 0 end Cuentas_pagoaval
  , Case when nivelrecup = 'Aval' then ( SUM(concobro)/Count(cuenta) ) * 100 else 0 end  porccoberturaaval
					From
					(
					 Select cuenta,nivelrecup,SUM(Cobrado) cobrado,MAX(concobro) concobro
					 From
					 ( -- B
						Select F.nomina,F.cuenta,F.movimiento ,F.factura 
						,F.nivelrecup , SUM(ifnull(cobros,0)) cobrado
						 , case when SUM(ifnull(cobros,0))  >0 Then  1 else 0 end concobro
					     From
						( -- F
  						 Select nomina,cuenta,movimiento,factura, case when  nivel  like '%lct%' Then 'LCT' else empresa end nivelrecup
   						 From cuentas_temp
   						 Where ejercicio = '".$ano."' and periodo = '".$_POST['quincena']."'
   						 And nomina = '".$nomina."'
 						 Union all 
 						 Select   agente,cliente,mov , movid,'Aval'
  						 From cob_asignacionavalesweb
						 WHERE agente='".$nomina."'
				         AND quincena='".$_POST['quincena']."' AND ejercicio = '".$ano."'

						) F  
						left join ( Select agente,padre_mavi, padre_idmavi,cobros 
						               , SUBSTRING(campo_extra,1,INSTR(campo_extra,'_') -1) porigenmavi, SUBSTRING(campo_extra,INSTR(campo_extra,'_')+1,255) porigenidmavi
									  from abonos_497d
             						where quincena = '".$_POST['quincena']."' and ejercicio = '".$ano."'
            							and agente = '".$nomina."'
         						  ) a On (a.padre_mavi = F.movimiento and a.padre_idmavi =  F.factura )  or (a.porigenmavi = F.movimiento and a.porigenidmavi =  F.factura )
					   Group by F.nomina,F.cuenta,F.movimiento,F.factura,F.nivelrecup
 					)B
				    Group by cuenta,nivelrecup
				 )D
				Group BY nivelrecup	 
				)Fin
				";
				
		$aval = 
			"
			Select sum(cobros) as r_av
			from abonos_497d 
			where quincena = '".$_POST['quincena']."' and ejercicio = '".$ano."' and agente = '".$nomina."' and nivel_cobranza = 'AVAL'
			";
		$especial = 
			"
			Select sum(cobros) as r_ma 
			from abonos_497d 
			where quincena = '".$_POST['quincena']."' and ejercicio = '".$ano."' and agente = '".$nomina."' and nivel_cobranza = 'ESPECIAL'
			";
		$lctesp = 
			"
			Select sum(cobros) as r_lc
			from abonos_497d 
			where quincena = '".$_POST['quincena']."' and ejercicio = '".$ano."' and agente = '".$nomina."' and nivel_cobranza = 'LCTESP 507'
			";
		$mcesp = 
			"
			Select sum(cobros) as r_mc 
			from abonos_497d 
			where quincena = '".$_POST['quincena']."' and ejercicio = '".$ano."' and agente = '".$nomina."' and nivel_cobranza = 'MCESP 507'
			";							
		$sel0 =
			"
			Select 
				id,
				cerrado
			From
				Estadisticas_cartera 
			Where e_nomina = '".$_POST['nomina']."' and quincena = '".$_POST['quincena']."' and anio = '".$_POST['ano']."'
			";
		$sel1 =
			"
				SELECT
					id,
					concepto AS concepto,
					liga
				FROM parametros
				WHERE liga = '".$nombre_tabla."'
			";
		$selxx =	
			"
				SELECT
					id,
					concepto,
					liga
				FROM parametros
				WHERE liga = '".$nombre_tabla."'
			";
		$sel2 =
			"
				SELECT
					id,
					criterio AS descripcion
				FROM parametros_resp
			";
			//			WHERE liga = '".$nombre_tabla."'	
		$sel3 =
			"
				SELECT
					id,
					puntos AS valor
				FROM parametros
				WHERE liga = '".$nombre_tabla."'
			";
		
		$rep1 =
			"
				SELECT
					historial
				FROM registros_sanciones
				WHERE nomina = '".$_POST['nomina']."'	
			";
			
		/**/
		///////////////////////////////////////////////	
		$bul1 = 
			" 
			Select 
				sum(sancion) totsancion 
			from 
				sancion_actgest 
			order by dia 
			";
			
			$res=mysql_query($bul1);
			$dato=mysql_fetch_assoc($res);
			$totsancion=$dato["totsancion"];
			
		
			
			$sql="
			UPDATE estadisticas_cartera
			SET
				puntos1  = 100,
				sancion1 = ".$totsancion."
			WHERE e_nomina = '".$_POST['nomina']."' and quincena = ".$_POST['quincena']." and anio = ".$_POST['ano']."
			";
			
			mysql_query($sql);
			
		$tot1 = 
			" 
			Select 
				dia,
				sancion 
			from 
				sancion_actgest 
			"; 	
			
				
		$bul2 =
			"
				SELECT
					id,
					nombre,
					activada
				FROM
					cuotas 
				WHERE activada = 1 
			";			
			
	/*	$bul4 =
			"
				SELECT
					uno.fecha,
					COUNT(*) AS total,
					SUM(contacto) AS contacto,
					ROUND(SUM(contacto)/COuNT(contacto)*100, 2) AS porcentaje
					FROM
					(
					  SELECT
							CASE WHEN (reg.intelisis IS NULL OR reg.intelisis = 'SIN AGE' OR reg.intelisis = ' ') THEN reg.promotor ELSE reg.intelisis END AS nomina,
							reg.fecha_inicio AS fecha,
							CASE WHEN reg.resultado = par.resultado AND condir = 'ON' THEN 1 ELSE 0 END AS contacto
					FROM cuentas_temp andr
					LEFT JOIN registros_tem reg ON CASE WHEN (reg.intelisis IS NULL OR reg.intelisis = 'SIN AGE' OR reg.intelisis = ' ') THEN REPLACE(andr.nomina,' ','') = REPLACE(reg.promotor,' ','') ELSE  REPLACE(andr.nomina,' ','') = REPLACE(reg.intelisis,' ','') END
					and REPLACE(reg.cuenta,' ','')=REPLACE(andr.cuenta,' ','')
					and REPLACE(andr.movimiento,' ','')=REPLACE(reg.mov,' ','')
					and REPLACE(andr.factura,' ','')=REPLACE(reg.factura,' ','')
					LEFT JOIN parametros_gestiones par ON reg.resultado = par.resultado			
					where reg.fecha_inicio is not null
					)uno 
				GROUP BY fecha					
			";
			mysql_query($bul4);*/
			
			$bul4 = " Select *  from ctas_contacto_directo  " ;
			
		$bul4T =
			"	
			 Select  sum(case when sancion >0 then 1 else 0 end)  totsan ,sum(sancion) sancion ,SUM(total) AS total,
				SUM(contacto) AS contacto,
				ROUND(SUM(contacto)/sum(total)*100, 2) AS porcentaje
				from ctas_contacto_directo
			";
			
		$bul5 =
			"
			SELECT
				uno.fecha,
				uno.cuenta,
				uno.sin_contacto,
				uno.comentario
				FROM
				(
				  SELECT
						reg.intelisis AS nomina,
						reg.fecha_inicio AS fecha,
						reg.cuenta,
						reg.comentario,
						CASE WHEN reg.resultado = par.resultado AND condir <> 'ON' THEN reg.resultado ELSE '' END AS sin_contacto
				  FROM registros_tem reg
				  LEFT JOIN parametros_gestiones par ON reg.resultado = par.resultado
				)uno
			where sin_contacto <> ''
			GROUP BY cuenta
			ORDER BY fecha
			"; 
			
	//	$rq_bul5 = mysql_query($bul5);	
	//	$rows5 = mysql_num_rows($rq_bul5);
			
		$ids5 = 
			"
				SELECT id FROM registros_tem LIMIT ".$rows5."
			";	
		$tblc06 =
			"
			SELECT 
				id,
				concepto,
				criterio,
				sancion,
				base,
				nivel,
				puntos,
				tipo,
				cantidad_requerida,
				p_req_cobertura,
				s_p_no_cubrir,
				liga
			FROM
				parametros
			WHERE concepto = 'PROMESAS DE PAGO' and liga = '".$nombre_tabla."'
			";
			$res=mysql_fetch_assoc(mysql_query($tblc06));	
			$cumplir = $res['p_req_cobertura'];			
		$bul6T1 =
			"
			Select  count(cuenta) tot_ctas_c_pago
			From (
							SELECT  
								reg.cuenta 
							FROM registros_tem reg
							JOIN abonos_497d  p
								On  REPLACE(reg.cuenta,' ','')=REPLACE(p.cliente,' ','') 
								  and  p.quincena = ".$_POST['quincena']." and p.ejercicio = ".$_POST['ano']."
								and REPLACE(p.padre_mavi,' ','')=REPLACE(reg.mov,' ','')
								and REPLACE(p.padre_idmavi,' ','')=REPLACE(reg.factura,' ','')
							WHERE reg.resultado = 'promesa'
						   group by  reg.cuenta 
				)T
			";
			$res=mysql_fetch_assoc(mysql_query($bul6T1));	
			$tot_ctas_c_pago = $res['tot_ctas_c_pago'];
		$bul61 =
			"
				Select ROUND(".$tot_ctas_c_pago." / count(cuenta)*100,2) AS p_cubierto			
				  from
					(
						SELECT  
							reg.cuenta  
						FROM registros_tem reg 
						WHERE reg.resultado = 'promesa'
						group by  reg.cuenta 
					) t			
			";
		$bul61a =
			"
			Select
				id,
				concepto,
				criterio,
				sancion,
				base,
				nivel,
				puntos,
				tipo,
				cantidad_requerida,
				p_req_cobertura AS p_checar,
				s_p_no_cubrir,
				liga
			FROM
				parametros
			WHERE concepto = 'PROMESAS DE PAGO' and liga = '".$nombre_tabla."'  
			";
			
		$bul6 =
			"
			SELECT
				reg.fecha_inicio AS fecha,
				reg.cuenta,
				reg.hora_inicio AS inicio,
				reg.hora_final AS final,
				TIMEDIFF(hora_final, hora_inicio) AS duracion,
				reg.pago_promesa AS fecha_promesa,
				reg.pago_importe AS monto_promesa,
				reg.comentario
			FROM registros_tem reg
			WHERE resultado = 'promesa' 
			";						
		$bul7 =
			"
				SELECT
					reg.fecha_inicio AS fecha,
					reg.cuenta,
					reg.resultado,
					reg.comentario
				FROM cuentas_temp andr
				INNER JOIN registros_tem reg ON CASE WHEN (reg.intelisis IS NULL OR reg.intelisis = 'SIN AGE' OR reg.intelisis = ' ') THEN REPLACE(andr.nomina,' ','') = REPLACE(reg.promotor,' ','') ELSE  REPLACE(andr.nomina,' ','') = REPLACE(reg.intelisis,' ','') END
				and REPLACE(reg.cuenta,' ','')=REPLACE(andr.cuenta,' ','')
				and REPLACE(andr.movimiento,' ','')=REPLACE(reg.mov,' ','')
				and REPLACE(andr.factura,' ','')=REPLACE(reg.factura,' ','')				
			";
//			echo $bul7;
	//	$rq_bul7 = mysql_query($bul7);	
	//	$rows7 = mysql_num_rows($rq_bul7);
			
		$ids7 = 
			"
				SELECT id FROM registros_tem LIMIT ".$rows7."
			";	
			
		$bul8 =
			"
				SELECT
					cob.idcliente AS cuenta,
					cob.proceso AS resultado,
					cob.fecha AS fecha,
					cob.comentario_rechazo AS rechazo,
					CASE WHEN cob.estatus=3   THEN '1' ELSE '' END AS con_jefe,
					CASE WHEN cob.estatus=2   THEN '1' ELSE '' END AS rechazado,
					CASE WHEN cob.estatus=100 THEN '1' ELSE '' END AS cancelado,
					CASE WHEN cob.estatus=4   THEN '1' ELSE '' END AS determinacion,
					CASE WHEN cob.estatus=101 THEN '1' ELSE '' END AS por_desasignar
				FROM bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta cob
				INNER JOIN cuentas_temp adr
				ON cob.idcliente = adr.cuenta
				WHERE cob.agente = '".$_POST['nomina']."'
				GROUP BY cuenta			
			";
			
		$bul9 =
			"
				SELECT
					fecha_inicio AS fecha
				FROM registros_tem
				WHERE DAYOFWEEK(fecha_inicio)>1 
				GROUP BY fecha_inicio  
			";
//		echo $bul9;
		//$rq_bul9 = mysql_query($bul9);	
		//$rows9 = mysql_num_rows($rq_bul9);
			
		$ids9 = 
			"
				SELECT id FROM registros_tem LIMIT ".$rows9."
			";	
			
		$bul10 =
			"
				SELECT
					fecha_inicio AS fecha,
					cuenta,
					domicilio,
					colonia,
					ciudad AS poblacion,
					resultado
				FROM registros_tem
				WHERE (promotor='".$_POST['nomina']."' or intelisis='".$_POST['nomina']."') and fecha_inicio ".$periodo." and resultado<>'' 
				GROUP BY fecha_inicio ASC  
			";
			
			// WHERE DAYOFWEEK(fecha_inicio)>1
			$cp_qui = $_POST['quincena'];
			$cp_ano = $_POST['ano'];
		   $cp_qui = $cp_qui - 1;
			if ($cp_qui == 1) 
			{
				$cp_ano = $cp_ano - 1;
				$cp_qui = 24;
			}
			
			
			
		$bul11 =
			"Select v_fecha, cuenta , v_resultado ,comentario
				From
			(
			Select 	CASE WHEN r.fecha_inicio IS NULL THEN 'SIN FECHA' ELSE r.fecha_inicio END AS v_fecha,
					c.cuenta,
					CASE WHEN r.resultado IS NULL THEN 'SIN VISITA' ELSE r.resultado END AS v_resultado, 
					r.comentario
			From
			(
			Select cg.cuenta, cg.factura
			From cuentas_temp ct
			join comentarios_gestiones cg on REPLACE(cg.cuenta,' ','')=REPLACE(ct.cuenta,' ','')
			 and  REPLACE(cg.nomina,' ','') = REPLACE(ct.nomina,' ','')
			where cg.quinsena = ".$cp_qui." and cg.ejercicio = ".$cp_ano." and ( prioridad_visita = 'on' or  prioridad_deter = 'on' )
			Group by ct.cuenta , ct.factura
			)c
			LEFT JOIN registros_tem  r on REPLACE(c.cuenta,' ','')=REPLACE(r.cuenta,' ','')
			 )Fin  Group by v_fecha, cuenta , v_resultado ,comentario					
			";
			
			// echo $bul11;
		
#cuentas sin visita con Dv
	$tblc15 =
				"
				SELECT rango_medio, sancion 
				FROM Parametros 
				WHERE concepto = 'CUENTAS SIN VISITA CON DV > RANGO MEDIO' and liga = '".$nombre_tabla."' 
				";
			$res15=mysql_query($tblc15);
			$data15=mysql_fetch_array($res15);	
			$maxdias=$data15["rango_medio"];
			
	//echo $tblc15;

		$bul15=
			"Select v_fecha, cuenta , v_resultado, dias_vencidos 
				From
			(
			 Select 	CASE WHEN r.fecha_inicio IS NULL THEN 'SIN FECHA' ELSE r.fecha_inicio END AS v_fecha,
			 		c.cuenta,
					CASE WHEN r.resultado IS NULL OR r.resultado='SIN VISITA' THEN 'SIN VISITA'  ELSE r.resultado END AS v_resultado, 
					c.dias_vencidos
			 From
			 (
			   Select ct.cuenta, max(ct.dias_vencidos)as dias_vencidos
			   From cuentas_temp ct
			   Group by ct.cuenta 
			 )c
			 LEFT JOIN registros_tem  r on REPLACE(c.cuenta,' ','')=REPLACE(r.cuenta,' ','')
			 )Fin  WHERE (v_resultado='SIN VISITA') AND dias_vencidos >". $maxdias . "
			 Group by fin.v_fecha, fin.cuenta , fin.v_resultado ,fin.dias_vencidos
			 ORDER BY fin.cuenta
			";
			//echo $bul15;	
		$bul15t=
			"Select count(cuenta) ctas , sum(sinvisita) ctasSinVisita, (sum(sinvisita) /count(cuenta))*100 xcentvis 
				From
			(
			 Select CASE WHEN r.fecha_inicio IS NULL THEN 'SIN FECHA' ELSE r.fecha_inicio END AS v_fecha,
			 		c.cuenta,
					CASE WHEN r.resultado IS NULL THEN 1 ELSE 0 END AS sinvisita, 
					c.dias_vencidos
			 From
			 (
			   Select ct.cuenta, max(ct.dias_vencidos)as dias_vencidos
			   From cuentas_temp ct
			   Group by ct.cuenta 
			 )c
			 LEFT JOIN registros_tem  r on REPLACE(c.cuenta,' ','')=REPLACE(r.cuenta,' ','')
			 )Fin  WHERE  dias_vencidos >". $maxdias . "
			";
			//echo $bul15t;
#Pago de cuentas  con Dv rango medio
	$tblc16 =
				"
				SELECT rango_medio, sancion 
				FROM Parametros 
				WHERE concepto = 'PAGO DE CUENTAS CON DV > RANGO MEDIO' and liga = '".$nombre_tabla."' 
				";
				
			$res16=mysql_query($tblc16);
			$data16=mysql_fetch_array($res16);	
			$maxdias2=$data16["rango_medio"];


-
		$bul16="
		       SELECT t.cuenta, t.ejercicio, t.periodo, t.sdo_tot,ifnull(a.qcobros,0)as cantcobrada ,(ifnull(a.qcobros,0)/t.sdo_tot) as xcentcobrado
		       	 ,dias_vencidos  
				FROM
			   ( 
			     SELECT  cuenta,  ejercicio,  periodo
				 ,SUM( CASE WHEN movimiento like 'Seguro%' THEN sdo_ven ELSE sdo_tot END) as sdo_tot 
		       	 ,max(dias_vencidos) as dias_vencidos  
			     FROM cuentas_temp  
				  GROUP BY cuenta, ejercicio,  periodo
		       )t			   
			   LEFT JOIN 
		       ( 
		        SELECT cliente,sum(cobros)as qcobros
				FROM abonos_497d  
				where ejercicio=". $v_ano . " and quincena=" . $v_qui."
				GROUP BY cliente,ejercicio
		       )as a
		       ON t.cuenta=a.cliente
			   WHERE dias_vencidos > " . $maxdias2 . "
			  
			   ORDER BY cuenta
			   ";
//echo $bul16;
		$bul16t="
			SELECT round(sum(f.cantcobrada),2) totalcobros, round(sum(f.sdo_tot),2) total, round((sum(f.cantcobrada)/sum(f.sdo_tot))*100,2) as xcenttotal
			 FROM(
			 SELECT t.cuenta, t.ejercicio, t.periodo, t.sdo_tot,ifnull(a.qcobros,0)as cantcobrada ,(ifnull(a.qcobros,0)/t.sdo_tot) as xcentcobrado
							 ,t.dias_vencidos 
						   FROM 
						   (
						    SELECT  cuenta,  ejercicio,  periodo
						 	 ,SUM( CASE WHEN movimiento like 'Seguro%' THEN sdo_ven ELSE sdo_tot END) as sdo_tot
							 ,max(dias_vencidos) as dias_vencidos  
							 FROM cuentas_temp  
							  GROUP BY cuenta, ejercicio,  periodo
						  )as t
						   LEFT JOIN 
						   ( 
							SELECT cliente, sum(cobros) as qcobros
							FROM abonos_497d
                            WHERE ejercicio=". $v_ano . " and quincena=" . $v_qui." 
                            GROUP BY cliente
						   )as a
						   ON t.cuenta=a.cliente
						   WHERE dias_vencidos >  " . $maxdias2 . "
						   GROUP BY t.cuenta
						)as f
				";               
			 //echo $bul16t;
			$m3_fecha= $fecha1["fecha1"];
			$m4_fecha= $fecha2["fecha2"];			

#porcentaje cuentas rango medio	
		$tblc17 =
				"
				SELECT rango_medio, sancion 
				FROM Parametros 
				WHERE concepto = '% DE CUENTAS DE RANGO MEDIO ASIGNADAS AL GESTOR' and liga = '".$nombre_tabla."' 
				";
			$res17=mysql_query($tblc17);
			$data17=mysql_fetch_array($res17);
			$maxdias=$data17["rango_medio"];
		//	echo mysql_num_rows($res17) . " = ". $maxdias;
		
		$bul17="
			SELECT count(f.cuenta) as totalctas,
		    sum(case when f.dv>". $maxdias . " then 1 else 0 end) ctasDV,
				   (sum(case when f.dv>". $maxdias . " then 1 else 0 end)/count(f.cuenta))*100 as xcentctas
			FROM( 
				SELECT cuenta, max(dias_vencidos) as dv
				FROM cuentas_temp
				GROUP BY cuenta
			)f	
		";
			
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------		
		$temp_asv_p1 =
			"
			CREATE TEMPORARY TABLE bitacora_gestioncobranza_intelisis.ctas_sinvisita AS
			Select 	ct.nomina,
					ct.cuenta
			From cuentas_temp ct
			Left Join registros_tem  r on ct.nomina = r.promotor and 
			REPLACE(r.cuenta,' ','')=REPLACE(ct.cuenta,' ','')
			Where r.promotor is null
			Group by ct.nomina, ct.cuenta
			";
			mysql_query($temp_asv_p1 ) or die("p1".mysql_error());
		
			$quincena_vi = $quincena_vi - 1;
			if ($quincena_vi == 1) 
			{
				$ano_vi = $ano_vi - 1;
				$quincena_vi = 24;
			}
			$fecha1 = mysql_fetch_assoc(mysql_query("Select quincena_cobranza1(".$quincena_vi.", ".$ano_vi.") as fecha1"));
			$fecha2 = mysql_fetch_assoc(mysql_query("Select quincena_cobranza2(".$quincena_vi.", ".$ano_vi.") as fecha2"));
			
			$m3_fecha= $fecha1["fecha1"];
			$m4_fecha= $fecha2["fecha2"];				
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------			
		$temp_asv_p2 =
			"
			CREATE TEMPORARY TABLE bitacora_gestioncobranza_intelisis.ctas_periodo_ant  AS 
			SELECT a.* FROM( 
			SELECT c.nomina, c.cuenta FROM android_reportes._cuentas_gestioncobranza c
			where c.nomina = '".$_POST['nomina']."' and c.periodo = ".$quincena_vi."  and c.ejercicio = ".$ano_vi."
			UNION ALL
			select ad.agente_asociado as nomina, agc.cuenta
			from (
			select agente_origen, agente_asociado from bitacora_gestioncobranza_intelisis.agente_doble
			where fecha_asigna between '".$m3_fecha."' and '".$m4_fecha."' 
			group by agente_origen, agente_asociado
			) as ad
			left join android_reportes._cuentas_gestioncobranza agc on agc.nomina=ad.agente_origen
			where agc.periodo= ".$quincena_vi." and agc.ejercicio= ".$ano_vi." and ad.agente_asociado='".$_POST['nomina']."'
			) a 
			Join ctas_sinvisita v on  v.nomina = a.nomina  and REPLACE(a.cuenta,' ','')=REPLACE(v.cuenta,' ','')
			Group by a.cuenta, a.nomina
			";
//			echo $temp_asv_p2."<br><br>";
			mysql_query($temp_asv_p2)or die("p2".mysql_error());
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	
		$temp_asv_p3 =
			"

			CREATE TEMPORARY TABLE bitacora_gestioncobranza_intelisis.registros_periodo_ant
			Select ca.* from  ctas_periodo_ant ca
			LEFT JOIN (  select  cuenta, ifnull(intelisis, promotor) promotor 
			from bitacora_gestioncobranza_intcob.registros
			where ( promotor='".$_POST['nomina']."' or intelisis='".$_POST['nomina']."' ) and fecha_inicio between '".$m3_fecha."' and '".$m4_fecha."'  and resultado <> ''
			union all
			select  cuenta, ifnull(intelisis, promotor) promotor from bitacora_gestioncobranza_intelisis.registros
			where (promotor= '".$_POST['nomina']."' or intelisis='".$_POST['nomina']."') and fecha_inicio between '".$m3_fecha."' and '".$m4_fecha."'  and resultado <> '' 
			) a ON ca.nomina = a.promotor  and REPLACE(a.cuenta,' ','')=REPLACE(ca.cuenta,' ','')
			where a.cuenta is null
			Group by cuenta, promotor
			";
			
//			echo $temp_asv_p3."<br><br>";
			mysql_query($temp_asv_p3)or die("p3".mysql_error());

			$quincena_vi = $quincena_vi - 1;
			if ($quincena_vi == 1) 
			{
				$ano_vi = $ano_vi - 1;
				$quincena_vi = 24;
			}
			$fecha3 = mysql_fetch_assoc(mysql_query("Select quincena_cobranza1(".$quincena_vi.", ".$ano_vi.") as fecha3"));
			$fecha4 = mysql_fetch_assoc(mysql_query("Select quincena_cobranza2(".$quincena_vi.", ".$ano_vi.") as fecha4"));
			
			$m5_fecha= $fecha3["fecha3"];
			$m6_fecha= $fecha4["fecha4"];
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------			
		$temp_asv_p4=
			"
			CREATE TEMPORARY TABLE bitacora_gestioncobranza_intelisis.ctas_periodo_ant2  AS 
			SELECT a.* FROM( 
			SELECT c.nomina, c.cuenta FROM android_reportes._cuentas_gestioncobranza c
			where c.nomina = '".$_POST['nomina']."' and c.periodo = ".$quincena_vi."  and c.ejercicio = ".$ano_vi."
			UNION ALL
			select ad.agente_asociado as nomina, agc.cuenta from (
			select agente_origen, agente_asociado from bitacora_gestioncobranza_intelisis.agente_doble
			where fecha_asigna between '".$m5_fecha."' and '".$m6_fecha."' 
			group by agente_origen, agente_asociado
			) as ad
			left join android_reportes._cuentas_gestioncobranza agc on agc.nomina=ad.agente_origen
			where agc.periodo= ".$quincena_vi." and agc.ejercicio= ".$ano_vi." and ad.agente_asociado='".$_POST['nomina']."'
			) a 
			Join registros_periodo_ant v on  v.nomina = a.nomina  and
				REPLACE(a.cuenta,' ','')=REPLACE(v.cuenta,' ','')
			Group by a.nomina, a.cuenta
			";
			
//			echo $temp_asv_p4."<br><br>";
			mysql_query($temp_asv_p4)or die("p4".mysql_error());
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	 	$temp_asv_p5 =
			"		
			CREATE TEMPORARY TABLE bitacora_gestioncobranza_intelisis.ctas_sinvisita_1 as
			Select ca.* from  ctas_periodo_ant2 ca
			LEFT JOIN (  select  cuenta, ifnull(intelisis, promotor) promotor
			from bitacora_gestioncobranza_intcob.registros
			where ( promotor='".$_POST['nomina']."' or intelisis='".$_POST['nomina']."' ) and fecha_inicio between '".$m5_fecha."' and '".$m6_fecha."' and resultado <> ''
			union all
			select  cuenta, ifnull(intelisis, promotor) promotor from bitacora_gestioncobranza_intelisis.registros
			where (promotor= '".$_POST['nomina']."' or intelisis='".$_POST['nomina']."') and fecha_inicio between '".$m5_fecha."' and '".$m6_fecha."' and resultado <> '' 
			) a ON ca.nomina = a.promotor  and REPLACE(a.cuenta,' ','')=REPLACE(ca.cuenta,' ','')
			where a.cuenta is null
			Group by cuenta
			";
			
//			echo $temp_asv_p5."<br><br>";
		mysql_query($temp_asv_p5)or die("p5".mysql_error());
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------		
		  $bul12 =	
		 "
			SELECT 	cuenta ,
					'".$m5_fecha."' as fecha_inicio
			FROM bitacora_gestioncobranza_intelisis.ctas_sinvisita_1
			"; 
//			echo $bul12; 
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		$tblc01 =
			"
			SELECT 
				id,
				concepto,
				criterio,
				sancion,
				base,
				nivel,
				puntos,
				tipo,
				cantidad_requerida,
				p_req_cobertura,
				s_p_no_cubrir,
				liga
			FROM
				parametros
			WHERE concepto = 'ACTUALIZACION DE GESTIONES' and liga = '".$nombre_tabla."' 
			";		
		$tblc02 =
			"
			SELECT 
				id,
				concepto,
				criterio,
				sancion,
				base,
				nivel,
				puntos,
				tipo,
				cantidad_requerida,
				p_req_cobertura,
				s_p_no_cubrir,
				liga
			FROM
				parametros
			WHERE concepto = 'CONOCIMIENTO DE CUOTAS' and liga = '".$nombre_tabla."' 
			";			
		$tblc03 =
			"
			SELECT 
				id,
				concepto,
				criterio,
				sancion,
				base,
				nivel,
				puntos,
				tipo,
				cantidad_requerida,
				p_req_cobertura,
				s_p_no_cubrir,
				liga
			FROM
				parametros
			WHERE concepto = 'VISITAS DIARIAS' and liga = '".$nombre_tabla."'
			";			
		$tblc04 =
			"
			SELECT 
				id,
				concepto,
				criterio,
				sancion,
				base,
				nivel,
				puntos,
				tipo,
				cantidad_requerida,
				p_req_cobertura,
				s_p_no_cubrir,
				liga
			FROM
				parametros
			WHERE concepto = 'CUENTAS CONTACTO DIRECTO' and liga = '".$nombre_tabla."'
			";			
		$tblc05 =
			"
			SELECT 
				id,
				concepto,
				criterio,
				sancion,
				base,
				nivel,
				puntos,
				tipo,
				cantidad_requerida,
				p_req_cobertura,
				s_p_no_cubrir,
				liga
			FROM
				parametros
			WHERE concepto = 'INVESTIGACION EN RESULTADOS SIN CONTACTO' and liga = '".$nombre_tabla."'
			";					
		$tblc07 =
			"
			SELECT 
				id,
				concepto,
				criterio,
				sancion,
				base,
				nivel,
				puntos,
				tipo,
				cantidad_requerida,
				p_req_cobertura,
				s_p_no_cubrir,
				liga
			FROM
				parametros
			WHERE concepto = 'INCONGRUENCIA EN RESULTADO DE GESTION' and liga = '".$nombre_tabla."'
			";		
		$tblc08 =
			"
			SELECT 
				id,
				concepto,
				criterio,
				sancion,
				base,
				nivel,
				puntos,
				tipo,
				cantidad_requerida,
				p_req_cobertura,
				s_p_no_cubrir,
				liga
			FROM
				parametros
			WHERE concepto = 'CUENTAS A DESASIGNAR' and liga = '".$nombre_tabla."'
			";				
		$tblc09 =
			"
			SELECT 
				id,
				concepto,
				criterio,
				sancion,
				base,
				nivel,
				puntos,
				tipo,
				cantidad_requerida,
				p_req_cobertura,
				s_p_no_cubrir,
				liga
			FROM
				parametros
			WHERE concepto = 'ENRUTADO' and liga = '".$nombre_tabla."'
			";		
		$tblc10 =
			"
			SELECT 
				id,
				concepto,
				criterio,
				sancion,
				base,
				nivel,
				puntos,
				tipo,
				cantidad_requerida,
				p_req_cobertura,
				s_p_no_cubrir,
				liga
			FROM
				parametros
			WHERE concepto = 'GPS' and liga = '".$nombre_tabla."'
			";			
		$tblc11 =
			"
			SELECT 
				id,
				concepto,
				criterio,
				sancion,
				base,
				nivel,
				puntos,
				tipo,
				cantidad_requerida,
				p_req_cobertura,
				s_p_no_cubrir,
				liga
			FROM
				parametros
			WHERE concepto = 'CUENTAS PRIORIDAD' and liga = '".$nombre_tabla."'
			";		
		$tblc12 =
			"
			SELECT 
				id,
				concepto,
				criterio,
				sancion,
				base,
				nivel,
				puntos,
				tipo,
				cantidad_requerida,
				p_req_cobertura,
				s_p_no_cubrir,
				liga
			FROM
				parametros
			WHERE concepto = 'CUENTAS ASIGNADAS CON MAS DE 30 DIAS SIN GESTION' and liga = '".$nombre_tabla."'
			";				
		$tblc13 =
			"
			SELECT 
				id,
				concepto,
				criterio,
				sancion,
				base,
				nivel,
				puntos,
				tipo,
				cantidad_requerida,
				p_req_cobertura,
				s_p_no_cubrir,
				liga
			FROM
				parametros
			WHERE concepto = 'COBERTURA DE CARTERA' and liga = '".$nombre_tabla."'
			";		
		$tblc14 =
			"
			SELECT 
				id,
				concepto,
				criterio,
				sancion,
				base,
				nivel,
				puntos,
				tipo,
				cantidad_requerida,
				p_req_cobertura,
				s_p_no_cubrir,
				liga
			FROM
				parametros
			WHERE concepto = 'ASISTENCIA A REVISION' and liga = '".$nombre_tabla."'
			";			
			
	//	$rq_bul10 = mysql_query($bul10);	
	//	$rows10 = mysql_num_rows($rq_bul10);
			
		$ids10 = 
			"
				SELECT id FROM registros_tem LIMIT ".$rows10."
			";		
		
		$zona =
			"
				SELECT
					zona
				FROM agentes_zonas
				WHERE agente = '".$_POST['nomina']."' LIMIT 1
			";
		$fecha =
			"
				SELECT 
					ultima_revision AS fecha
				FROM estadisticas_cartera 
				WHERE e_nomina = '".$_POST['nomina']."' AND cerrado='SI'
				ORDER BY ultima_revision DESC
				LIMIT 1
			";	
	$sancion =
			"
			SELECT 
				id,
				concepto,
				criterio,
				sancion,
				base,
				nivel,
				puntos,
				tipo,
				cantidad_requerida,
				p_req_cobertura,
				s_p_no_cubrir,
				liga
			FROM
				parametros
				WHERE
					concepto = 'VISITAS DIARIAS' and liga = '".$nombre_tabla."'
			";			
			
		switch($_POST['quincena']){
			case 1:  if(date('d')>16 && date('m')==1) 
						{			
						 $dias = "'".$ano."-01-02' and '".$ano."-01-16'";
						}else
						{
						$dias = "'".$ano."-01-02' and '".date('Y-m-d', strtotime('-1 day'))."'";
						}
			break;
			case 2:   if(date('d')>1 && date('m')==2) 
						{
						 $dias = "'".$ano."-01-17' and '".$ano."-02-01'";
						}else
						{
						 $dias = "'".$ano."-01-17' and '".date('Y-m-d', strtotime('-1 day'))."'";
						}
			break;
			case 3:   if(date('d')>16 && date('m')==2) 
						{
						 $dias = "'".$ano."-02-02' and '".$ano."-02-16'";
						}else
						{
						 $dias = "'".$ano."-02-02' and '".date('Y-m-d', strtotime('-1 day'))."'";
						}						

			break;
			case 4:  if(date('d')>1 && date('m')==3) 
						{
						$dias = "'".$ano."-02-17' and '".$ano."-03-01'";
						}else
						{
						$dias = "'".$ano."-02-17' and '".date('Y-m-d', strtotime('-1 day'))."'";
						}						
			break;
			case 5:  if(date('d')>16 && date('m')==3)
						{
						$dias = "'".$ano."-03-02' and '".$ano."-03-16'";
						}else
						{
						$dias = "'".$ano."-03-02' and '".date('Y-m-d', strtotime('-1 day'))."'";
						}						
			break;
			case 6:  if(date('d')>1 && date('m')==4)
						{
						$dias = "'".$ano."-03-17' and '".$ano."-04-01'";
						}else
						{
						$dias = "'".$ano."-03-17' and '".date('Y-m-d', strtotime('-1 day'))."'";
						}						
			break;
			case 7:  if(date('d')>16 && date('m')==4)
						{
						$dias = "'".$ano."-04-02' and '".$ano."-04-16'";
						}else
						{
						$dias = "'".$ano."-04-02' and '".date('Y-m-d', strtotime('-1 day'))."'";
						}						
			break;
			case 8:  if(date('d')>1 && date('m')==5)
						{
						$dias = "'".$ano."-04-17' and '".$ano."-05-01'";
						}else
						{
						$dias = "'".$ano."-04-17' and '".date('Y-m-d', strtotime('-1 day'))."'";
						}						
			break;
			case 9:  if(date('d')>16 && date('m')==5)
						{
						$dias = "'".$ano."-05-02' and '".$ano."-05-16'";
						}else
						{
						$dias = "'".$ano."-05-02' and '".date('Y-m-d', strtotime('-1 day'))."'";
						}						
			break;
			case 10: if(date('d')>1 && date('m')==6)
						{
						$dias = "'".$ano."-05-17' and '".$ano."-06-01'";
						}else
						{
						$dias = "'".$ano."-05-17' and '".date('Y-m-d', strtotime('-1 day'))."'";
						}						
			break;
			case 11: if(date('d')>16 && date('m')==6)
						{
						$dias = "'".$ano."-06-02' and '".$ano."-06-16'";
						}else
						{
						$dias = "'".$ano."-06-02' and '".date('Y-m-d', strtotime('-1 day'))."'";
						}						
			break;
			case 12: if(date('d')>1 && date('m')==7)
						{
						$dias = "'".$ano."-06-17' and '".$ano."-07-01'";
						}else
						{
						$dias = "'".$ano."-06-17' and '".date('Y-m-d', strtotime('-1 day'))."'";
						}						
			break;
			case 13: if(date('d')>16 && date('m')==7)
						{
						$dias = "'".$ano."-07-02' and '".$ano."-07-16'";
						}else
						{
						$dias = "'".$ano."-07-02' and '".date('Y-m-d', strtotime('-1 day'))."'";
						}						
			break;
			case 14: if(date('d')>1 && date('m')==8)
						{
						$dias = "'".$ano."-07-17' and '".$ano."-08-01'";
						}else
						{
						$dias = "'".$ano."-07-17' and '".date('Y-m-d', strtotime('-1 day'))."'";
						}						
			break;
			case 15: if(date('d')>16 && date('m')==8)
						{
						$dias = "'".$ano."-08-02' and '".$ano."-08-16'";
						}else
						{
						$dias = "'".$ano."-08-02' and '".date('Y-m-d', strtotime('-1 day'))."'";
						}						
			break;
			case 16: if(date('d')>1 && date('m')==9)
						{
						$dias = "'".$ano."-08-17' and '".$ano."-09-01'";
						}else
						{
						$dias = "'".$ano."-08-17' and '".date('Y-m-d', strtotime('-1 day'))."'";
						}						
			break;
			case 17: if(date('d')>16 && date('m')==9)
						{
						$dias = "'".$ano."-09-02' and '".$ano."-09-16'";
						}else
						{
						$dias = "'".$ano."-09-02' and '".date('Y-m-d', strtotime('-1 day'))."'";
						}						
			break;
			case 18: if(date('d')>1 && date('m')==10)
						{
						$dias = "'".$ano."-09-17' and '".$ano."-10-01'";
						}else
						{
						$dias = "'".$ano."-09-17' and '".date('Y-m-d', strtotime('-1 day'))."'";
						}						
			break;
			case 19: if(date('d')>16 && date('m')==10)
						{
						$dias = "'".$ano."-10-02' and '".$ano."-10-16'";
						}else
						{
						$dias = "'".$ano."-10-02' and '".date('Y-m-d', strtotime('-1 day'))."'";
						}						
			break;
			case 20: if(date('d')>1 && date('m')==11)
						{
						$dias = "'".$ano."-10-17' and '".$ano."-11-01'";
						}else
						{
						$dias = "'".$ano."-10-17' and '".date('Y-m-d', strtotime('-1 day'))."'";
						}						
			break;
			case 21: if(date('d')>16 && date('m')==11)
						{
						$dias = "'".$ano."-11-02' and '".$ano."-11-16'";
						}else
						{
						$dias = "'".$ano."-11-02' and '".date('Y-m-d', strtotime('-1 day'))."'";
						}						
			break;
			case 22: if(date('d')>1 && date('m')==12)
						{
						$dias = "'".$ano."-11-17' and '".$ano."-12-01'";
						}else
						{
						$dias = "'".$ano."-11-17' and '".date('Y-m-d', strtotime('-1 day'))."'";
						}						
			break;
			case 23: if(date('d')>16 && date('m')==12)
						{
						$dias = "'".$ano."-12-02' and '".$ano."-12-16'";
						}else
						{
						$dias = "'".$ano."-12-02' and '".date('Y-m-d', strtotime('-1 day'))."'";
						}						
			break;
			case 24: if(date('d')>1 && date('m')==1)
						{
						$dias = "'".$ano."-12-17' and '".$nano."-01-01'";
						}else
						{
						$dias = "'".$ano."-12-17' and '".date('Y-m-d', strtotime('-1 day'))."'";
						}						
			break;
		}	
		
	$rdias =		
			" 
				SELECT DAY(dia) as id, dia as fecha from dias
					  
			";
	$info="SELECT '" . $nombre_tabla . "' as configuracion,". $GLOBALS['quincena'] .  " as quincena," . $GLOBALS['nano'] . " as anio";		
			
		//// Template ////
		$this->tpl="gestionescobranza/administrador_cartera/reporte_adminc.htm";		
	
		//// Consulta TinyButStrong ////
		$bloq = array
				(
					"post" => empty($_POST)?array():array($_POST),
					"rc1"  => $sql1,
					"rc11" => $sql11,
					"rc2"  => $sql2,
					"rc3"  => $sql3,
					"rc4"  => $sqll4,
					"sel0" => $sel0,
					"sel1" => $sel1,
					"selx" => $selxx,
					"sel2" => $sel2,
					"sel3" => $sel3,
					"rep1" => $rep1,
					"bul2" => $bul2,
					"bul4" => $bul4,
					"bul4T"=> $bul4T,
					"bul5" => $bul5,					
					"bul6" => $bul6,
					"bu6a" => $bul61a,
					"bul7" => $bul7,
					"bul8" => $bul8,
					"bul9" => $bul9,
					"bul10"=> $bul10,
					"bul11"=> $bul11,
					"bul12"=> $bul12,
					"bul15"=> $bul15,
					"bul15t"=>$bul15t,
					"bul16"=> $bul16,
					"bul16t"=>$bul16t,
					"bul17"=> $bul17,
					"zona" => $zona,
					"fecha"=> $fecha,
					"dias" => $rdias,
					"rc1p" => $sqlp,
					"tb01" => $tblc01,
					"tb02" => $tblc02,
					"tb03" => $tblc03,
					"tb04" => $tblc04,
					"tb05" => $tblc05,
					"tb06" => $tblc06,
					"tb07" => $tblc07,
					"tb08" => $tblc08,
					"tb09" => $tblc09,
					"tb10" => $tblc10,
					"tb11" => $tblc11,
					"tb12" => $tblc12,
					"tb13" => $tblc13,
					"tb14" => $tblc14,
					"tb15" => $tblc15,
					"tb16" => $tblc16,
					"tb17" => $tblc17,
					"bu61" => $bul61,
					"sanc" => $sancion,
					"bul1" => $bul1,
					"tot1" => $tot1,
					"s_av" => $aval,
					"s_ma" => $especial,
					"s_lc" => $lctesp,
					"s_mc" => $mcesp,
					"info" => $info
	
				);
		return $bloq;
	 } //fin _reporte_adminc

	function _admin_cts_drts(){	
	
		//// Template ////
		$this->tpl="gestionescobranza/administrador_cartera/admin_cts_drcts.htm";
		
		$sql =
			"
				SELECT
					resultado AS valores,
					resultado AS `name`,
					resultado AS `value`,
					condir,
					desasig
				FROM parametros_gestiones
			";
		
		$bloq = array
				(
					"post" => empty($_POST)?array():array($_POST),
					"sql"  => $sql
				);
		return $bloq;
		
	}

	function _control_cts_drts(){
		
		$vars= 
			"
				SELECT
					DISTINCT(resultado) AS valor
				FROM parametros_gestiones
			";
					
		$rq_vars = mysql_query($vars);
		while ($var = mysql_fetch_assoc($rq_vars)){
				$query = 
					"
						UPDATE parametros_gestiones
							SET 
							condir = '".$_POST['condir_'.$var['valor']]."',
							desasig = '".$_POST['desasig_'.$var['valor']]."'
						WHERE resultado = '".$_POST['resul_'.$var['valor']]."'
					";
				mysql_query($query);
		}
		//// Template ////
		$this->tpl="gestionescobranza/administrador_cartera/admin_cts_drcts.htm";
		
		$sql =
			"
				SELECT
					resultado AS valores,
					resultado AS `name`,
					resultado AS `value`,
					condir,
					desasig
				FROM parametros_gestiones
			";
		
		$bloq = array
				(
					"post" => empty($_POST)?array():array($_POST),
					"sql"  => $sql
				);
		return $bloq;
		
	}

	function _dias_sin_visita(){
		
		//// Template ////
		$this->tpl="gestionescobranza/administrador_cartera/revision_de_cartera.htm";
		
		/*$sel1 =
			"
				SELECT
				  DISTINCT(es) AS nivel
				FROM agentes age
				WHERE age.es NOT LIKE '%TEL%'
				AND age.es NOT LIKE '%GER%'
				AND age.es NOT LIKE '%JEFE%'
			";
			
		$sel2 = 
			"
				SELECT 
					DISTINCT(SUBSTRING(fecha_inicio, 1, 4))AS periodo
				FROM registros 
				WHERE fecha_inicio BETWEEN '2013-01-01' AND NOW()
				ORDER BY periodo
			";*/
			$sel1 =
			"
				SELECT distinct TRIM(substring_index(MID(es, 5, 100), '0', 1)) as nivel FROM agentes where nomina=agente order by es
			";
			$sel2 = 
			"
				SELECT distinct TRIM(substring_index(MID(es, 5, 100), '0', 2)) as nombre FROM agentes where nomina=agente order by es
			";
			
		
	
		$bloq = array
				(
					"post" => empty($_POST)?array():array($_POST),
					"sel1" => $sel1,
					"sel2" => $sel2
				);
		return $bloq;
		
	}
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function _param_vars(){
        $param = "SELECT * FROM parametros_variables";
        //// Template ////
        $this->tpl = "gestionescobranza/administrador_cartera/parametros_variables.htm";

        $bloq = array(
            "post" => empty($_POST) ? array() : array($_POST),
            "blk1" => $param
        );

        return $bloq;
    }

    function _rep_revs(){
        //// Template ////
        $this->tpl = "gestionescobranza/administrador_cartera/revision_de_carteras.htm";

        $sel1 = "SELECT DISTINCT(es) AS nivel FROM agentes age
            WHERE age.es NOT LIKE '%TEL%' AND age.es NOT LIKE '%GER%' AND age.es NOT LIKE '%JEFE%'";

        $sel2 = "SELECT DISTINCT(SUBSTRING(fecha_inicio, 1, 4))AS periodo FROM registros 
            WHERE fecha_inicio BETWEEN '2013-01-01' AND NOW()
            ORDER BY periodo";

        $bloq = array(
            "post" => empty($_POST) ? array() : array($_POST),
            "sel1" => $sel1,
            "sel2" => $sel2
        );

        return $bloq;
    }

    function _asig_fol(){
        $this->tpl = "AsignaciondeFolios/AsignaFolios.html";
        $data = array();
        $aux['date'] = date('Y')."-".date('m')."-".date('d');
        $aux['nomina'] = $_SESSION['nomina'];
        $data[] = $aux;
        $arrayBlock = array("blk1" => $data);

        return $arrayBlock;
    }

    function _fol_rev(){
        $this->tpl = "AsignaciondeFolios/FoliosRevision.html";
        $data = array();
        $aux['date'] = date('Y')."-".date('m')."-".date('d');
        $aux['nomina'] = $_SESSION['nomina'];
        $data[] = $aux;
        $GLOBALS['nomina_recibe'] = $_SESSION['nomina'];
        $GLOBALS['nombre_recibe'] = $_SESSION['nombre'];
        $arrayBlock = array("blk1" => $data);

        return $arrayBlock;
    }

    function _fol_cap(){
        $this->tpl = "AsignaciondeFolios/FoliosCaptura.html";
        $data = array();
        $aux['date'] = date('Y')."-".date('m')."-".date('d');
        $aux['nomina'] = $_SESSION['nomina'];
        $data[] = $aux;
        $Movimientos = 'SELECT MOV FROM bitacora_gestioncobranza_intelisis.registros WHERE MOV != "" GROUP BY MOV';
        $Movimientos2 = 'SELECT MOV FROM bitacora_gestioncobranza_intelisis.registros WHERE MOV != "" GROUP BY MOV';
        $Adjudicados = 'SELECT id_Articulo,Articulo,Estado FROM folios.ArticuloEvaluado where descontinuado="NO"';

        $arrayBlock = array(
            "post" => empty($_POST) ? array() : array($_POST),
            "blk1" => $data,
            "blk2" => $Adjudicados,
            "blk3" => $Movimientos,
            "blk4" => $Movimientos2
        );

        return $arrayBlock;
    }

    function _fol_Rea(){
        $this->tpl = "AsignaciondeFolios/FoliosReasignacion.html";
        $data = array();
        $aux['date'] = date('Y')."-".date('m')."-".date('d');
        $aux['date2'] = $this->myWeek(date('D')).", ".date('d')." de ".$this->myMonth(date('m'))." de ".date('Y');
        $aux['nomina'] = $_SESSION['nomina'];
        $data[] = $aux;
        $arrayBlock = array("blk1" => $data);

        return $arrayBlock;
    }

    function _fol_cor(){
        $this->tpl = "AsignaciondeFolios/FoliosCorrecion.html";
        $data = array();
        $aux['date'] = date('Y')."-".date('m')."-".date('d');
        $aux['nomina'] = $_SESSION['nomina'];
        $data[] = $aux;
        $Adjudicados = 'SELECT id_Articulo,Articulo,Estado FROM folios.ArticuloEvaluado';
        $Movimientos = 'SELECT MOV FROM bitacora_gestioncobranza_intelisis.registros WHERE MOV != "" GROUP BY MOV';
        $Movimientos2 = 'SELECT MOV FROM bitacora_gestioncobranza_intelisis.registros WHERE MOV != "" GROUP BY MOV';
        $arrayBlock = array(
            "post" => empty($_POST) ? array() : array($_POST),
            "blk1" => $data,
            "blk2" => $Adjudicados,
            "blk3" => $Movimientos,
            "blk4" => $Movimientos2
        );

        return $arrayBlock;
    }

    function _fol_con(){
        $this->tpl = "AsignaciondeFolios/FolioConsulta.html";
        $data = array();
        $aux['date'] = date('Y')."-".date('m')."-".date('d');
        $aux['nomina'] = $_SESSION['nomina'];
        $data[] = $aux;
        $Adjudicados = 'SELECT id_Articulo, Articulo, Estado FROM folios.ArticuloEvaluado';
        $arrayBlock = array(
            "post" => empty($_POST) ? array() : array($_POST),
            "blk1" => $data,
            "blk2" => $Adjudicados
        );

        return $arrayBlock;
    }

    function _fol_his(){
        $this->tpl = "AsignaciondeFolios/FolioHistorica.html";
        $data = array();
        $aux['date'] = date('Y')."-".date('m')."-".date('d');
        $aux['date2'] = $this->myWeek(date('D')).", ".date('d')." de ".$this->myMonth(date('m'))." de ".date('Y');
        $aux['nomina'] = $_SESSION['nomina'];
        $data[] = $aux;
        $arrayBlock = array("blk1" => $data);

        return $arrayBlock;
    }

    function _fol_rep(){
        $nivel = "SELECT DISTINCT(es) AS nivel FROM agentes";
        $this->tpl = "AsignaciondeFolios/FolioReporte.html";

        $bloq = array(
            "post" => empty($_POST) ? array() : array($_POST),
            "sel1" => $nivel
        );

        return $bloq;
    }

	function _cat_adjud(){
		$this->tpl = "gestionescobranza/Catalogo_Adjudicados.html";
	}

    public function _parTipoDtrmin() {
        if( !empty( $_POST ) ) {
            if( isset( $_POST['accion'] ) ) {   
                $this->tpl = "gestionescobranza/armadoExpedientes/parTipoDtrmin.htm";
                $this->datos = array( 
                    'tipoAgregar'   => $_POST['tipoAgregar'], 
                    //'agregarValor'  => htmlentities( strtoupper( utf8_encode( trim( $_POST['agregarValor'] ) ) ) ), 
                    'agregarValor'  => strtoupper( trim( $_POST['agregarValor'] ) ), 
                    'accion'        => $_POST['accion'], 
                    'tipoEliminar'  => $_POST['tipoEliminar'],
                    'valorEliminar' => $_POST['valorEliminar'],
					'campoARelacionar' => $_POST['campoARelacionar']
                );
                if( $this->datos['accion'] == 'Eliminar' ) {
                    switch ( $this->datos['tipoEliminar'] ) {
                        case 'p': case 'r':
                            $this->sql  = "DELETE FROM tabla_principal";
                            $this->sql .= " WHERE id = " . htmlentities( utf8_encode( $this->datos['valorEliminar'] ) );
							$this->sql .= " AND id NOT IN(57,58,59,60,61,75,76,77)";
                        break;
                        case 'tipoDeterminacion':
                            $this->sql  = "ALTER TABLE tabla_principal DROP ";
                            $this->sql .= "`".$this->datos['valorEliminar']."`";
                        break;
                        default:
                            exit( "Error no entro al switch" );
                        break;
                    }
                } else {
                    switch ( $this->datos['tipoAgregar'] ) {                    
                        case 'parametro':
                            $this->sql  = "INSERT INTO tabla_principal( NOMBRE, TIPO )";
                            $this->sql .= "VALUES('".htmlentities( utf8_encode( $this->datos['agregarValor'] ) )."', 'p')";
                        break;
                        case 'requisito':
                            $this->sql  = "INSERT INTO tabla_principal( NOMBRE, TIPO )";
                            $this->sql .= "VALUES('".htmlentities( utf8_encode( $this->datos['agregarValor'] ) )."', 'r')";
                        break;
                        case 'tipoDeterminacion':
                            $this->sql  = "ALTER TABLE tabla_principal ADD(";
                            $this->sql .= "`" . str_replace( chr(32), "_", $this->datos['agregarValor'] . "`" );
                            $this->sql .= " VARCHAR(255))";
                        break;
                        default:
                            exit( "error" );
                        break;
                    }
                }
                $exca = mysql_query( $this->sql ) or die( mysql_error() );
				if( $this->datos['accion'] == 'Eliminar' && $this->datos['tipoEliminar'] == 'p' ) {
					$sql = "SELECT 
								CAMPO_DELA_TABLA, ID_PARAMETRO  
							FROM bitacora_gestioncobranza_intelisis.equivalencia_parametros_tipodeter
							WHERE ID_PARAMETRO=".$this->datos['valorEliminar'];
					$exc = mysql_query( $sql ) or die( mysql_error() );
					$dats = mysql_fetch_assoc( $exc );
					//ACTUALIZACION DEL ARCHIVO DE TEXTO, para que vuelva a aparecer en el valor select el campo de la tabla
						$archivo = fopen( $GLOBALS['pathCobra']."armadoExpedientes/camposExistentes.txt", "a" );
						fputs( $archivo, "," . $dats['CAMPO_DELA_TABLA'] );
						fclose( $archivo );
					//FIN DE ACTUALIZACION					
					$sql = "DELETE FROM bitacora_gestioncobranza_intelisis.equivalencia_parametros_tipodeter
							WHERE ID_PARAMETRO=".$dats['ID_PARAMETRO'];
					mysql_query( $sql ) or die( mysql_error() );
					
				} else if( $this->datos['tipoAgregar'] == 'parametro' ) {					
					$id = mysql_insert_id();
					$this->sql = "INSERT INTO bitacora_gestioncobranza_intelisis.equivalencia_parametros_tipodeter(
								  CAMPO_DELA_TABLA, ID_PARAMETRO ) 
								  VALUES( '".$this->datos['campoARelacionar']."', ".$id." )";
					mysql_query( $this->sql ) or die( mysql_error() );
					
					//Funcion para actualizar el txt, y que ya no aparezca el valor en el select
						$lectura = fopen( $GLOBALS['pathCobra']."armadoExpedientes/camposExistentes.txt", "r" ) or die( "No se pudo leer el archivo" );
						while( !feof( $lectura ) ) {
							$linea = fgets( $lectura );
							$divide = explode( ",", $linea );
						}
						fclose( $lectura );		
						$queNoEsteEn = array( $this->datos['campoARelacionar'] );
						$camposHacerReferencia = array_diff( $divide, $queNoEsteEn );
						
						$archivo = fopen( $GLOBALS['pathCobra']."armadoExpedientes/camposExistentes.txt", "w" );
							foreach ( $camposHacerReferencia as $key => $value ) {
								fputs( $archivo, ( ( $key == 0 ) ? "":"," ) . $value );
							}
						fclose( $archivo );
					//Fin de funcion 
				}
				
				if( mysql_affected_rows() > 0 ){
					header( "Location: inicio.php?mod=bgc.configura&ac=parTipoDtrmin&reg" );
				} else {
					header( "Location: inicio.php?mod=bgc.configura&ac=parTipoDtrmin" );
				}
				
                   
            } else if( isset( $_POST['registrar'] ) ) {  

                $ids = "SELECT * FROM tabla_principal ORDER BY TIPO";
                $campos = " SELECT 
                                COLUMN_NAME as COLUMNA 
                                FROM INFORMATION_SCHEMA.Columns 
                            WHERE TABLE_NAME = 'tabla_principal' 
                                AND COLUMN_NAME NOT IN ( 'TIPO', 'NOMBRE', 'ID' )";
                $exc = mysql_query( $campos ) or die( mysql_error() );
                $campos = array();
                while ( $r = mysql_fetch_assoc( $exc ) ) {
                    $campos[] = $r['COLUMNA'];
                }
                //Para poner todos los checkbox en 0
                foreach ( $campos as $value ) {
                    $this->sql = "UPDATE tabla_principal SET ".$value." = 0
                                  WHERE TIPO = 'r'";
                    mysql_query( $this->sql ) or die( mysql_error() );
                }

                $exc = mysql_query( $ids ) or die( mysql_error() );
                while ( $r = mysql_fetch_assoc( $exc ) ) {
                    $id = $r['ID'];
                    
                    foreach ( $campos as $value ) {
                        $valorName = trim( $_POST[$id."|".$value] );
                        $name = $id."|".$value;                        
                        $split = explode( "|", $name );
                        $id = $split[0];
                        $columna = $split[1];

                        if( $r['TIPO'] == 'p' ) {
                            $this->sql = "
                                UPDATE tabla_principal 
                                SET ".$columna." = '".$valorName."'
                                WHERE ID = " . $id;
                            mysql_query( $this->sql ) or die( mysql_error() );
                        } else {
                            if( $valorName != "" ) {
                                $this->sql = "
                                    UPDATE tabla_principal 
                                    SET ".$columna." = 1
                                    WHERE ID = " . $id;
                                mysql_query( $this->sql ) or die( mysql_error() );
                            }
                        }
                    }
                }
                header( "Location: inicio.php?mod=bgc.configura&ac=parTipoDtrmin&reg" );              
            } else 
                exit( "error catastrofico" );
        } else {
            $this->tpl = "gestionescobranza/armadoExpedientes/parTipoDtrmin.htm";
            ( isset( $_GET['reg'] ) ) ? $this->msgaviso = "ACTUALIZADO":null;
            
            $columnas = "SELECT 
                            COLUMN_NAME as COLUMNA 
                         FROM INFORMATION_SCHEMA.Columns 
                         WHERE TABLE_NAME = 'tabla_principal' 
                         AND COLUMN_NAME NOT IN ( 'TIPO' )";
          
            $exc = mysql_query( $columnas ) or die( mysql_error() );
            $columns = array();
            while( $r = mysql_fetch_assoc( $exc ) ) { 
                $columns[] = $r['COLUMNA'];
            } 

            $this->sql = "SELECT * FROM tabla_principal WHERE TIPO='p'";
            $parametros = $this->valores( $columns, $this->sql );
            $this->datos = array();

            $this->sql = "SELECT * FROM tabla_principal WHERE TIPO='r'";  
            $requisitos = $this->valores( $columns, $this->sql );
            $this->datos = array();			
			
			$camposHacerReferencia = array();
			$camposExistentes = array();
			$sql = "SELECT CAMPO_DELA_TABLA FROM equivalencia_parametros_tipodeter";
			$exc = mysql_query( $sql ) or die( mysql_error() );
			while( $r = mysql_fetch_assoc( $exc ) ) {
				$camposExistentes[] = $r['CAMPO_DELA_TABLA'];
			}
			//print_r( $camposExistentes );
			
			if( !file_exists( $GLOBALS['pathCobra']."armadoExpedientes/camposExistentes.txt" ) ) {
				$arc = fopen( $GLOBALS['pathCobra']."armadoExpedientes/camposExistentes.txt", "w" );
				fputs( $arc, "fechafactura,factura,cuenta,nombre,pzo,importefactura,fechaprimerabono,fechaultimopago,importeultimopago,saldocapital,intereses,saldototal,diasinactividad,diasvencidos,saldovencido,rutacobro,druta,articulo,agente,mov,rutacobro_int,tipo_cliente,abonos_c086,IDCLIENTE,COBROAVAL,DEFUNCION,INSOLVENC,LOCALIZAC,NEGATIVA,ADJUDICAC,AVISO,LCT,LEGAL,PAGO,PROMESA,RECADO,NUM_INFORMES,resultado" );
				fclose( $arc );
			}
			$lectura = fopen( $GLOBALS['pathCobra']."armadoExpedientes/camposExistentes.txt", "r" ) or die( "No se pudo leer el archivo" );
			while( !feof( $lectura ) ) {
				$linea = fgets( $lectura );
				$divide = explode( ",", $linea );
			}
			fclose( $lectura );
			
			
			$camposHacerReferencia = array_diff( $divide, $camposExistentes );
			$queNoEsteEn = array( "BASE" );
			$camposHacerReferencia = array_diff( $camposHacerReferencia, $queNoEsteEn );
			
            $bloque = array(
                "col, cl, c, p" => $columns,
                "blk"        => ( count( $parametros ) > 0 ) ? $parametros:array(),
                "blk2"       => ( count( $requisitos ) > 0 ) ? $requisitos:array(),
				"blk3"		 => $camposHacerReferencia
            );
            return $bloque; 
        }
    }

    private function valores($columns, $sql){
        $exc = mysql_query($sql) or die(mysql_error());
        $indice = 0;

        if(mysql_num_rows($exc) > 0){
            while($r = mysql_fetch_assoc($exc)){
                foreach($columns as $value){
                    $this->datos[$indice][$value] = $r[$value];
                }

                $indice++;
            }
        }

        return $this->datos;
    }

    public function _armadoExpedientes() {
        $this->tpl = "gestionescobranza/armadoExpedientes/armadoExpedientes.htm";
        global $pathCobra, $band;
        if( isset( $_GET['reg'] ) ) {
            $this->msgaviso = ( $_GET['reg'] == 1 ) ? "PROCESO INICIADO":"NO EXISTE INFORMACION";    
        }
        
        $band = 0;
        if( isset( $_POST['aExcel'] ) ) {   
       
            ini_set('memory_limit', '-1');
            set_time_limit( 0 );
            require_once ( 'PHPExcel.php' );
            require_once ( 'PHPExcel/IOFactory.php');
            require_once ( 'PHPExcel/Writer/Excel5.php' );
            require_once ( 'template_htm/reporteQuejas/estilos.php' );

            $excel = new PHPExcel(); 
            $excel->getActiveSheet()->getColumnDimension( 'A' )->setAutoSize( true );    
            $excel->getActiveSheet()->getColumnDimension( 'B' )->setAutoSize( true );
            $excel->getActiveSheet()->getColumnDimension( 'C' )->setAutoSize( true );
            $excel->getActiveSheet()->getColumnDimension( 'D' )->setAutoSize( true );
            $excel->getActiveSheet()->getColumnDimension( 'E' )->setAutoSize( true );
            $excel->getActiveSheet()->getColumnDimension( 'F' )->setAutoSize( true );
            $excel->getActiveSheet()->getColumnDimension( 'G' )->setAutoSize( true );
            $excel->getActiveSheet()->getColumnDimension( 'H' )->setAutoSize( true );
            $excel->getActiveSheet()->getColumnDimension( 'I' )->setAutoSize( true );
            $excel->getActiveSheet()->getColumnDimension( 'J' )->setAutoSize( true );
            $excel->getActiveSheet()->getColumnDimension( 'K' )->setAutoSize( true );
            $excel->getActiveSheet()->getColumnDimension( 'L' )->setAutoSize( true );
            $excel->getActiveSheet()->getColumnDimension( 'M' )->setAutoSize( true );
            $excel->getActiveSheet()->getColumnDimension( 'N' )->setAutoSize( true );
            $excel->getActiveSheet()->getColumnDimension( 'O' )->setAutoSize( true );
            $excel->getActiveSheet()->getColumnDimension( 'P' )->setAutoSize( true );

            $excel->getActiveSheet()->freezePaneByColumnAndRow( 0, 2 );
            //$excel->getActiveSheet()->freezePaneByColumnAndRow( 1, 0 );

            $excel->getActiveSheet()->setTitle( 'ARMADO DE EXPEDIENTES' );
            $excel->getActiveSheet()->setCellValue( "A1", 'CUENTA' );
            $excel->getActiveSheet()->setCellValue( "B1", 'NOMBRE' );
            $excel->getActiveSheet()->setCellValue( "C1", 'FACTURA' );
            $excel->getActiveSheet()->setCellValue( "D1", utf8_encode( 'CONDICIÓN' ) );
            $excel->getActiveSheet()->setCellValue( "E1", utf8_encode( 'FECHA EMISIÓN' ) );
            $excel->getActiveSheet()->setCellValue( "F1", 'ULTIMO PAGO' );
            $excel->getActiveSheet()->setCellValue( "G1", 'DV' );
            $excel->getActiveSheet()->setCellValue( "H1", 'DI' );
            $excel->getActiveSheet()->setCellValue( "I1", 'IMPORTE INICIAL' );
            $excel->getActiveSheet()->setCellValue( "J1", 'ABONOS' );
            $excel->getActiveSheet()->setCellValue( "K1", '% ABONOS VS IMPORTE INICIAL' );
            $excel->getActiveSheet()->setCellValue( "L1", 'SALDO CAPITAL' );
            $excel->getActiveSheet()->setCellValue( "M1", 'INTERES MORATORIO' );
            $excel->getActiveSheet()->setCellValue( "N1", 'SALDO TOTAL' );
            $excel->getActiveSheet()->setCellValue( "O1", utf8_encode( 'DESCRIPCIÓN DE ARTICULO' ) );
            $excel->getActiveSheet()->setCellValue( "P1", utf8_encode( 'TIPO DETERMINACIÓN' ) );
            $excel->getActiveSheet()->getStyle( 'A1:P1' )->applyFromArray( $res_gris );

            $letra = 65;
            foreach( $_POST as $key => $value ) {
                $cont = 2;
                if( is_array( $value ) ) {
                    foreach ( $value as $indice => $valor ) {
                        if( chr( $letra ) == 'K' ) {
                            if( strrpos( $valor, "." ) )
                                $excel->getActiveSheet()->setCellValue( chr( $letra ) . $cont, number_format( $valor*100, 0 )."%" );   
                            else
                                $excel->getActiveSheet()->setCellValue( chr( $letra ) . $cont, $valor."%" );       
                        } else {
                            $excel->getActiveSheet()->setCellValue( chr( $letra ) . $cont, utf8_encode( $valor ) );
                        }
                        $cont++;
                    }
                    $letra++;
                }
            }
            $objWriter = new PHPExcel_Writer_Excel5( $excel );          
            header('Content-type: application/octet-stream');
            header('Content-Disposition: attachment; filename=Armado de Expedientes.xls');
            $objWriter->save('php://output');

        }
        
        if( isset( $_POST['iniciaProceso'] ) ) {  
			$sql = "TRUNCATE cargacsvtmp";
            mysql_query( $sql ) or die( mysql_error() );
			
            foreach ( $_POST['cuenta'] as $key => $value ) {
                if( $value != "" && $_POST['TIPO_DETERMINACION'][$key] != "" ) {
					$sql = "INSERT INTO cargacsvtmp(CUENTA, TIPO_DETERMINACION) 
							VALUES('".$value."', '".$_POST['TIPO_DETERMINACION'][$key]."')";
					mysql_query( $sql ) or die( mysql_error() );
                }
            }
            $sql = "
                INSERT INTO bitacora_gestioncobranza_intelisis.procesos_iniciados(
                CUENTA, NOMBRE, FACTURA, CONDICION, FECHA_EMISION, FECHA_ULTIMO_PAGO, DV, DI,
                IMPORTE_INICIAL, ABONOS, SALDO_CAPITAL, SALDO_VENCIDO, INTERES_MONATORIO, ARTICULO,
                TIPO_CLIENTE, TIPO_DETERMINACION, ESTATUS, BASE, FECHA, HORA, MOV ) 
                    SELECT 
                        A.cuenta, A.nombre, A.factura, A.pzo, A.fechafactura,
                        A.fechaultimopago, A.diasvencidos, A.diasinactividad,
                        A.importefactura, A.abonos_c086, A.saldocapital,
                        A.saldovencido,
                        A.intereses, A.articulo, A.tipo_cliente, C.TIPO_DETERMINACION,
                        'EN_PROCESO' AS ESTATUS, 'INTCOB' AS BASE,
                        CURRENT_DATE AS FECHA, CURRENT_TIME AS HORA, A.mov 
                    FROM bitacora_interes.datos_info_c086_cob_intel A
                    INNER JOIN cargacsvtmp C ON C.CUENTA=A.cuenta
                    WHERE A.mov<>'Seguro Vida'
                    UNION ALL
                    SELECT 
                        B.cuenta, B.nombre, B.factura, B.pzo, B.fechafactura,
                        B.fechaultimopago, B.diasvencidos, B.diasinactividad,
                        B.importefactura, B.abonos_c086, B.saldocapital,
                        B.saldovencido,
                        B.intereses, B.articulo, B.tipo_cliente, C.TIPO_DETERMINACION,
                        'EN_PROCESO' AS ESTATUS, 'INT' AS BASE,
                        CURRENT_DATE AS FECHA, CURRENT_TIME AS HORA, B.mov 
                    FROM bitacora_interes.datos_info_c086_intel B 
                    INNER JOIN cargacsvtmp C ON C.CUENTA=B.cuenta
                    WHERE B.mov<>'Seguro Vida'
                    ORDER BY CUENTA, FACTURA
            ";
            //echo $sql;
            mysql_query( $sql ) or die( mysql_error() );
            $reg = ( mysql_affected_rows() > 0 ) ? 1 : 0;
            header( "Location: inicio.php?mod=bgc.configura&ac=armadoExpedientes&reg=" . $reg );
        }
        if( is_uploaded_file( $_FILES['archivocsv']['tmp_name'] ) ) {            
            $archivo = $_FILES['archivocsv']['name'];
            $ext = pathinfo( $archivo, PATHINFO_EXTENSION );
            if( $ext == "csv" ) {
                $archivoCsv = $_FILES['archivocsv']['tmp_name'];

                $sql = "TRUNCATE cargacsvtmp";
                mysql_query( $sql ) or die( mysql_error() );

                $sql = "LOAD DATA INFILE '".str_replace("\\","/",$_FILES['archivocsv']['tmp_name'] )."'
                    INTO TABLE `cargacsvtmp`
                    FIELDS TERMINATED BY ',' 
                    OPTIONALLY ENCLOSED BY '\\\"'
                    LINES TERMINATED BY '\\r\\n'
                    IGNORE 1 LINES"; 
                mysql_query( $sql ) or die( mysql_error() );
                $band = 1;

                $csv = "
                    SELECT 
                        'INTCOB' AS BASE, 
                        C.TIPO_DETERMINACION, 
                        A.* 
                    FROM bitacora_interes.datos_info_c086_cob_intel A
                    INNER JOIN( 
                        SELECT A.*, B.ESTATUS FROM cargacsvtmp A
                        LEFT JOIN(
                            SELECT A.CUENTA, A.ESTATUS FROM bitacora_gestioncobranza_intelisis.procesos_iniciados A
                            INNER JOIN (
                                SELECT MAX( ID ) as maxId FROM bitacora_gestioncobranza_intelisis.procesos_iniciados
                                GROUP BY CUENTA ) ultimoReg ON ultimoReg.maxId=A.id ) B ON A.CUENTA=B.CUENTA
                        WHERE ( B.ESTATUS='CONCLUIDO' OR B.ESTATUS IS NULL OR B.ESTATUS = '' )
                        GROUP BY A.CUENTA
                    )C ON C.CUENTA=A.cuenta
                    UNION ALL
                    SELECT 
                        'INT' AS BASE, 
                        C.TIPO_DETERMINACION,
                        B.* 
                    FROM bitacora_interes.datos_info_c086_intel B 
                    INNER JOIN( 
                        SELECT A.*, B.ESTATUS FROM cargacsvtmp A
                        LEFT JOIN(
                            SELECT A.CUENTA, A.ESTATUS FROM bitacora_gestioncobranza_intelisis.procesos_iniciados A
                            INNER JOIN (
                                SELECT MAX( ID ) as maxId FROM bitacora_gestioncobranza_intelisis.procesos_iniciados
                                GROUP BY CUENTA ) ultimoReg ON ultimoReg.maxId=A.id ) B ON A.CUENTA=B.CUENTA
                        WHERE ( B.ESTATUS='CONCLUIDO' OR B.ESTATUS IS NULL OR B.ESTATUS = '' )
                        GROUP BY A.CUENTA
                    )C ON C.CUENTA=B.cuenta
                    ORDER BY CUENTA, FACTURA
                ";
                $exc = mysql_query( $csv );
                $GLOBALS['totalReg'] = mysql_num_rows( $exc );
                $GLOBALS['cont'] = 0;
            }
        }

        $nivelCobranza = "
            SELECT rutacobro FROM(
                SELECT rutacobro FROM bitacora_interes.datos_info_c086_cob_intel
                WHERE ( rutacobro IS NOT NULL AND rutacobro <> '' )
                UNION
                SELECT DISTINCT rutacobro 
                FROM bitacora_interes.datos_info_c086_intel 
                WHERE rutacobro IS NOT NULL AND rutacobro <> ''
            )A ORDER BY rutacobro;
        ";
        $resultadosGestion = "SELECT * FROM bitacora_gestioncobranza_intelisis.catalogo_resultado GROUP BY nombre";

        $tipoDeterminacion = "
            SELECT 
                COLUMN_NAME as TIPO_DETERMINACION
            FROM INFORMATION_SCHEMA.Columns 
            WHERE TABLE_NAME = 'tabla_principal'
            AND COLUMN_NAME NOT IN( 'ID', 'NOMBRE', 'TIPO' )";
        //$this->dts = 20;
        $bloque = array(
            'tipoDeterminacion, tip, tipoDeter' => $tipoDeterminacion,
            'resultadosGestion' => $resultadosGestion,
            //'col, co'           => $columnas,
            //'blk'               => $datos,
            'blk, bl'               => $csv,
            'nivelCobranza'     => $nivelCobranza
        );
        return $bloque;

    }

    public function _procDeter(){
        $this->tpl = "gestionescobranza/armadoExpedientes/procDeter.htm";
        isset($_GET['reg']) ? $this->msgaviso = "ACTUALIZADO" : null;
        $this->dts = 50;

        $sql = "SELECT ID, CUENTA, NOMBRE, TIPO_DETERMINACION, FECHA FROM procesos_iniciados A
                INNER JOIN (SELECT MAX(ID) AS ultimoReg FROM procesos_iniciados GROUP BY CUENTA) B ON B.ultimoReg = A.ID
            WHERE A.ESTATUS='EN_PROCESO'";
        $bloque = array('blk' => $sql);

        return $bloque;
    }

    public function _editarArmado() {
		$idProcesosIniciados = $_GET['id'];
		$sql = "SELECT CUENTA, NOMBRE, BASE, TIPO_CLIENTE, FECHA, HORA, TIPO_DETERMINACION FROM procesos_iniciados WHERE ID=".$idProcesosIniciados;        
        $exc = mysql_query( $sql ) or die( mysql_error() );
        $datos = mysql_fetch_assoc( $exc );
		
		if( !empty( $_POST ) ) {
			$cont = 0;
			$band = 1;
			$inserto = 0;
			foreach( $_FILES as $key ) {
				//$num = ( $cont < 10 ) ? "0".$cont : $cont;
				$archivo = $key['name'];
				$ext = pathinfo( $archivo, PATHINFO_EXTENSION );
                $tipo = getimagesize( $key['tmp_name'] );        
                
                
				//if( $ext == 'jpg' || $ext == 'jpeg' ) {
                if( $tipo['mime'] == 'image/jpeg' ) {
					$sql = "SELECT * FROM imgs_requisitos 
							WHERE ID_CLIENTE_MAX=".$_POST['idClienteMax']." 
							AND ID_REQUISITO=".$_POST['idRequisito'];
					$exc = mysql_query( $sql ) or die( mysql_error() );			
					$num = mysql_num_rows( $exc );
					$nuevoNombreImg = $_POST['idClienteMax']."-".$_POST['idRequisito']."_".$num.$cont.".".$ext;
					
					copy( $key['tmp_name'], "template_htm/gestionescobranza/armadoExpedientes/archivos/" . $nuevoNombreImg ) or die();
					
					$sql = "INSERT INTO imgs_requisitos( ID_REQUISITO, ID_CLIENTE_MAX, NOMBRE_ORIGINAL, IMAGEN )
							VALUES( ".$_POST['idRequisito'].", ".$_POST['idClienteMax'].",
							'".$key['name']."', '".$nuevoNombreImg."')";
					mysql_query( $sql ) or die( mysql_error() );	
					$inserto = 1;
				} else {
					$band = 0;
				}					  
				$cont++;
			}
			//Seccion para si se completo el checklist al 100% cambiar el estatus a concluido
				if( $inserto == 1 ) {
					$checkList = array();
					$arregloOK = array( 58, 59, 60, 61, 76, 77 );
					$informeDe = "SELECT INFORME_DE FROM informe_de WHERE ID_CLIENTE_MAX = ".$idProcesosIniciados;
					$exc = mysql_query( $informeDe ) or die( mysql_error() );
					$hayInforme = mysql_num_rows( $exc );
					$sql = "SELECT
								ID, 
								NOMBRE, 
								".$datos['TIPO_DETERMINACION'].",
								B.ID_REQUISITO, B.ID_CLIENTE_MAX
							FROM bitacora_gestioncobranza_intelisis.tabla_principal A
							LEFT JOIN(
								SELECT ID_REQUISITO, ID_CLIENTE_MAX 
								FROM imgs_requisitos 
								WHERE ID_CLIENTE_MAX = ".$idProcesosIniciados."
								GROUP BY ID_REQUISITO
							) B ON B.ID_REQUISITO=A.ID
							WHERE TIPO='r'
							AND ".$datos['TIPO_DETERMINACION']."=1
							ORDER BY ID";
					$exc = mysql_query( $sql ) or die( mysql_error() ); 
					$numRegs = mysql_num_rows( $exc );
					$cont = 0;
					while ( $r = mysql_fetch_assoc( $exc ) ) {
						if( in_array( $r['ID'], $arregloOK ) || $r['ID_REQUISITO'] != '' ) {
							$cont++;
						} else if( $r['NOMBRE'] == "RESUMEN DE CUENTA" && $hayInforme > 0 ) {
							$cont++;
						} 
					}
					if( ( $numRegs - $cont ) == 0 ) {
						//Ya esta concluido la cuenta a un 100% de los requisitos que se pedian
						$sql = "UPDATE procesos_iniciados 
									SET ESTATUS='CONCLUIDO' 
								WHERE CUENTA='".$datos['CUENTA']."'
									AND FECHA='".$datos['FECHA']."' 
									AND HORA='".$datos['HORA']."'";
						mysql_query( $sql ) or die( mysql_error() );
					}
				}
			//Fin 
			header( "Location: inicio.php?mod=bgc.configura&ac=editarArmado&reg=".$band."&id=".$_GET['id'] );
		}
        $this->tpl = "gestionescobranza/armadoExpedientes/editArmado.htm";
		if( isset( $_GET['reg'] ) ) {
            $this->msgaviso = ( $_GET['reg'] == 1 ) ? "IMGS REGISTRADAS":"IMAGENES QUE NO ESTEN EN FORMATO JPG,JPEG NO SE REGISTRARON";    
        }
        
        $sql = "SELECT
                    ID, 
                    NOMBRE, 
                    ".$datos['TIPO_DETERMINACION']."
                  FROM bitacora_gestioncobranza_intelisis.tabla_principal 
                  WHERE TIPO='r'";
        $exc = mysql_query( $sql ) or die( mysql_error() );  		
		$mostrar = array();
		while( $r = mysql_fetch_assoc( $exc ) ) {
			$mostrar[] = array(
				"idRequisito" => $r['ID'],
				"nombre" => $r['NOMBRE'],
				"estatus" => $r[$datos['TIPO_DETERMINACION']]
			);
		}		
        $GLOBALS['idCteMaximo'] = $_GET['id'];
        $GLOBALS['tipoDeterminacion'] = $datos['TIPO_DETERMINACION'];
        $GLOBALS['base'] = $datos['BASE'];
        $GLOBALS['tipoCliente'] = $datos['TIPO_CLIENTE'];
        $GLOBALS['cuenta'] = $datos['CUENTA'];
        $GLOBALS['nombre'] = $datos['NOMBRE'];
        $tabla = ( $datos['BASE'] == 'INTCOB' ) ? 
            "bitacora_interes.datos_info_231_ficha_cob_intel" :
            "bitacora_interes.datos_info_231_ficha_intel";
        /*            
            if( $datos['BASE'] == 'INTCOB' ) {
                $tabla = "bitacora_interes.datos_info_231_ficha_cob_intel";
            } else {
                $tabla = "bitacora_interes.datos_info_231_ficha_intel";
            }
        */
		//RESUMEN CUENTA
			$datosCteAval = "SELECT
								cuen, nomb, domi,     colo,    entr, ciud,
									  ava1, domiava1, coloava1, ciudava1, teleava1,
									  ava2, domiava2, coloava2, ciudava2, teleava2
							FROM ".$tabla."
							WHERE cuen = '".$datos['CUENTA']."'";
			$facturas = "SELECT 
							CUENTA, FECHA, HORA, BASE,
							FACTURA, ARTICULO, CONDICION, IMPORTE_INICIAL,
							SALDO_CAPITAL, INTERES_MONATORIO, ABONOS, SALDO_VENCIDO,
							FECHA_ULTIMO_PAGO, DV, DI, TIPO_DETERMINACION
						FROM procesos_iniciados 
						WHERE CUENTA='".$datos['CUENTA']."' 
							AND FECHA = '".$datos['FECHA']."'
							AND HORA = '".$datos['HORA']."'
							AND BASE = '".$datos['BASE']."'";
			$sumaTotalFacturas = "SELECT 
									SUM( IMPORTE_INICIAL ) AS SUMA_IMPORTE_INICIAL,
									SUM( ABONOS ) AS SUMA_ABONOS,
									CASE WHEN SUM( IMPORTE_INICIAL ) > 0 THEN
										( SUM( ABONOS ) ) / SUM( IMPORTE_INICIAL )
										ELSE 0
									END AS ABONOS_VS_IMPORTE,
									SUM( SALDO_CAPITAL ) AS SUMA_SALDO_CAPITAL,
									SUM( INTERES_MONATORIO ) AS SUMA_INTERES_MONATORIO,
									SUM(SALDO_CAPITAL) + SUM(INTERES_MONATORIO) AS SALDO_TOTAL,
									SUM(SALDO_VENCIDO) AS SUMA_SALDO_VENCIDO
								  FROM procesos_iniciados 
								  WHERE CUENTA='".$datos['CUENTA']."' 
									AND FECHA='".$datos['FECHA']."' 
									AND HORA='".$datos['HORA']."'
									AND BASE='".$datos['BASE']."'
								  GROUP BY CUENTA";
			$informeDe = "SELECT INFORME_DE FROM informe_de WHERE ID_CLIENTE_MAX = ".$idProcesosIniciados;
		//fIN $resumenCuenta;
		
        //CHEKLIST
			$checkList = array();
			$arregloOK = array( 58, 59, 60, 61, 76, 77 );
			/*$sql = "SELECT
						ID, 
						NOMBRE, 
						".$datos['TIPO_DETERMINACION']."
					  FROM bitacora_gestioncobranza_intelisis.tabla_principal 
					  WHERE TIPO='r'
						AND ".$datos['TIPO_DETERMINACION']."=1"; 
			*/
			$exc = mysql_query( $informeDe ) or die( mysql_error() );
			$hayInforme = mysql_num_rows( $exc );
			$sql = "SELECT
						ID, 
						NOMBRE, 
						".$datos['TIPO_DETERMINACION'].",
						B.ID_REQUISITO, B.ID_CLIENTE_MAX
					FROM bitacora_gestioncobranza_intelisis.tabla_principal A
					LEFT JOIN(
						SELECT ID_REQUISITO, ID_CLIENTE_MAX 
						FROM imgs_requisitos 
						WHERE ID_CLIENTE_MAX = ".$idProcesosIniciados."
						GROUP BY ID_REQUISITO
					) B ON B.ID_REQUISITO=A.ID
					WHERE TIPO='r'
					AND ".$datos['TIPO_DETERMINACION']."=1
					ORDER BY ID";
			$exc = mysql_query( $sql ) or die( mysql_error() ); 
			$numRegs = mysql_num_rows( $exc );
			$cont = 0;
			while ( $r = mysql_fetch_assoc( $exc ) ) {
				if( in_array( $r['ID'], $arregloOK ) || $r['ID_REQUISITO'] != '' ) {
					$checkList[] = array( 
						"NOMBRE" => $r['NOMBRE'],
						"VALOR" => "OK"
					);
					$cont++;
				} else if( $r['NOMBRE'] == "RESUMEN DE CUENTA" && $hayInforme > 0 ) {
					$checkList[] = array( 
						"NOMBRE" => $r['NOMBRE'],
						"VALOR" => "OK"
					);
					$cont++;
				} else {
					$checkList[] = array( 
						"NOMBRE" => $r['NOMBRE'],
						"VALOR" => ""
					);
				}
			}
			$GLOBALS['calcPorc'] = ( $cont / $numRegs != 0 ) ? $cont / $numRegs : 1;
		//fIN $checkList;
		
        //CARATULA DE PRESENTACION
        //fIN $caratula;
		
		//DICTAMEN DE INCOBRABILIDAD, 
		//fIN $dictamen;
		
		//INFORME DE DETERMINACION
            //Para ver a que bd le vamos hacer la consulta
			$BD = "SELECT cuenta 
				   FROM bitacora_gestioncobranza_intelisis.registros 
				   WHERE cuenta = '".$datos['CUENTA']."'";
			$exc = mysql_query( $BD ) or die( mysql_error() ); 
			$numRgs = mysql_num_rows( $exc );
			$BD = ( $numRgs > 0 ) ? "bitacora_gestioncobranza_intelisis" : "bitacora_gestioncobranza_intcob";
			
			
			  
			//Cuenta con su comentario
            $procso = "SELECT 
                replace(substring(substring(IDPROCESO,(select length(IDPROCESO) - 18 + 1),18),1,10),'/','-') AS fechaIdProceso,
                A.IDPROCESO, A.PROCESO, A.IDCLIENTE, B.nombre AS nombreCte, C.NOMBRE AS nombreAg, 
				B.intelisis AS NOMINA, B.domicilio, B.telefono, B.comentario, B.valor_resultado
            FROM cob_fichas_procesos_estcuenta A
            LEFT JOIN ".$BD.".registros B 
                ON A.IDCLIENTE = B.cuenta 
				AND A.PROCESO=B.resultado
                AND B.fecha_captura = replace(substring(substring(IDPROCESO,(select length(IDPROCESO) - 18 + 1),18),1,10),'/','-')
            LEFT JOIN varios.empleados_rh201_intelisis C ON C.CLAVE=B.intelisis 
            WHERE A.IDCLIENTE = '".$datos['CUENTA']."' AND A.ESTATUS = 101
            ORDER BY fechaIdProceso DESC";
					   
            $aval = "SELECT
                        IDCTO, PARENTESCO,
                        NOMBRE, DIRECCION, NUMERO, TELEFONO
                    FROM bitacora_gestioncobranza_intelisis.cob_procesos_contactos
                    WHERE IDPROCESO='%p1%' 
                        AND PARENTESCO='AVAL'";
            $vecinos = "SELECT 
                            A.IDPROCESO, B.NOMBRE_VECINO, A.NUMERO_DE_CASA, B.ESTATURA, 
                            B.COMPLEXION, B.COLOR_CABELLO, A.TIPODEFINCA,
                            A.CANCEL, B.COMENTARIO_VECINO, A.COCHERA,
                            B.SEXO, B.COLOR_PIEL, B.CABELLO, A.COLOR,
                            A.NIVELES
                        FROM cob_procesos_descripcion_finca_vec A
                        INNER JOIN cob_procesos_descripcion_fisica_vec B
                            ON A.IDPROCESO=B.IDPROCESO AND A.IDVECINO=B.IDVECINO
                        WHERE A.IDPROCESO='%p1%'";
			$referencias = "SELECT 
								A.NOMBRE, A.DIRECCION, A.NUMERO, 
								A.COLONIA, A.DELEGACION, A.TELEFONO,
								B.RESULTADO
							FROM cob_procesos_contactos A
							INNER JOIN cob_procesos_estado_contacto B
									ON B.IDCLIENTE = A.IDCLIENTE
									AND B.IDCONTACTO = A.IDCTO
									AND B.IDPROCESO = A.IDPROCESO
							WHERE
								A.IDPROCESO = '%p1%'
								AND A.PARENTESCO not in ( 'AVAL', 'Cliente' )
								AND A.IDCTO <> 'EMPRESA'";
			$imagenes = "SELECT
								doc.nombre AS ImgName, ARCHIVO
						FROM cob_fichas_procesos_estcuenta cue
						INNER JOIN cob_procesos_indexarc arc ON arc.IDPROCESO = cue.IDPROCESO
						INNER JOIN cob_procesos_catalogodoc doc ON doc.iddoc = arc.iddoc
						INNER JOIN cob_procesos_archivos archivo ON archivo.idarchivo = arc.idarchivo
						WHERE cue.idproceso='%p1%'";
						
			//Negativa exclusiva
				$quien = "SELECT
							A.quien, B.id, B.IDPROCESO
						  FROM quien A
						  INNER JOIN cob_procesos_conexionneg B ON B.id = A.id_registro
						  WHERE B.IDPROCESO='%p1%'";
				$desFisFinca = "SELECT  
									A.sexo, A.estatura, A.complexion, A.colorpiel, A.cabello, A.colorcabello, 
									B.tipo, B.cochera, B.cancel, B.niveles, B.color, C.IDPROCESO, C.id
								FROM descripcion_fisica A
								INNER JOIN tipo_finca B ON B.id_registro=A.id_registro
								INNER JOIN cob_procesos_conexionneg C ON A.id_registro=C.id
								WHERE C.IDPROCESO='%p1%'";	
				$solvencia = "SELECT 
								A.articulo, A.cantidad, A.estado, B.id, B.IDPROCESO
							  FROM solvencia A
							  INNER JOIN cob_procesos_conexionneg B ON B.id=A.id_registro
							  WHERE B.IDPROCESO='%p1%'";
			//Fin negativa
		//Fin $informe
		

        //HISTORICO DE GESTIONES WEB ( CAMPO Y TELEFONICA )
            //cobranzaCampo
            $cobranzaCampo = "SELECT 
                                r.fecha_inicio as 'fechaVisita',
                                r.hora_inicio AS 'horaInicio',
                                TIMEDIFF(r.hora_final, r.hora_inicio) AS 'tGestion',
                                r.factura AS 'factura',
                                r.diasinactividad AS 'DI',
                                r.diasvencidos AS 'DV',
                                CASE
                                    WHEN CAST(r.rutacobro AS UNSIGNED) = 0 THEN rh.NOMBRE
                                    ELSE r.rutacobro
                                END 'rutaAgente',
                                c.nombre AS 'resultado',
                                r.valor_resultado AS 'compenso',
                                r.comentario AS 'comentario'
                            FROM
                                registros AS r
                            LEFT JOIN
                                varios.empleados_rh201_intelisis rh ON r.intelisis = rh.CLAVE
                            LEFT JOIN
                                catalogo_resultado AS c ON c.id = r.resultado
                            WHERE
                                BINARY( r.cuenta ) = BINARY( '".$datos['CUENTA']."') 
                                and r.resultado <> '' 
                            GROUP BY r.id
                            ORDER BY r.fecha_inicio , r.cuenta , r.factura";            
            //cobranzaTelefonica
            $cobranzaTelefonica = "SELECT
                                        t.tel_registro_usuario AS USUARIO,
                                        t.tel_registro_cuenta AS CUENTA,
                                        t.tel_registro_hora AS HORA,
                                        t.tel_registro_fecha AS FECHA,
                                        ( SELECT tel_calificacion_valor 
                                          FROM tel_calificacion 
                                          WHERE tel_calificacion_id = t.tel_registro_calificacion) AS CALIFICACION,
                                        t.tel_registro_comentario AS COMENTARIO
                                   FROM
                                       tel_registro t
                                   LEFT JOIN usuarios u ON u.user = t.tel_registro_usuario
                                   WHERE 1 AND t.tel_registro_cuenta = '".$datos['CUENTA']."'";
			$empresa = "SELECT 
							A.NOMBRE, A.TELEFONO, B.resultado 
						FROM cob_procesos_contactos A
						INNER JOIN cob_procesos_estado_contacto B
							ON B.IDCLIENTE = A.IDCLIENTE
							AND B.IDCONTACTO = A.IDCTO
							AND B.IDPROCESO = A.IDPROCESO
						WHERE A.IDCTO='EMPRESA' AND A.IDPROCESO = '%p1%'";
        //fIN $historicoGestiones;       
		  
		//Imagenes
			$imgs = "SELECT 
						SUBSTRING_INDEX( IMAGEN, '.', -1 ) as ext,
						IMAGEN 
					 FROM imgs_requisitos
					 WHERE ID_REQUISITO=%p1% 
					 AND ID_CLIENTE_MAX=".$idProcesosIniciados;
					      
        $bloque = array(    
            'blkCte, blkNombre, blkAvals' => $datosCteAval,
			'informeDe' 				  => $informeDe,
            'blkFact, blkSaldoCap' 		  => $facturas,
            'blkTotFact, blkTotSaldoCap'  => $sumaTotalFacturas,
            'checkList' 				  => $checkList,
            'cobCampo' 					  => $cobranzaCampo,
            'cobTel' 					  => $cobranzaTelefonica,
			'mostrar' 					  => $mostrar,
			'img' 						  => $imgs,
            'blk, blk2'             	  => $procso,
            'aval'                        => $aval,
            'vec'                   	  => $vecinos,
			'refrs'						  => $referencias,
			'mprsa'					  	  => $empresa,
			'imgs'						  => $imagenes,
			'quien'						  => $quien,
			'desFisFinca'				  => $desFisFinca,
			'solvencia'					  => $solvencia
        );
        return $bloque;
    }

    public function _repArmadoExp() {
        $this->tpl = "gestionescobranza/armadoExpedientes/repArmadoExp.htm";
        if( isset( $_POST['aExcel'] ) ) {   
       
            ini_set('memory_limit', '-1');
            set_time_limit( 0 );
            require_once ( 'PHPExcel.php' );
            require_once ( 'PHPExcel/IOFactory.php');
            require_once ( 'PHPExcel/Writer/Excel5.php' );
            require_once ( 'template_htm/reporteQuejas/estilos.php' );

            $excel = new PHPExcel(); 
            $excel->getActiveSheet()->getColumnDimension( 'A' )->setAutoSize( true );    
            $excel->getActiveSheet()->getColumnDimension( 'B' )->setAutoSize( true );
            $excel->getActiveSheet()->getColumnDimension( 'C' )->setAutoSize( true );
            $excel->getActiveSheet()->getColumnDimension( 'D' )->setAutoSize( true );
            $excel->getActiveSheet()->getColumnDimension( 'E' )->setAutoSize( true );
            $excel->getActiveSheet()->getColumnDimension( 'F' )->setAutoSize( true );
            $excel->getActiveSheet()->getColumnDimension( 'G' )->setAutoSize( true );
            $excel->getActiveSheet()->getColumnDimension( 'H' )->setAutoSize( true );
            $excel->getActiveSheet()->getColumnDimension( 'I' )->setAutoSize( true );
            $excel->getActiveSheet()->getColumnDimension( 'J' )->setAutoSize( true );
            $excel->getActiveSheet()->getColumnDimension( 'K' )->setAutoSize( true );
            $excel->getActiveSheet()->getColumnDimension( 'L' )->setAutoSize( true );
            $excel->getActiveSheet()->getColumnDimension( 'M' )->setAutoSize( true );
            $excel->getActiveSheet()->getColumnDimension( 'N' )->setAutoSize( true );
            $excel->getActiveSheet()->getColumnDimension( 'O' )->setAutoSize( true );
            $excel->getActiveSheet()->getColumnDimension( 'P' )->setAutoSize( true );
            $excel->getActiveSheet()->getColumnDimension( 'Q' )->setAutoSize( true );
            $excel->getActiveSheet()->getColumnDimension( 'S' )->setAutoSize( true );
            $excel->getActiveSheet()->getColumnDimension( 'T' )->setAutoSize( true );
            $excel->getActiveSheet()->getColumnDimension( 'U' )->setAutoSize( true );

            $excel->getActiveSheet()->freezePaneByColumnAndRow( 0, 2 );
            //$excel->getActiveSheet()->freezePaneByColumnAndRow( 1, 0 );

            $excel->getActiveSheet()->setTitle( 'ARMADO DE EXPEDIENTES' );
            $excel->getActiveSheet()->setCellValue( "A1", 'EXPEDIENTE' );
            $excel->getActiveSheet()->setCellValue( "B1", 'FECHA EXPEDIENTE' );
            $excel->getActiveSheet()->setCellValue( "C1", 'DIAS EN PROCESO' );
            $excel->getActiveSheet()->setCellValue( "D1", '% DE AVANCE' );
            $excel->getActiveSheet()->setCellValue( "E1", 'CUENTA' );
            $excel->getActiveSheet()->setCellValue( "F1", 'NOMBRE' );
            $excel->getActiveSheet()->setCellValue( "G1", 'FACTURA' );
            $excel->getActiveSheet()->setCellValue( "H1", utf8_encode( 'CONDICIÓN' ) );
            $excel->getActiveSheet()->setCellValue( "I1", utf8_encode( 'FECHA EMISIÓN' ) );
            $excel->getActiveSheet()->setCellValue( "J1", 'ULTIMO PAGO' );
            $excel->getActiveSheet()->setCellValue( "K1", 'DV' );
            $excel->getActiveSheet()->setCellValue( "L1", 'DI' );
            $excel->getActiveSheet()->setCellValue( "M1", 'IMPORTE INICIAL' );
            $excel->getActiveSheet()->setCellValue( "N1", 'ABONOS' );
            $excel->getActiveSheet()->setCellValue( "O1", '% ABONOS VS IMPORTE INICIAL' );
            $excel->getActiveSheet()->setCellValue( "P1", 'SALDO CAPITAL' );
            $excel->getActiveSheet()->setCellValue( "Q1", 'INTERES MORATORIO' );
            $excel->getActiveSheet()->setCellValue( "R1", 'SALDO TOTAL' );
            $excel->getActiveSheet()->setCellValue( "S1", utf8_encode( 'DESCRIPCIÓN DE ARTICULO' ) );
            $excel->getActiveSheet()->setCellValue( "T1", utf8_encode( 'TIPO DETERMINACIÓN' ) );
            $excel->getActiveSheet()->getStyle( 'A1:T1' )->applyFromArray( $res_gris );

            $letra = 65;
            foreach( $_POST as $key => $value ) {
                $cont = 2;
                if( is_array( $value ) ) {
                    foreach ( $value as $indice => $valor ) {
                        if( chr( $letra ) == 'O' ) {
                            if( strrpos( $valor, "." ) )
                                $excel->getActiveSheet()->setCellValue( chr( $letra ) . $cont, number_format( $valor*100, 0 )."%" );   
                            else
                                $excel->getActiveSheet()->setCellValue( chr( $letra ) . $cont, $valor."%" );       
                        } else {
                            $excel->getActiveSheet()->setCellValue( chr( $letra ) . $cont, utf8_encode( $valor ) );
                        }
                        $cont++;
                    }
                    $letra++;
                }
            }
            $objWriter = new PHPExcel_Writer_Excel5( $excel );          
            header('Content-type: application/octet-stream');
            header('Content-Disposition: attachment; filename=Armado de Expedientes.xls');
            $objWriter->save('php://output');
        }
    }

    function _fol_eva(){
        $nivel = "SELECT Folio FROM folios.Folios WHERE (Pagar ='' OR Pagar IS NULL) AND Capturado='SI'";
        $Cont  = "SELECT COUNT(*) AS Contador FROM folios.Folios WHERE Pagar ='' AND Capturado='SI'";
        $aux['nomina'] = $_SESSION['nomina'];
        $data[] = $aux;
        $this->tpl = "AsignaciondeFolios/FoliosEvaluador.html";

        $bloq = array(
            "post" => empty($_POST) ? array() : array($_POST),
            "sel1" => $nivel,
            "sel2" => $Cont,
            "blk1" => $data
        );

        return $bloq;
    }

    function _rep_lista_folio(){
        $nivel = "SELECT * FROM folios.Folios";
        $this->tpl = "AsignaciondeFolios/ListadoFolios.html";
        $bloq = array(
            "post" => empty($_POST) ? array() : array($_POST),
            "sel1" => $nivel,

        );

        return $bloq;
    }

    function _fecha_reporte(){
        //// Template ////
        $this->tpl = "gestionescobranza/Cuenta_Cobrada_Determinacion/fecha_reporte.htm";
        $a = $_POST['q2'];
        $b = $_POST['q1'];

        if($a == 1){ $ultimodiaint = 31; }
        else {
            $ultimodiaint = intval($a);
            $ultimodiaint = $ultimodiaint - 1;
        }

        $dia = intval($_POST['q1']);
        $dia = $dia - 1;

        if(strlen($a) == '1'){ $q2 = '0' . $a; }
            else{ $q2 = $a; }

        if(strlen($ultimodiaint) == 1){ $q1 = '0' . $ultimodiaint; }
            else{ $q1 = $ultimodiaint; }

        if($_POST['q1'] != ''){
            $sql = "UPDATE cob_dias_envio SET dia ='".$_POST['q1']."', ultimodia='".$q1."' WHERE quincena = 2";
            mysql_query($sql);

            $sql2 = "UPDATE cob_dias_envio SET dia ='".$q2."', ultimodia='".$dia."' WHERE quincena = 1";
            mysql_query($sql2);
        }

        $sqle3 = "SELECT dia, ultimodia FROM cob_dias_envio WHERE quincena = 1";
        $sqle4 = "SELECT dia, ultimodia FROM cob_dias_envio WHERE quincena = 2";

        $bloq = array(
            "post" => empty($_POST) ? array() : array($_POST),
            "blk2" => $sqle3,
            "blk3" => $sqle4,
        );

        return $bloq;
    }

    function _adm_correo(){
        //// Template ////
        $this->tpl = "gestionescobranza/Cuenta_Cobrada_Determinacion/adm_correo.htm";
        $sql_correos = "SELECT * FROM cob_adm_correo ORDER BY puesto";
        //// Consulta TinyButStrong ////
        $bloq = array(
            "post" => empty($_POST) ? array() : array($_POST),
            "blkCo" => $sql_correos
        );

        return $bloq;
    }

    function _ins_correo(){
        if(!empty($_POST)){
            $partes = explode("@", $_POST['correo']);

            $rq_correos = "SELECT nombre, puesto, CASE WHEN mailinterno != '' THEN mailinterno ELSE mailexterno END AS mail 
                FROM intranet_home.directoriogral_intelisis WHERE mailinterno='".$_POST['correo']."' OR mailexterno='".$_POST['correo']."'";

            $result = mysql_query($rq_correos);

            if(mysql_num_rows($result) == 0){
                $sql_nuevocorreo = "INSERT INTO cob_adm_correo (correo) VALUES ('".$_POST['correo']."')";
                mysql_query($sql_nuevocorreo);
            } else{
                while($row = mysql_fetch_assoc($result)){
                    $sql_nuevocorreo = "INSERT INTO cob_adm_correo (correo,nombre,puesto) 
                        VALUES('".$_POST['correo']."','".$row["nombre"]."','".$row["puesto"]."')";
                    mysql_query($sql_nuevocorreo);
                }
            }
        }

        $this->tpl  = "gestionescobranza/Cuenta_Cobrada_Determinacion/adm_correo.htm";
        $rq_correos = "SELECT * FROM cob_adm_correo WHERE 1";

        $bloq = array(
            "post" => empty($_POST) ? array() : array($_POST),
            "blkCo" => $rq_correos
        );

        return $bloq;
    }

    function _eli_correo(){
        $sql_eliminar = "DELETE FROM cob_adm_correo WHERE correo='".$_GET['correo']."'";

        mysql_query($sql_eliminar);
        //// Recargar Template ////
        $this->tpl = "gestionescobranza/Cuenta_Cobrada_Determinacion/adm_correo.htm";
        //// Consulta MySQL ////
        $rq_correos = "SELECT * FROM cob_adm_correo WHERE 1";
        //// Consulta TinyButStrong ////
        $bloq = array(
            "post" => empty($_POST) ? array() : array($_POST),
            "blkCo" => $rq_correos
        );

        return $bloq;
    }

    function _importe_minimo(){
        //// Template ////
        $this->tpl = "gestionescobranza/Cuenta_Cobrada_Determinacion/importe_minimo.htm";

        $sql = "UPDATE cob_importe_minimo SET monto =".$_POST['importedet']." WHERE id_tipo = 'determinacion'";
        mysql_query($sql);

        $sql2 = "UPDATE cob_importe_minimo SET monto =".$_POST['importejef']." WHERE id_tipo = 'jefe'";
        mysql_query($sql2);

        $sql3 = "UPDATE cob_importe_minimo SET monto =".$_POST['importeseg']." WHERE id_tipo = 'seguimiento'";
        mysql_query($sql3);

        $sqle3 = "SELECT * FROM cob_importe_minimo WHERE id_tipo = 'determinacion'";
        $sqle4 = "SELECT * FROM cob_importe_minimo WHERE id_tipo = 'jefe'";
        $sqle5 = "SELECT * FROM cob_importe_minimo WHERE id_tipo = 'seguimiento'";

        $bloq = array(
            "post" => empty($_POST) ? array() : array($_POST),
            "blk2" => $sqle3,
            "blk3" => $sqle4,
            "blk4" => $sqle5,
        );

        return $bloq;
    }

    function _niveles_especiales(){
        //// Template ////
        $this->tpl = "gestionescobranza/Cuenta_Cobrada_Determinacion/niveles_especiales.htm";

        $sql_correos  = "SELECT * FROM cob_niveles_especiales_mavi ORDER BY nivel";
        $sql_correos2 = "SELECT * FROM cob_niveles_especiales_mavicob ORDER BY nivel2";

        //// Consulta TinyButStrong ////
        $bloq = array(
            "post" => empty($_POST) ? array() : array($_POST),
            "blkCo" => $sql_correos,
            "blkCo2" => $sql_correos2
        );

        return $bloq;
    }

    function _niveles_especiales_mavicob(){
        //// Template ////
        $this->tpl = "gestionescobranza/Cuenta_Cobrada_Determinacion/niveles_especiales_mavicob.htm";
        $sql_correos2 = "SELECT * FROM cob_niveles_especiales_mavicob ORDER BY nivel2";

        //// Consulta TinyButStrong ////
        $bloq = array(
            "post" => empty($_POST) ? array() : array($_POST),
            "blkCo2" => $sql_correos2
        );

        return $bloq;
    }

    function _ins_nivel(){
        if(!empty($_POST)){
            $sql_nuevocorreo = "INSERT INTO cob_niveles_especiales_mavi (nivel) VALUES ('".$_POST['nivel']."')";
            mysql_query($sql_nuevocorreo);
        }

        $this->tpl = "gestionescobranza/Cuenta_Cobrada_Determinacion/niveles_especiales.htm";
        $rq_correos = "SELECT * FROM cob_niveles_especiales_mavi WHERE 1";

        $bloq = array(
            "post" => empty($_POST) ? array() : array($_POST),
            "blkCo" => $rq_correos
        );

        return $bloq;
    }

    function _eli_nivel(){
        $sql_eliminar = "DELETE FROM cob_niveles_especiales_mavi WHERE nivel='".$_GET['nivel']."'";
        mysql_query($sql_eliminar);

        $this->tpl = "gestionescobranza/Cuenta_Cobrada_Determinacion/niveles_especiales.htm";
        $rq_correos = "SELECT * FROM cob_niveles_especiales_mavi WHERE 1";

        $bloq = array(
            "post" => empty($_POST) ? array() : array($_POST),
            "blkCo" => $rq_correos
        );

        return $bloq;
    }

    function _ins_nivel2(){
        if(!empty($_POST)){
            $sql_nuevocorreo = "INSERT INTO cob_niveles_especiales_mavicob (nivel2) VALUES ('".$_POST['nivel2']."')";
            mysql_query($sql_nuevocorreo);
        }

        $this->tpl = "gestionescobranza/Cuenta_Cobrada_Determinacion/niveles_especiales_mavicob.htm";
        $rq_correos = "SELECT * FROM cob_niveles_especiales_mavicob WHERE 1";

        $bloq = array(
            "post" => empty($_POST) ? array() : array($_POST),
            "blkCo2" => $rq_correos
        );

        return $bloq;
    }

    function _eli_nivel2(){
        $sql_eliminar = "DELETE FROM cob_niveles_especiales_mavicob WHERE nivel2='".$_GET['nivel2']."'";
        mysql_query($sql_eliminar);

        $this->tpl = "gestionescobranza/Cuenta_Cobrada_Determinacion/niveles_especiales_mavicob.htm";
        $rq_correos = "SELECT * FROM cob_niveles_especiales_mavicob WHERE 1";

        $bloq = array(
            "post" => empty($_POST) ? array() : array($_POST),
            "blkCo2" => $rq_correos
        );

        return $bloq;
    }

    function _Procesos(){
        $this->tpl = "gestionescobranza/Cuenta_Cobrada_Determinacion/procesos_informes.htm";

        $sql_correos = "SELECT * FROM cob_resultados ORDER BY resultado";

        $bloq = array(
            "post" => empty($_POST) ? array() : array($_POST),
            "blkCo" => $sql_correos,
            "blkCo2" => $sql_correos2
        );

        return $bloq;
    }

    function _ins_proceso(){
        //// Envio de datos ////
        if(!empty($_POST)){
            $partes = explode(',', $_POST['resultado']);
            $sql_nuevocorreo = "INSERT INTO cob_resultados (id,resultado) VALUES ('".$partes[0]."','".$partes[1]."')";
            mysql_query($sql_nuevocorreo);
        }

        $this->tpl = "gestionescobranza/Cuenta_Cobrada_Determinacion/procesos_informes.htm";
        $rq_correos = "SELECT * FROM cob_resultados WHERE 1";

        $bloq = array(
            "post" => empty($_POST) ? array() : array($_POST),
            "blkCo" => $rq_correos
        );

        return $bloq;
    }

    function _eli_proceso(){
        $sql_eliminar = "DELETE FROM cob_resultados WHERE resultado='".$_GET['resultado']."'";
        mysql_query($sql_eliminar);

        $this->tpl = "gestionescobranza/Cuenta_Cobrada_Determinacion/procesos_informes.htm";
        $rq_correos = "SELECT * FROM cob_resultados WHERE 1";

        $bloq = array(
            "post" => empty($_POST) ? array() : array($_POST),
            "blkCo" => $rq_correos
        );

        return $bloq;
    }

    function _descuentos(){
        //// Template ////
        $this->tpl = "gestionescobranza/Cuenta_Cobrada_Determinacion/descuentos.htm";
        $sql_correos2 = "SELECT * FROM cob_descuentos ORDER BY tipo";
        //// Consulta TinyButStrong ////
        $bloq = array(
            "post" => empty($_POST) ? array() : array($_POST),
            "blkCo" => $sql_correos2
        );

        return $bloq;
    }

    function _ins_descuentos(){
        //// Envio de datos ////
        if(!empty($_POST)){
            $sql_nuevocorreo = "INSERT INTO cob_descuentos (tipo, forma, imp_gestor, imp_jefe, imp_subgerente, imp_gerente,
                    porc_gestor, porc_jefe, porc_subgerente, porc_gerente, porc_comision) VALUES(
                '".$_POST['tipo']."',         '".$_POST['forma']."',          '".$_POST['imp_gestor']."',
                '".$_POST['imp_jefe']."',     '".$_POST['imp_subgerente']."', '".$_POST['imp_gerente']."',
                '".$_POST['porc_gestor']."',  '".$_POST['porc_jefe']."',      '".$_POST['porc_subgerente']."',
                '".$_POST['porc_gerente']."', '".$_POST['porc_comision']."')";

            mysql_query($sql_nuevocorreo);
        }

        $this->tpl = "gestionescobranza/Cuenta_Cobrada_Determinacion/descuentos.htm";
        $rq_correos = "SELECT * FROM cob_descuentos WHERE 1";

        $bloq = array(
            "post" => empty($_POST) ? array() : array($_POST),
            "blkCo" => $rq_correos
        );

        return $bloq;
    }

    function _eli_descuentos(){
        $sql_eliminar = "DELETE FROM cob_descuentos WHERE tipo='".$_GET['tipo']."'";
        mysql_query($sql_eliminar);

        $this->tpl = "gestionescobranza/Cuenta_Cobrada_Determinacion/descuentos.htm";
        $rq_correos = "SELECT * FROM cob_descuentos WHERE 1";

        $bloq = array(
            "post" => empty($_POST) ? array() : array($_POST),
            "blkCo" => $rq_correos
        );

        return $bloq;
    }

    function _mod_descuentos(){
        $this->tpl = "gestionescobranza/Cuenta_Cobrada_Determinacion/mod_descuentos.htm";
        $sql_correos2 = "SELECT * FROM cob_descuentos where tipo='".$_GET['tipo']."' ORDER BY tipo LIMIT 1";

        $bloq = array(
            "post" => empty($_POST) ? array() : array($_POST),
            "blkCo" => $sql_correos2
        );

        return $bloq;
    }

	function _modificar_descuentos(){
		
		//// Envio de datos ////
		if(!empty($_POST)){
			
			$sql_nuevocorreo="UPDATE cob_descuentos SET forma = '".$_POST['forma']."' , imp_gestor = '".$_POST['imp_gestor']."' , imp_jefe = '".$_POST['imp_jefe']."' , imp_subgerente = '".$_POST['imp_subgerente']."' , imp_gerente = '".$_POST['imp_gerente']."' , porc_gestor = '".$_POST['porc_gestor']."' , porc_jefe = '".$_POST['porc_jefe']."' , porc_subgerente = '".$_POST['porc_subgerente']."' , porc_gerente = '".$_POST['porc_gerente']."' , porc_comision = '".$_POST['porc_comision']."' WHERE tipo = '".$_POST['tipo']."'";
			mysql_query($sql_nuevocorreo)  or die(mysql_error());
			
			header("location: inicio.php?mod=bgc.configura&ac=descuentos");
			
			}
		
		
		
        		
	}

	function _rep_cob_det(){
	
	    $this->tpl = "gestionescobranza/Cuenta_Cobrada_Determinacion/reporte.deter.htm";
		//$this->tpl = "gestionescobranza/reporte.negativa_disenio.html";
		
		
		
		if($_POST['cuenta'] != ""){
			$where.=" and cuenta = '".$_POST['cuenta']."'";
		}
		
		if($_POST['nivel'] != ""){
			$where.=" and rutacobro = '".$_POST['nivel']."'";
		}
		
		if($_POST['agente'] != ""){
			$where.=" and AGENTE = '".$_POST['agente']."'";
		}
		if($_POST['gestor'] != ""){
			$where.=" and gestor = '".$_POST['gestor']."'";
		}
		if($_POST['jefe'] != ""){
			$where.=" and jefe_gestor = '".$_POST['jefe']."'";
		}
		
		
		$sqlfechas = "SELECT quincena_cobranza(".$_POST['quincena'].",".$_POST['ejercicio'].") as quincena";
		$resultado1 = mysql_query($sqlfechas);
		while ($fila = mysql_fetch_assoc($resultado1)) {
		$partes= explode('||',$fila["quincena"]);
		}
		$dato1=$partes[0];
		$dato2=$partes[1];
		
		
		
		$fecha1="select quincena_cobranza1(".$_POST['quincena'].",".$_POST['ejercicio'].") as fecha";
		$res=mysql_fetch_assoc(mysql_query($fecha1));
		$desde=$res['fecha'];
		$fecha2="select quincena_cobranza2(".$_POST['quincena'].",".$_POST['ejercicio'].") as fecha";
		$res=mysql_fetch_assoc(mysql_query($fecha2));
		$hasta=$res['fecha'];
		
		//echo $desde.' '.$hasta;		
		$sqlcont = "set @a:=0";
		mysql_query($sqlcont);
		

		
		$sql=" CREATE TEMPORARY TABLE IF NOT EXISTS reportede AS select @a:=@a + 1 as num,a.*, round((importegestor + importejefe + importesubgerente + importegerente),2) as total from(
        select 
            a . *,
                case when a.estatusgestor='BAJA' then '' else (sum(porc_gestor) + sum(porcgestor)) end as porcentajegestor,
                case when a.estatusjefe='BAJA' then '' else (sum(porc_jefe) + sum(porcjefe)) end as porcentajejefe,
    			case when a.estatussubgerente='BAJA' then '' else  (sum(porc_subgerente)+sum(porcsubgerente)) end as porcentajesubgerente,
    			case when a.estatusgerente='BAJA' then '' else (sum(porc_gerente)+sum(porcgerente)) end as porcentajegerente,

                case when a.estatusgestor='BAJA' then '' else (sum(imp_gestor) + sum(impgestor)) end as importegestor,
                case when a.estatusjefe='BAJA' then '' else (sum(imp_jefe) + sum(impjefe)) end as importejefe,
    			case when a.estatussubgerente='BAJA' then '' else (sum(imp_subgerente)+sum(impsubgerente)) end as importesubgerente,
    			case when a.estatusgerente='BAJA' then '' else (sum(imp_gerente)+sum(impgerente)) end as importegerente
        	
            from
                (select 
                universo . *,
                    des.porc_gestor,
                    des.porc_jefe,
                    des.porc_subgerente,
                    des.porc_gerente,
                    des.imp_gestor,
                    des.imp_jefe,
                    des.imp_subgerente,
                    des.imp_gerente,
                    case
                        when
                            des.forma = 'PORCENTAJE'
                                and cobros >= universo.monto
                        then
                            ROUND((cobros * des.porc_gestor)/100, 2)
                        else 0
                    end as impgestor,
                    case
                        when
                            des.forma = 'PORCENTAJE'
                                and cobros >= universo.monto
                        then
                            ROUND((cobros * des.porc_jefe)/100, 2)
                        else 0
                    end as impjefe,
                    case
                        when
                            des.forma = 'PORCENTAJE'
                                and cobros >= universo.monto
                        then
                            ROUND((cobros * des.porc_subgerente)/100, 2)
                        else 0
                    end as impsubgerente,
                    case
                        when
                            des.forma = 'PORCENTAJE'
                                and cobros >= universo.monto
                        then
                            ROUND((cobros * des.porc_gerente)/100, 2)
                        else 0
                    end as impgerente,
                    case
                        when
                            des.forma = 'IMPORTE'
                                and cobros >= universo.monto
                        then
                            ROUND((des.imp_gestor / cobros) * 100, 2)
                        else 0
                    end as porcgestor,
                    case
                        when
                            des.forma = 'IMPORTE'
                                and cobros >= universo.monto
                        then
                            ROUND((des.imp_jefe / cobros) * 100, 2)
                        else 0
                    end as porcjefe,
                    case
                        when
                            des.forma = 'IMPORTE'
                                and cobros >= universo.monto
                        then
                            ROUND((des.imp_subgerente / cobros) * 100, 2)
                        else 0
                    end as porcsubgerente,
                    case
                        when
                            des.forma = 'IMPORTE'
                                and cobros >= universo.monto
                        then
                            ROUND((des.imp_gerente / cobros) * 100, 2)
                        else 0
                    end as porcgerente
            from
                (select 
        		universo.agente as agen,
                im.id_tipo,
                    procesos.IDPROCESO,
                    procesos.IDCLIENTE,
                    procesos.PROCESO,
                    procesos.ESTATUS,
        			DATE_FORMAT(procesos.FECHA,'%Y %b %e') AS fecha_letra_des,
                    procesos.FECHA,
                    procesos.AGENTE,
                    procesos.JEFE,
                    universo.rutacobro,
                    universo.cobros,
                    e.resultado,
                    universo.cuenta,
        			DATE_FORMAT(universo.fecha_emision,'%Y %b %e') AS fecha_letra_emision,
                    universo.fecha_emision,
                    universo.diasinactividad as dias_inactivos,
                    universo.diasvencidos as dias_vencidos,
                    universo.ejercicio,
                    universo.quincena,
                    universo.nombre,
                    im.monto,
                    case
                        when cobros >= im.monto then 'SI'
                        ELSE 'NO'
                    END as cm,
                    case
                        when estatus_a.EDO = 'V' then 'ACTIVO'
                        else 'BAJA'
                    end as estatusgestor,
                    case
                        when a.nomina != '' then a.nomina
                        else 'NO EXISTE'
                    end as gestor,
                    case
                        when estatus_b.EDO = 'V' then 'ACTIVO'
                        else 'BAJA'
                    end as estatusjefe,
                    case
                        when b.Nomina != '' then b.Nomina
                        else 'NO EXISTE'
                    end as Jefe_gestor,
                    case
                        when estatus_c.EDO = 'V' then 'ACTIVO'
                        else 'BAJA'
                    end as estatussubgerente,
                    case
                        when c.Nomina != '' then c.Nomina
                        else 'NO EXISTE'
                    end as Sub_Gerente,
                    case
                        when estatus_d.EDO = 'V' then 'ACTIVO'
                        else 'BAJA'
                    end as estatusgerente,
                    case
                        when d.Nomina != '' then d.Nomina
                        else 'NO EXISTE'
                    end as Gerente
            from
                (SELECT 
                asig.cuenta,
                    asig.nombre,
                    asig.dias_inactivos as diasinactividad,
                    asig.dias_vencidos as diasvencidos,
                    asig.nivel as rutacobro,
                    asig.nomina as agente,
                    a.cobros,
                    a.fecha_emision,
                    asig.ejercicio,
                    asig.periodo as quincena
            FROM
                android_reportes._cuentas_gestioncobranza asig
            inner join bitacora_gestioncobranza_intelisis.cob_niveles_especiales_mavi ne ON asig.nivel = ne.nivel
        		inner join (select 
        						cliente,
        							sum(cobros) as cobros,
        							MAX(fecha_emision) as fecha_emision,
        							ejercicio,
        							quincena
        					from
        						bitacora_gestioncobranza_intelisis.abonos_497d
        					where fecha_emision between '".$desde."' and '".$hasta."'
        					group by cliente) a ON a.cliente = asig.cuenta
        	where asig.periodo=".$_POST['quincena']." and asig.ejercicio=".$_POST['ejercicio']."
            group by asig.cuenta 
        	union all SELECT 
                asig.cuenta,
                    asig.nombre,
                    asig.dias_inactivos as diasinactividad,
                    asig.dias_vencidos as diasvencidos,
                    asig.nivel as rutacobro,
                    asig.nomina as agente,
                    a.cobros,
                    a.fecha_emision,
                    asig.ejercicio,
                    asig.periodo as quincena
            FROM
                android_reportes._cuentas_gestioncobranza asig
            inner join bitacora_gestioncobranza_intelisis.cob_niveles_especiales_mavicob ne ON asig.nivel = ne.nivel2
            inner join (select 
        					cliente,
        						sum(cobros) as cobros,
        						MAX(fecha_emision) as fecha_emision,
        						ejercicio,
        						quincena
        				from
        					bitacora_gestioncobranza_intelisis.abonos_497d
        				where fecha_emision between '".$desde."' and '".$hasta."'
        				group by cliente) a ON a.cliente = asig.cuenta
           	where asig.periodo=".$_POST['quincena']." and asig.ejercicio=".$_POST['ejercicio']."				
            group by asig.cuenta
        	
        	UNION ALL
        	SELECT 
                asig.cuenta,
                    asig.nombre,
                    asig.dias_inactivos as diasinactividad,
                    asig.dias_vencidos as diasvencidos,
                    asig.nivel as rutacobro,
                    ad.agente_asociado as agente,
                    a.cobros,
                    a.fecha_emision,
                    asig.ejercicio,
                    asig.periodo as quincena
            FROM(select agente_origen, agente_asociado from agente_doble
        							where fecha_asigna between '".$desde."' and '".$hasta."'
        							group by agente_origen, agente_asociado) ad
        		left join android_reportes._cuentas_gestioncobranza asig on asig.nomina=ad.agente_origen
        		inner join (select 
        						cliente,
        							sum(cobros) as cobros,
        							MAX(fecha_emision) as fecha_emision,
        							ejercicio,
        							quincena
        					from
        						bitacora_gestioncobranza_intelisis.abonos_497d
        					where fecha_emision between '".$desde."' and '".$hasta."'
        					group by cliente) a ON a.cliente = asig.cuenta
            inner join bitacora_gestioncobranza_intelisis.cob_niveles_especiales_mavi ne ON asig.nivel = ne.nivel
        	where asig.periodo=".$_POST['quincena']." and asig.ejercicio=".$_POST['ejercicio']."				
            group by asig.cuenta
        	
        	UNION ALL
        	SELECT 
                asig.cuenta,
                    asig.nombre,
                    asig.dias_inactivos as diasinactividad,
                    asig.dias_vencidos as diasvencidos,
                    asig.nivel as rutacobro,
                    ad.agente_asociado as agente,
                    a.cobros,
                    a.fecha_emision,
                    asig.ejercicio,
                    asig.periodo as quincena
            FROM(select agente_origen, agente_asociado from agente_doble
        							where fecha_asigna between '".$desde."' and '".$hasta."'
        							group by agente_origen, agente_asociado) ad
        		left join android_reportes._cuentas_gestioncobranza asig on asig.nomina=ad.agente_origen
        		inner join (select 
        						cliente,
        							sum(cobros) as cobros,
        							MAX(fecha_emision) as fecha_emision,
        							ejercicio,
        							quincena
        					from
        						bitacora_gestioncobranza_intelisis.abonos_497d
        					where fecha_emision between '".$desde."' and '".$hasta."'
        					group by cliente) a ON a.cliente = asig.cuenta
        	inner join bitacora_gestioncobranza_intelisis.cob_niveles_especiales_mavicob ne ON asig.nivel = ne.nivel2
        	where asig.periodo=".$_POST['quincena']." and asig.ejercicio=".$_POST['ejercicio']."				
            group by asig.cuenta
        	
        	
        	) universo
            inner join (SELECT 
                A . *
            FROM
                bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta A
            INNER JOIN (SELECT 
                MAX(replace(replace(substring(IDPROCESO, (select length(IDPROCESO) - 18 + 1), 18), '/', ''), ':', '')) AS ULTIMO_REG,
                    idcliente
            FROM
                bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta pc
            inner join cob_resultados cr ON cr.id = pc.proceso
            group by idcliente) B ON B.idcliente = A.IDCLIENTE
                and ESTATUS = '101'
                AND B.ULTIMO_REG = (SELECT replace(replace(substring(A.IDPROCESO, (SELECT length(A.IDPROCESO) - 18 + 1), 18), '/', ''), ':', ''))) procesos ON universo.cuenta = procesos.idcliente
            inner join bitacora_gestioncobranza_intelisis.cob_resultados e ON procesos.PROCESO = e.id
            inner join cob_importe_minimo im ON im.id_tipo = 'determinacion'
            left join bitacora_gestioncobranza_intelisis.agentes a ON a.agente = procesos.agente
            left join bitacora_gestioncobranza_intelisis.agentes b ON a.celula = b.agente
            left join bitacora_gestioncobranza_intelisis.agentes c ON a.division = c.agente
            left join bitacora_gestioncobranza_intelisis.agentes d ON a.gerencia = d.agente
            left join varios.empleados_rh201_intelisis estatus_a ON a.nomina = estatus_a.CLAVE
            left join varios.empleados_rh201_intelisis estatus_b ON b.nomina = estatus_b.CLAVE
            left join varios.empleados_rh201_intelisis estatus_c ON c.nomina = estatus_c.CLAVE
            left join varios.empleados_rh201_intelisis estatus_d ON d.nomina = estatus_d.CLAVE) universo
            left join cob_descuentos des ON des.tipo = 'DETERMINADOR'
                and cobros >= universo.monto) a
           where 1=1

        ".$where."  group by idproceso)a";
        //echo $sql;
		mysql_query($sql);
		
	

		
		$sql1="Select * from reportede";
		$res = mysql_query($sql1);
	
		$sqltotal="Select count(*) as cuentas, sum(cobros)  as cobros, sum(importegestor) as sumagestor, sum(importejefe) as sumajefe, sum(importesubgerente) as sumasubgerente , sum(importegerente) as sumagerente, sum(total) as totalimp from reportede";
		
		$primerdia="select DATE_FORMAT(quincena_cobranza1(".$_POST['quincena'].",".$_POST['ejercicio']."),'%Y %b %e')  		AS pd";	
		$segundodia="select DATE_FORMAT(quincena_cobranza2(".$_POST['quincena'].",".$_POST['ejercicio']."),'%Y %b %e')
		AS sd";
					
					if (mysql_num_rows($res) != 0) {
    				$this->tpl = "gestionescobranza/Cuenta_Cobrada_Determinacion/rep_cob_det.htm";
					
					
					}
					
					
					$bloq = array
					(
						"post"  => empty($_POST)?array():array($_POST),
						"blk2" => (!empty($_POST))?$sql1:array($_POST),
						"blk4" => (!empty($_POST))?$sqltotal:array($_POST),
						"primer" => (!empty($_POST))?$primerdia:array($_POST),
						"segundo" => (!empty($_POST))?$segundodia:array($_POST)
						
					);
					
					
					
					
			return $bloq;
	}

	function _rep_cob_jef(){
	
        $this->tpl = "gestionescobranza/Cuenta_Cobrada_Determinacion/reporte.jefe.htm";
		//$this->tpl = "gestionescobranza/reporte.negativa_disenio.html";
		
		
		if($_POST['cuenta'] != ""){
			$where.=" and cliente = '".$_POST['cuenta']."'";
		}
		if($_POST['agente'] != ""){
			$where.=" and JEFE = '".$_POST['agente']."'";
		}
		if($_POST['gestor'] != ""){
			$where.=" and AGENTE = '".$_POST['gestor']."'";
		}


		$fecha1="select quincena_cobranza1(".$_POST['quincena'].",".$_POST['ejercicio'].") as fecha";
		$res=mysql_fetch_assoc(mysql_query($fecha1));
		$desde=$res['fecha'];
		$fecha2="select quincena_cobranza2(".$_POST['quincena'].",".$_POST['ejercicio'].") as fecha";
		$res=mysql_fetch_assoc(mysql_query($fecha2));
		$hasta=$res['fecha'];		
		
		
		$sqlcont = "set @a:=0";
		mysql_query($sqlcont);
	
	    $sql=" CREATE TEMPORARY TABLE IF NOT EXISTS reportejef AS

            SELECT @a:=@a + 1 as num,UNIVERSO.* FROM(
            SELECT  UNIVERSO.*,(sum(porc_gestor) + sum(porcgestor)) as porcentajegestor,(sum(imp_gestor) + sum(impgestor)) as importegestor FROM(
            SELECT UNI.*,TABLAPROCESO.resultado,case when AGENTES.Nombre != '' then AGENTES.Nombre ELSE 'BAJA' END as NombreJefe,CLIENTES.Nombre as NombreCliente,
                case when AGENTES.Es != '' then AGENTES.Es ELSE 'BAJA' END as Es,DES.tipo,
            	ROUND((des.porc_comision * cobrossuma) / 100,  2) as imp_comision,
            	des.porc_comision,
            	des.imp_gestor,
                des.porc_gestor,
            	case
                    when
                        DES.forma = 'PORCENTAJE'
                            and cobrossuma >= IMPORTE.monto
                    then
                        ROUND((DES.porc_gestor * cobrossuma) / 100,
                                2)
                    else 0
                end as impgestor,
                case
                    when
                        DES.forma = 'IMPORTE'
                            and cobrossuma >= IMPORTE.monto
                    then
                        ROUND((DES.imp_gestor / cobrossuma) * 100,
                                2)
                    else 0
                end as porcgestor,
            	CASE
                    WHEN cobrossuma >= IMPORTE.monto THEN 'SI'
                    ELSE 'NO'
                END AS cm 
            FROM(

            SELECT UNIVERSO.*,
            	
                
            	
            CASE
                    WHEN estatus_gestor.EDO = 'V' then 'ACTIVO'
                    ELSE 'BAJA'
                END AS estatusgestor,
            CASE
                    WHEN estatus_jefe.EDO = 'V' then 'ACTIVO'
                    ELSE 'BAJA'
                END AS estatusjefe
             
            FROM(
            SELECT *,
            DATE_FORMAT(fecha_inicio,'%Y %b %e') as fecha_letra_inicio,
             ROUND(sum(cobros), 2) AS cobrossuma FROM(
            select 
                    cliente,
            		DATE_FORMAT(fecha_emision,'%Y %b %e') as fecha_letra_cobro,
                        fecha_emision AS fecha_cobro,
                        cobros,
                        dias_vencidos,
                        dias_inactivos
            			
                from
                    bitacora_gestioncobranza_intelisis.abonos_497d)CUENTAS
                    INNER JOIN
                (
            	
            	SELECT
            		  AGENTE,
            		  case when JEFE != '' then JEFE ELSE 'E000000' END as JEFE,
            		  replace(replace(substring(IDPROCESO, (select length(IDPROCESO) - 18 + 1), 18), '/', ''), ':', '') AS ULTIMO_REG,
            		  SUBSTRING(substring(IDPROCESO, (select length(IDPROCESO) - 18 + 1), 18), 1, 10) AS fecha_inicio, pc.idcliente, PROCESO, ESTATUS
            		FROM bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta pc
            		inner join (SELECT
            				MAX(replace(replace(substring(IDPROCESO, (select length(IDPROCESO) - 18 + 1), 18), '/', ''), ':', '')) AS ULTIMO_REG,
            					idcliente
            			FROM
            				bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta pc
            			inner join cob_resultados cr ON cr.id = pc.proceso
            			group by idcliente) cr ON cr.idcliente = pc.IDCLIENTE
            			
            			
                        inner join registros r on r.cuenta=pc.idcliente and r. resultado in ('pago','LCT') and CASE WHEN (r.intelisis IS NULL OR r.intelisis = 'SIN AGE' OR r.intelisis = ' ') THEN REPLACE(pc.jefe,' ','') = REPLACE(r.promotor,' ','') ELSE  REPLACE(pc.jefe,' ','') = REPLACE(r.intelisis,' ','') end	
            			
            								
            		where pc.ESTATUS = '100'
            		and pc.fecha  between '".$desde."' and '".$hasta."'
            		
            	) PROCESO  ON CUENTAS.cliente = PROCESO.idcliente
            	and CUENTAS.fecha_cobro > PROCESO.fecha_inicio 
            	GROUP BY CUENTAS.cliente)UNIVERSO
            	left join
                varios.empleados_rh201_intelisis estatus_gestor ON UNIVERSO.AGENTE = estatus_gestor.CLAVE
            	left join
                varios.empleados_rh201_intelisis estatus_jefe ON UNIVERSO.JEFE = estatus_jefe.CLAVE)UNI


            	left JOIN
                bitacora_gestioncobranza_intelisis.cob_importe_minimo IMPORTE ON IMPORTE.id_tipo = 'jefe' AND UNI.cobrossuma >= IMPORTE.monto
            	inner join
                bitacora_gestioncobranza_intelisis.cob_resultados TABLAPROCESO ON UNI.PROCESO = TABLAPROCESO.id
            	LEFT join
                bitacora_gestioncobranza_intelisis.agentes AGENTES ON UNI.JEFE = AGENTES.Nomina
            	left join
                bitacora_gestioncobranza_intelisis.registros CLIENTES ON UNI.cliente = CLIENTES.Cuenta
            	
            	left join
                bitacora_gestioncobranza_intelisis.cob_descuentos DES ON  UNI.cobrossuma >= IMPORTE.monto AND (AGENTES.Es LIKE CONCAT('% ', DES.Tipo)
                    OR AGENTES.Es LIKE CONCAT('% ', DES.Tipo, ' %') ) AND UNI.estatusgestor ='ACTIVO' AND UNI.estatusjefe ='ACTIVO'
            	GROUP BY UNI.cliente)UNIVERSO GROUP BY UNIVERSO.cliente)UNIVERSO
            	where 1=1
                	
            ".$where;

        mysql_query($sql);

        //echo $sql;

        $sql2="Select * from reportejef";
		$res = mysql_query($sql2);
	
		$sqltotal1="Select count(*) as cuentas, sum(cobrossuma)  as total, sum(imp_gestor+impgestor) as sumagestor,sum(imp_comision) as sumajefe    from reportejef";
		
		$primerdia="select DATE_FORMAT(quincena_cobranza1(".$_POST['quincena'].",".$_POST['ejercicio']."),'%Y %b %e')  		AS pd";	
		$segundodia="select DATE_FORMAT(quincena_cobranza2(".$_POST['quincena'].",".$_POST['ejercicio']."),'%Y %b %e')
		AS sd";

		if (mysql_num_rows($res) != 0) {
		$this->tpl = "gestionescobranza/Cuenta_Cobrada_Determinacion/rep_cob_jef.htm";
		
		
		}
		
		$bloq = array
		(
			"post"  => empty($_POST)?array():array($_POST),
			"blk2" => (!empty($_POST))?$sql2:array($_POST),
			"blk4" => (!empty($_POST))?$sqltotal1:array($_POST),
			"primer" => (!empty($_POST))?$primerdia:array($_POST),
			"segundo" => (!empty($_POST))?$segundodia:array($_POST)
		);
		return $bloq;

	
	
	}

	function _rep_cue_cob(){
	
	    $this->tpl = "gestionescobranza/Cuenta_Cobrada_Determinacion/reporte.hits.htm";
		//$this->tpl = "gestionescobranza/reporte.negativa_disenio.html";
		
		
		
		if($_POST['agente'] != ""){
			$where.=" and agente = '".$_POST['agente']."'";
		}
		if($_POST['nivel'] != ""){
			$whereruta.=" and rutacobro = '".$_POST['nivel']."'";
		}
		
		if($_POST['quincena'] != ""){
			$where.=" and quincena = '".$_POST['quincena']."'";
		}
		if($_POST['ejercicio'] != ""){
			$where.=" and ejercicio = '".$_POST['ejercicio']."'";
		}
		
		$fecha1="select quincena_cobranza1(".$_POST['quincena'].",".$_POST['ejercicio'].") as fecha";
		$res=mysql_fetch_assoc(mysql_query($fecha1));
		$desde=$res['fecha'];
		$fecha2="select quincena_cobranza2(".$_POST['quincena'].",".$_POST['ejercicio'].") as fecha";
		$res=mysql_fetch_assoc(mysql_query($fecha2));
		$hasta=$res['fecha'];		
	

    	$sql="CREATE TEMPORARY TABLE IF NOT EXISTS hits_tem AS select 
    		a.nivel,
    		a.agente,
    		a.nombre,
    		case when jefes.total!='' then jefes.total else 0 end as cuentas_enviadas_det,
            case when jefes.tot!=''   then jefes.tot   else 0 end as cuentas_cobradas_jefe,
            case when jefes.porc_cuentas_jefe!='' then jefes.porc_cuentas_jefe else 0 end as porc_cuentas_jefe ,
            case when jefes.impgestor!='' then jefes.impgestor else 0 end as impgestor,
            case when jefes.enviadas_det!='' then jefes.enviadas_det else 0 end as enviadas_det,
            case when deter.total!='' then deter.total else 0 end as total_ceuntas_determinador,
            case when deter.totdet!='' then deter.totdet else 0 end  as cuentas_cobradas_deter,
            case when deter.porc_cuentas_deter!='' then deter.porc_cuentas_deter else 0 end as porc_cuentas_deter,
            case when deter.descGes!=''  then deter.descGes else 0 end  as cuentas_cobradas_deter_imp,
            (case when jefes.tot!='' then jefes.tot else 0 end +case when deter.totdet!='' then deter.totdet else 0 end) as total_cuentas_cobradas,
            round(((case when jefes.tot!='' then jefes.tot else 0 end +case when deter.totdet!='' then deter.totdet else 0 end)/(case when jefes.total!='' then jefes.total else 0 end+case when deter.total!='' then deter.total else 0 end))*100,2) as porc_total,
        	case when jefes.impgestor!='' then jefes.impgestor else 0 end+case when deter.descGes!=''  then deter.descGes else 0 end as importe_total

    			from (SELECT
    			  Niv.Rutacobro as nivel,
    			  A.agente,
    			  P.nombre
    			FROM
    			bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta A
    			INNER JOIN (SELECT
    							MAX(replace(replace(substring(IDPROCESO, (select length(IDPROCESO) - 18 + 1), 18), '/', ''), ':', '')) AS ULTIMO_REG,
    								idcliente
    						FROM
    							bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta pc
    						inner join cob_resultados cr ON cr.id = pc.proceso
    						group by idcliente) B ON B.idcliente = A.IDCLIENTE AND B.ULTIMO_REG = (SELECT replace(replace(substring(A.IDPROCESO, (SELECT length(A.IDPROCESO) - 18 + 1), 18), '/', ''), ':', ''))
    			left join varios.empleados_rh201_intelisis p on p.clave=A.agente
    			left join (SELECT agente,TRIM(substring_index(MID(es,5,100),'0',1)) as Rutacobro FROM agentes where nomina=agente) niv on niv.agente=a.agente
    			where (A.ESTATUS = 100 and A.fecha between '".$desde."' and '".$hasta."') or A.ESTATUS = 101
    			GROUP BY A.AGENTE,P.Nombre,Niv.Rutacobro) a
    			left join (select
    			agentes.*,jefes.tot,round(((jefes.tot/agentes.total)*100),2) as porc_cuentas_jefe,jefes.cobros,agentes.total-jefes.tot as enviadas_det,jefes.impgestor

            from (SELECT
			  Niv.Rutacobro as nivel,
			  A.AGENTE,
			  P.Nombre,
			  CASE WHEN A.ESTATUS=100 THEN 'JEFES' ELSE 'DETERMINADOR' END AS TIPO,
			  COUNT(A.IDCLIENTE) AS TOTAL
			FROM
			bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta A
			INNER JOIN (SELECT
							MAX(replace(replace(substring(IDPROCESO, (select length(IDPROCESO) - 18 + 1), 18), '/', ''), ':', '')) AS ULTIMO_REG,
								idcliente
						FROM
							bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta pc
						inner join cob_resultados cr ON cr.id = pc.proceso
						group by idcliente) B ON B.idcliente = A.IDCLIENTE AND B.ULTIMO_REG = (SELECT replace(replace(substring(A.IDPROCESO, (SELECT length(A.IDPROCESO) - 18 + 1), 18), '/', ''), ':', ''))
			left join varios.empleados_rh201_intelisis p on p.clave=A.agente
			left join (SELECT agente,TRIM(substring_index(MID(es,5,100),'0',1)) as Rutacobro FROM agentes where nomina=agente) niv on niv.agente=a.agente
			where (A.ESTATUS = 100 and A.fecha between '".$desde."' and '".$hasta."') or A.ESTATUS = 101
			GROUP BY A.AGENTE,P.Nombre,A.ESTATUS) agentes
            left join(
            		select
                    Rutacobro,
                    agente,
                    count(idcliente) as tot,
                    sum(cobros) as cobros,
                    sum(impgestor) as impgestor
                    from(
                        select
                			niv.Rutacobro,
                			porcte.agente,
                			porcte.idcliente,
                			(porcte.cobrossuma) as cobros,
                            case when des.forma = 'PORCENTAJE' then ROUND(((porcte.cobrossuma) * des.porc_gestor) / 100, 2) else des.imp_gestor end as impgestor
                		from(
                            SELECT
                				  PROCESO.AGENTE,
                				  PROCESO.idcliente,
                				  ROUND(sum(CUENTAS.cobros), 2) AS cobrossuma
            			    FROM abonos_497d CUENTAS
            			    INNER JOIN (
                                SELECT AGENTE,
                    			    SUBSTRING(substring(pc.IDPROCESO, (select length(pc.IDPROCESO) - 18 + 1), 18), 1, 10) AS fecha_inicio,
                                    pc.idcliente
            				    FROM bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta pc
            				    inner join (
            						    SELECT
            								MAX(replace(replace(substring(IDPROCESO, (select length(IDPROCESO) - 18 + 1), 18), '/', ''), ':', '')) AS ULTIMO_REG,
            								idcliente
            								FROM bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta pc
            								inner join cob_resultados cr ON cr.id = pc.proceso
            								group by idcliente
            						    ) cr ON cr.idcliente = pc.IDCLIENTE
            				    where pc.ESTATUS = '100' and pc.fecha between '".$desde."' and '".$hasta."'
                            ) PROCESO ON CUENTAS.cliente = PROCESO.idcliente and CUENTAS.fecha_emision > PROCESO.fecha_inicio
            						   GROUP BY PROCESO.AGENTE,PROCESO.idcliente
                        )porcte
            	        left join cob_importe_minimo imp on imp.id_tipo='jefe' and porcte.cobrossuma>=imp.monto
            		    INNER JOIN (SELECT agente,TRIM(substring_index(MID(es,5,100),'0',1)) as Rutacobro FROM agentes where nomina=agente) niv on niv.Agente=porcte.AGENTE
            		    inner join cob_descuentos des on niv.Rutacobro=des.tipo
                    )poragte
                    group by poragte.agente
    		)jefes on agentes.agente=jefes.agente where agentes.tipo='JEFES')jefes on jefes.agente=a.agente
            		
            			left join (SELECT agentes.total,deter.*,round(((deter.totdet/agentes.total)*100),2) as porc_cuentas_deter 
            			FROM (SELECT
            			  Niv.Rutacobro as nivel,
            			  A.AGENTE,
            			  P.Nombre,
            			  CASE WHEN A.ESTATUS=100 THEN 'JEFES' ELSE 'DETERMINADOR' END AS TIPO,
            			  COUNT(A.IDCLIENTE) AS TOTAL
            			FROM
            			bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta A
            			INNER JOIN (SELECT
            							MAX(replace(replace(substring(IDPROCESO, (select length(IDPROCESO) - 18 + 1), 18), '/', ''), ':', '')) AS ULTIMO_REG,
            								idcliente
            						FROM
            							bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta pc
            						inner join cob_resultados cr ON cr.id = pc.proceso
            						group by idcliente) B ON B.idcliente = A.IDCLIENTE AND B.ULTIMO_REG = (SELECT replace(replace(substring(A.IDPROCESO, (SELECT length(A.IDPROCESO) - 18 + 1), 18), '/', ''), ':', ''))
            			left join varios.empleados_rh201_intelisis p on p.clave=A.agente
            			left join (SELECT agente,TRIM(substring_index(MID(es,5,100),'0',1)) as Rutacobro FROM agentes where nomina=agente) niv on niv.agente=a.agente
            			where (A.ESTATUS = 100 and A.fecha between '".$desde."' and '".$hasta."') or A.ESTATUS = 101
            			GROUP BY A.AGENTE,P.Nombre,A.ESTATUS) agentes
            						  left join (
            				  select
            											Agente,
            											count(cuenta) as totdet,
            											sum(cobros) as sumacobrosdet,
            											sum(impgestor) as descGes
            									  from ( select
            												  pordet.cuenta,pordet.cobros,pordet.impgestor,procesos.agente
            											 from( SELECT
            														 asig.cuenta,
            														 asig.nomina as agente,
            														 a.cobros,
            														 case when des.forma = 'PORCENTAJE' then ROUND((a.cobros * des.porc_gestor) / 100, 2) else des.imp_gestor end as impgestor
            												   FROM android_reportes._cuentas_gestioncobranza asig
            												   inner join bitacora_gestioncobranza_intelisis.cob_niveles_especiales_mavi ne ON asig.nivel = ne.nivel
            												   inner join (select
            																   cliente,
            																   sum(cobros) as cobros
            															   from bitacora_gestioncobranza_intelisis.abonos_497d
            															   where fecha_emision between '".$desde."' and '".$hasta."' group by cliente) a ON a.cliente = asig.cuenta
            												   left join cob_importe_minimo imp on imp.id_tipo='determinacion' and a.cobros>=imp.monto
            												   LEFT JOIN cob_descuentos des on des.tipo='DETERMINADOR'
            												   where asig.periodo=".$_POST['quincena']." and asig.ejercicio=".$_POST['ejercicio']."
            												   group by asig.cuenta
            												   union all
            												   SELECT
            														 asig.cuenta,
            														 asig.nomina as agente,
            														 a.cobros,
            														 case when des.forma = 'PORCENTAJE' then ROUND((a.cobros * des.porc_gestor) / 100, 2) else des.imp_gestor end as impgestor
            												   FROM android_reportes._cuentas_gestioncobranza asig
            												   inner join bitacora_gestioncobranza_intelisis.cob_niveles_especiales_mavicob ne ON asig.nivel = ne.nivel2
            												   inner join (select
            																	 cliente,
            																	 sum(cobros) as cobros
            															   from bitacora_gestioncobranza_intelisis.abonos_497d
            															   where fecha_emision between '".$desde."' and '".$hasta."' group by cliente) a ON a.cliente = asig.cuenta
            												   left join cob_importe_minimo imp on imp.id_tipo='determinacion' and a.cobros>=imp.monto
            												   LEFT JOIN cob_descuentos des on des.tipo='DETERMINADOR'
            												   where asig.periodo=".$_POST['quincena']." and asig.ejercicio=".$_POST['ejercicio']."
            												   group by asig.cuenta
            												   
            												   UNION ALL
            												   
            												   SELECT
            														 asig.cuenta,
            														 ad.agente_asociado as agente,
            														 a.cobros,
            														 case when des.forma = 'PORCENTAJE' then ROUND((a.cobros * des.porc_gestor) / 100, 2) else des.imp_gestor end as impgestor
            												   FROM(select agente_origen, agente_asociado from agente_doble
            																		where fecha_asigna between '".$desde."' and '".$hasta."'
            																		group by agente_origen, agente_asociado) ad
            													left join android_reportes._cuentas_gestioncobranza asig on asig.nomina=ad.agente_origen
            												   inner join bitacora_gestioncobranza_intelisis.cob_niveles_especiales_mavi ne ON asig.nivel = ne.nivel
            												   inner join (select
            																   cliente,
            																   sum(cobros) as cobros
            															   from bitacora_gestioncobranza_intelisis.abonos_497d
            															   where fecha_emision between '".$desde."' and '".$hasta."' group by cliente) a ON a.cliente = asig.cuenta
            												   left join cob_importe_minimo imp on imp.id_tipo='determinacion' and a.cobros>=imp.monto
            												   LEFT JOIN cob_descuentos des on des.tipo='DETERMINADOR'
            												   where asig.periodo=".$_POST['quincena']." and asig.ejercicio=".$_POST['ejercicio']."
            												   group by asig.cuenta
            												   union all
            												   SELECT
            														 asig.cuenta,
            														 ad.agente_asociado as agente,
            														 a.cobros,
            														 case when des.forma = 'PORCENTAJE' then ROUND((a.cobros * des.porc_gestor) / 100, 2) else des.imp_gestor end as impgestor
            												   FROM(select agente_origen, agente_asociado from agente_doble
            																		where fecha_asigna between '".$desde."' and '".$hasta."'
            																		group by agente_origen, agente_asociado) ad
            													left join android_reportes._cuentas_gestioncobranza asig on asig.nomina=ad.agente_origen
            												   inner join bitacora_gestioncobranza_intelisis.cob_niveles_especiales_mavicob ne ON asig.nivel = ne.nivel2
            												   inner join (select
            																	 cliente,
            																	 sum(cobros) as cobros
            															   from bitacora_gestioncobranza_intelisis.abonos_497d
            															   where fecha_emision between '".$desde."' and '".$hasta."' group by cliente) a ON a.cliente = asig.cuenta
            												   left join cob_importe_minimo imp on imp.id_tipo='determinacion' and a.cobros>=imp.monto
            												   LEFT JOIN cob_descuentos des on des.tipo='DETERMINADOR'
            												   where asig.periodo=".$_POST['quincena']." and asig.ejercicio=".$_POST['ejercicio']."
            												   group by asig.cuenta
            												
            												   
            												   
            												)pordet
            												inner join (SELECT 
            																A . *
            															FROM
            																bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta A
            															INNER JOIN (SELECT 
            																MAX(replace(replace(substring(IDPROCESO, (select length(IDPROCESO) - 18 + 1), 18), '/', ''), ':', '')) AS ULTIMO_REG,
            																	idcliente
            															FROM
            																bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta pc
            															inner join cob_resultados cr ON cr.id = pc.proceso
            															group by idcliente) B ON B.idcliente = A.IDCLIENTE
            																and ESTATUS = '101'
            																AND B.ULTIMO_REG = (SELECT replace(replace(substring(A.IDPROCESO, (SELECT length(A.IDPROCESO) - 18 + 1), 18), '/', ''), ':', ''))) procesos ON pordet.cuenta = procesos.idcliente
            									  )poragente
            									  group by Agente
            				  )deter on agentes.agente=deter.agente where agentes.tipo='DETERMINADOR')deter on deter.agente=a.agente
            				  where (jefes.tot is not null or deter.totdet is not null)
            			";
        //echo $sql;
        mysql_query($sql);

        //echo $sql;
        $sql2="Select * from hits_tem";
        $res = mysql_query($sql2);
        	
		$sqltotal1="Select  sum(cuentas_enviadas_det)  as nocuentassuma,
						 	sum(cuentas_cobradas_jefe) as agentessuma,
							sum(impgestor) as totalsuma ,
							sum(enviadas_det) as enviodetsuma,
							sum(total_ceuntas_determinador) as sumatotaldet,
							sum(cuentas_cobradas_deter) as cuentaagentesbsuma,
							sum(cuentas_cobradas_deter_imp) as cuentaagentesbsumaimp,
							sum(total_cuentas_cobradas) as importegestorsuma ,
							sum(porc_total) as totalcuentassuma,
							sum(importe_total) as importetotalsuma  
							from hits_tem"; 
							
		$primerdia="select DATE_FORMAT(quincena_cobranza1(".$_POST['quincena'].",".$_POST['ejercicio']."),'%Y %b %e')  		AS pd";	
		$segundodia="select DATE_FORMAT(quincena_cobranza2(".$_POST['quincena'].",".$_POST['ejercicio']."),'%Y %b %e')
		AS sd";
        	
        	
    	if (mysql_num_rows($res) != 0) {
        				$this->tpl = "gestionescobranza/Cuenta_Cobrada_Determinacion/rep_cue_cob.htm";
    					
    					
		}
    					
    	$bloq = array
    	(
    		"post"  => empty($_POST)?array():array($_POST),
    		"blk2" => (!empty($_POST))?$sql2:array($_POST),
    		"blk3" => (!empty($_POST))?$sqltotal1:array($_POST),
    		"primer" => (!empty($_POST))?$primerdia:array($_POST),
    		"segundo" => (!empty($_POST))?$segundodia:array($_POST)
		);
		return $bloq;
	
	}

	function _rep_dedu(){
	
	    $this->tpl = "gestionescobranza/Cuenta_Cobrada_Determinacion/reporte.dedu.htm";
		//$this->tpl = "gestionescobranza/reporte.negativa_disenio.html";
	
	
	
		
		$fecha1="select quincena_cobranza1(".$_POST['quincena'].",".$_POST['ejercicio'].") as fecha";
		$res=mysql_fetch_assoc(mysql_query($fecha1));
		$desde=$res['fecha'];
		$fecha2="select quincena_cobranza2(".$_POST['quincena'].",".$_POST['ejercicio'].") as fecha";
		$res=mysql_fetch_assoc(mysql_query($fecha2));
		$hasta=$res['fecha'];
		
		
		$sql="CREATE TEMPORARY TABLE IF NOT EXISTS reportededu AS select 
            a . gestor,
            case when a.estatusgestor='BAJA' THEN NULL ELSE a.porc_gestor end as porc_gestor,
            case when a.estatusgestor='BAJA' THEN NULL ELSE a.porcgestor end as porcgestor,
            case when a.estatusgestor='BAJA' THEN NULL ELSE a.imp_gestor end as imp_gestor,
            case when a.estatusgestor='BAJA' THEN NULL ELSE a.impgestor end as impgestor,
            a.Jefe_gestor,
            case when a.estatusjefe='BAJA' THEN NULL ELSE a.porc_jefe end as porc_jefe,
            case when a.estatusjefe='BAJA' THEN NULL ELSE a.porcjefe end as porcjefe,
            case when a.estatusjefe='BAJA' THEN NULL ELSE a.imp_jefe end as imp_jefe,
            case when a.estatusjefe='BAJA' THEN NULL ELSE a.impjefe end as impjefe,
            a.Sub_Gerente,
            case when a.estatussubgerente='BAJA' THEN NULL ELSE a.porc_subgerente end as porc_subgerente,
            case when a.estatussubgerente='BAJA' THEN NULL ELSE a.porcsubgerente end as porcsubgerente,
            case when a.estatussubgerente='BAJA' THEN NULL ELSE a.imp_subgerente end as imp_subgerente,
            case when a.estatussubgerente='BAJA' THEN NULL ELSE a.impsubgerente end as impsubgerente,
            a.Gerente, 
            case when a.estatusgerente='BAJA' THEN NULL ELSE a.porc_gerente end as porc_gerente,
            case when a.estatusgerente='BAJA' THEN NULL ELSE a.porcgerente end as porcgerente,
            case when a.estatusgerente='BAJA' THEN NULL ELSE a.imp_gerente end as imp_gerente,
        case when a.estatusgerente='BAJA' THEN NULL ELSE a.impgerente end as impgerente
            from
                (select 
                universo . *,
                    des.porc_gestor,
                    des.porc_jefe,
                    des.porc_subgerente,
                    des.porc_gerente,
                    des.imp_gestor,
                    des.imp_jefe,
                    des.imp_subgerente,
                    des.imp_gerente,
                    case
                        when
                            des.forma = 'PORCENTAJE'
                                and cobros >= universo.monto
                        then
                            ROUND((cobros * des.porc_gestor)/100, 2)
                        else 0
                    end as impgestor,
                    case
                        when
                            des.forma = 'PORCENTAJE'
                                and cobros >= universo.monto
                        then
                            ROUND((cobros * des.porc_jefe)/100, 2)
                        else 0
                    end as impjefe,
                    case
                        when
                            des.forma = 'PORCENTAJE'
                                and cobros >= universo.monto
                        then
                            ROUND((cobros * des.porc_subgerente)/100, 2)
                        else 0
                    end as impsubgerente,
                    case
                        when
                            des.forma = 'PORCENTAJE'
                                and cobros >= universo.monto
                        then
                            ROUND((cobros * des.porc_gerente)/100, 2)
                        else 0
                    end as impgerente,
                    case
                        when
                            des.forma = 'IMPORTE'
                                and cobros >= universo.monto
                        then
                            ROUND((des.imp_gestor / cobros) * 100, 2)
                        else 0
                    end as porcgestor,
                    case
                        when
                            des.forma = 'IMPORTE'
                                and cobros >= universo.monto
                        then
                            ROUND((des.imp_jefe / cobros) * 100, 2)
                        else 0
                    end as porcjefe,
                    case
                        when
                            des.forma = 'IMPORTE'
                                and cobros >= universo.monto
                        then
                            ROUND((des.imp_subgerente / cobros) * 100, 2)
                        else 0
                    end as porcsubgerente,
                    case
                        when
                            des.forma = 'IMPORTE'
                                and cobros >= universo.monto
                        then
                            ROUND((des.imp_gerente / cobros) * 100, 2)
                        else 0
                    end as porcgerente
            from
                (select 
                    procesos.IDPROCESO,
                    procesos.IDCLIENTE,
                    procesos.AGENTE,
                    universo.cobros,
                    im.monto,
                    case
                        when cobros >= im.monto then 'SI'
                        ELSE 'NO'
                    END as cm,
                    case
                        when estatus_a.EDO = 'V' then 'ACTIVO'
                        else 'BAJA'
                    end as estatusgestor,
                    case
                        when a.nomina != '' then a.nomina
                        else 'NO EXISTE'
                    end as gestor,
                    case
                        when estatus_b.EDO = 'V' then 'ACTIVO'
                        else 'BAJA'
                    end as estatusjefe,
                    case
                        when b.Nomina != '' then b.Nomina
                        else 'NO EXISTE'
                    end as Jefe_gestor,
                    case
                        when estatus_c.EDO = 'V' then 'ACTIVO'
                        else 'BAJA'
                    end as estatussubgerente,
                    case
                        when c.Nomina != '' then c.Nomina
                        else 'NO EXISTE'
                    end as Sub_Gerente,
                    case
                        when estatus_d.EDO = 'V' then 'ACTIVO'
                        else 'BAJA'
                    end as estatusgerente,
                    case
                        when d.Nomina != '' then d.Nomina
                        else 'NO EXISTE'
                    end as Gerente
            from
                (SELECT 
                asig.cuenta,
                    asig.nombre,
                    asig.dias_inactivos as diasinactividad,
                    asig.dias_vencidos as diasvencidos,
                    asig.nivel as rutacobro,
                    asig.nomina as agente,
                    a.cobros,
                    a.fecha_emision,
                    asig.ejercicio,
                    asig.periodo as quincena
            FROM
                android_reportes._cuentas_gestioncobranza asig
            inner join bitacora_gestioncobranza_intelisis.cob_niveles_especiales_mavi ne ON asig.nivel = ne.nivel
        		inner join (select 
        						cliente,
        							sum(cobros) as cobros,
        							MAX(fecha_emision) as fecha_emision,
        							ejercicio,
        							quincena
        					from
        						bitacora_gestioncobranza_intelisis.abonos_497d
        					where fecha_emision between '".$desde."' and '".$hasta."'
        					group by cliente) a ON a.cliente = asig.cuenta
        		where asig.periodo=".$_POST['quincena']." and asig.ejercicio=".$_POST['ejercicio']."
            group by asig.cuenta 
        	
        	
        	union all
        	
        	SELECT 
                asig.cuenta,
                    asig.nombre,
                    asig.dias_inactivos as diasinactividad,
                    asig.dias_vencidos as diasvencidos,
                    asig.nivel as rutacobro,
                    asig.nomina as agente,
                    a.cobros,
                    a.fecha_emision,
                    asig.ejercicio,
                    asig.periodo as quincena
            FROM
                android_reportes._cuentas_gestioncobranza asig
            inner join bitacora_gestioncobranza_intelisis.cob_niveles_especiales_mavicob ne ON asig.nivel = ne.nivel2
            inner join (select 
        					cliente,
        						sum(cobros) as cobros,
        						MAX(fecha_emision) as fecha_emision,
        						ejercicio,
        						quincena
        				from
        					bitacora_gestioncobranza_intelisis.abonos_497d
        				where fecha_emision between '".$desde."' and '".$hasta."'
        				group by cliente) a ON a.cliente = asig.cuenta
           	where asig.periodo=".$_POST['quincena']." and asig.ejercicio=".$_POST['ejercicio']."				
            group by asig.cuenta
        	
        	union all 
        	
        	SELECT 
                asig.cuenta,
                    asig.nombre,
                    asig.dias_inactivos as diasinactividad,
                    asig.dias_vencidos as diasvencidos,
                    asig.nivel as rutacobro,
                    agente_asociado as agente,
                    a.cobros,
                    a.fecha_emision,
                    asig.ejercicio,
                    asig.periodo as quincena
            FROM(select agente_origen, agente_asociado from bitacora_gestioncobranza_intelisis.agente_doble
        							where fecha_asigna between '".$desde."' and '".$hasta."'
        							group by agente_origen, agente_asociado) ad
        		left join android_reportes._cuentas_gestioncobranza asig on asig.nomina=ad.agente_origen
        		inner join bitacora_gestioncobranza_intelisis.cob_niveles_especiales_mavi ne ON asig.nivel = ne.nivel
        		inner join (select 
        						cliente,
        							sum(cobros) as cobros,
        							MAX(fecha_emision) as fecha_emision,
        							ejercicio,
        							quincena
        					from
        						bitacora_gestioncobranza_intelisis.abonos_497d
        					where fecha_emision between '".$desde."' and '".$hasta."'
        					group by cliente) a ON a.cliente = asig.cuenta
        	where asig.periodo=".$_POST['quincena']." and asig.ejercicio=".$_POST['ejercicio']."				
            group by asig.cuenta
        	
        	
        	union all 
        	
        	SELECT 
                asig.cuenta,
                    asig.nombre,
                    asig.dias_inactivos as diasinactividad,
                    asig.dias_vencidos as diasvencidos,
                    asig.nivel as rutacobro,
                    agente_asociado as agente,
                    a.cobros,
                    a.fecha_emision,
                    asig.ejercicio,
                    asig.periodo as quincena
            FROM(select agente_origen, agente_asociado from bitacora_gestioncobranza_intelisis.agente_doble
        							where fecha_asigna between '".$desde."' and '".$hasta."'
        							group by agente_origen, agente_asociado) ad
        		left join android_reportes._cuentas_gestioncobranza asig on asig.nomina=ad.agente_origen
        		inner join bitacora_gestioncobranza_intelisis.cob_niveles_especiales_mavicob ne ON asig.nivel = ne.nivel2
        		inner join (select 
        						cliente,
        							sum(cobros) as cobros,
        							MAX(fecha_emision) as fecha_emision,
        							ejercicio,
        							quincena
        					from
        						bitacora_gestioncobranza_intelisis.abonos_497d
        					where fecha_emision between '".$desde."' and '".$hasta."'
        					group by cliente) a ON a.cliente = asig.cuenta
        	where asig.periodo=".$_POST['quincena']." and asig.ejercicio=".$_POST['ejercicio']."				
            group by asig.cuenta
        	
        	
        	) universo
            inner join (SELECT 
                A . *
            FROM
                bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta A
            INNER JOIN (SELECT 
                MAX(replace(replace(substring(IDPROCESO, (select length(IDPROCESO) - 18 + 1), 18), '/', ''), ':', '')) AS ULTIMO_REG,
                    idcliente
            FROM
                bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta pc
            inner join cob_resultados cr ON cr.id = pc.proceso
            group by idcliente) B ON B.idcliente = A.IDCLIENTE
                and ESTATUS = '101'
                AND B.ULTIMO_REG = (SELECT replace(replace(substring(A.IDPROCESO, (SELECT length(A.IDPROCESO) - 18 + 1), 18), '/', ''), ':', ''))) procesos ON universo.cuenta = procesos.idcliente
            inner join bitacora_gestioncobranza_intelisis.cob_resultados e ON procesos.PROCESO = e.id
            inner join cob_importe_minimo im ON im.id_tipo = 'determinacion'
            left join bitacora_gestioncobranza_intelisis.agentes a ON a.agente = procesos.agente
            left join bitacora_gestioncobranza_intelisis.agentes b ON a.celula = b.agente
            left join bitacora_gestioncobranza_intelisis.agentes c ON a.division = c.agente
            left join bitacora_gestioncobranza_intelisis.agentes d ON a.gerencia = d.agente
            left join varios.empleados_rh201_intelisis estatus_a ON a.nomina = estatus_a.CLAVE
            left join varios.empleados_rh201_intelisis estatus_b ON b.nomina = estatus_b.CLAVE
            left join varios.empleados_rh201_intelisis estatus_c ON c.nomina = estatus_c.CLAVE
            left join varios.empleados_rh201_intelisis estatus_d ON d.nomina = estatus_d.CLAVE) universo
            left join cob_descuentos des ON des.tipo = 'DETERMINADOR'
                #and universo.estatusgestor = 'ACTIVO'
                #AND universo.estatusjefe = 'ACTIVO'
                #AND universo.estatussubgerente = 'ACTIVO'
                #AND universo.estatusgerente = 'ACTIVO'
                and cobros >= universo.monto) a
           where 1=1
         group by idproceso ";
         
        	
        	mysql_query($sql);
        	
        	$sql1="select 
           b.Gerente as AGENTE,sum(b.imp_gerente) + sum(b.impgerente) as total
        from reportededu b
        	 where imp_gerente is not null group by b.Gerente ";
        	 $res1 = mysql_query($sql1);
        	 
        	 $sql2="select
        	b.Sub_Gerente as AGENTE,sum(b.imp_subgerente) + sum(b.impsubgerente) as total
        from reportededu b
        	 where  imp_subgerente is not null  group by b.Sub_Gerente ";
        	 $res2 = mysql_query($sql2);
        	 
        	 $sql3="select 
           b.Jefe_gestor as AGENTE,sum(b.imp_jefe) + sum(b.impjefe) as total
        from reportededu b
        	 where  imp_jefe is not null  group by b.Jefe_gestor";
        	 $res3 = mysql_query($sql3);
        	 
        	  $sql4="select 
            b.gestor as AGENTE,sum(b.imp_gestor) + sum(b.impgestor) as total
        from reportededu b
        	 where imp_gestor is not null group by b.gestor";
        	 $res4 = mysql_query($sql4);
        	 
        	 $sql5="SELECT UNIVERSO.agente, sum(UNIVERSO.imp_gestor) + sum(UNIVERSO.impgestor) as total FROM(
        SELECT  UNIVERSO.* FROM(
        SELECT UNI.*,TABLAPROCESO.resultado,case when AGENTES.Nombre != '' then AGENTES.Nombre ELSE 'BAJA' END as NombreJefe,CLIENTES.Nombre as NombreCliente,
            case when AGENTES.Es != '' then AGENTES.Es ELSE 'BAJA' END as Es,DES.tipo,
        	ROUND((des.porc_comision * cobrossuma) / 100,  2) as imp_comision,
        	des.porc_comision,
        	des.imp_gestor,
            des.porc_gestor,
        	case
                when
                    DES.forma = 'PORCENTAJE'
                        and cobrossuma >= IMPORTE.monto
                then
                    ROUND((DES.porc_gestor * cobrossuma) / 100,
                            2)
                else 0
            end as impgestor,
            case
                when
                    DES.forma = 'IMPORTE'
                        and cobrossuma >= IMPORTE.monto
                then
                    ROUND((DES.imp_gestor / cobrossuma) * 100,
                            2)
                else 0
            end as porcgestor,
        	CASE
                WHEN cobrossuma >= IMPORTE.monto THEN 'SI'
                ELSE 'NO'
            END AS cm 
        FROM(

        SELECT UNIVERSO.*,
        	
            
        	
        CASE
                WHEN estatus_gestor.EDO = 'V' then 'ACTIVO'
                ELSE 'BAJA'
            END AS estatusgestor,
        CASE
                WHEN estatus_jefe.EDO = 'V' then 'ACTIVO'
                ELSE 'BAJA'
            END AS estatusjefe
         
        FROM(
        SELECT *, ROUND(sum(cobros), 2) AS cobrossuma FROM(
        select 
                cliente,
                    fecha_emision AS fecha_cobro,
                    cobros,
                    dias_vencidos,
                    dias_inactivos,
        			quincena,
        			ejercicio
        			
            from
                bitacora_gestioncobranza_intelisis.abonos_497d)CUENTAS
            INNER JOIN
            (
        	
        	SELECT
        		  AGENTE,
        		  case when JEFE != '' then JEFE ELSE 'E000000' END as JEFE,
        		  replace(replace(substring(IDPROCESO, (select length(IDPROCESO) - 18 + 1), 18), '/', ''), ':', '') AS ULTIMO_REG,
        		  SUBSTRING(substring(IDPROCESO, (select length(IDPROCESO) - 18 + 1), 18), 1, 10) AS fecha_inicio, pc.idcliente, PROCESO, ESTATUS
        		FROM bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta pc
        		inner join (SELECT
        				MAX(replace(replace(substring(IDPROCESO, (select length(IDPROCESO) - 18 + 1), 18), '/', ''), ':', '')) AS ULTIMO_REG,
        					idcliente
        			FROM
        				bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta pc
        			inner join cob_resultados cr ON cr.id = pc.proceso
        			group by idcliente) cr ON cr.idcliente = pc.IDCLIENTE
                    inner join registros r on r.cuenta=pc.idcliente and r. resultado in ('pago','LCT') and CASE WHEN (r.intelisis IS NULL OR r.intelisis = 'SIN AGE' OR r.intelisis = ' ') THEN REPLACE(pc.jefe,' ','') = REPLACE(r.promotor,' ','') ELSE  REPLACE(pc.jefe,' ','') = REPLACE(r.intelisis,' ','') end						
        		where pc.ESTATUS = '100'
        		and pc.fecha between '".$desde."' and '".$hasta."'
        		
        	) PROCESO  ON CUENTAS.cliente = PROCESO.idcliente
        	and CUENTAS.fecha_cobro > PROCESO.fecha_inicio GROUP BY CUENTAS.cliente)UNIVERSO
        	left join
            varios.empleados_rh201_intelisis estatus_gestor ON UNIVERSO.AGENTE = estatus_gestor.CLAVE
        	left join
            varios.empleados_rh201_intelisis estatus_jefe ON UNIVERSO.JEFE = estatus_jefe.CLAVE)UNI


        	left JOIN
            bitacora_gestioncobranza_intelisis.cob_importe_minimo IMPORTE ON IMPORTE.id_tipo = 'jefe' AND UNI.cobrossuma >= IMPORTE.monto
        	inner join
            bitacora_gestioncobranza_intelisis.cob_resultados TABLAPROCESO ON UNI.PROCESO = TABLAPROCESO.id
        	LEFT join
            bitacora_gestioncobranza_intelisis.agentes AGENTES ON UNI.JEFE = AGENTES.Nomina
        	left join
            bitacora_gestioncobranza_intelisis.registros CLIENTES ON UNI.cliente = CLIENTES.Cuenta
        	
        	left join
            bitacora_gestioncobranza_intelisis.cob_descuentos DES ON  UNI.cobrossuma >= IMPORTE.monto AND (AGENTES.Es LIKE CONCAT('% ', DES.Tipo)
                OR AGENTES.Es LIKE CONCAT('% ', DES.Tipo, ' %') ) AND UNI.estatusgestor ='ACTIVO' AND UNI.estatusjefe ='ACTIVO'
        	GROUP BY UNI.cliente)UNIVERSO)UNIVERSO where tipo != ''  group by UNIVERSO.Agente";
        	$res = mysql_query($sql5);
        	
        	
        	$sql6="CREATE TEMPORARY TABLE IF NOT EXISTS reporteprio AS 
        	
        	select * from(select universo.*,count(importecobro) as cuentasconcobro,count(proceso) as cuentasconproceso,universo.cuentastotales-universo.cuentascumplidas as cuentasincumplidas,
        		(universo.cuentastotales-universo.cuentascumplidas)*pri.importe as importedescuentogestor,pri.cumplimiento,pri.sancion_jefe,pri.sancion_subgerente,pri.sancion_gerente,
        		case when estatus_b.EDO = 'V' then 'ACTIVO' else 'BAJA' end as estatusjefe,
        		case when bb.Nomina != '' then bb.Nomina else 'NO EXISTE' end as Jefe_gestor,
        		case when estatus_c.EDO = 'V' then 'ACTIVO' else 'BAJA' end as estatussubgerente,
        		case when cc.Nomina != '' then cc.Nomina else 'NO EXISTE' end as Sub_Gerente,
        		case when estatus_d.EDO = 'V' then 'ACTIVO' else 'BAJA' end as estatusgerente,
        	    case when dd.Nomina != '' then dd.Nomina else 'NO EXISTE' end as Gerente,
        		round((((universo.cuentastotales-universo.cuentascumplidas)*pri.importe)/100)*pri.sancion_jefe,2) as importedescuentojefe,
        		round((((universo.cuentastotales-universo.cuentascumplidas)*pri.importe)/100)*pri.sancion_subgerente,2) as importedescuentosubgerente,
        		round((((universo.cuentastotales-universo.cuentascumplidas)*pri.importe)/100)*pri.sancion_gerente,2) as importedescuentogerente,
        		round(((universo.cuentastotales-universo.cuentascumplidas)*pri.importe)+((((universo.cuentastotales-universo.cuentascumplidas)*pri.importe)/100)*pri.sancion_jefe)+((((universo.cuentastotales-universo.cuentascumplidas)*pri.importe)/100)*pri.sancion_subgerente)+((((universo.cuentastotales-universo.cuentascumplidas)*pri.importe)/100)*pri.sancion_gerente),2) as importetotal

        from(
        SELECT 
            universo.cuenta,
            universo.nomina as gestor,
        	universo.dv,
        	universo.di,
            TRIM(substring_index(MID(nivel.es, 5, 100), '0', 1)) as nivel,
            agente.nombre,
        	cobros.importecobro,
        	procesos.nombre as proceso,
        	case when cumplidas.cuentascumplidas is null then '0' else cumplidas.cuentascumplidas end as cuentascumplidas,
        	case when totales.cuentastotales is null then '0' else totales.cuentastotales end as cuentastotales,
        	case when cobros.importecobro!='' or  procesos.nombre!='' then 'SI' else 'NO' end as cumplimientosino,
        	round((100*(case when cumplidas.cuentascumplidas is null then '0' else cumplidas.cuentascumplidas end))/(case when totales.cuentastotales is null then '0' else totales.cuentastotales end),2) as porcentajecumplidas

        FROM
            (SELECT cuenta,max(dv) as dv,max(di) as di,nomina,quinsena,ejercicio
          		FROM comentarios_gestiones
            where
                prioridad_visita = 'on' and quinsena = ".$_POST['quincena']."
                    and ejercicio = ".$_POST['ejercicio']." 
        		  group by cuenta,quinsena,ejercicio ) universo
        left join
            bitacora_gestioncobranza_intelisis.agentes nivel ON nivel.nomina = universo.nomina
                and nivel.nomina = nivel.agente
        left join
            varios.empleados_rh201_intelisis agente ON agente.clave = universo.nomina

        left join(SELECT 
                cliente, round(sum(cobros),2) as importecobro
            FROM
                bitacora_gestioncobranza_intelisis.abonos_497d abonos
            inner join bitacora_gestioncobranza_intelisis.cob_importe_minimo im ON im.id_tipo = 'seguimiento'
                and abonos.cobros >= im.monto
            where
         fecha_emision between '".$desde."' and '".$hasta."' group by cliente) cobros on cobros.cliente=universo.cuenta


        left join(
        select a.gestor,count(a.cumplimientosino) as cuentascumplidas from(
        SELECT 
            universo.nomina as gestor,
        	case when cobros.importecobro!='' or  procesos.nombre!='' then 'SI' else 'NO' end as cumplimientosino
        FROM
            (SELECT cuenta,max(dv) as dv,max(di) as di,nomina,quinsena,ejercicio
          		FROM comentarios_gestiones
            where
                prioridad_visita = 'on' and quinsena = ".$_POST['quincena']."
                    and ejercicio = ".$_POST['ejercicio']." 
        		  group by cuenta,quinsena,ejercicio ) universo
        left join
            bitacora_gestioncobranza_intelisis.agentes nivel ON nivel.nomina = universo.nomina
                and nivel.nomina = nivel.agente
        left join
            varios.empleados_rh201_intelisis agente ON agente.clave = universo.nomina
        left join(SELECT 
                cliente, round(sum(cobros),2) as importecobro
            FROM
                bitacora_gestioncobranza_intelisis.abonos_497d abonos
            inner join bitacora_gestioncobranza_intelisis.cob_importe_minimo im ON im.id_tipo = 'seguimiento'
                and abonos.cobros >= im.monto
            where
         fecha_emision between '".$desde."' and '".$hasta."' group by cliente) cobros on cobros.cliente=universo.cuenta
        left join
        		(SELECT 
            cuenta, saldocapital,
        min(replace(fecha_inicio,'-','')) as id
        FROM
            bitacora_gestioncobranza_intelisis.registros
        group by cuenta) saldo on saldo.cuenta=universo.cuenta
        	left join(
        select * from(
        SELECT 
                A . *,res.nombre
            FROM
                bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta A
            left JOIN (SELECT 
                MAX(replace(replace(substring(IDPROCESO, (select length(IDPROCESO) - 18 + 1), 18), '/', ''), ':', '')) AS ULTIMO_REG,
                    idcliente
            FROM
                bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta pc
            left join cob_resultados cr ON cr.id = pc.proceso
            group by idcliente) B ON B.idcliente = A.IDCLIENTE
                AND B.ULTIMO_REG = (SELECT replace(replace(substring(A.IDPROCESO, (SELECT length(A.IDPROCESO) - 18 + 1), 18), '/', ''), ':', ''))
        	left join bitacora_gestioncobranza_intelisis.catalogo_resultado res on res.id=A.proceso
            group by idcliente)a where a.estatus='101')procesos on procesos.idcliente=universo.cuenta 

        inner join bitacora_gestioncobranza_intelisis.cob_cuentas_prioridad prioridad on TRIM(substring_index(MID(nivel.es, 5, 100), '0', 1))=prioridad.nivel)a 
        where a.cumplimientosino='SI' group by a.gestor
        	)cumplidas on cumplidas.gestor=universo.nomina


        left join(
        select * from(
        SELECT 
                A . *,res.nombre
            FROM
                bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta A
            left JOIN (SELECT 
                MAX(replace(replace(substring(IDPROCESO, (select length(IDPROCESO) - 18 + 1), 18), '/', ''), ':', '')) AS ULTIMO_REG,
                    idcliente
            FROM
                bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta pc
            left join cob_resultados cr ON cr.id = pc.proceso
            group by idcliente) B ON B.idcliente = A.IDCLIENTE
                AND B.ULTIMO_REG = (SELECT replace(replace(substring(A.IDPROCESO, (SELECT length(A.IDPROCESO) - 18 + 1), 18), '/', ''), ':', ''))
        	left join bitacora_gestioncobranza_intelisis.catalogo_resultado res on res.id=A.proceso
            group by idcliente)a where a.estatus='101')procesos on procesos.idcliente=universo.cuenta 


        left join(select a.gestor,count(a.cuenta) as cuentastotales from(
        SELECT 
            universo.cuenta,
            universo.nomina as gestor,
        	universo.dv,
        	universo.di,
            universo.quinsena,
            universo.ejercicio,
            TRIM(substring_index(MID(nivel.es, 5, 100), '0', 1)) as nivel,
            agente.nombre,
        	saldo.saldocapital,
        	cobros.importecobro,
        	procesos.nombre as proceso,
        	case when cobros.importecobro!='' or  procesos.nombre!='' then 'SI' else 'NO' end as cumplimientosino
        FROM
            (SELECT cuenta,max(dv) as dv,max(di) as di,nomina,quinsena,ejercicio
          		FROM comentarios_gestiones
            where
                prioridad_visita = 'on' and quinsena = ".$_POST['quincena']."
                    and ejercicio = ".$_POST['ejercicio']." 
        		  group by cuenta,quinsena,ejercicio ) universo
        left join
            bitacora_gestioncobranza_intelisis.agentes nivel ON nivel.nomina = universo.nomina
                and nivel.nomina = nivel.agente
        left join
            varios.empleados_rh201_intelisis agente ON agente.clave = universo.nomina
        left join(SELECT 
                cliente, round(sum(cobros),2) as importecobro
            FROM
                bitacora_gestioncobranza_intelisis.abonos_497d abonos
            inner join bitacora_gestioncobranza_intelisis.cob_importe_minimo im ON im.id_tipo = 'seguimiento'
                and abonos.cobros >= im.monto
            where
         fecha_emision between '".$desde."' and '".$hasta."' group by cliente) cobros on cobros.cliente=universo.cuenta
        left join
        		(SELECT 
            cuenta, saldocapital,
        min(replace(fecha_inicio,'-','')) as id
        FROM
            bitacora_gestioncobranza_intelisis.registros
        group by cuenta) saldo on saldo.cuenta=universo.cuenta
        	left join(
        select* from(
        SELECT 
                A . *,res.nombre
            FROM
                bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta A
            left JOIN (SELECT 
                MAX(replace(replace(substring(IDPROCESO, (select length(IDPROCESO) - 18 + 1), 18), '/', ''), ':', '')) AS ULTIMO_REG,
                    idcliente
            FROM
                bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta pc
            left join cob_resultados cr ON cr.id = pc.proceso
            group by idcliente) B ON B.idcliente = A.IDCLIENTE
                AND B.ULTIMO_REG = (SELECT replace(replace(substring(A.IDPROCESO, (SELECT length(A.IDPROCESO) - 18 + 1), 18), '/', ''), ':', ''))
        	left join bitacora_gestioncobranza_intelisis.catalogo_resultado res on res.id=A.proceso
            group by idcliente)a where a.estatus='101')procesos on procesos.idcliente=universo.cuenta 

        inner join bitacora_gestioncobranza_intelisis.cob_cuentas_prioridad prioridad on TRIM(substring_index(MID(nivel.es, 5, 100), '0', 1))=prioridad.nivel)a 
        group by a.gestor)totales on totales.gestor=universo.nomina 

        inner join bitacora_gestioncobranza_intelisis.cob_cuentas_prioridad prioridad on TRIM(substring_index(MID(nivel.es, 5, 100), '0', 1))=prioridad.nivel

        )universo 

        left join bitacora_gestioncobranza_intelisis.cob_cuentas_prioridad pri on universo.porcentajecumplidas<pri.cumplimiento and universo.nivel=pri.nivel and universo.cumplimientosino='NO'
        left join bitacora_gestioncobranza_intelisis.agentes aa ON aa.agente = universo.gestor
            left join bitacora_gestioncobranza_intelisis.agentes bb ON aa.celula = bb.agente
            left join bitacora_gestioncobranza_intelisis.agentes cc ON aa.division = cc.agente
            left join bitacora_gestioncobranza_intelisis.agentes dd ON aa.gerencia = dd.agente
        	left join varios.empleados_rh201_intelisis estatus_a ON aa.nomina = estatus_a.CLAVE
            left join varios.empleados_rh201_intelisis estatus_b ON bb.nomina = estatus_b.CLAVE
            left join varios.empleados_rh201_intelisis estatus_c ON cc.nomina = estatus_c.CLAVE
            left join varios.empleados_rh201_intelisis estatus_d ON dd.nomina = estatus_d.CLAVE

        	where (universo.cuentastotales-universo.cuentascumplidas)*pri.importe!='0'
            group by universo.gestor)a";
			$res6 = mysql_query($sql6);
			
			
			$sql7="select * from reporteprio";
	
			$sql8="select *,sum(importedescuentojefe) as importejefe from reporteprio where Jefe_gestor!='NO EXISTE'  group by Jefe_gestor";
			
			$sql9="select *,sum(importedescuentosubgerente) as importesubgerente from reporteprio where Sub_Gerente!='NO EXISTE'  group by Sub_Gerente";
			
			$sql10="select *,sum(importedescuentogerente) as importegerente from reporteprio where Gerente!='NO EXISTE'  group by Gerente";
	
		$primerdia="select DATE_FORMAT(quincena_cobranza1(".$_POST['quincena'].",".$_POST['ejercicio']."),'%Y %b %e')  		AS pd";	
		$segundodia="select DATE_FORMAT(quincena_cobranza2(".$_POST['quincena'].",".$_POST['ejercicio']."),'%Y %b %e')
		AS sd";
			
			if (mysql_num_rows($res1) != 0 || mysql_num_rows($res2) != 0 || mysql_num_rows($res3) != 0 || mysql_num_rows($res4) != 0  || mysql_num_rows($res5) != 0) {
    				$this->tpl = "gestionescobranza/Cuenta_Cobrada_Determinacion/rep_dedu.htm";
					
					}
					
    	//echo $sql5;
    	
    	$bloq = array
		(
    		"post"  => empty($_POST)?array():array($_POST),
    		"blk2"  => (!empty($_POST))?$sql1:array($_POST),
    		"blk3" => (!empty($_POST))?$sql2:array($_POST),
    		"blk4" => (!empty($_POST))?$sql3:array($_POST),
    		"blk5" => (!empty($_POST))?$sql4:array($_POST),
    		"blk6" => (!empty($_POST))?$sql5:array($_POST),
    		"blk7" => (!empty($_POST))?$sql7:array($_POST),
    		"blk8" => (!empty($_POST))?$sql8:array($_POST),
    		"blk9" => (!empty($_POST))?$sql9:array($_POST),
    		"blk10" => (!empty($_POST))?$sql10:array($_POST),
    		"primer" => (!empty($_POST))?$primerdia:array($_POST),
    		"segundo" => (!empty($_POST))?$segundodia:array($_POST)
		);
				
		return $bloq;  
	
	}

	function _cuentas_prioridad(){
		
		//// Template ////
		$this->tpl="gestionescobranza/Cuenta_Cobrada_Determinacion/cuentas_prioridad.htm";
		$sqlp="SELECT * FROM cob_cuentas_prioridad"; 
        
		//// Consulta TinyButStrong ////
        $bloq = array(
                    "post"  => empty($_POST)?array():array($_POST),
					"blkCo" => $sqlp
                );
        return $bloq;	 
	}

	function _ins_cuentas_prioridad(){
		
		//// Envio de datos ////
		if(!empty($_POST)){
		
			$sql_cuentas="INSERT INTO cob_cuentas_prioridad (nivel,numero_cuentas,cumplimiento,importe,sancion_jefe,sancion_subgerente,sancion_gerente) VALUES ('".$_POST['nivel']."','".$_POST['prioridad']."','".$_POST['cumplimiento']."','".$_POST['importe']."','".$_POST['sancion_jefe']."','".$_POST['sancion_subgerente']."','".$_POST['sancion_gerente']."')";
			mysql_query($sql_cuentas);
			
			}
		$this->tpl="gestionescobranza/Cuenta_Cobrada_Determinacion/cuentas_prioridad.htm";
		$cuentas="SELECT * FROM cob_cuentas_prioridad WHERE 1"; 
		
        $bloq = array(
                    "post"  => empty($_POST)?array():array($_POST),
                    "blkCo" => $cuentas
                );
        return $bloq;			
	}

	function _eli_cuentas_prioridad(){
		
		$sql_eliminar="DELETE FROM cob_cuentas_prioridad WHERE nivel='".$_GET['nivel']."' ";
		mysql_query($sql_eliminar);
		
		$this->tpl="gestionescobranza/Cuenta_Cobrada_Determinacion/cuentas_prioridad.htm";
		$rq_eli="SELECT * FROM cob_cuentas_prioridad WHERE 1"; 
		
        $bloq = array(
                    "post"  => empty($_POST)?array():array($_POST),
                    "blkCo" => $rq_eli
                );
        return $bloq;	
	}

	function _mod_cuentas_prioridad(){
		
			$this->tpl="gestionescobranza/Cuenta_Cobrada_Determinacion/mod_cuentas_prioridad.htm";
		$sql_mod="SELECT * FROM cob_cuentas_prioridad where nivel='".$_GET['nivel']."' ORDER BY nivel LIMIT 1"; 
			 $bloq = array(
                    "post"  => empty($_POST)?array():array($_POST),
					"blkCo" => $sql_mod
                );
        return $bloq;	
			
       
	}

	function _modificar_cuentas_prioridad(){
		
		//// Envio de datos ////
		if(!empty($_POST)){
			
			
			
			$sql_nuevocorreo="UPDATE cob_cuentas_prioridad SET numero_cuentas = '".$_POST['numero_cuentas']."' , cumplimiento = '".$_POST['cumplimiento']."' ,  importe = '".$_POST['importe']."' , sancion_jefe = '".$_POST['sancion_jefe']."' , sancion_subgerente = '".$_POST['sancion_subgerente']."' , sancion_gerente = '".$_POST['sancion_gerente']."'  WHERE nivel = '".$_POST['nivel']."'";
			mysql_query($sql_nuevocorreo)  or die(mysql_error());
			
			header("location: inicio.php?mod=bgc.configura&ac=cuentas_prioridad");
			
			}
				
	}

	function _rep_prioridad_res(){
	
	$this->tpl = "gestionescobranza/Cuenta_Cobrada_Determinacion/reporte.prires.htm";
		//$this->tpl = "gestionescobranza/reporte.negativa_disenio.html";
		
		
		if($_POST['nivel'] != ""){
			$where.=" and universo.nivel = '".$_POST['nivel']."'";
		}
		if($_POST['agente'] != ""){
			$where.=" and universo.gestor = '".$_POST['agente']."'";
		}
		if($_POST['cuenta'] != ""){
			$where.=" and universo.cuenta = '".$_POST['cuenta']."'";
		}
		
		$fecha1="select quincena_cobranza1(".$_POST['quincena'].",".$_POST['ejercicio'].") as fecha";
		$res=mysql_fetch_assoc(mysql_query($fecha1));
		$desde=$res['fecha'];
		$fecha2="select quincena_cobranza2(".$_POST['quincena'].",".$_POST['ejercicio'].") as fecha";
		$res=mysql_fetch_assoc(mysql_query($fecha2));
		$hasta=$res['fecha'];
		
		$sql="CREATE TEMPORARY TABLE IF NOT EXISTS tmp_comentarios_gestiones as
			SELECT cuenta,max(dv) as dv,max(di) as di,nomina,quinsena,ejercicio
  		FROM comentarios_gestiones
    where
        prioridad_visita = 'on' and quinsena = ".$_POST['quincena']."
            and ejercicio = ".$_POST['ejercicio']." 
		  group by cuenta,quinsena,ejercicio
		";
		
		mysql_query($sql) ;
		
		
		
		$sql="CREATE TEMPORARY TABLE tmp_cob_fichas_procesos_estcuenta AS 
		
				SELECT 
						A . *,res.nombre
					FROM
						bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta A
					left JOIN (SELECT 
						MAX(replace(replace(substring(IDPROCESO, (select length(IDPROCESO) - 18 + 1), 18), '/', ''), ':', '')) AS ULTIMO_REG,
							idcliente
					FROM
						bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta pc
					left join cob_resultados cr ON cr.id = pc.proceso
					group by idcliente) B ON B.idcliente = A.IDCLIENTE
						AND B.ULTIMO_REG = (SELECT replace(replace(substring(A.IDPROCESO, (SELECT length(A.IDPROCESO) - 18 + 1), 18), '/', ''), ':', ''))
					left join bitacora_gestioncobranza_intelisis.catalogo_resultado res on res.id=A.proceso
					group by idcliente";
		
		mysql_query($sql) ;
		
		
		
		$sql="CREATE TEMPORARY TABLE tmp_abonos_497d as
			SELECT 
					cliente, round(sum(cobros),2) as importecobro
				FROM
					bitacora_gestioncobranza_intelisis.abonos_497d abonos
				inner join bitacora_gestioncobranza_intelisis.cob_importe_minimo im ON im.id_tipo = 'seguimiento'
					and abonos.cobros >= im.monto
				where
			 fecha_emision between '".$desde."' and '".$hasta."' group by cliente";
		
		mysql_query($sql) ;
		
		
		$sql=" CREATE TEMPORARY TABLE IF NOT EXISTS reporte_pri_res as 
		
select universo.*,count(importecobro) as cuentasconcobro,count(proceso) as cuentasconproceso,universo.cuentastotales-universo.cuentascumplidas as cuentasincumplidas,
		(universo.cuentastotales-universo.cuentascumplidas)*pri.importe as importedescuentogestor,
		case when estatus_b.EDO = 'V' then 'ACTIVO' else 'BAJA' end as estatusjefe,
		case when bb.Nomina != '' then bb.Nomina else 'NO EXISTE' end as Jefe_gestor,
		case when estatus_c.EDO = 'V' then 'ACTIVO' else 'BAJA' end as estatussubgerente,
		case when cc.Nomina != '' then cc.Nomina else 'NO EXISTE' end as Sub_Gerente,
		case when estatus_d.EDO = 'V' then 'ACTIVO' else 'BAJA' end as estatusgerente,
	    case when dd.Nomina != '' then dd.Nomina else 'NO EXISTE' end as Gerente,
		round((((universo.cuentastotales-universo.cuentascumplidas)*pri.importe)/100)*pri.sancion_jefe,2) as importedescuentojefe,
		round((((universo.cuentastotales-universo.cuentascumplidas)*pri.importe)/100)*pri.sancion_subgerente,2) as importedescuentosubgerente,
		round((((universo.cuentastotales-universo.cuentascumplidas)*pri.importe)/100)*pri.sancion_gerente,2) as importedescuentogerente,
		round(((universo.cuentastotales-universo.cuentascumplidas)*pri.importe)+((((universo.cuentastotales-universo.cuentascumplidas)*pri.importe)/100)*pri.sancion_jefe)+((((universo.cuentastotales-universo.cuentascumplidas)*pri.importe)/100)*pri.sancion_subgerente)+((((universo.cuentastotales-universo.cuentascumplidas)*pri.importe)/100)*pri.sancion_gerente),2) as importetotal

from(
SELECT 
    universo.cuenta,
    universo.nomina as gestor,
	universo.dv,
	universo.di,
    TRIM(substring_index(MID(nivel.es, 5, 100), '0', 1)) as nivel,
    agente.nombre,
	cobros.importecobro,
	case when cobros.importecobro is not null then null else procesos.nombre end as proceso,
	case when cumplidas.cuentascumplidas is null then '0' else cumplidas.cuentascumplidas end as cuentascumplidas,
	case when totales.cuentastotales is null then '0' else totales.cuentastotales end as cuentastotales,
	case when cobros.importecobro!='' or  procesos.nombre!='' then 'SI' else 'NO' end as cumplimientosino,
	round((100*(case when cumplidas.cuentascumplidas is null then '0' else cumplidas.cuentascumplidas end))/(case when totales.cuentastotales is null then '0' else totales.cuentastotales end),2) as porcentajecumplidas,
	prioridad.cumplimiento,prioridad.sancion_jefe,prioridad.sancion_subgerente,prioridad.sancion_gerente

FROM
    (SELECT cuenta,max(dv) as dv,max(di) as di,nomina,quinsena,ejercicio
  		FROM comentarios_gestiones
    where
        prioridad_visita = 'on' and quinsena = ".$_POST['quincena']."
            and ejercicio = ".$_POST['ejercicio']." 
		  group by cuenta,quinsena,ejercicio) universo
left join
    bitacora_gestioncobranza_intelisis.agentes nivel ON nivel.nomina = universo.nomina
        and nivel.nomina = nivel.agente
left join
    varios.empleados_rh201_intelisis agente ON agente.clave = universo.nomina

left join(SELECT 
        cliente, round(sum(cobros),2) as importecobro
    FROM
        bitacora_gestioncobranza_intelisis.abonos_497d abonos
    inner join bitacora_gestioncobranza_intelisis.cob_importe_minimo im ON im.id_tipo = 'seguimiento'
        and abonos.cobros >= im.monto
    where
 fecha_emision between '".$desde."' and '".$hasta."' group by cliente) cobros on cobros.cliente=universo.cuenta


left join(
select a.gestor,count(a.cumplimientosino) as cuentascumplidas from(
SELECT 
    universo.nomina as gestor,
	case when cobros.importecobro!='' or  procesos.nombre!='' then 'SI' else 'NO' end as cumplimientosino
FROM
    (SELECT cuenta,max(dv) as dv,max(di) as di,nomina,quinsena,ejercicio
  		FROM comentarios_gestiones
    where
        prioridad_visita = 'on' and quinsena = ".$_POST['quincena']."
            and ejercicio = ".$_POST['ejercicio']." 
		  group by cuenta,quinsena,ejercicio) universo
left join
    bitacora_gestioncobranza_intelisis.agentes nivel ON nivel.nomina = universo.nomina
        and nivel.nomina = nivel.agente
left join
    varios.empleados_rh201_intelisis agente ON agente.clave = universo.nomina
left join(SELECT 
        cliente, round(sum(cobros),2) as importecobro
    FROM
        bitacora_gestioncobranza_intelisis.abonos_497d abonos
    inner join bitacora_gestioncobranza_intelisis.cob_importe_minimo im ON im.id_tipo = 'seguimiento'
        and abonos.cobros >= im.monto
    where
 fecha_emision between '".$desde."' and '".$hasta."' group by cliente) cobros on cobros.cliente=universo.cuenta
left join
		(SELECT 
    cuenta, saldocapital,
min(replace(fecha_inicio,'-','')) as id
FROM
    bitacora_gestioncobranza_intelisis.registros
group by cuenta) saldo on saldo.cuenta=universo.cuenta
	left join(

select * from(
SELECT 
        A . *,res.nombre
    FROM
        bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta A
    left JOIN (SELECT 
        MAX(replace(replace(substring(IDPROCESO, (select length(IDPROCESO) - 18 + 1), 18), '/', ''), ':', '')) AS ULTIMO_REG,
            idcliente
    FROM
        bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta pc
    left join cob_resultados cr ON cr.id = pc.proceso
    group by idcliente) B ON B.idcliente = A.IDCLIENTE
        AND B.ULTIMO_REG = (SELECT replace(replace(substring(A.IDPROCESO, (SELECT length(A.IDPROCESO) - 18 + 1), 18), '/', ''), ':', ''))
	left join bitacora_gestioncobranza_intelisis.catalogo_resultado res on res.id=A.proceso
    group by idcliente)a where a.estatus='101')procesos on procesos.idcliente=universo.cuenta 

inner join bitacora_gestioncobranza_intelisis.cob_cuentas_prioridad prioridad on TRIM(substring_index(MID(nivel.es, 5, 100), '0', 1))=prioridad.nivel)a 
where a.cumplimientosino='SI' group by a.gestor
	)cumplidas on cumplidas.gestor=universo.nomina


left join(
select * from(
SELECT 
        A . *,res.nombre
    FROM
        bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta A
    left JOIN (SELECT 
        MAX(replace(replace(substring(IDPROCESO, (select length(IDPROCESO) - 18 + 1), 18), '/', ''), ':', '')) AS ULTIMO_REG,
            idcliente
    FROM
        bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta pc
    left join cob_resultados cr ON cr.id = pc.proceso
    group by idcliente) B ON B.idcliente = A.IDCLIENTE
        AND B.ULTIMO_REG = (SELECT replace(replace(substring(A.IDPROCESO, (SELECT length(A.IDPROCESO) - 18 + 1), 18), '/', ''), ':', ''))
	left join bitacora_gestioncobranza_intelisis.catalogo_resultado res on res.id=A.proceso
    group by idcliente)a where a.estatus='101' )procesos on procesos.idcliente=universo.cuenta 


left join(select a.gestor,count(a.cuenta) as cuentastotales from(
SELECT 
    universo.cuenta,
    universo.nomina as gestor,
	universo.dv,
	universo.di,
    universo.quinsena,
    universo.ejercicio,
    TRIM(substring_index(MID(nivel.es, 5, 100), '0', 1)) as nivel,
    agente.nombre,
	saldo.saldocapital,
	cobros.importecobro,
	procesos.nombre as proceso,
	case when cobros.importecobro!='' or  procesos.nombre!='' then 'SI' else 'NO' end as cumplimientosino
	#cuentas.cuentasgestor

	#cuentas.porcentajecumplidas

FROM
    (SELECT cuenta,max(dv) as dv,max(di) as di,nomina,quinsena,ejercicio
  		FROM comentarios_gestiones
    where
        prioridad_visita = 'on' and quinsena = ".$_POST['quincena']."
            and ejercicio = ".$_POST['ejercicio']." 
		  group by cuenta,quinsena,ejercicio) universo
left join
    bitacora_gestioncobranza_intelisis.agentes nivel ON nivel.nomina = universo.nomina
        and nivel.nomina = nivel.agente
left join
    varios.empleados_rh201_intelisis agente ON agente.clave = universo.nomina
left join(SELECT 
        cliente, round(sum(cobros),2) as importecobro
    FROM
        bitacora_gestioncobranza_intelisis.abonos_497d abonos
    inner join bitacora_gestioncobranza_intelisis.cob_importe_minimo im ON im.id_tipo = 'seguimiento'
        and abonos.cobros >= im.monto
    where
 fecha_emision between '".$desde."' and '".$hasta."' group by cliente) cobros on cobros.cliente=universo.cuenta
left join
		(SELECT 
    cuenta, saldocapital,
min(replace(fecha_inicio,'-','')) as id
FROM
    bitacora_gestioncobranza_intelisis.registros
group by cuenta) saldo on saldo.cuenta=universo.cuenta
	left join(

select * from(
SELECT 
        A . *,res.nombre
    FROM
        bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta A
    left JOIN (SELECT 
        MAX(replace(replace(substring(IDPROCESO, (select length(IDPROCESO) - 18 + 1), 18), '/', ''), ':', '')) AS ULTIMO_REG,
            idcliente
    FROM
        bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta pc
    left join cob_resultados cr ON cr.id = pc.proceso
    group by idcliente) B ON B.idcliente = A.IDCLIENTE
        AND B.ULTIMO_REG = (SELECT replace(replace(substring(A.IDPROCESO, (SELECT length(A.IDPROCESO) - 18 + 1), 18), '/', ''), ':', ''))
	left join bitacora_gestioncobranza_intelisis.catalogo_resultado res on res.id=A.proceso
    group by idcliente)a where a.estatus='101')procesos on procesos.idcliente=universo.cuenta 

inner join bitacora_gestioncobranza_intelisis.cob_cuentas_prioridad prioridad on TRIM(substring_index(MID(nivel.es, 5, 100), '0', 1))=prioridad.nivel)a 
group by a.gestor)totales on totales.gestor=universo.nomina 

inner join bitacora_gestioncobranza_intelisis.cob_cuentas_prioridad prioridad on TRIM(substring_index(MID(nivel.es, 5, 100), '0', 1))=prioridad.nivel

)universo 

left join bitacora_gestioncobranza_intelisis.cob_cuentas_prioridad pri on universo.porcentajecumplidas<pri.cumplimiento and universo.nivel=pri.nivel and universo.cumplimientosino='NO'
left join bitacora_gestioncobranza_intelisis.agentes aa ON aa.agente = universo.gestor
    left join bitacora_gestioncobranza_intelisis.agentes bb ON aa.celula = bb.agente
    left join bitacora_gestioncobranza_intelisis.agentes cc ON aa.division = cc.agente
    left join bitacora_gestioncobranza_intelisis.agentes dd ON aa.gerencia = dd.agente
	left join varios.empleados_rh201_intelisis estatus_a ON aa.nomina = estatus_a.CLAVE
    left join varios.empleados_rh201_intelisis estatus_b ON bb.nomina = estatus_b.CLAVE
    left join varios.empleados_rh201_intelisis estatus_c ON cc.nomina = estatus_c.CLAVE
    left join varios.empleados_rh201_intelisis estatus_d ON dd.nomina = estatus_d.CLAVE
where 1
".$where."

group by universo.gestor";

//echo $sql;
	mysql_query($sql);
	
	
	
	$sql2="Select * from reporte_pri_res";
	$res = mysql_query($sql2);
	
		$sqltotal1="Select  
							round((100*(sum(a.cuentascumplidas)))/(sum(a.cuentastotales)),2) as sumporcentajecumplidas,
							sum(a.cuentastotales)  as sumcuentasgestor,
						 	sum(a.cuentasconcobro) as sumcuentascobro,
							sum(a.cuentasconproceso) as sumcuentasproceso ,
							sum(a.cuentascumplidas) as sumcuentascumplidas,
							sum(a.cuentasincumplidas) as sumcuentasincumplidas,
							sum(a.importedescuentogestor) as sumimportedescuentogestor,
							sum(a.importedescuentojefe) as sumimportedescuentojefe,
							sum(a.importedescuentosubgerente) as sumimportedescuentosubgerente ,
							sum(a.importedescuentogerente) as sumimportedescuentogerente,
							sum(a.importetotal) as sumtotaldescuento  
							from reporte_pri_res a"; 
							
							
		$primerdia="select DATE_FORMAT(quincena_cobranza1(".$_POST['quincena'].",".$_POST['ejercicio']."),'%Y %b %e')  		AS pd";	
		$segundodia="select DATE_FORMAT(quincena_cobranza2(".$_POST['quincena'].",".$_POST['ejercicio']."),'%Y %b %e')
		AS sd";
	
	if (mysql_num_rows($res) != 0) {
    				$this->tpl = "gestionescobranza/Cuenta_Cobrada_Determinacion/rep_prioridad_res.htm";
					
					}
					
	$bloq = array(
                    "post"  => empty($_POST)?array():array($_POST),
					"blk2" => (!empty($_POST))?$sql2:array($_POST),
					"blk3" => (!empty($_POST))?$sqltotal1:array($_POST),
					"primer" => (!empty($_POST))?$primerdia:array($_POST),
					"segundo" => (!empty($_POST))?$segundodia:array($_POST)
                );
        return $bloq;
	}

	function _rep_prioridad_det(){
	
	$this->tpl = "gestionescobranza/Cuenta_Cobrada_Determinacion/reporte.pridet.htm";
		//$this->tpl = "gestionescobranza/reporte.negativa_disenio.html";
		
		if($_POST['nivel'] != ""){
			$where.=" and universo.nivel = '".$_POST['nivel']."'";
		}
		if($_POST['agente'] != ""){
			$where.=" and universo.gestor = '".$_POST['agente']."'";
		}
		if($_POST['cuenta'] != ""){
			$where.=" and universo.cuenta = '".$_POST['cuenta']."'";
		}
		
		$fecha1="select quincena_cobranza1(".$_POST['quincena'].",".$_POST['ejercicio'].") as fecha";
		$res=mysql_fetch_assoc(mysql_query($fecha1));
		$desde=$res['fecha'];
		$fecha2="select quincena_cobranza2(".$_POST['quincena'].",".$_POST['ejercicio'].") as fecha";
		$res=mysql_fetch_assoc(mysql_query($fecha2));
		$hasta=$res['fecha'];
		
		
		
	$sql="CREATE TEMPORARY TABLE IF NOT EXISTS reporte_pri_det as 
select universo.*,
		pri.importe,
		pri.sancion_jefe,
		pri.sancion_subgerente,
		pri.sancion_gerente,
		case when estatus_b.EDO = 'V' then 'ACTIVO' else 'BAJA' end as estatusjefe,
		case when bb.Nomina != '' then bb.Nomina else 'NO EXISTE' end as Jefe_gestor,
		case when estatus_c.EDO = 'V' then 'ACTIVO' else 'BAJA' end as estatussubgerente,
		case when cc.Nomina != '' then cc.Nomina else 'NO EXISTE' end as Sub_Gerente,
		case when estatus_d.EDO = 'V' then 'ACTIVO' else 'BAJA' end as estatusgerente,
	    case when dd.Nomina != '' then dd.Nomina else 'NO EXISTE' end as Gerente,
		round((pri.importe/100)*pri.sancion_jefe,2) as importedescuentojefe,
		round((pri.importe/100)*pri.sancion_subgerente,2) as importedescuentosubgerente,
		round((pri.importe/100)*pri.sancion_gerente,2) as importedescuentogerente,
		round(pri.importe+((pri.importe/100)*pri.sancion_jefe)+((pri.importe/100)*pri.sancion_subgerente)+((pri.importe/100)*pri.sancion_gerente),2) as importetotal
 from(
SELECT 
    universo.cuenta,
    universo.nomina as gestor,
	universo.dv,
	universo.di,
    TRIM(substring_index(MID(nivel.es, 5, 100), '0', 1)) as nivel,
    agente.nombre,
	saldo.saldocapital,
	cobros.importecobro,
	procesos.nombre as proceso,
	case when cobros.importecobro!='' or  procesos.nombre!='' then 'SI' else 'NO' end as cumplimientosino,
	round((100*(case when cumplidas.cuentascumplidas is null then '0' else cumplidas.cuentascumplidas end))/(case when totales.cuentastotales is null then '0' else totales.cuentastotales end),2) as porcentajecumplidas

FROM
    (SELECT cuenta,max(dv) as dv,max(di) as di,nomina,quinsena,ejercicio
  		FROM comentarios_gestiones
    where
        prioridad_visita = 'on' and quinsena = ".$_POST['quincena']."
            and ejercicio = ".$_POST['ejercicio']." 
		  group by cuenta,quinsena,ejercicio) universo
left join
    bitacora_gestioncobranza_intelisis.agentes nivel ON nivel.nomina = universo.nomina
        and nivel.nomina = nivel.agente
left join
    varios.empleados_rh201_intelisis agente ON agente.clave = universo.nomina


left join(SELECT 
        cliente, round(sum(cobros),2) as importecobro
    FROM
        bitacora_gestioncobranza_intelisis.abonos_497d abonos
    inner join bitacora_gestioncobranza_intelisis.cob_importe_minimo im ON im.id_tipo = 'seguimiento'
        and abonos.cobros >= im.monto
    where
 fecha_emision between '".$desde."' and '".$hasta."' group by cliente) cobros on cobros.cliente=universo.cuenta



left join
		(SELECT 
    cuenta, saldocapital,
min(replace(fecha_inicio,'-','')) as id
FROM
    bitacora_gestioncobranza_intelisis.registros
group by cuenta) saldo on saldo.cuenta=universo.cuenta



	left join(
select * from(
SELECT 
        A . *,res.nombre
    FROM
        bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta A
    left JOIN (SELECT 
        MAX(replace(replace(substring(IDPROCESO, (select length(IDPROCESO) - 18 + 1), 18), '/', ''), ':', '')) AS ULTIMO_REG,
            idcliente
    FROM
        bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta pc
    left join cob_resultados cr ON cr.id = pc.proceso
    group by idcliente) B ON B.idcliente = A.IDCLIENTE
        
        AND B.ULTIMO_REG = (SELECT replace(replace(substring(A.IDPROCESO, (SELECT length(A.IDPROCESO) - 18 + 1), 18), '/', ''), ':', ''))
	left join bitacora_gestioncobranza_intelisis.catalogo_resultado res on res.id=A.proceso
    group by idcliente)a where a.estatus='101')procesos on procesos.idcliente=universo.cuenta 

	left join(
select a.gestor,count(a.cumplimientosino) as cuentascumplidas from(
SELECT 
    universo.nomina as gestor,
	case when cobros.importecobro!='' or  procesos.nombre!='' then 'SI' else 'NO' end as cumplimientosino
FROM
    (SELECT cuenta,max(dv) as dv,max(di) as di,nomina,quinsena,ejercicio
  		FROM comentarios_gestiones
    where
        prioridad_visita = 'on' and quinsena = ".$_POST['quincena']."
            and ejercicio = ".$_POST['ejercicio']." 
		  group by cuenta,quinsena,ejercicio) universo
left join
    bitacora_gestioncobranza_intelisis.agentes nivel ON nivel.nomina = universo.nomina
        and nivel.nomina = nivel.agente
left join
    varios.empleados_rh201_intelisis agente ON agente.clave = universo.nomina
left join(SELECT 
        cliente, round(sum(cobros),2) as importecobro
    FROM
        bitacora_gestioncobranza_intelisis.abonos_497d abonos
    inner join bitacora_gestioncobranza_intelisis.cob_importe_minimo im ON im.id_tipo = 'seguimiento'
        and abonos.cobros >= im.monto
    where
 fecha_emision between '".$desde."' and '".$hasta."' group by cliente) cobros on cobros.cliente=universo.cuenta
left join
		(SELECT 
    cuenta, saldocapital,
min(replace(fecha_inicio,'-','')) as id
FROM
    bitacora_gestioncobranza_intelisis.registros
group by cuenta) saldo on saldo.cuenta=universo.cuenta
	left join(
select * from(
SELECT 
        A . *,res.nombre
    FROM
        bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta A
    left JOIN (SELECT 
        MAX(replace(replace(substring(IDPROCESO, (select length(IDPROCESO) - 18 + 1), 18), '/', ''), ':', '')) AS ULTIMO_REG,
            idcliente
    FROM
        bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta pc
    left join cob_resultados cr ON cr.id = pc.proceso
    group by idcliente) B ON B.idcliente = A.IDCLIENTE
        AND B.ULTIMO_REG = (SELECT replace(replace(substring(A.IDPROCESO, (SELECT length(A.IDPROCESO) - 18 + 1), 18), '/', ''), ':', ''))
	left join bitacora_gestioncobranza_intelisis.catalogo_resultado res on res.id=A.proceso
    group by idcliente)a where a.estatus='101')procesos on procesos.idcliente=universo.cuenta 

inner join bitacora_gestioncobranza_intelisis.cob_cuentas_prioridad prioridad on TRIM(substring_index(MID(nivel.es, 5, 100), '0', 1))=prioridad.nivel)a 
where a.cumplimientosino='SI' group by a.gestor
	)cumplidas on cumplidas.gestor=universo.nomina



left join(select a.gestor,count(a.cuenta) as cuentastotales from(
SELECT 
    universo.cuenta,
    universo.nomina as gestor,
	universo.dv,
	universo.di,
    universo.quinsena,
    universo.ejercicio,
    TRIM(substring_index(MID(nivel.es, 5, 100), '0', 1)) as nivel,
    agente.nombre,
	saldo.saldocapital,
	cobros.importecobro,
	procesos.nombre as proceso,
	case when cobros.importecobro!='' or  procesos.nombre!='' then 'SI' else 'NO' end as cumplimientosino
	#cuentas.cuentasgestor

	#cuentas.porcentajecumplidas

FROM
    (SELECT cuenta,max(dv) as dv,max(di) as di,nomina,quinsena,ejercicio
  		FROM comentarios_gestiones
    where
        prioridad_visita = 'on' and quinsena = ".$_POST['quincena']."
            and ejercicio = ".$_POST['ejercicio']." 
		  group by cuenta,quinsena,ejercicio ) universo
left join
    bitacora_gestioncobranza_intelisis.agentes nivel ON nivel.nomina = universo.nomina
        and nivel.nomina = nivel.agente
left join
    varios.empleados_rh201_intelisis agente ON agente.clave = universo.nomina
left join(SELECT 
        cliente, round(sum(cobros),2) as importecobro
    FROM
        bitacora_gestioncobranza_intelisis.abonos_497d abonos
    inner join bitacora_gestioncobranza_intelisis.cob_importe_minimo im ON im.id_tipo = 'seguimiento'
        and abonos.cobros >= im.monto
    where
 fecha_emision between '".$desde."' and '".$hasta."' group by cliente) cobros on cobros.cliente=universo.cuenta
left join
		(SELECT 
    cuenta, saldocapital,
min(replace(fecha_inicio,'-','')) as id
FROM
    bitacora_gestioncobranza_intelisis.registros
group by cuenta) saldo on saldo.cuenta=universo.cuenta
	left join(
select * from(
SELECT 
        A . *,res.nombre
    FROM
        bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta A
    left JOIN (SELECT 
        MAX(replace(replace(substring(IDPROCESO, (select length(IDPROCESO) - 18 + 1), 18), '/', ''), ':', '')) AS ULTIMO_REG,
            idcliente
    FROM
        bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta pc
    left join cob_resultados cr ON cr.id = pc.proceso
    group by idcliente) B ON B.idcliente = A.IDCLIENTE
        AND B.ULTIMO_REG = (SELECT replace(replace(substring(A.IDPROCESO, (SELECT length(A.IDPROCESO) - 18 + 1), 18), '/', ''), ':', ''))
	left join bitacora_gestioncobranza_intelisis.catalogo_resultado res on res.id=A.proceso
    group by idcliente)a where a.estatus='101')procesos on procesos.idcliente=universo.cuenta 

inner join bitacora_gestioncobranza_intelisis.cob_cuentas_prioridad prioridad on TRIM(substring_index(MID(nivel.es, 5, 100), '0', 1))=prioridad.nivel)a 
group by a.gestor)totales on totales.gestor=universo.nomina

	inner join bitacora_gestioncobranza_intelisis.cob_cuentas_prioridad prioridad on TRIM(substring_index(MID(nivel.es, 5, 100), '0', 1))=prioridad.nivel)universo
	left join bitacora_gestioncobranza_intelisis.cob_cuentas_prioridad pri on universo.porcentajecumplidas<pri.cumplimiento and universo.nivel=pri.nivel and universo.cumplimientosino='NO'
	left join bitacora_gestioncobranza_intelisis.agentes aa ON aa.agente = universo.gestor
    left join bitacora_gestioncobranza_intelisis.agentes bb ON aa.celula = bb.agente
    left join bitacora_gestioncobranza_intelisis.agentes cc ON aa.division = cc.agente
    left join bitacora_gestioncobranza_intelisis.agentes dd ON aa.gerencia = dd.agente
	left join varios.empleados_rh201_intelisis estatus_a ON aa.nomina = estatus_a.CLAVE
    left join varios.empleados_rh201_intelisis estatus_b ON bb.nomina = estatus_b.CLAVE
    left join varios.empleados_rh201_intelisis estatus_c ON cc.nomina = estatus_c.CLAVE
    left join varios.empleados_rh201_intelisis estatus_d ON dd.nomina = estatus_d.CLAVE
	
	where 1
".$where." order by cuenta";
mysql_query($sql);


	
	$sql2="Select * from reporte_pri_det";
	$res = mysql_query($sql2);
	
		$sqltotal1="Select  
	count(a.cuenta) as contarcuenta,
	sum(a.importecobro) as sumimportecobro,
	count(a.proceso) as contarproceso,
	count(a.cumplimientosino) as contarcumplimiento,
	sum(a.importe) as sumimportegestor,
	sum(a.importedescuentojefe) as sumimportejefe,
	sum(a.importedescuentosubgerente) as sumimportesubgerente,
	sum(a.importedescuentogerente) as sumimportegerente,
	sum(a.importetotal) as sumimportetotal

							from reporte_pri_det a"; 
							
		$sqlcum="select count(a.cumplimientosino) as cumplimientosino from reporte_pri_det a where a.cumplimientosino='SI'";
		
		
		$primerdia="select DATE_FORMAT(quincena_cobranza1(".$_POST['quincena'].",".$_POST['ejercicio']."),'%Y %b %e')  		AS pd";	
		$segundodia="select DATE_FORMAT(quincena_cobranza2(".$_POST['quincena'].",".$_POST['ejercicio']."),'%Y %b %e')
		AS sd";
	
	
	if (mysql_num_rows($res) != 0) {
    				$this->tpl = "gestionescobranza/Cuenta_Cobrada_Determinacion/rep_prioridad_det.htm";
					
					}
					
	$bloq = array(
                    "post"  => empty($_POST)?array():array($_POST),
					"blk2" => (!empty($_POST))?$sql2:array($_POST),
					"blk3" => (!empty($_POST))?$sqltotal1:array($_POST),
					"blk4" => (!empty($_POST))?$sqlcum:array($_POST),
					"primer" => (!empty($_POST))?$primerdia:array($_POST),
					"segundo" => (!empty($_POST))?$segundodia:array($_POST)
                );
        return $bloq;
	}

    function FormatoFecha($Fecha){
		$FechaFormato=split("/",$Fecha);
		switch($FechaFormato[1]) {
			case '01':
				$Mes="ENERO";
			break;
			case '02':
				$Mes="FEBRERO";
			break;
			case '03':
				$Mes="MARZO";
			break;
			case '04':
				$Mes="ABRIL";
			break;
			case '05':
				$Mes="MAYO";
			break;
			case '06':
				$Mes="JUNIO";
			break;
			case '07':
				$Mes="JULIO";
			break;
			case '08':
				$Mes="AGOSTO";
			break;
			case '09':
				$Mes="SEPTIEMBRE";
			break;
			case '10':
				$Mes="OCTUBRE";
			break;
			case '11':
				$Mes="NOVIEMBRE";
			break;
			case '12':
				$Mes="DICIEMBRE";
			break;
		}
		return $FechaFormato[2]." DE ".$Mes." DEL ".$FechaFormato[0];
	}

	public function _catalogos() {
		if( isset( $_POST['agregar'] ) ) {
			$this->tpl="bitacLocaliz/agValores.htm";
			
		} else if( isset( $_GET['editar'] ) ) {
			$this->tpl = "bitacLocaliz/modificarCatalogo.htm";
			$this->sql = "SELECT * FROM bitacora_localizacion.catalogos WHERE id = " . $_GET['id'];
			$bloque = array( 'blk' => $this->sql );
			return $bloque;

		} else if( isset( $_GET['eliminar'] ) ) {
			$this->sql = "UPDATE bitacora_localizacion.catalogos set estatus=0 WHERE id = " . $_GET['id'];
			mysql_query( $this->sql ) or $this->mysql_throw_ex();
			header( "Location: inicio.php?mod=bgc.configura&ac=catalogos&msg" );
		} else {
			$this->tpl="bitacLocaliz/catalogos.htm";
			if( isset( $_GET['msg'] ) ) 
				$this->msgaviso = "Actualizado";
			$this->sql = "SELECT * FROM bitacora_localizacion.catalogos ORDER BY campo, valor";
			$bloque = array( 'blk' => $this->sql );
			return $bloque;
		}	
	}
	/*Asignación de Nivel*/
	function _AsignaNivel()
	 {
		$this->tpl="gestionescobranza/asignaNivel.htm";
		$GLOBALS['path'] = "../template_htm/gestionescobranza";
        $GLOBALS['ev'] = "AsignaNivel"; 
		
		
		$nomina = $_POST["txtAgente"];
		$cuenta = $_POST["txtCuenta"];
		$fecha = $_REQUEST["ddlFecha"];
		$proceso = $_REQUEST["ddlProceso"];
		$estado = $_REQUEST["ddlEstado"];
		$ciudad = $_REQUEST["ddlCiudad"];
		$colonia = $_REQUEST["ddlColonia"];
		
		$GLOBALS["txtAgente"] = $nomina;
		$GLOBALS["CtaBusqueda"] = $cuenta;
		$GLOBALS["ddlFecha"] = $fecha;
		$GLOBALS["ddlProceso"] = $proceso;
		$GLOBALS["ddlEstado"] = $estado;
		$GLOBALS["ddlCiudad"] = $ciudad;
		$GLOBALS["ddlColonia"] = $colonia;
		
		 
		$where="";
		
		/*Filtros*/
		if($nomina != '')
			$where.=" AND A.AGENTE = '".$nomina."'";			
		if($cuenta != '')
			$where.= " AND A.IDCLIENTE = '".$cuenta."'";
		if($proceso != '')
			$where.= " AND A.PROCESO = '".$proceso."'";
		if($fecha != '')
			$where.= " AND A.FECHA = '".$fecha."'";
		if($estado != '')
			$where.= " AND registros.estado = '".$estado."'";
		if($ciudad != '')
			$where.= " AND registros.ciudad = '".$ciudad."'";
		if($colonia != '')
			$where.= " AND registros.colonia = '".$colonia."'";
		
		
		$res_quincena=mysql_fetch_assoc(mysql_query("select Case When Day(CURDATE()) > 17 then Month(CURDATE())*2 else (Month(CURDATE())*2)-1 end as quincena"));
		$quincena=$res_quincena["quincena"];
		
		$res_ejercicio=mysql_fetch_assoc(mysql_query("select EXTRACT(YEAR FROM CURDATE()) as ejercicio") );
		$ejercicio=$res_ejercicio["ejercicio"];
		
		$res_fecha1=mysql_fetch_assoc(mysql_query("select  bitacora_gestioncobranza_intelisis.quincena_cobranza1(".$quincena.", ".$ejercicio.") as fecha_inicio"));
		//$res_fecha1=mysql_fetch_assoc(mysql_query("select  bitacora_gestioncobranza_intelisis.quincena_cobranza1(15, ".$ejercicio.") as fecha_inicio"));
		$fecha1=$res_fecha1["fecha_inicio"];
		
		$res_fecha2=mysql_fetch_assoc(mysql_query("select  bitacora_gestioncobranza_intelisis.quincena_cobranza2(".$quincena.", ".$ejercicio.") as fecha_fin"));
		//$res_fecha2=mysql_fetch_assoc(mysql_query("select  bitacora_gestioncobranza_intelisis.quincena_cobranza2(15, ".$ejercicio.") as fecha_fin"));
		$fecha2=$res_fecha2["fecha_fin"];
		
		/*Consulta*/
		if (isset($_POST['btn_mostrar']) || isset($_GET["pg"])){
		//consulta de las cuentas
        $sql = "SELECT 
				A.FECHA,
				CASE WHEN registros.cuenta is null
				THEN
					 CASE
					 WHEN a.proceso in ('negativa','pago','recado') THEN registrosintcob.valor_resultado
					 WHEN a.proceso in ('localizac','promesa','promesaAdj') THEN registrosintcob.pago_quien
					 WHEN a.proceso in ('cobroaval') THEN 'AVAL'
					 ELSE 'N/A' 
					 END
				ELSE
					 CASE
					 WHEN a.proceso in ('negativa','pago','recado') THEN registros.valor_resultado
					 WHEN a.proceso in ('localizac','promesa','promesaAdj') THEN registros.pago_quien
					 WHEN a.proceso in ('cobroaval') THEN 'AVAL'
					 ELSE 'N/A' 
					 END				
				END AS valor_resultado,
				di.nombre,
				A.IDCLIENTE,
				A.AGENTE,
				A.PROCESO,
				dp.Nivel,
				registros.domicilio, 
				registros.estado, 
				registros.colonia, 
				registros.ciudad as poblacion,
				A.IDPROCESO
			FROM bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta A
			LEFT JOIN varios.empleados_rh201_intelisis di ON di.clave = A.AGENTE
			LEFT JOIN bitacora_gestioncobranza_intelisis.cuentas_desasig_procesonivel dp ON dp.IdProceso=A.IDPROCESO
			/*LEFT JOIN (SELECT 
							MAX(replace(replace(substring(IDPROCESO, (select length(IDPROCESO) - 18 + 1), 18), '/', ''), ':', '')) AS divide,
								idcliente
						 FROM bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta
						 group by idcliente) B ON B.idcliente = A.idcliente
						  and B.divide = (SELECT 
								          replace(replace(substring(A.IDPROCESO,
												(SELECT length(A.IDPROCESO) - 18 + 1),
												18),
											'/',
											''),
										':',
										'')
							)*/
				LEFT JOIN
				(SELECT id, cuenta, valor_resultado, comentario, pago_quien,domicilio, estado, colonia, ciudad
				 FROM bitacora_gestioncobranza_intelisis.registros
				 INNER JOIN (select MAX(id) as ultimoId
							from bitacora_gestioncobranza_intelisis.registros
							group by cuenta) ultimoRegistro ON registros.id = ultimoRegistro.ultimoId) registros ON registros.cuenta = A.IDCLIENTE
				LEFT JOIN
				(SELECT id, cuenta, valor_resultado, comentario, pago_quien
				 FROM bitacora_gestioncobranza_intcob.registros
				  INNER JOIN (select MAX(id) as ultimoId
							  from bitacora_gestioncobranza_intcob.registros
							   group by cuenta) ultimoRegistro ON registros.id = ultimoRegistro.ultimoId) registrosintcob ON registrosintcob.cuenta = A.IDCLIENTE
			WHERE
				A.estatus = 101 and fecha between '".$fecha1."' and '".$fecha2."' ".$where."
				/*AND EXISTS(SELECT IdProceso FROM bitacora_gestioncobranza_intelisis.cuentas_desasig_procesonivel dp WHERE dp.IdProceso=A.IDPROCESO)*/
			ORDER BY fecha DESC , agente";
			
			//echo $sql;
		}
		else{
			$sql=" SELECT '' AS FECHA,
				'' as valor_resultado,
				'' AS nombre,
				'' as IDCLIENTE,
				'' as AGENTE,
				'' as PROCESO,
				'' as Nivel,
				'' as domicilio,
				'' as colonia,
				'' as poblacion,
				'' as estado,
				'' as IDPROCESO";
		}
		
		//fechas
		$sqlFechas="SELECT 
						SUBSTRING(SUBSTRING(IDPROCESO, - 18),
							1,
							10) as fecha_inicio
					FROM
						bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta A
							INNER JOIN
						(SELECT 
							MAX(replace(replace(substring(IDPROCESO, (select length(IDPROCESO) - 18 + 1), 18), '/', ''), ':', '')) AS divide,
								idcliente
						FROM
							bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta
						group by idcliente) B ON B.idcliente = A.idcliente
							and B.divide = (SELECT 
								replace(replace(substring(A.IDPROCESO,
												(SELECT length(A.IDPROCESO) - 18 + 1),
												18),
											'/',
											''),
										':',
										'')
							)
					WHERE
						A.estatus = 101
					GROUP BY fecha_inicio
					ORDER BY fecha_inicio DESC";	
					
		//Si es el botón de guardar >_<
		if(isset($_POST["btn_guardar"]))
		{
			/*Recorremos cada check e insertamos a la tabla */
			$CtasCheck = $_POST["checkProc"];
			foreach($CtasCheck as $valor)
			{
				$sqlInsert="INSERT INTO cuentas_desasig_procesonivel (IdProceso, Nivel, FechaRegistro, Usuario)
						VALUES ('".$valor."', '".$_POST["NivelSel"]."', NOW(), '".$_SESSION["nomina"]."')";
				mysql_query($sqlInsert) or die(mysql_error());
			}
		}
		//$this->dts = 30;
		//bloque D:
        $bloque = array(
					'blkFechas' => $sqlFechas,
					'blk' => $sql
				  );
        return $bloque;
		
	 }
	/*Configuración de Proceso-Nivel*/
	function _ConfigProcesoNivel()
	 {
		$this->tpl="gestionescobranza/configProcesoNivel.htm";
		 
		$GLOBALS['path'] = "../template_htm/gestionescobranza";
        $GLOBALS['ev'] = "ConfigProcesoNivel"; 
		
		//Si se le dio click al botón de guardar: //		
		if(isset($_POST["btn_guardar"])){
			 /*-----------------------------------------------------------------------------------*/
			 //Si existe un archivo .csv			 
			  if(is_uploaded_file($_FILES['archivo']['tmp_name'])){
				$ruta= pathinfo($_FILES['archivo']['name']);		
				
				if($ruta['extension'] == 'csv')
				{
					$sql = "UPDATE relacion_proceso_nivel SET Estatus=0";
					$rq = mysql_query($sql);
					
					$sql = "LOAD DATA LOCAL INFILE '".str_replace("\\", "/", $_FILES['archivo']['tmp_name'])."'
                            INTO TABLE relacion_proceso_nivel
                            FIELDS TERMINATED BY ','
                            OPTIONALLY ENCLOSED BY '".chr(34)."'
                            LINES TERMINATED BY '\\r\\n'
							(Proceso, Nivel)";
                    echo $sql;
					$rq = mysql_query($sql);
				}
			  }
			 /*-----------------------------------------------------------------------------------------*/
			else
			{
				$sqlSave="INSERT INTO relacion_proceso_nivel (Proceso, Nivel, FechaCaptura, Estatus)
								VALUES('".$_POST['ddlProceso']."', '".$_POST['ddlNivel']."', NOW(), 1)";
				mysql_query($sqlSave) or die(mysql_error());
			}
		}
		 
		 //Si le dio click al botón de eliminar
		 if(isset($_GET["id"]))
		 {
			$sqlDel="UPDATE relacion_proceso_nivel SET Estatus=0 WHERE Id='".$_GET['id']."' ";
			mysql_query($sqlDel) or die(mysql_error());
		 }
		 
		 //Niveles Cobranza
		$sqlNiveles="SELECT Nombre as Nivel
					FROM niveles_especiales_cob_mavi ORDER BY Nombre ASC";
		//Tabla Relación Proceso-Nivel
		$sqlNivProc = "SELECT Id, Proceso, Nivel FROM relacion_proceso_nivel WHERE Estatus=1 ORDER BY Proceso, Nivel ASC";
		
		//bloque D:*/
        $bloque = array( 					
					'blkNivel' => $sqlNiveles,
					'blk' => $sqlNivProc
				  );
        return $bloque;
	 }

    function _rep_jefatura(){
	
	//	$this->tpl = "gestionescobranza/Cuenta_Cobrada_Determinacion/reporte.jefe.htm";
		
		
		} 

	function _RepInvLoc(){
		
	}

	function _ReporteRevisionCarteraCliente(){
		$this->tpl = "gestionescobranza/reporte_revision_cartera_cliente.html";
            $bloque = array(
                    "blkPost" => empty($_POST)?array():$_POST,
                    "blkNivCob" => "SELECT DISTINCT nivel_cobranza AS id, nivel_cobranza AS nombre FROM catalogo_promotores WHERE nivel_cobranza <> '' ORDER BY nombre"
            );
			date_default_timezone_set("America/Mexico_City");
            if(!empty($_POST)){
                    $rq = mysql_query("SELECT * FROM config ORDER BY id DESC LIMIT 1");
                    $config = mysql_fetch_assoc($rq);
            
                    $cuenta = $_POST['cuenta'];

                    $CambiarIdioma="SET lc_time_names = 'es_MX'";
                    mysql_query($CambiarIdioma);
                    $fecha1="select quincena_cobranza1(".$_POST['quincena'].",".$_POST['ejercicio'].") as fecha";
                    $res=mysql_fetch_assoc(mysql_query($fecha1));
                    $desde=$res['fecha'];
                    $fecha2="select quincena_cobranza2(".$_POST['quincena'].",".$_POST['ejercicio'].") as fecha";
                    $res=mysql_fetch_assoc(mysql_query($fecha2));
                    $hasta=$res['fecha'];

                    $fecha_inicio = str_replace('/', '-', $desde);
                    $fecha_fin = str_replace('/', '-', $hasta);
            
                    if($_POST['quincena'] != ''){
                            $fecha = " AND ec.ultima_revision <='".$fecha_fin."' ";
                    }
                    $cliente = "SELECT 
										*
									FROM
										(select 
											cuenta,
												nombre,
												domicilio,
												ruta,
												druta,
												CASE
													WHEN
														(intelisis IS NULL
															OR intelisis = 'SIN AGE'
															OR intelisis = ' ')
													THEN
														promotor
													ELSE intelisis
												END as promo,
												fecha_inicio
										from
											registros
										where
											cuenta =  '".$cuenta."'  UNION ALL select 
											cuenta,
												nombre,
												domicilio,
												ruta,
												druta,
												CASE
													WHEN
														(intelisis IS NULL
															OR intelisis = 'SIN AGE'
															OR intelisis = ' ')
													THEN
														promotor
													ELSE intelisis
												END as promo,
												fecha_inicio
										from
											bitacora_gestioncobranza_intcob.registros
										where
											cuenta =  '".$cuenta."') A
									order by fecha_inicio desc
									limit 1";
                    $rq = mysql_query($cliente);
					//echo $cliente;              
         		
					                      
					$meses = array("ENE","FEB","MAR","ABR","MAY","JUN","JUL","AGO","SEP","OCT","NOV","DIC");
					$xpld  = explode("-",$fecha_inicio);
					$xpld1 = explode("-",$fecha_fin);
                    $fechas  = array("inicio" => "".$fecha_inicio, "fin" => "".date($fecha_fin));
				    $fechas1 = array("inicio" => "".$xpld[0]."-".$meses[$xpld[1]-1]."-".$xpld[2], "fin" => "".$xpld1[0]."-".$meses[$xpld1[1]-1]."-".$xpld1[2]);
            
					

					$facturas="SELECT 
									d.factura,
									d.pzo,
									concat('$', d.saldocapital) AS sc,
									concat('$', d.intereses) as intereses,
									concat('$', d.saldovencido) AS sv,
									d.diasinactividad AS di,
									d.diasvencidos AS dv
								FROM
									data_c086 AS d
								WHERE
									d.cuenta =  '".$cuenta."'
								UNION
								SELECT 
									d.factura,
									d.pzo,
									concat('$', d.saldocapital) AS sc,
									concat('$', d.intereses) as intereses,
									concat('$', d.saldovencido) AS sv,
									d.diasinactividad AS di,
									d.diasvencidos AS dv
								FROM
									bitacora_gestioncobranza_intcob.data_c086 AS d
								WHERE
									d.cuenta =  '".$cuenta."'";
            		//echo $facturas;                   
					$totales = array();
                    $porcent = array();
                    $numcol = mysql_num_rows(mysql_query("SELECT * FROM catalogo_resultado"));
                    //$visitas=0;
                    for($i=0,$j=0;$j<$numcol;$j++){
                            if($num>0 && $i<$num){
                                    $orden = mysql_result($rq, $i, 'orden');
                            }
                            else{
                                    $orden = -99;
                            }
                            if($orden-1 == $j){
                                    $hist[$j] = mysql_result($rq, $i, 'val');
                        //          $visitas=$visitas+$hist[$j];
                                    $porc[$j] = $hist[$j] / $visitas;
                                    $hist[$j] = 0;
                                    $porc[$j] = 0;
                            }
                    }
                                    $i++;
                            }else{
					//print_r($hist);
					
					
					//se obtienen los registros para lo de revision de cartera(proceso)
					$sql= " CREATE TEMPORARY TABLE IF NOT EXISTS reporte_temp_comentarios AS
							Select 
								promotor,
								intelisis,
							Case when fecha_inicio= concat(year(fecha_inicio),'-01-01') then 24
							else
								(MONTH(fecha_inicio) * 2 - (CASE
								WHEN DAYOFMONTH(fecha_inicio) = 1 THEN 2
										WHEN DAYOFMONTH(fecha_inicio) < 17 THEN 1
										ELSE 0
									END))

							end as periodo,
								YEAR(fecha_inicio) as ejercicio,
								rutacobro,
								ruta,
								druta,
								comentario,
								cuenta,
								factura,
								fecha_inicio,
								hora_inicio,
								hora_final,
								resultado,
								mov
							FROM
								registros
							WHERE
								cuenta =  '".$cuenta."'													
							UNION ALL
							 Select 
								promotor,
								intelisis,
							Case when fecha_inicio= concat(year(fecha_inicio),'-01-01') then 24
							else
								(MONTH(fecha_inicio) * 2 - (CASE
								WHEN DAYOFMONTH(fecha_inicio) = 1 THEN 2
										WHEN DAYOFMONTH(fecha_inicio) < 17 THEN 1
										ELSE 0
									END))

							end as periodo,
								YEAR(fecha_inicio) as ejercicio,
								rutacobro,
								ruta,
								druta,
								comentario,
								cuenta,
								factura,
								fecha_inicio,
								hora_inicio,
								hora_final,
								resultado,
								mov
							FROM
								bitacora_gestioncobranza_intcob.registros
							WHERE
								cuenta =  '".$cuenta."'
								order by fecha_inicio desc
								";
					//echo $sql;				
                    mysql_query($sql);
					
			 
            
			 $promotor = "select cg.cuenta, cg.factura, cg.comentario, cg.nomina, cg.quinsena, cg.ejercicio, 
CONCAT('Q', cg.quinsena, ' - ', cg.ejercicio) AS 'Quincena', B.ruta, B.comentario as comm,  
ec.usuario as nomina_revisor, ec.nombre_usuario as nombre_revisor, cg.comentario as comentario_revisor,
CASE
									 WHEN cg.prioridad_visita='on' and cg.prioridad_deter='on' THEN 'Prioridad Visita / A Determinar' 
									 WHEN cg.prioridad_visita='on' and cg.prioridad_deter=''  THEN 'Prioridad Visita'
									WHEN cg.prioridad_visita='' and cg.prioridad_deter='on'  THEN 'A Determinar'
									 ELSE '' 											 
									 END as resultado,
									  rh.NOMBRE AS nombre
from comentarios_gestiones cg 
left join reporte_temp_comentarios B   ON cg.cuenta = B.cuenta and cg.factura = B.factura and cg.quinsena = B.periodo and cg.ejercicio = B.ejercicio

join estadisticas_cartera ec on ec.e_nomina=cg.nomina and ec.quincena=cg.quinsena and ec.anio=cg.ejercicio
LEFT JOIN varios.empleados_rh201_intelisis rh ON cg.nomina = rh.CLAVE	
where cg.cuenta='".$cuenta."' ".$fecha."
 GROUP BY cg.Quinsena,cg.nomina
									ORDER BY cg.ejercicio desc, cg.quinsena desc";
			
			
			
			
			
                    $form = array(
                            "base"      => $config['base']
                    );

                    $this->tpl = "gestionescobranza/reporte_revision_cartera_cliente_detalle.html";
                    $bloque = array(
				               			               
                                "blkCliente"        => $cliente,
                                "blkFac"            => $facturas,
                                "blkProm"           => $promotor, 
								"blkFecha1"			=> array($fechas1)
                               
                    );
            }
            return $bloque;
		
	} //

	function _ReporteCuentasDesasig()
	{
		$this->tpl = "gestionescobranza/reporte_cuentas_desasignacion.html";
		
		if(!empty($_POST))
		{
			
			$fecha1="select quincena_cobranza1(".$_POST['quincena'].",".$_POST['ejercicio'].") as fecha";
			$res=mysql_fetch_assoc(mysql_query($fecha1));
			$desde=$res['fecha'];
			$fecha2="select quincena_cobranza2(".$_POST['quincena'].",".$_POST['ejercicio'].") as fecha";
			$res=mysql_fetch_assoc(mysql_query($fecha2));
			$hasta=$res['fecha'];
			
			$fecha_inicio = str_replace('/', '-', $desde);
            $fecha_fin = str_replace('/', '-', $hasta);
			
			$meses = array("ENE","FEB","MAR","ABR","MAY","JUN","JUL","AGO","SEP","OCT","NOV","DIC");
			$xpld  = explode("-",$fecha_inicio);
			$xpld1 = explode("-",$fecha_fin);
			$fechas  = array("inicio" => "".$fecha_inicio, "fin" => "".date($fecha_fin));
			$fechas1 = array("inicio" => "".$xpld[0]."-".$meses[$xpld[1]-1]."-".$xpld[2], "fin" => "".$xpld1[0]."-".$meses[$xpld1[1]-1]."-".$xpld1[2]);
			
			
			$where="";
			
			if($_POST["nomina"]!="")
			{
				$where.=" and his.nomina='".$_POST["nomina"]."'";
			}	
			
			if($_POST["cuenta"]!="")
			{
				$where.=" and his.idproceso like '%".$_POST["cuenta"]."%'";
			}
			
			if($_POST["estatus"]!="")
			{
				$where.=" and his.estatus = '".$_POST["estatus"]."'";
			}


			$sql="CREATE TEMPORARY TABLE IF NOT EXISTS procesos_ctas AS
						Select his.idproceso, his.fecha, his.usuario, his.nomina, his.estatus, his.comentario, his.modulo, est.idcliente as cuenta 
						from cob_fichas_procesos_update_his his
							left join cob_fichas_procesos_estcuenta est on his.idproceso=est.idproceso 
							where his.fecha between '".$desde."' and '".$hasta."' and his.modulo='Determinacion'  ".$where;
			
							
			mysql_query($sql);			
			
			
			//se obtienen los registros 
					$sql= " CREATE TEMPORARY TABLE IF NOT EXISTS gest2_temp AS
					Select 
								r.promotor,
								r.intelisis,
								r.ruta,							
								r.cuenta,
								r.nombre,
								r.fecha_inicio,
								r.hora_final,
								r.factura,								
								r.mov
							FROM
								registros as r	
							where r.cuenta in (select cuenta from procesos_ctas)
					
								";
			//echo $sql;				
			mysql_query($sql);		
			
			
			$sql= " INSERT INTO gest2_temp
					Select 
								r.promotor,
								r.intelisis,
								r.ruta,							
								r.cuenta,
								r.nombre,
								r.fecha_inicio,
								r.hora_final,
								r.factura,								
								r.mov
							FROM
								bitacora_gestioncobranza_intcob.registros as r	
							where r.cuenta in (select cuenta from procesos_ctas)
					
								";
			//echo $sql;				
			mysql_query($sql);
					
            
                    $promotor = "SELECT 
					est.idproceso,
							est.nomina as nomina_revisor,
							rh2.NOMBRE as nombre_revisor,
							est.comentario as comentario_revisor,
							CASE 
							when est.estatus=5 then 'Aprobado'
							when est.estatus=2 then 'Rechazado'
							when est.estatus=100 then 'Eliminado'
							else ''
							end as resultado_revisor,
							 r.cuenta, r.nombre, r.ruta, r.promotor FROM procesos_ctas est
							join gest2_temp r on est.idproceso=concat(r.promotor, r.cuenta,replace(r.fecha_inicio,'-','/'), r.hora_final)
							LEFT JOIN `varios`.`empleados_rh201_intelisis` rh2 ON est.nomina = rh2.CLAVE ";
			
			
			
			$sql2=$promotor;

			$res=mysql_query($sql2);	
			
			$num=mysql_num_rows($res);	
			
			 if($num == 0){
					$this->msgaviso = "No se encontraron registros.";
					mysql_free_result($res);
					return $bloque;
			}
			
			
			
			$this->tpl = "gestionescobranza/reporte_cuentas_desasignacion_detalle.html";
			
			$bloque = array(

                                "blkProm"           => $promotor, 
								"blkFecha1"			=> array($fechas1)
                               
                    );
			
		}
		
		
		return $bloque;
	} //_ReporteCuentasDesasig
	//sanciones para rechazos de cuentas enviadas a proceso
	function _confsan (){	

	if(!empty($_POST)){
	
		if($_POST['agregar']=="Nuevo")
		{
		$this->tpl = "gestionescobranza/guarda_sanciones.php";
			
		} else 	
			if($_POST['guardar']=='GUARDAR')
		{
		$insert="INSERT INTO bitacora_gestioncobranza_intelisis.configuracion_sanciones(M_inicial,M_final,p_jefe,p_subgerente,p_gerente,p_gerente_grl) 
					VALUES ('".$_POST['inicial']."','".$_POST['final']."','".$_POST['jefe']."',
					'".$_POST['subgerente']."','".$_POST['gerente']."','".$_POST['gerente_grl']."')";	
		mysql_query($insert);
		$this->tpl = "gestionescobranza/configuraciones.htm";
			$sql = "SELECT * FROM bitacora_gestioncobranza_intelisis.configuracion_sanciones ORDER BY M_inicial";
			$bloq = array( 'blksancion' => $sql );
			return $bloq;

		}
	}else {		
		
			$this->tpl = "gestionescobranza/configuraciones.htm";
			$sql = "SELECT * FROM bitacora_gestioncobranza_intelisis.configuracion_sanciones ORDER BY M_inicial";
			$bloq = array( 'blksancion' => $sql );
			return $bloq;
			}
	} //fin 

	function _ReporteRechazos()
	{
		$this->tpl = "gestionescobranza/reporte_sanciones_jefeGrupo.html";
		
		if(!empty($_POST))
		{
			
			$quincena_inicio=$_POST["quincena"];
			$ejercicio_inicio=$_POST["ejercicio"];
			$quincena_fin=$_POST["quincena2"];
			$ejercicio_fin=$_POST["ejercicio2"];
			
			$jefe=$_POST["nomina"];
			$revisor=$_POST["revisor"];
			
			$region=$_POST["region"];
			$etapa=$_POST["etapa"];
			
			$where="";
			
			if($jefe!="") $where.=" and a.nomina_j_grupo='".$jefe."'";
			
			if($revisor!="") $where.=" and a.nomina_revisor='".$revisor."'";
			
			if($region!="") $where.=" and a.region='".$region."'";
			
			if($etapa!="") $where.=" and a.etapa='".$etapa."'";
			
			
			
			
			$fecha1="select DATE_FORMAT(quincena_cobranza1((".$quincena_inicio."-1), ".$ejercicio_inicio."), '%Y-%m-%d') as fecha";
			$res=mysql_fetch_assoc(mysql_query($fecha1));
			$f_inicio_r=$res['fecha'];
			
			
			$fecha2="select DATE_FORMAT(quincena_cobranza2((".$quincena_fin."-1), ".$ejercicio_fin."), '%Y-%m-%d') as fecha";
			$res=mysql_fetch_assoc(mysql_query($fecha2));
			$f_final_r=$res['fecha'];
			
			
			$fecha3="select DATE_FORMAT(quincena_cobranza1((".$quincena_inicio."), ".$ejercicio_inicio."), '%Y-%m-%d') as fecha";
			$res=mysql_fetch_assoc(mysql_query($fecha3));
			$f_inicio_sancion=$res['fecha'];
			
			$fecha4="select DATE_FORMAT(quincena_cobranza2((".$quincena_fin."), ".$ejercicio_fin."), '%Y-%m-%d') as fecha";
			$res=mysql_fetch_assoc(mysql_query($fecha4));
			$f_final_sancion=$res['fecha'];			
			
			
			$fecha5="select DATE_FORMAT(quincena_cobranza1((".$quincena_inicio."), ".$ejercicio_inicio."), '%Y-%m-%d') as fecha";
			$res=mysql_fetch_assoc(mysql_query($fecha5));
			$f_inicio=$res['fecha'];
			
			
			$fecha6="select DATE_FORMAT(quincena_cobranza2((".$quincena_fin."), ".$ejercicio_fin."), '%Y-%m-%d') as fecha";
			$res=mysql_fetch_assoc(mysql_query($fecha6));
			$f_final=$res['fecha'];			
		
			
			$meses = array("ENE","FEB","MAR","ABR","MAY","JUN","JUL","AGO","SEP","OCT","NOV","DIC");
			$xpld  = explode("-",$f_inicio_sancion);
			$xpld1 = explode("-",$f_final_sancion);
			$fechas  = array("inicio" => "".$f_inicio_sancion, "fin" => "".date($f_final_sancion));
			$fechas1 = array("inicio" => "".$xpld[0]."-".$meses[$xpld[1]-1]."-".$xpld[2], "fin" => "".$xpld1[0]."-".$meses[$xpld1[1]-1]."-".$xpld1[2]);
			
						
			$sql_tabla="CREATE TEMPORARY TABLE bitacora_gestioncobranza_intelisis.reporte
			(
				idproceso 		varchar(50),
				cuenta 			varchar(20),
				cliente 		varchar(100),
				nomina_agente 	varchar(15),
				nombre_agente 	varchar(100),
				gestion 		varchar (15),
				region 			varchar(50),
				etapa 			varchar(50),
				nomina_j_grupo 	varchar(15),
				nombre_j_grupo 	varchar(100),
				aprobacion 		varchar (15),
				s_j_grupo 		varchar(10),
				nomina_revisor 	varchar(15),
				nombre_revisor 	varchar(100),
				rechazo 		varchar (15),
				nomina_subgerente 	varchar(15),
				sancion_subgerente  varchar(10),
				nomina_gerente 		varchar(15),
				sancion_gerente  	varchar(10),
				nomina_gerenteg 	varchar(15),
				sancion_gerenteg  	varchar(10), 
				fecha_hist  varchar(15)
				  ) ;";
				  
			mysql_query($sql_tabla);
			
			$sql="  INSERT INTO bitacora_gestioncobranza_intelisis.reporte
						select h.idproceso,substring(h.idproceso, 8, 9) as cuenta,'',ec.AGENTE,'',h.fecha,'','',h.sancion,'','','','','','',h.sancion_s,'',h.sancion_g,'',h.sancion_gg,'',h.fecha_sancion
						from bitacora_gestioncobranza_intelisis.cob_fichas_procesos_update_his h
						join bitacora_gestioncobranza_intelisis.cob_fichas_procesos_estcuenta ec on h.idproceso=ec.IDPROCESO
					  where  h.fecha_sancion between '".$f_inicio_r."' and '".$f_final."';";
					  
			mysql_query($sql);
			
						$sqldelete=" delete from bitacora_gestioncobranza_intelisis.reporte
        where nomina_j_grupo='0';";
					  
			mysql_query($sqldelete);
			
			#------------------------------------------------ update para el llenado de nombres---------------------------------------------------------------
			
			/*****gestores*****/

			$sql="UPDATE bitacora_gestioncobranza_intelisis.reporte c ,varios.empleados_rh201_intelisis v
				SET c.nombre_agente = v.nombre
				WHERE c.nomina_agente = v.clave;";
			
			mysql_query($sql);
			
			/*****jefes*****/
			
			$sql="UPDATE bitacora_gestioncobranza_intelisis.reporte c ,varios.empleados_rh201_intelisis v
				SET c.nombre_j_grupo = v.nombre
				WHERE c.nomina_j_grupo = v.clave;
				";
			
			mysql_query($sql);
			
			
			#------------------------------------------------ fecha gestion---------------------------------------------------------------
			
			$sql="  CREATE TEMPORARY TABLE bitacora_gestioncobranza_intelisis.fecha
				(
					idproceso varchar(50),
					fecha varchar(15),
					sancion_j varchar(15),
					sancion_s  varchar(15),
					sancion_g varchar(15),
					sancion_gg varchar(15),
					fecha_gestion varchar(30),
					nomina_revisor 	varchar(15),
					rechazo 		varchar (15)
					  ) ;";
			
			mysql_query($sql);
			
			$sql="INSERT INTO bitacora_gestioncobranza_intelisis.fecha
 select  idproceso, substring(fecha, 1, 10) as fecha,'','','','','','','' 
 from bitacora_gestioncobranza_intelisis.cob_fichas_procesos_update_his 
 where estatus=3 and modulo='Envio Gestor' and idproceso in (select idproceso from bitacora_gestioncobranza_intelisis.reporte where fecha_hist<>0);";
			
			mysql_query($sql);
			
			
			$sql="UPDATE bitacora_gestioncobranza_intelisis.reporte c ,bitacora_gestioncobranza_intelisis.fecha v
			SET c.gestion = v.fecha
			WHERE c.idproceso = v.idproceso;";

			mysql_query($sql);
			
			
			#------------------------------------------------ sancion---------------------------------------------------------------
			
			
			$sql_o="UPDATE bitacora_gestioncobranza_intelisis.fecha c ,(select nomina, sancion, idproceso 
			from  aplicaciones_web._sanciones_registro reg
			join bitacora_gestioncobranza_intelisis.reporte r on reg.fecha=r.fecha_hist and reg.Nomina=r.nomina_j_grupo
			where Observaciones like'Sancion por rechazos de % cuenta(s)') v
			SET c.sancion_j = v.sancion
			WHERE c.idproceso = v.idproceso;";
			
			
			mysql_query($sql_o) or die(mysql_error());
			
			
			
			
			$sql="UPDATE bitacora_gestioncobranza_intelisis.fecha c ,(select nomina, sancion, idproceso 
			from  aplicaciones_web._sanciones_registro reg
			join bitacora_gestioncobranza_intelisis.reporte r on reg.fecha=r.fecha_hist and reg.Nomina=r.nomina_subgerente
			where Observaciones like'Sancion por rechazos de % cuenta(s)') v
			SET c.sancion_s = v.sancion
			WHERE c.idproceso = v.idproceso;";
			
			mysql_query($sql) or die(mysql_error());
			
			
			$sql="UPDATE bitacora_gestioncobranza_intelisis.fecha c ,(select nomina, sancion, idproceso 
			from  aplicaciones_web._sanciones_registro reg
			join bitacora_gestioncobranza_intelisis.reporte r on reg.fecha=r.fecha_hist and reg.Nomina=r.nomina_gerente
			where Observaciones like'Sancion por rechazos de % cuenta(s)') v
			SET c.sancion_g = v.sancion
			WHERE c.idproceso = v.idproceso;";
			
			mysql_query($sql) or die(mysql_error());
			
			
			$sql="UPDATE bitacora_gestioncobranza_intelisis.fecha c ,(select nomina, sancion, idproceso 
			from  aplicaciones_web._sanciones_registro reg
			join bitacora_gestioncobranza_intelisis.reporte r on reg.fecha=r.fecha_hist and reg.Nomina=r.nomina_gerenteg
			where Observaciones like'Sancion por rechazos de % cuenta(s)') v
			SET c.sancion_gg = v.sancion
			WHERE c.idproceso = v.idproceso;";
			
			mysql_query($sql) or die(mysql_error());
			
			
			#------------------------------------------------ sancion a tabla reporte---------------------------------------------------------------
			
			$sql="UPDATE bitacora_gestioncobranza_intelisis.reporte c , bitacora_gestioncobranza_intelisis.fecha v
			SET c.s_j_grupo = v.sancion_j
			WHERE c.idproceso = v.idproceso;";
			
			mysql_query($sql);
			
			
			$sql="UPDATE bitacora_gestioncobranza_intelisis.reporte c , bitacora_gestioncobranza_intelisis.fecha v
			SET c.sancion_subgerente = v.sancion_s
			WHERE c.idproceso = v.idproceso;";
			
			mysql_query($sql);
			
			
			$sql="UPDATE bitacora_gestioncobranza_intelisis.reporte c , bitacora_gestioncobranza_intelisis.fecha v
			SET c.sancion_gerente = v.sancion_g
			WHERE c.idproceso = v.idproceso;";
			
			mysql_query($sql);
			
			
			$sql="UPDATE bitacora_gestioncobranza_intelisis.reporte c , bitacora_gestioncobranza_intelisis.fecha v
			SET c.sancion_gerenteg = v.sancion_gg
			WHERE c.idproceso = v.idproceso;";
			
			mysql_query($sql);
			
			
			$sql="UPDATE bitacora_gestioncobranza_intelisis.fecha c , (select fecha, idproceso from  bitacora_gestioncobranza_intelisis.cob_fichas_procesos_update_his  
			where modulo='Cuentas en Proceso Desasignacion' and idproceso in (select idproceso from bitacora_gestioncobranza_intelisis.reporte )
			 and estatus=4) v
			SET c.fecha_gestion = v.fecha
			WHERE c.idproceso = v.idproceso;";
			
			mysql_query($sql);
			
			
			$sql="UPDATE bitacora_gestioncobranza_intelisis.fecha c , (select fecha, idproceso, nomina from  bitacora_gestioncobranza_intelisis.cob_fichas_procesos_update_his  
			where modulo='determinacion' and idproceso in (select idproceso from bitacora_gestioncobranza_intelisis.reporte )
			 and estatus=100) v
			SET c.rechazo = v.fecha, c.nomina_revisor= v.nomina
			WHERE c.idproceso = v.idproceso;";
			
			mysql_query($sql);
			
			
			
			
			$sql="UPDATE bitacora_gestioncobranza_intelisis.reporte c ,bitacora_gestioncobranza_intelisis.fecha v
			SET c.s_j_grupo = v.sancion_j , c.sancion_subgerente = v.sancion_s, c.sancion_gerente = v.sancion_g, c.sancion_gerenteg = v.sancion_gg
					, c.aprobacion = v.fecha_gestion , c.nomina_revisor = v.nomina_revisor , c.rechazo = v.rechazo
			WHERE c.idproceso = v.idproceso;
			";
			
			mysql_query($sql);
			
			
			$sql="UPDATE bitacora_gestioncobranza_intelisis.reporte c ,varios.empleados_rh201_intelisis v
			SET c.nombre_revisor = v.nombre
			WHERE c.nomina_revisor = v.clave;";
			
			mysql_query($sql);
			
			
			$sql="UPDATE bitacora_gestioncobranza_intelisis.reporte u ,android_reportes._cuentas_gestioncobranza  cc
			SET u.region = cc.poblacion, u.etapa = cc.movimiento, u.cliente = cc.nombre
			WHERE u.cuenta =cc.cuenta ;";
			
			mysql_query($sql);
			
			
			
			
			
			$sql_info_reporte="SELECT * from(
select idproceso, cuenta, cliente, nomina_agente, nombre_agente, gestion, region, etapa, nomina_j_grupo, nombre_j_grupo, aprobacion
, nomina_revisor, nombre_revisor, rechazo, nomina_subgerente,  nomina_gerente, nomina_gerenteg,  fecha_hist

from bitacora_gestioncobranza_intelisis.reporte)a
						where a.fecha_hist between '".$f_inicio_sancion."' and '".$f_final_sancion."'	".$where;
			
			
			
			
			
			$this->tpl = "gestionescobranza/reporte_sanciones_jefeGrupo_detalle.html";
			
			$bloque = array(

                                "blkProm"           => $sql_info_reporte, 
								"blkFecha1"			=> array($fechas1)
                               
                    );
			
		}
		
		
		return $bloque;
	}
    function _ReporteHistoricoProceso()
	{
		$this->tpl = "gestionescobranza/reporte_historico_procesos.html";
		
		
		
		
		if(!empty($_POST))
		{
			
			$fecha1="select quincena_cobranza1(".$_POST['quincena'].",".$_POST['ejercicio'].") as fecha";
			$res=mysql_fetch_assoc(mysql_query($fecha1));
			$desde=$res['fecha'];
			$fecha2="select quincena_cobranza2(".$_POST['quincena'].",".$_POST['ejercicio'].") as fecha";
			$res=mysql_fetch_assoc(mysql_query($fecha2));
			$hasta=$res['fecha'];
			
			$fecha_inicio = str_replace('/', '-', $desde);
            $fecha_fin = str_replace('/', '-', $hasta);
			
			$meses = array("ENE","FEB","MAR","ABR","MAY","JUN","JUL","AGO","SEP","OCT","NOV","DIC");
			$xpld  = explode("-",$fecha_inicio);
			$xpld1 = explode("-",$fecha_fin);
			$fechas  = array("inicio" => "".$fecha_inicio, "fin" => "".date($fecha_fin));
			$fechas1 = array("inicio" => "".$xpld[0]."-".$meses[$xpld[1]-1]."-".$xpld[2], "fin" => "".$xpld1[0]."-".$meses[$xpld1[1]-1]."-".$xpld1[2]);
			
			
			$where="";
			
			if($_POST["cuenta"]!="")
			{
				$where.=" and a.cuenta like '".$_POST["cuenta"]."'";
			}
			
			
			if($_POST["nomina"]!="")
			{
				$where.=" and a.agente='".$_POST["nomina"]."'";	
				
			}
			
			if($_POST["estatus"]!="")
			{
				$where.=" and a.estatus = '".$_POST["estatus"]."'";
			}
			
			
			if($_POST["idproceso"]!="")
			{
				$where.=" and a.idproceso = '".$_POST["idproceso"]."'";
			}
			

            
			$promotor = "
			select * from(
			select his.idproceso, his.estatus,  his.fecha, his.usuario, his.nomina as nomina_revisor, rh2.nombre as nombre_revisor,
				pro.idcliente as cuenta, pro.proceso, pro.agente, est.nombre as estatus_nombre, cast(his.fecha as date) as fecha1 
				from cob_fichas_procesos_update_his his
				left join cob_fichas_procesos_estcuenta pro on pro.idproceso=his.idproceso
				left join estatus_procesos est on est.estatus=his.estatus
				LEFT JOIN varios.empleados_rh201_intelisis rh2 on rh2.clave=his.nomina
				
				) a
				where a.fecha1 between '".$fecha_inicio."' and '".$fecha_fin."' ".$where. " order by a.fecha";
				
			//echo $promotor;	
			
			
			$sql2=$promotor;

			$res=mysql_query($sql2);	
			
			$num=mysql_num_rows($res);	
			
			 if($num == 0){
					$this->msgaviso = "No se encontraron registros.";
					mysql_free_result($res);
					
					$sql="select estatus, nombre from bitacora_gestioncobranza_intelisis.estatus_procesos";			
					$bloque = array(

										"blkProcesos" => $sql
							);
					
					return $bloque;
			}
			
			
			
			$this->tpl = "gestionescobranza/reporte_historico_procesos_detalle.html";
			
			$bloque = array(

                                "blkProm"           => $promotor, 
								"blkFecha1"			=> array($fechas1)
                               
                    );
			
		}else{
			
			
			$sql="select estatus, nombre from bitacora_gestioncobranza_intelisis.estatus_procesos";
			
			$bloque = array(

                                "blkProcesos" => $sql
                    );
			
		}

		return $bloque;
	}  // Fin _ReporteHistoricoProceso()


    function _ReporteGralPapeleria( ) {
		$this->tpl = "reporte.gral.papeleria.htm";
        $typelist = array('FOLIOS'=>'Folios','TODOS'=>'Todos','RECIBOS'=>'Recibos','CONVENIOS'=>'Convenios');
        $data = array();
		$aux['fecha'] = date('Y')."-".date('m')."-".date('d');
		$data[] = $aux;
        $models[] =  array();
        array_pop($models);

        if (isset($_POST['fechaDesde']) && isset($_POST['fechaDesde']) && $_POST['tipoPapeleria']) {
            $fechaDesde = $_POST['fechaDesde'];
            $fechaHasta = $_POST['fechaHasta'];
            $tipoPapeleria = $_POST['tipoPapeleria'];
            $query = '';                                                                                                                                                                                                                                                                   //where FechaAsignacion between \''.$fechaDesde.'\' and \''.$fechaHasta.'\'  ';

            $queryFolio = 'select distinct \'Folio\'  as tipo 
                           , Folio    as \'serie\'
                           , case when entregado <> \'SI\' then \'NO\' else \'SI\' end AS \'entregado\'
                           , case when capturado <> \'SI\' then \'NO\' else \'SI\' end AS \'capturado\'
                           , case when cancelado <> \'SI\' then \'NO\' else \'SI\' end AS \'cancelado\'
                           , case when Reasignar <> \'SI\' then \'NO\' else \'SI\' end AS \'Reasignar\'
                           , case when pagar <> \'SI\' then \'NO\' else \'SI\' end AS \'pagar\' 
                           , N_Promotor as \'NominaAgente\'
                           , Fecha_Salida as \'FechaAsignacion\',       
                                        CONCAT_WS(\' \',
                                        organigrama.base_organigrama.Nombre,
                                        organigrama.base_organigrama.Apellido_Paterno,
                                        organigrama.base_organigrama.Apellido_Materno
                                        ) as \'nombreAgente\',                           
                                        organigrama.agentenivelhistmavi.Nivel as \'NivelCobranza\'
                           , Fecha_Elaboracion
                           , Fecha_Entrada
                           , Cuenta
                           , Cliente
                           , Factura1  as \'IdMovimiento\'
                           , comentario_cancelacion
                           , N_Captura AS \'AuxiliarCancelacion\'
                                        from folios.folios 
                                        left join organigrama.base_organigrama
                                          on folios.folios.N_Promotor = organigrama.base_organigrama.Personal
                                        left join organigrama.agentenivelhistmavi on folios.folios.N_promotor = organigrama.agentenivelhistmavi.Agente
                                        where Fecha_Salida between \'' . $fechaDesde . '\' and \'' . $fechaHasta . '\'  ';

            $queryRecibos ='select 
                           distinct \'Recibo\'as tipo ,
                           Recibo as \'serie\' 
                           , case when entregado <> \'SI\' then \'NO\' else \'SI\' end AS \'entregado\'
                           , case when capturado <> \'SI\' then \'NO\' else \'SI\' end AS \'capturado\'
                           , case when cancelado <> \'SI\' then \'NO\' else \'SI\' end AS \'cancelado\'
                           , case when Reasignar <> \'SI\' then \'NO\' else \'SI\' end AS \'Reasignar\'
                           ,\'NO\'  as \'pagar\' 
                           ,Nomina_promotor as \'NominaAgente\'
                           ,F_entrega    as \'FechaAsignacion\',       
                           CONCAT_WS(\' \',
                           organigrama.base_organigrama.Nombre,
                           organigrama.base_organigrama.Apellido_Paterno,
                           organigrama.base_organigrama.Apellido_Materno
                           ) as \'nombreAgente\',                           
                           organigrama.agentenivelhistmavi.Nivel as \'NivelCobranza\'
                           ,F_captura as \'Fecha_Elaboracion\'
                           ,F_entrega as \'Fecha_Entrada\'
                           ,Cuenta
                           ,Cliente
                           ,Movimiento  as \'IdMovimiento\'
                           ,comentario_cancelacion
                           ,Nomina_captura AS \'AuxiliarCancelacion\'
                           from folios.folios_recibos 
                           left join organigrama.base_organigrama
                             on folios.folios_recibos.Nomina_promotor = organigrama.base_organigrama.Personal
                           left join organigrama.agentenivelhistmavi 
                             on folios.folios_recibos.Nomina_promotor = organigrama.agentenivelhistmavi.Agente
                           where F_entrega between \''.$fechaDesde.'\' and \''.$fechaHasta.'\'  ';

            $queryConvenios ='select distinct \'Convenios\'as tipo ,
                           convenio as \'serie\' 
                           , case when entregado <> \'SI\' then \'NO\' else \'SI\' end AS \'entregado\'
                           , case when capturado <> \'SI\' then \'NO\' else \'SI\' end AS \'capturado\'
                           , case when cancelado <> \'SI\' then \'NO\' else \'SI\' end AS \'cancelado\'
                           , case when Reasignar <> \'SI\' then \'NO\' else \'SI\' end AS \'Reasignar\'
                           , \'NO\'  as \'pagar\'                                                      
                           , Nomina_promotor as \'NominaAgente\'
                           , F_entrega as \'FechaAsignacion\',       
                           CONCAT_WS(\' \',
                           organigrama.base_organigrama.Nombre,
                           organigrama.base_organigrama.Apellido_Paterno,
                           organigrama.base_organigrama.Apellido_Materno
                           ) as \'nombreAgente\',                           
                           organigrama.agentenivelhistmavi.Nivel as \'NivelCobranza\',F_captura AS  \'Fecha_Elaboracion\',F_entrega AS  \'Fecha_Entrada\',Cuenta,Cliente,Movimiento  as \'IdMovimiento\',comentario_cancelacion,
                           Nomina_captura AS \'AuxiliarCancelacion\'
                           from folios.folios_convenios 
                           left join organigrama.base_organigrama
                             on folios.folios_convenios.Nomina_promotor = organigrama.base_organigrama.Personal
                           left join organigrama.agentenivelhistmavi on folios.folios_convenios.Nomina_promotor = organigrama.agentenivelhistmavi.Agente  
                           where F_entrega between \''.$fechaDesde.'\' and \''.$fechaHasta.'\'  ';
//Fecha ENtrada  =  Fecha Recepcion
            $queryTodo  = 'select distinct tabla.*,
                            CONCAT_WS(\' \', organigrama.base_organigrama.Nombre, organigrama.base_organigrama.Apellido_Paterno,
                                      organigrama.base_organigrama.Apellido_Materno) as \'nombreAgente\',
                            organigrama.agentenivelhistmavi.Nivel                    as \'NivelCobranza\'
                            from (select \'Folio\'           as tipo,
                                     folio             as \'serie\',
                                     case when entregado <> \'SI\' then \'NO\' else \'SI\' end      AS \'entregado\',
                                     case when capturado <> \'SI\' then \'NO\' else \'SI\' end      AS \'capturado\',
                                     case when cancelado <> \'SI\' then \'NO\' else \'SI\' end      AS \'cancelado\',
                                     case when Reasignar <> \'SI\' then \'NO\' else \'SI\' end      AS \'Reasignar\',
                                     case when pagar     <> \'SI\' then \'NO\' else \'SI\' end      AS \'pagar\',
                                     N_Promotor        as \'NominaAgente\',
                                     Fecha_Salida      as \'FechaAsignacion\',
                                     \'NivelCobranza\',
                                     Fecha_Elaboracion AS \'Fecha_Elaboracion\',
                                     Fecha_Entrada     as \'Fecha_Entrada\',
                                     Cuenta,
                                     Cliente,
                                     Factura1          as \'IdMovimiento\',
                                     comentario_cancelacion,
                                     N_Captura         AS \'AuxiliarCancelacion\'
                              from folios.folios
                              union all
                              select \'Convenio\'      as tipo,
                                     convenio        as \'serie\',
                                     case when entregado <> \'SI\' then \'NO\' else \'SI\' end      AS \'entregado\',
                                     case when capturado <> \'SI\' then \'NO\' else \'SI\' end      AS \'capturado\',
                                     case when cancelado <> \'SI\' then \'NO\' else \'SI\' end      AS \'cancelado\',
                                     case when Reasignar <> \'SI\' then \'NO\' else \'SI\' end      AS \'Reasignar\',
                                     \'NO\'      AS \'pagar\',
                                     Nomina_promotor as \'NominaAgente\',
                                     F_entrega       as \'FechaAsignacion\',
                                     \'NivelCobranza\',
                                     F_captura       AS \'Fecha_Elaboracion\',
                                     F_entrega       as \'Fecha_Entrada\',
                                     Cuenta,
                                     Cliente,
                                     Movimiento      as \'IdMovimiento\',
                                     comentario_cancelacion,
                                     Nomina_captura  AS \'AuxiliarCancelacion\'
                              from folios.folios_convenios
                              union all
                              select \'Recibos\'       as tipo,
                                     Recibo          as \'serie\',
                                     case when entregado <> \'SI\' then \'NO\' else \'SI\' end      AS \'entregado\',
                                     case when capturado <> \'SI\' then \'NO\' else \'SI\' end      AS \'capturado\',
                                     case when cancelado <> \'SI\' then \'NO\' else \'SI\' end      AS \'cancelado\',
                                     case when Reasignar <> \'SI\' then \'NO\' else \'SI\' end      AS \'Reasignar\',
                                     \'NO\' AS \'pagar\',
                                     Nomina_promotor as \'NominaAgente\',
                                     F_entrega       as \'FechaAsignacion\',
                                     \'NivelCobranza\',
                                     F_captura       AS \'Fecha_Elaboracion\',
                                     F_entrega       as \'Fecha_Entrada\',
                                     Cuenta,
                                     Cliente,
                                     Movid           as \'IdMovimiento\',
                                     comentario_cancelacion,
                                     Nomina_captura  AS \'AuxiliarCancelacion\'
                              from folios.folios_recibos) as tabla
                                 left join organigrama.base_organigrama on tabla.NominaAgente = organigrama.base_organigrama.Personal
                                 left join organigrama.agentenivelhistmavi on tabla.NominaAgente = organigrama.agentenivelhistmavi.Agente
                              where tabla.FechaAsignacion between \''.$fechaDesde.'\' and \''.$fechaHasta.'\'  ';

            switch ($tipoPapeleria) {
                case "TODOS":
                    $query = $queryTodo;
                    break;
                case "FOLIOS":
                    $query = $queryFolio;
                    break;
                case "RECIBOS":
                    $query = $queryRecibos;
                    break;
                case "CONVENIOS":
                    $query = $queryConvenios;
                    break;
            }

            if ($_POST['nomina'] != ''){
//                $query = $query .' and N_Promotor = \''. $_POST['nomina'].'\' ';
//                //Nomina_promotor
//                if ($_POST['tipoPapeleria'] == 'TODO' ){
//                    $query = $query .' and NominaAgente = \''. $_POST['nomina'].'\' ';
//                }
                switch ($_POST['tipoPapeleria']){
                    case 'TODOS':
                        //$query = $query .' and serie  = '. $_POST['folio'].' ';
                        $query = $query .' and NominaAgente = \''. $_POST['nomina'].'\' ';
                        break;
                    case 'FOLIOS':
//                        $query = $query .' and Folio  = '. $_POST['folio'].' ';
                        $query = $query .' and N_Promotor = \''. $_POST['nomina'].'\' ';
                        break;
                    case 'CONVENIOS':
//                        $query = $query .' and convenio  = '. $_POST['folio'].' ';
                        $query = $query .' and Nomina_promotor = \''. $_POST['nomina'].'\' ';
                        break;
                    case 'RECIBOS':
//                        $query = $query .' and Recibo  = '. $_POST['folio'].' ';
                        $query = $query .' and Nomina_promotor = \''. $_POST['nomina'].'\' ';
                        break;
                }
            }

            if ($_POST['folio'] != '' ){
                switch ($_POST['tipoPapeleria']){
                    case 'TODOS':
                        $query = $query .' and serie  = '. $_POST['folio'].' ';
                        break;
                    case 'FOLIOS':
                        $query = $query .' and Folio  = '. $_POST['folio'].' ';
                        break;
                    case 'CONVENIOS':
                        $query = $query .' and convenio  = '. $_POST['folio'].' ';
                        break;
                    case 'RECIBOS':
                        $query = $query .' and Recibo  = '. $_POST['folio'].' ';
                        break;
                }
            }

            $query = $query . ';';
            echo $query;
            $rq = mysql_query($query);
            while($res = mysql_fetch_array($rq)) {
                $models[] = $res;
            }
        }

		$bloque = array(
			"blkPost" => empty($_POST)?array():$_POST,
            "typeblk" => $typelist,
            "blkData" =>  $models,
			"blkFechaActual" =>  $data, //useless
            "blkQueryFolio" =>  $queryFolio,
            "blkQueryRecibos" =>  "select 'Conveio'as tipo ,convenio as 'No DE SERIE' ,capturado,cancelado,Reasignar,''   ,'Nomina Agente',F_entrega   ,'nombreAgente','NivelCobranza',F_captura        ,F_entrega    ,Cuenta,Cliente,Movimiento,comentario_cancelacion from folios.folios_convenios", //usless
		);
		return $bloque;
    }

/*PROGRAMADOR: Getsemani Avila Quezada ༼ つ ◕_◕ ༽つ
  DESSAROLLO : INTERFACE PARA CARGA DE CSV LINKS BBVA
  FECHA      : 21/07/2020*/
    function _NIPdeBancomer( ) {
	    //?mod=bgc.configura&ac=
        $this->tpl = "proceso.sms_BBVA.htm";
        $bloque = array(
            "blkQueryRecibos" =>  "select 'Conveio'as tipo ,convenio as 'No DE SERIE' ,capturado,cancelado,Reasignar,''   ,'Nomina Agente',F_entrega   ,'nombreAgente','NivelCobranza',F_captura        ,F_entrega    ,Cuenta,Cliente,Movimiento,comentario_cancelacion from folios.folios_convenios", //usless
        );
        return $bloque;
    }
}
