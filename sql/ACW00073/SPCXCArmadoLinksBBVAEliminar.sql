SET DATEFIRST 7
SET ANSI_NULLS ON
SET ANSI_WARNINGS ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET LOCK_TIMEOUT -1
SET QUOTED_IDENTIFIER OFF
SET NOCOUNT ON
SET IMPLICIT_TRANSACTIONS OFF
GO
--=======================================================================================================
-- NOMBRE         : SPCXCArmadoLinksBBVAEliminar
-- AUTOR          : Getsemani Avila Quezada ༼ つ ◕_◕ ༽つ
-- FECHA CREACION : 08/07/2020
-- DESCRIPCION    : PURGADO DE LA TABLA  CXCCArmadoLinkBBVA
-- DESARROLLO     : ACW00073 links Bancomer
-- EJEMPLO        : Exec SPCXCArmadoLinksBBVAEliminar
--=======================================================================================================
CREATE PROCEDURE [dbo].[SPCXCArmadoLinksBBVAEliminar]
AS
BEGIN

  Truncate Table CXCCArmadoLinkBBVA

END