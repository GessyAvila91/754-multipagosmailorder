SET DATEFIRST 7
SET ANSI_NULLS ON
SET ANSI_WARNINGS ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET LOCK_TIMEOUT -1
SET QUOTED_IDENTIFIER OFF
SET NOCOUNT ON
SET IMPLICIT_TRANSACTIONS OFF
GO
-- ========================================================================================================================================
-- NOMBRE          : FNCXCPagoLiquidaBBVA
-- AUTOR           : Getsemani Avila Quezada ༼ つ ◕_◕ ༽つ
-- FECHA CREACION  : 15/sep/2020
-- DESARROLLO      : Liks BBVA
-- MODULO          : CXC
-- DESCRIPCION     : Funcion para calcular el Importe a liquidar
-- EJEMPLO         : select dbo.FNCXCPagoLiquidaBBVA ('Seguro Vida','ZBT145')
-- ========================================================================================================================================
CREATE FUNCTION [dbo].[FNCXCPagoLiquidaBBVA] (@Fac varchar(25), @Facid varchar(25))
RETURNS money
AS
BEGIN
  DECLARE @tipobonif AS TABLE (
    ID int PRIMARY KEY IDENTITY (1, 1),
    Bonificacion varchar(50),
    Mov varchar(25),
    MovId varchar(25),
    PorcBon float,
    Linea money,
    Canal int
  )
  DECLARE @docsconcluidos AS TABLE (
    ID int PRIMARY KEY IDENTITY (1, 1),
    padremavi varchar(25),
    Padreidmavi varchar(25),
    cliente varchar(10),
    concluidos int
  )
  DECLARE @ProxVenc AS TABLE (
    ID int PRIMARY KEY IDENTITY (1, 1),
    FechaProximoVenc datetime,
    Padremavi varchar(25),
    Padreidmavi varchar(25),
    cliente varchar(10),
    docs int,
    Vencehoy int,
    saldo money
  )

  DECLARE @impte money,
          @BonificacionCC money,
          @FechaEmision datetime,
          @FechaEmisionFac datetime,
          @Sucursal int


  IF EXISTS (SELECT
      SolC.FechaEmision
    FROM Venta Fac WITH (NOLOCK)
    INNER JOIN Venta Ped WITH (NOLOCK)
      ON Fac.Origen = Ped.Mov
      AND Fac.OrigenID = Ped.MovID
    INNER JOIN Venta AnaC WITH (NOLOCK)
      ON Ped.Origen = AnaC.Mov
      AND Ped.OrigenID = AnaC.MovID
    INNER JOIN Venta SolC WITH (NOLOCK)
      ON AnaC.Origen = SolC.Mov
      AND AnaC.OrigenID = SolC.MovID
    WHERE Fac.Mov = @fac
    AND Fac.MovID = @facid)
  BEGIN
    SELECT
      @FechaEmision = SolC.FechaEmision,
      @FechaEmisionFac = Fac.FechaEmision,
      @Sucursal = Ped.SucursalVenta
    FROM Venta Fac WITH (NOLOCK)
    INNER JOIN Venta Ped WITH (NOLOCK)
      ON Fac.Origen = Ped.Mov
      AND Fac.OrigenID = Ped.MovID
    INNER JOIN Venta AnaC WITH (NOLOCK)
      ON Ped.Origen = AnaC.Mov
      AND Ped.OrigenID = AnaC.MovID
    INNER JOIN Venta SolC WITH (NOLOCK)
      ON AnaC.Origen = SolC.Mov
      AND AnaC.OrigenID = SolC.MovID
    WHERE Fac.Mov = @fac
    AND Fac.MovID = @facid
  END
  ELSE
  BEGIN
    SELECT
      @FechaEmision = fac.FechaEmision,
      @FechaEmisionFac = Fac.FechaEmision
    FROM Venta Fac WITH (NOLOCK)
    WHERE Fac.Mov = @fac
    AND Fac.MovID = @facid
  END


  INSERT INTO @tipobonif (Bonificacion, Mov, MovID, Porcbon, Linea, Canal)
    SELECT
      F.Bonificacion,
      C.Mov,
      C.MovID,
      PorcBon1,
      F.Linea,
      CanalVenta
    FROM (SELECT
      B.Id,
      U.Uen,
      B.Bonificacion,
      Estatus,
      PorcBon1,
      linea,
      FechaIni,
      FechaFin,
      M.Movimiento,
      C.Condicion,
      B.PlazoEjeFin,
      B.Financiamiento,
      B.AplicaA,
      B.orden,
      s.sucursal,
      V.CanalVenta,
      ISNULL(CV.MaxDV, 0) MaxDV,
      ISNULL(CV.PorcBon, 0) PorcBonconven,
      B.VencimientoAntes,
      B.DiasMenoresA
    FROM MaviBonificacionConf B WITH (NOLOCK)
    JOIN MaviBonificacionCondicion C WITH (NOLOCK)
      ON B.Id = C.IdBonificacion
    JOIN MaviBonificacionMov M WITH (NOLOCK)
      ON B.Id = M.IdBonificacion
    JOIN MaviBonificacionUEN U WITH (NOLOCK)
      ON B.Id = U.IdBonificacion
    JOIN MaviBonificacionSucursal S WITH (NOLOCK)
      ON B.Id = S.IdBonificacion
    LEFT JOIN MaviBonificacionCanalVta V WITH (NOLOCK)
      ON V.IdBonificacion = B.id
    LEFT JOIN MaviBonificacionConVencimiento CV WITH (NOLOCK)
      ON CV.IdBonificacion = B.id
    WHERE Estatus = 'Concluido') F
    INNER JOIN cxc C WITH (NOLOCK)
      ON C.Mov = F.Movimiento
      AND C.Condicion = F.Condicion
      AND C.Uen = F.Uen
      AND C.Condicion = F.Condicion
    LEFT JOIN Sucursal sc WITH (NOLOCK)
      ON sc.Tipo = F.sucursal
      AND sc.sucursal = ISNULL(@Sucursal, c.SucursalOrigen)
    WHERE Mov = @Fac
    AND MovId = @Facid
    AND ISNULL(@FechaEmision, C.fechaemision) BETWEEN F.FechaIni AND F.FechaFin
    AND c.ClienteEnviarA =
                          CASE
                            WHEN F.CanalVenta IS NULL THEN c.ClienteEnviarA
                            ELSE F.CanalVenta
                          END
    GROUP BY F.Bonificacion,
             C.Mov,
             C.MovID,
             F.PorcBon1,
             F.Linea,
             f.CanalVenta

  -- BONIFICACION PAGO PUNTUAL

  SELECT
    @impte = SUM(T.Bonificacion)
  FROM (SELECT
    A.PadreMAVI,
    A.PadreIDMAVI,
    Bonificacion =
                  CASE
                    WHEN A.Bonificacion = 0.00 THEN A.Saldo
                    ELSE A.Bonificacion
                  END
  FROM (SELECT
    C.PadreMAVI,
    C.PadreIDMAVI,
    Bonificacion = ISNULL(dbo.FN_MAVIRM0906CalculaBonifCC(PadreMAVI, PADREIDMAVI, 'Pago Puntual', C.Mov, C.MovID), 0),
    C.Saldo
  FROM Cxc C WITH (NOLOCK)
  WHERE PadreMAVI = @Fac
  AND PadreIDMAVI = @Facid
  AND C.Estatus = 'PENDIENTE') A) T
  GROUP BY T.PadreMAVI,
           T.PadreIDMAVI

  -- BONIFICACION CONTADO COMERCIAL

  IF EXISTS (SELECT
      *
    FROM @tipobonif
    WHERE Bonificacion LIKE '%Contado comercial%'
    AND Mov = @Fac
    AND MovID = @Facid)
  BEGIN

    SELECT
      @BonificacionCC = T.Bonificacion
    FROM (SELECT
      ROW_NUMBER() OVER (PARTITION BY C.PadreIDMAVI ORDER BY C.MovID) AS R,
      C.PadreMAVI,
      C.PadreIDMAVI,
      c.mov,
      c.movid,
      Bonificacion = ISNULL(dbo.FN_MAVIRM0906CalculaBonifCC(c.PadreMAVI, c.PADREIDMAVI, ISNULL(B.Bonificacion, 'Contado Comercial'), C.Mov, C.MovID), 0)

    FROM Cxc C WITH (NOLOCK)
    LEFT JOIN @tipobonif B
      ON B.Mov = @Fac
      AND B.MovID = @Facid
      AND B.Bonificacion LIKE '%Contado Comercial%'
    WHERE C.PadreMAVI = @Fac
    AND C.PadreIDMAVI = @Facid
    AND C.Estatus = 'PENDIENTE') T
    WHERE T.PADREMAVI = @Fac
    AND T.PADREIDMAVI = @Facid
    AND T.R = 2

  END

  IF ISNULL(@BonificacionCC, 0) > 0
    SET @impte = @BonificacionCC

  -- BONIFICACION ADELANTO EN PAGOS

  IF EXISTS (SELECT
      *
    FROM @tipobonif
    WHERE Bonificacion LIKE '%Adelanto%')
  BEGIN

    INSERT INTO @docsconcluidos (padremavi, Padreidmavi, cliente, concluidos)
      SELECT
        c.Padremavi,
        c.Padreidmavi,
        c.Cliente,
        COUNT(*) Concluidos
      FROM CXC c WITH (NOLOCK)
      WHERE estatus IN ('concluido')
      AND c.mov = 'Documento'
      AND c.PadreMAVI = @Fac
      AND c.PadreIDMAVI = @Facid
      AND c.vencimiento > CONVERT(datetime, CONVERT(varchar(10), GETDATE(), 10))
      GROUP BY c.Padremavi,
               c.Padreidmavi,
               c.Cliente

    INSERT INTO @ProxVenc (fECHApROXIMOvENC, PADREMAVI, PADREIDMAVI, CLIENTE, DOCS, VENCEHOY, SALDO)
      SELECT
        MIN(CP.Vencimiento) FechaProximoVenc,
        CP.PadreMavi,
        CP.PadreIdMavi,
        CP.Cliente,
        COUNT(*) Docs,
        COUNT(CASE
          WHEN Vencimiento = CONVERT(datetime, CONVERT(varchar(10), GETDATE(), 10)) THEN id
        END) Vencehoy,
        SUM(saldo)
      FROM Cxc CP WITH (NOLOCK)
      WHERE Vencimiento >= CONVERT(datetime, CONVERT(varchar(10), GETDATE(), 10))
      AND cp.PadreMAVI = @Fac
      AND cp.PadreIDMAVI = @Facid
      AND CP.Estatus = 'PENDIENTE'
      AND CP.Mov = 'Documento'
      GROUP BY CP.PadreMavi,
               CP.PadreIdMavi,
               CP.Cliente

    DECLARE @n1 float,
            @n2 float

    SELECT
      @impte = ISNULL(A.SaldoMov, 0) - ISNULL(A.Bonif, 0),
      @n1 = ISNULL(A.SaldoMov, 0),
      @n2 = ISNULL(A.Bonif, 0)
    FROM (SELECT
      ((Im.MR * (ISNULL(Docs, 0) - ISNULL(VenceHoy, 0) + ISNULL(D.Concluidos, 0))) * (T.Linea / 100)) * (Docs - ISNULL(Vencehoy, 0) + ISNULL(D.Concluidos, 0)) Bonif,
      Im.Mov,
      Im.MovID,
      s.SaldoMov
    FROM (SELECT
      (((v.Importe + v.Impuestos) - ISNULL(P.Abono, 0)) / ISNULL(C.DANumeroDocumentos, 0)) MR,
      V.Mov,
      V.MovID,
      C.Condicion,
      v.Importe + v.Impuestos AS Importe
    FROM CXC V WITH (NOLOCK)
    LEFT JOIN AuxiliarP P WITH (NOLOCK)
      ON v.MovId = P.MovID
      AND v.Mov = P.Mov
      AND P.Abono IS NOT NULL
    LEFT JOIN Condicion C WITH (NOLOCK)
      ON C.Condicion = V.Condicion
    WHERE V.Mov = @Fac
    AND v.Movid = @Facid) Im
    INNER JOIN @ProxVenc X
      ON X.PadreMavi = Im.Mov
      AND X.PadreIdMavi = Im.MovID
    LEFT JOIN @docsconcluidos D
      ON Im.Mov = d.PadreMavi
      AND Im.MovID = d.PadreIdMavi
    LEFT JOIN @tipobonif T
      ON T.Mov = Im.Mov
      AND T.MovID = Im.MovID
    LEFT JOIN (SELECT
      PadreMAVI,
      PadreIDMAVI,
      SUM(Saldo) AS SaldoMov
    FROM Cxc c WITH (NOLOCK)
    WHERE Estatus = 'PENDIENTE'
    AND PadreMAVI = @Fac
    AND PadreIDMAVI = @Facid
    AND Mov IN ('Documento', 'Nota Cargo')
    GROUP BY PadreMAVI,
             PadreIDMAVI) S
      ON s.PadreMAVI = im.mov
      AND s.PadreIDMAVI = im.movid
    WHERE Im.Mov NOT LIKE 'Nota%'
    AND T.Bonificacion LIKE '%Adelanto%') A
    WHERE a.Mov = @Fac
    AND a.Movid = @Facid
  END

  IF (ISNULL(@impte, 0.00) - ROUND(ISNULL(@impte, 0.00), 2, 1)) > 0.00
    SELECT
      @impte = ROUND(ISNULL(@impte, 0.00) + .01, 2, 1)
  ELSE
    SELECT
      @impte = ROUND(ISNULL(@impte, 0.00), 2)

  RETURN (@impte)
END