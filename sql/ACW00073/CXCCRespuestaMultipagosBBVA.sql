-- =======================================================================================================
--                : MultipagosMailOrder CXCCRespuestaMultipagosBBVA
-- NOMBRE         : CXCCRespuestaMultipagosBBVA
-- AUTOR          : Getsemani Avila Quezada ༼ つ ◕_◕ ༽つ
-- FECHA CREACION : 08/07/2020
-- DESARROLLO     : ACW00073 Links BBVA
-- DESCRIPCION    : Respuestas de Respuesta S2S de MultipagosMailOrder by BBVA
-- =======================================================================================================
CREATE TABLE CXCCRespuestaMultipagosBBVA (

   Orden Varchar(30),                 -- mp_order
   Referencia Varchar(30),            -- mp_reference
   ImporteTotal Money,                -- mp_amount
   CodigoRespuesta char(3),           -- mp_response
   DescripcionRespuesta Varchar(40),  -- mp_responsemsg

   AprovacionBancaria char(10),       -- mp_authorization
   CodigoHASH  varchar(95) ,          -- mp_signature -- HASH

)

EXEC SpAgregarDescripcionTabla @nombreTabla = N'CXCCRespuestaMultipagosBBVA',
                               @descripcion = N'Tabla de Registros de MultipagosMailOrder S2S by BBVA';


EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCRespuestaMultipagosBBVA',
                                          @nombreColumna = N'Orden',
                                          @descripcion = N'Orden de la transaccion';

EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCRespuestaMultipagosBBVA',
                                          @nombreColumna = N'Referencia',
                                          @descripcion = N'Referencia de la transaccion';

EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCRespuestaMultipagosBBVA',
                                          @nombreColumna = N'ImporteTotal',
                                          @descripcion = N'Importe de la transaccion';

EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCRespuestaMultipagosBBVA',
                                          @nombreColumna = N'CodigoRespuesta',
                                          @descripcion = N'Codigo de respuesta HTTPS';

EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCRespuestaMultipagosBBVA',
                                          @nombreColumna = N'DescripcionRespuesta',
                                          @descripcion = N'Mensaje de respuesta HTTPS';


EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCRespuestaMultipagosBBVA',
                                          @nombreColumna = N'AprovacionBancaria',
                                          @descripcion = N'Numnero de Aprovacion Bancaria';

EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCRespuestaMultipagosBBVA',
                                          @nombreColumna = N'CodigoHASH',
                                          @descripcion = N'Valor cifrado utilizado para seguridad';
