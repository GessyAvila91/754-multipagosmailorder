--=======================================================================================================
-- NOMBRE         : CXCCReferenciaMultipagoBBVA
-- AUTOR          : Getsemani Avila Quezada ༼ つ ◕_◕ ༽つ
-- FECHA CREACION : 08/07/2020
-- DESARROLLO     : ACW00073 links Bancomer
--=======================================================================================================
create TABLE CXCCReferenciaMultipagoBBVA (
  IdReferenciaMultipagoBBVA int IDENTITY (1,1)

  ,Fecha DateTime
  ,Referencia varchar (40)
  ,Factura varchar (41)
  ,Cantidad money
)

EXEC SpAgregarDescripcionTabla @nombreTabla = N'CXCCReferenciaMultipagoBBVA',
                               @descripcion = N'Tabla que guarda la informacion de los Links y Nips de BBVA';

EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCReferenciaMultipagoBBVA',
                                          @nombreColumna = N'IdReferenciaMultipagoBBVA',
                                          @descripcion = N'Id de la tabla para llevar el indexado de Registros';

EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCReferenciaMultipagoBBVA',
                                          @nombreColumna = N'Fecha',
                                          @descripcion = N'Id de la tabla para llevar el indexado de Registros';

EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCReferenciaMultipagoBBVA',
                                          @nombreColumna = N'Referencia',
                                          @descripcion = N'Id de la tabla para llevar el indexado de Registros';

EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCReferenciaMultipagoBBVA',
                                          @nombreColumna = N'Factura',
                                          @descripcion = N'Id de la tabla para llevar el indexado de Registros';

EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCReferenciaMultipagoBBVA',
                                          @nombreColumna = N'Cantidad',
                                          @descripcion = N'Id de la tabla para llevar el indexado de Registros';
