SET DATEFIRST 7
SET ANSI_NULLS ON
SET ANSI_WARNINGS ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET LOCK_TIMEOUT -1
SET QUOTED_IDENTIFIER OFF
SET NOCOUNT ON
SET IMPLICIT_TRANSACTIONS OFF
GO
--=======================================================================================================
-- NOMBRE         : SPCXCCobrosClientesBBVA
-- AUTOR          : Getsemani Avila Quezada ༼ つ ◕_◕ ༽つ
-- FECHA CREACION : 08/07/2020
-- DESCRIPCION    : SELECCION DE MOVIMIENTOS PARA PAGOS CON BBVA
-- DESARROLLO     : ACW00073  links Bancomer
-- EJEMPLO        : Exec SPCXCCobrosClientesBBVA 'C00000820'
--=======================================================================================================
CREATE PROCEDURE [dbo].[SPCXCCobrosClientesBBVA] @Cliente varchar(10)
AS
BEGIN

  --#MovCxcTemp
  IF EXISTS (SELECT
      NAME
    FROM TEMPDB.SYS.SYSOBJECTS
    WHERE ID = OBJECT_ID('Tempdb.dbo.#MovCxcTemp')
    AND TYPE = 'U')
    DROP TABLE #MovCxcTemp
  --#DoctosHijos
  IF EXISTS (SELECT
      NAME
    FROM TEMPDB.SYS.SYSOBJECTS
    WHERE ID = OBJECT_ID('Tempdb.dbo.#DoctosHijos')
    AND TYPE = 'U')
    DROP TABLE #DoctosHijos
  --#DoctosPadres
  IF EXISTS (SELECT
      NAME
    FROM TEMPDB.SYS.SYSOBJECTS
    WHERE ID = OBJECT_ID('Tempdb.dbo.#DoctosPadres')
    AND TYPE = 'U')
    DROP TABLE #DoctosPadres
  --#MaviUltimoPagoTemp
  IF EXISTS (SELECT
      NAME
    FROM TEMPDB.SYS.SYSOBJECTS
    WHERE ID = OBJECT_ID('Tempdb.dbo.#MaviUltimoPagoTemp')
    AND TYPE = 'U')
    DROP TABLE #MaviUltimoPagoTemp
  --#PadreMAVIPagoLiquidaTemp
  IF EXISTS (SELECT
      NAME
    FROM TEMPDB.SYS.SYSOBJECTS
    WHERE ID = OBJECT_ID('Tempdb.dbo.#PadreMAVIPagoLiquidaTemp')
    AND TYPE = 'U')
    DROP TABLE #PadreMAVIPagoLiquidaTemp
  --#LiquidaCon
  IF EXISTS (SELECT
      NAME
    FROM TEMPDB.SYS.SYSOBJECTS
    WHERE ID = OBJECT_ID('Tempdb.dbo.#LiquidaCon')
    AND TYPE = 'U')
    DROP TABLE #LiquidaCon


  DECLARE @DVConf int,
          @DIConf int,
          @CteFinal varchar(10) = NULL

  DECLARE @ImporteDeVenta money = 0.00, -- IMPORTEVTA
          @Atraso money = 0.00,         --
          @SaldoCapital money = 0.00,   -- SALDOTOTAL
          @moratorio money = 0.00,      -- INTERESES
          @LiquidaCon money = 0.00

  /*  --  TABLA PARA MOVIMIENTOS DEL CLIENTE  --    */
  IF ISNULL(OBJECT_ID('Tempdb..#MovCxcTemp'), 0) != 0
    DROP TABLE #MovCxcTemp
  SELECT
    Cxc.Cliente,
    Cxc.ID,
    Cxc.Mov,
    Cxc.MovID,
    Cxc.Vencimiento,
    Cxc.Importe,
    Cxc.Impuestos,
    Cxc.Saldo,
    Cxc.Estatus,
    Cxc.PadreMAVI,
    Cxc.PadreIDMAVI,
    Cxc.ImpApoyoDima,
    Cxc.SaldoApoyoDima,
    Cxc.CobroCteFinal,
    Cxc.ClienteEnviarA,
    Cxc.SucursalOrigen,
    Cxc.Concepto,
    Cxc.Empresa,
    Cxc.FechaEmision,
    Cxc.Condicion,
    ISNULL(Cxc.CteFinal, '0') CteFinal,
    Cxc.BonifCC INTO #MovCxcTemp
  FROM Cxc WITH (NOLOCK)
  JOIN MovTipo MT WITH (NOLOCK)
    ON MT.Mov = Cxc.Mov
    AND MT.Modulo = 'CXC'
    AND MT.Clave IN ('CXC.F', 'CXC.CAP', 'CXC.CA', 'CXC.C', 'CXC.AE', 'CXC.D', 'CXC.CD')
  WHERE Cxc.Cliente = @Cliente
  AND Cxc.Estatus IN ('PENDIENTE', 'CONCLUIDO')

  IF NOT EXISTS (SELECT
      object_id
    FROM tempdb.sys.indexes
    WHERE name = 'idx_MovCxcTemp_PadreMAVI'
    AND OBJECT_ID('Tempdb..#MovCxcTemp') = OBJECT_ID)
    CREATE NONCLUSTERED INDEX idx_MovCxcTemp_PadreMAVI ON #MovCxcTemp (PadreMAVI)
    INCLUDE (Cliente, ID, Mov, MovID, Vencimiento, Importe, Impuestos, Saldo, Estatus, PadreIDMAVI, ImpApoyoDima, SaldoApoyoDima, CobroCteFinal, ClienteEnviarA, SucursalOrigen, Concepto, Empresa, FechaEmision, Condicion, CteFinal)

  IF NOT EXISTS (SELECT
      object_id
    FROM tempdb.sys.indexes
    WHERE name = 'idx_MovCxcTemp_Mov_MovID'
    AND OBJECT_ID('Tempdb..#MovCxcTemp') = OBJECT_ID)
    CREATE CLUSTERED INDEX idx_MovCxcTemp_Mov_MovID ON #MovCxcTemp (Mov, MovID)

  /*    --	TABLA PARA DOCUMENTOS HIJOS	--	*/
  IF ISNULL(OBJECT_ID('Tempdb..#DoctosHijos'), 0) != 0
    DROP TABLE #DoctosHijos
  CREATE TABLE #DoctosHijos (
    Cliente varchar(10) NULL,
    ID int NULL,
    Mov varchar(20) NULL,
    MovID varchar(20) NULL,
    Vencimiento datetime NULL,
    ImporteTotal money NULL,
    Saldo money NULL,
    Moratorios money NULL,
    Estatus varchar(15) NULL,
    DiasVencidos float NULL,
    PadreMAVI varchar(20) NULL,
    PadreIDMAVI varchar(20) NULL,
    BonifPP money NULL,
    BonifCC money NULL,
    ImpApoyoDima money NULL,
    SaldoApoyoDima money NULL,
    CobroCteFinal int NULL,
    ClienteEnviarA int NULL,
    SucursalOrigen int NULL,
    Concepto varchar(50) NULL,
    Empresa varchar(5) NULL,
    FechaEmision datetime NULL,
    Condicion varchar(50) NULL,
    CteFinal varchar(10) NULL
  )

  /*	--	TABLA PARA DOCUMENTOS PADRES	--	*/
  IF ISNULL(OBJECT_ID('Tempdb..#DoctosPadres'), 0) != 0
    DROP TABLE #DoctosPadres
  CREATE TABLE #DoctosPadres (
    Empresa varchar(5) NULL,
    Cliente varchar(10) NULL,
    ClienteEnviarA int NULL,
    Categoria varchar(50) NULL,
    ID int NULL,
    PadreMAVI varchar(20) NULL,
    PadreIDMAVI varchar(20) NULL,
    FechaEmision datetime NULL,
    Condicion varchar(50) NULL,
    PrimerVencimiento datetime NULL,
    FormaPagoTipo varchar(50) NULL,
    DAPeriodo char(15) NULL,
    DANumeroDocumentos int NULL,
    DescripcionArt varchar(100) NULL,
    ImporteVta money NULL,
    Enganche money NULL,
    Monedero money NULL,
    SucursalOrigen int NULL,
    CobroXPolitica varchar(2) NULL DEFAULT 'NO',
    UltimoPago datetime NULL,
    DiasInactivos int NULL,
    CteFinal varchar(10) NULL,
    NomCteFinal varchar(302) NULL,
    RfcCteFinal varchar(15) NULL,
    IdVta int NULL,
    DVCxcMavi int NULL,
    DICxcMavi int NULL
  )

  /*	--	CONSULTA DE DOCUMENTOS HIJOS	--	*/
  INSERT INTO #DoctosHijos (Cliente, ID, Mov, MovID, Vencimiento, ImporteTotal, Saldo, Estatus, DiasVencidos, PadreMAVI, PadreIDMAVI, BonifPP, BonifCC,
  ImpApoyoDima, SaldoApoyoDima, CobroCteFinal, ClienteEnviarA, SucursalOrigen, Concepto, Empresa, FechaEmision, Condicion, CteFinal)
    SELECT
      CP.Cliente,
      CP.ID,
      CP.Mov,
      CP.MovID,
      CP.Vencimiento,
      ImporteTotal = ISNULL(CP.Importe, 0) + ISNULL(CP.Impuestos, 0),
      Saldo = CP.Saldo,
      CP.Estatus,
      DiasVencidos =
                    CASE
                      WHEN CP.Estatus = 'PENDIENTE' THEN DATEDIFF(DAY, CP.Vencimiento, GETDATE())
                      ELSE 0
                    END,
      CP.PadreMAVI,
      CP.PadreIDMAVI,
      BonifPP = CP.Saldo,
      BonifCC = ISNULL(CP.BonifCC, 0),
      CP.ImpApoyoDima,
      CP.SaldoApoyoDima,
      CP.CobroCteFinal,
      CP.ClienteEnviarA,
      CP.SucursalOrigen,
      CP.Concepto,
      CP.Empresa,
      CP.FechaEmision,
      CP.Condicion,
      CP.CteFinal
    FROM #MovCxcTemp CP
    JOIN MovTipo MT WITH (NOLOCK)
      ON MT.Mov = CP.PadreMAVI
      AND MT.Modulo = 'CXC'
      AND MT.Clave IN ('CXC.F', 'CXC.CAP', 'CXC.CA', 'CXC.CD')
    WHERE (CP.Estatus = 'PENDIENTE'
    OR (CP.Estatus = 'CONCLUIDO'
    AND ISNULL(CP.CobroCteFinal, 0) IN (1, 2, 4)))

  IF ISNULL(@CteFinal, '0') != '0'
    DELETE #DoctosHijos
    WHERE CteFinal != ISNULL(@CteFinal, '0')

  IF EXISTS (SELECT
      ID
    FROM #DoctosHijos)
  BEGIN	--EXISTS(SELECT TOP 1 ID FROM #DoctosHijos)
    IF EXISTS (SELECT
        ID
      FROM #MovCxcTemp
      WHERE Mov LIKE 'Nota Cargo%'
      AND Concepto LIKE 'CANC COBRO%')
      DELETE #MovCxcTemp
      WHERE ID IN (SELECT
          CobroId = CAST(CASE
            WHEN CHARINDEX('_', REVERSE(M.Valor)) > 0 THEN RIGHT(M.Valor, (CHARINDEX('_', REVERSE(M.Valor))) - 1)
          END AS int)
        FROM MovCampoExtra M WITH (NOLOCK)
        JOIN #MovCxcTemp CC
          ON M.ID = CC.ID
          AND M.CampoExtra IN ('NC_COBRO', 'NCV_COBRO', 'NCM_COBRO')
          AND M.Modulo = 'CXC'
          AND ISNULL(M.Valor, '') != ''
          AND CC.Estatus IN ('CONCLUIDO', 'PENDIENTE')
          AND CC.Mov LIKE 'Nota Cargo%'
          AND CC.Concepto LIKE 'CANC COBRO%')

    UPDATE DH
    SET DH.Moratorios = ROUND(dbo.Fn_MaviCalculaMoratorios(DH.ID), 2)
    FROM #DoctosHijos DH
    WHERE DH.Estatus = 'PENDIENTE'
    AND DH.DiasVencidos > 0

    /*	--	CONSULTA DE DOCUMENTOS PADRES	--	*/
    INSERT INTO #DoctosPadres (Empresa, ID, Cliente, FechaEmision, Condicion, PrimerVencimiento, ImporteVta, SucursalOrigen, ClienteEnviarA,
    DescripcionArt, PadreMAVI, PadreIDMAVI, CteFinal)
      SELECT
        C.Empresa,
        C.ID,
        C.Cliente,
        C.FechaEmision,
        C.Condicion,
        C.Vencimiento,
        ImporteVta = C.Importe + C.Impuestos,
        C.SucursalOrigen,
        C.ClienteEnviarA,
        C.Concepto,
        C.PadreMAVI,
        C.PadreIDMAVI,
        C.CteFinal
      FROM (SELECT
        DH.PadreMAVI,
        DH.PadreIDMAVI
      FROM #DoctosHijos DH
      GROUP BY DH.PadreMAVI,
               DH.PadreIDMAVI) CP
      JOIN #MovCxcTemp C
        ON CP.PadreMAVI = C.Mov
        AND CP.PadreIDMAVI = C.MovID

    UPDATE CP
    SET CP.DVCxcMavi = CM.DiasVencActMAVI,
        CP.DICxcMavi = CM.DiasInacActMAVI
    FROM #DoctosPadres CP
    JOIN CxcMavi CM WITH (NOLOCK)
      ON CM.ID = CP.ID

    SELECT
      @DVConf = MIN(Conf.DV),
      @DIConf = MIN(Conf.DI)
    FROM TcIRM0906_ConfigDivisionYParam Conf WITH (NOLOCK)

    UPDATE DP
    SET DP.CobroXPolitica = dbo.FN_MAVIRM0906CobxPol(DP.ID)
    FROM #DoctosPadres DP
    WHERE DP.DVCxcMavi > @DVConf
    OR DP.DICxcMavi > @DIConf

    /*	--	CONSULTA Y TABLA PARA ULTIMO PAGO	--	*/
    IF ISNULL(OBJECT_ID('tempdb..#MaviUltimoPagoTemp'), 0) != 0
      DROP TABLE #MaviUltimoPagoTemp

    SELECT
      PadreMAVI,
      PadreIDMAVI,
      MAX(MaviUltimoPago) MaviUltimoPago INTO #MaviUltimoPagoTemp
    FROM (SELECT
      CP.PadreMAVI,
      CP.PadreIDMAVI,
      MAX(Cob.FechaEmision) MaviUltimoPago
    FROM #MovCxcTemp Cob
    JOIN MovTipo MT WITH (NOLOCK)
      ON MT.Mov = Cob.Mov
      AND MT.Modulo = 'CXC'
      AND MT.Clave = 'CXC.C'
      AND Cob.Estatus = 'CONCLUIDO'
    JOIN CxcD CobD WITH (NOLOCK)
      ON CobD.ID = Cob.ID
    JOIN #MovCxcTemp Doc
      ON Doc.Mov = CobD.Aplica
      AND Doc.MovID = CobD.AplicaID
      AND Doc.Estatus IN ('CONCLUIDO', 'PENDIENTE')
    JOIN #DoctosPadres CP
      ON CP.PadreMAVI = Doc.PadreMAVI
      AND CP.PadreIDMAVI = Doc.PadreIDMAVI
    GROUP BY CP.PadreMAVI,
             CP.PadreIDMAVI

    UNION

    SELECT
      SUBSTRING(MCE.Valor, 1, CHARINDEX('_', MCE.Valor, 1) - 1) PadreMAVI,
      SUBSTRING(MCE.Valor, CHARINDEX('_', MCE.Valor, 1) + 1, LEN(MCE.Valor)) PadreIDMAVI,
      MAX(Cob.FechaEmision) MaviUltimoPago
    FROM #MovCxcTemp Cob
    JOIN MovTipo MT WITH (NOLOCK)
      ON MT.Mov = Cob.Mov
      AND MT.Modulo = 'CXC'
      AND MT.Clave = 'CXC.C'
      AND Cob.Estatus = 'CONCLUIDO'
    JOIN CxcD CobD WITH (NOLOCK)
      ON CobD.ID = Cob.ID
    JOIN #MovCxcTemp Doc
      ON Doc.Mov = CobD.Aplica
      AND Doc.MovID = CobD.AplicaID
      AND Doc.Estatus IN ('CONCLUIDO', 'PENDIENTE')
      AND Doc.Concepto = 'MORATORIOS MENUDEO'
    JOIN MovTipo MTNC WITH (NOLOCK)
      ON MTNC.Mov = Doc.Mov
      AND MTNC.Modulo = 'CXC'
      AND MTNC.Clave = 'CXC.CA'
    JOIN MovCampoExtra MCE WITH (NOLOCK)
      ON MCE.Modulo = 'CXC'
      AND MCE.ID = Doc.ID
      AND MCE.CampoExtra IN ('NC_FACTURA', 'NCV_FACTURA')
      AND CHARINDEX('_', MCE.Valor, 1) > 0
    GROUP BY MCE.Valor

    UNION

    SELECT
      DP.PadreMAVI,
      DP.PadreIDMAVI,
      BM.MaviUltimoPago
    FROM #DoctosPadres DP
    JOIN BonifSIMavi BM WITH (NOLOCK)
      ON BM.IDCxc = DP.ID) UltPago
    GROUP BY PadreMAVI,
             PadreIDMAVI

    UPDATE DP
    SET DP.Categoria = CV.Categoria,
        DP.DAPeriodo = CON.DAPeriodo,
        DP.DANumeroDocumentos = CON.DANumeroDocumentos,
        DP.ImporteVta = DP.ImporteVta,
        DP.UltimoPago = MUP.MaviUltimoPago,
        DP.DiasInactivos =
                          CASE
                            WHEN DATEDIFF(DD, CASE
                                WHEN DP.PrimerVencimiento > ISNULL(MUP.MaviUltimoPago, '19000101') THEN DP.PrimerVencimiento
                                ELSE MUP.MaviUltimoPago
                              END, GETDATE()) < 0 THEN 0
                            ELSE DATEDIFF(DD, CASE
                                WHEN DP.PrimerVencimiento > ISNULL(MUP.MaviUltimoPago, '19000101') THEN DP.PrimerVencimiento
                                ELSE MUP.MaviUltimoPago
                              END, GETDATE())
                          END,
        DP.NomCteFinal = Cte_Final.ApellidoPaterno + ' ' + Cte_Final.ApellidoMaterno + ' ' + Cte_Final.Nombre,
        DP.RfcCteFinal = Cte_Final.RFC
    FROM #DoctosPadres DP
    LEFT JOIN #MaviUltimoPagoTemp MUP
      ON MUP.PadreMAVI = DP.PadreMAVI
      AND MUP.PadreIDMAVI = DP.PadreIDMAVI
    LEFT JOIN Condicion CON WITH (NOLOCK)
      ON CON.Condicion = DP.Condicion
    LEFT JOIN VentasCanalMAVI CV WITH (NOLOCK)
      ON CV.ID = DP.ClienteEnviarA
    LEFT JOIN Cte_Final WITH (NOLOCK)
      ON Cte_Final.ClienteF = DP.CteFinal

    UPDATE DP
    SET DP.Monedero = ISNULL(Mon.Abono, 0)
    FROM #DoctosPadres DP
    JOIN AuxiliarP Mon WITH (NOLOCK)
      ON Mon.Mov = DP.PadreMAVI
      AND Mon.MovID = DP.PadreIDMAVI
      AND Mon.Modulo = 'VTAS'
      AND ISNULL(Mon.Abono, 0) > 0
      AND Mon.EsCancelacion = 0

    UPDATE DP
    SET DP.Enganche = Enganche.Total
    FROM #DoctosPadres DP
    JOIN (SELECT
      C.Cliente,
      Total = SUM(ISNULL(CD.Importe, 0)),
      CD.Aplica,
      CD.AplicaId
    FROM #MovCxcTemp C
    JOIN CxcD CD WITH (NOLOCK)
      ON CD.ID = C.ID
    JOIN #DoctosPadres CP
      ON CP.PadreMAVI = CD.Aplica
      AND CP.PadreIDMAVI = CD.AplicaID
    JOIN MovTipo M WITH (NOLOCK)
      ON M.Mov = CP.PadreMAVI
      AND M.Modulo = 'VTAS'
      AND M.Clave = 'VTAS.F'
    WHERE C.Mov = 'Aplicacion Saldo'
    AND C.Estatus = 'CONCLUIDO'
    GROUP BY C.Cliente,
             CD.Aplica,
             CD.AplicaID) Enganche
      ON Enganche.Aplica = DP.PadreMAVI
      AND Enganche.AplicaID = DP.PadreIDMAVI
      AND ISNULL(DP.DANumeroDocumentos, 0) > 0

    UPDATE DP
    SET DP.FormaPagoTipo = V.FormaPagoTipo,
        DP.IdVta = V.ID
    FROM #DoctosPadres DP
    JOIN Venta V WITH (NOLOCK)
      ON V.Mov = DP.PadreMAVI
      AND V.MovID = DP.PadreIDMAVI

    UPDATE DP
    SET DP.DescripcionArt = ISNULL(VD.Descripcion1, DP.DescripcionArt)
    FROM #DoctosPadres DP
    JOIN (SELECT
      NR = ROW_NUMBER() OVER (PARTITION BY VD.ID ORDER BY VD.Renglon),
      Art.Descripcion1,
      Padre.ID
    FROM #DoctosPadres Padre
    JOIN VentaD VD WITH (NOLOCK)
      ON VD.ID = Padre.IdVta
    JOIN Art WITH (NOLOCK)
      ON ART.Articulo = VD.Articulo
    WHERE ISNULL(Padre.IdVta, 0) > 0) VD
      ON VD.ID = DP.ID
      AND VD.NR = 1

  END	--EXISTS(SELECT TOP 1 ID FROM #DoctosHijos)

  IF ISNULL(OBJECT_ID('Tempdb..#PadreMAVIPagoLiquidaTemp'), 0) != 0
    DROP TABLE #PadreMAVIPagoLiquidaTemp
  CREATE TABLE #PadreMAVIPagoLiquidaTemp (
    PadreMAVI varchar(20) NULL,
    PadreIDMAVI varchar(20) NULL,
  )

  INSERT INTO #PadreMAVIPagoLiquidaTemp (PadreMAVI, PadreIDMAVI)
    SELECT
      CP.PadreMAVI,
      CP.PadreIDMAVI
    FROM Cxc CP WITH (NOLOCK)
    JOIN (SELECT
      Movimiento
    FROM MaviBonificacionMoV WITH (NOLOCK)
    GROUP BY Movimiento) MB
      ON MB.Movimiento = CP.PadreMAVI
    WHERE CP.Cliente = @Cliente
    AND CP.Estatus = 'PENDIENTE'
    GROUP BY CP.PadreMAVI,
             CP.PadreIDMAVI

  CREATE TABLE #LiquidaCon (
    PadreMAVI varchar(20),
    PadreIDMAVI varchar(20),
    PagoLiquida money
  )

/*  EXECUTE sp_executesql @SQL,
                        N'@Cliente VARCHAR(10), @CteFinal VARCHAR(10)',
                        @Cliente = @Cliente,
                        @CteFinal = @CteFinal*/

--   SELECT
--     @LiquidaCon = SUM(dbo.FNDM0253PagoLiquida(PadreMAVI, PadreIDMAVI))
--   FROM #DoctosPadres
--   where PadreMAVI not in ('Seguro Vida')

  --Select * from #DoctosPadres
  --Select  * from #DoctosHijos

  SELECT
    @ImporteDeVenta = SUM(ImporteVta)
  FROM #DoctosPadres
  where PadreMAVI not in ('Seguro Vida')

  Select
  @LiquidaCon = SUM(dbo.FNCXCPagoLiquidaBBVA(PadreMAVI, PadreIDMAVI))
  FROM #DoctosPadres

/*Select PadreMAVI, PadreIDMAVI,
  dbo.FNDM0253PagoLiquida(PadreMAVI, PadreIDMAVI),
     dbo.FNCXCPagoLiquidaBBVA(PadreMAVI, PadreIDMAVI)
  FROM #DoctosPadres*/

  SELECT
    @Atraso = SUM(CASE
      WHEN DiasVencidos > 0 THEN ISNULL(Saldo, 0.0)
      ELSE 0.0
    END),
    --@SaldoCapital = SUM(ISNULL(Saldo, 0.0)),
    @moratorio = SUM(ISNULL(Moratorios, 0.0))
  FROM #DoctosHijos

  SELECT
    @SaldoCapital = SUM(ISNULL(Saldo, 0.0))
  FROM #DoctosHijos
  where PadreMAVI not in  ('Seguro Vida')


  SELECT DISTINCT
    DP.PadreMAVI Mov, --1
    DP.PadreIDMAVI Movid, --2
    DP.ClienteEnviarA idCanalVenta, --3
    ISNULL(DH.Moratorios, 0.00) AS Moratorios, --4
    DH.Vencimiento AS FechaVencimiento, --5

    A.Descripcion1 AS Articulo, --6
    --D.Cantidad as cantidad,
    DP.FechaEmision AS 'Fecha Emision',--7
    ImporteVta 'Importe Total',--8
    CASE
      WHEN DH.DiasVencidos > 0 THEN DH.DiasVencidos
      ELSE 0
    END AS Vencimiento, -- 9
    dbo.[FnMavi1erVencimPendPagoPP](DP.PadreMAVI, DP.PadreIDMAVI) + 1 AS PagoPuntual,--10

    SUM(CASE
      WHEN DiasVencidos > 0 THEN dh.Saldo
      ELSE 0
    END) Abono,--11

    Cte.Nombre AS NombreCliente,
    @ImporteDeVenta AS importeVenta,
    @SaldoCapital AS SaldoCapital,
    @Atraso AS Atraso,
    @moratorio AS Moratorio,

    @Atraso + @Moratorio AS AdeudoVencido,
    @SaldoCapital + @Moratorio AS AdeudoTotal,
    @LiquidaCon + @Moratorio AS LiquidaCon

  FROM #DoctosPadres DP
  JOIN Cte WITH (NOLOCK)
    ON Cte.Cliente = DP.Cliente
  JOIN Venta V WITH (NOLOCK)
    ON V.Mov = DP.PadreMAVI
    AND V.MovID = DP.PadreIDMAVI
  JOIN VentaD D WITH (NOLOCK)
    ON D.id = v.id
    AND D.Renglon = 2048
    AND D.RenglonID = 1
  JOIN Art A WITH (NOLOCK)
    ON A.Articulo = D.Articulo
  LEFT JOIN #DoctosHijos DH
    ON DH.PadreIDMAVI = DP.PadreIDMAVI
    AND DH.PadreMAVI = DP.PadreMAVI

  GROUP BY DP.PadreMavi,
           DP.PadreIDMAVI,
           DP.PadreMAVI,
           DP.PadreIDMAVI,
           DP.ClienteEnviarA,
           A.Descripcion1,
           DP.FechaEmision,
           ImporteVta,
           Cte.Nombre,
           DH.Moratorios,
           DH.DiasVencidos,
           DH.Vencimiento,
           DH.BonifPP

  --#MovCxcTemp
  IF EXISTS (SELECT
      NAME
    FROM TEMPDB.SYS.SYSOBJECTS
    WHERE ID = OBJECT_ID('Tempdb.dbo.#MovCxcTemp')
    AND TYPE = 'U')
    DROP TABLE #MovCxcTemp
  --#DoctosHijos
  IF EXISTS (SELECT
      NAME
    FROM TEMPDB.SYS.SYSOBJECTS
    WHERE ID = OBJECT_ID('Tempdb.dbo.#DoctosHijos')
    AND TYPE = 'U')
    DROP TABLE #DoctosHijos
  --#DoctosPadres
  IF EXISTS (SELECT
      NAME
    FROM TEMPDB.SYS.SYSOBJECTS
    WHERE ID = OBJECT_ID('Tempdb.dbo.#DoctosPadres')
    AND TYPE = 'U')
    DROP TABLE #DoctosPadres
  --#MaviUltimoPagoTemp
  IF EXISTS (SELECT
      NAME
    FROM TEMPDB.SYS.SYSOBJECTS
    WHERE ID = OBJECT_ID('Tempdb.dbo.#MaviUltimoPagoTemp')
    AND TYPE = 'U')
    DROP TABLE #MaviUltimoPagoTemp
  --#PadreMAVIPagoLiquidaTemp
  IF EXISTS (SELECT
      NAME
    FROM TEMPDB.SYS.SYSOBJECTS
    WHERE ID = OBJECT_ID('Tempdb.dbo.#PadreMAVIPagoLiquidaTemp')
    AND TYPE = 'U')
    DROP TABLE #PadreMAVIPagoLiquidaTemp
  --#LiquidaCon
  IF EXISTS (SELECT
      NAME
    FROM TEMPDB.SYS.SYSOBJECTS
    WHERE ID = OBJECT_ID('Tempdb.dbo.#LiquidaCon')
    AND TYPE = 'U')
    DROP TABLE #LiquidaCon

END