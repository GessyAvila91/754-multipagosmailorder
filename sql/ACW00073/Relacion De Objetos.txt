-------------------------------------------------------
--> TITULO: ACW00073 links Bancomer
-------------------------------------------------------
OBJETOS CONTENIDOS
-------------------------------------------------------
--> INTELISIS
-------------------------------------------------------
NUEVOS:

MODIFICADOS:

A INTEGRAR:

A ELIMINAR:

EXISTENTES:

-------------------------------------------------------
--> SQL
-------------------------------------------------------
NUEVOS:
    --Tablas para WS
    CXCCConciliacionMultipagosBBVA.sql
    CXCCMultipagoOrdenesCorreosBBVA.sql
    CXCCReferenciaMultipagoBBVA.sql
    CXCCRespuestaMultipagosBBVA.sql

    --Objetos para desarrollo web
    FNCXCPagoLiquidaBBVA.sql
    SPCXCCobrosClientesBBVA.sql

MODIFICADOS:

A ELIMINAR:

EXISTENTES:

    CXCCArmadoLinkBBVA.sql
    SPCXCArmadoLinksBBVAEliminar.SQL

-------------------------------------------------------
--> VARIABLES
-------------------------------------------------------
NUEVOS:

MODIFICADOS:

A ELIMINAR:

EXISTENTES:

-------------------------------------------------------
--> FIN
-------------------------------------------------------