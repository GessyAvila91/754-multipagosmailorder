--=======================================================================================================
-- NOMBRE         : CXCCArmadoLinkBBVA
-- AUTOR          : Getsemani Avila Quezada ༼ つ ◕_◕ ༽つ
-- FECHA CREACION : 08/07/2020
-- DESARROLLO     : ACW00073 links Bancomer
--=======================================================================================================
create TABLE CXCCArmadoLinkBBVA (
  IdArmadoLinkBBVA int IDENTITY (1,1),
  Cliente varchar (10),
  Canal  int,
  Nip  varchar(8)
)

EXEC SpAgregarDescripcionTabla @nombreTabla = N'CXCCArmadoLinkBBVA',
                               @descripcion = N'Tabla que guarda la informacion de los Links y Nips de BBVA';

EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCArmadoLinkBBVA',
                                          @nombreColumna = N'IdArmadoLinkBBVA',
                                          @descripcion = N'Id de la tabla para llevar el indexado de Registros';

EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCArmadoLinkBBVA',
                                          @nombreColumna = N'Cliente',
                                          @descripcion = N'Cliente de Intelisis';

EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCArmadoLinkBBVA',
                                          @nombreColumna = N'Canal',
                                          @descripcion = N'Canal de Venta del Cliente';

EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCArmadoLinkBBVA',
                                          @nombreColumna = N'Nip',
                                          @descripcion = N'Nip Generado del cliente';



insert into  CXCCArmadoLinkBBVA(Cliente ,Canal ,Nip) values
('C01395344',3,'FUqQ003'),
('C01811354',3,'G6Oa003'),
('C01797494',3,'G212003'),
('C01705718',3,'Ggb2003'),
('C01154892',7,'EZ9M007'),
('C01804754',76,'G4nS076'),
('C01557196',7,'F8LM007'),
('C01550329',78,'F6f5078')

