--=======================================================================================================
-- NOMBRE         : CXCCConciliacionMultipagosBBVA
-- AUTOR          : Getsemani Avila Quezada ༼ つ ◕_◕ ༽つ
-- FECHA CREACION : 08/07/2020
-- DESARROLLO     : ACW00073 links Bancomer
--=======================================================================================================
create TABLE CXCCConciliacionMultipagosBBVA (
  --NombreCampo                                   -- Nombre Campo en el archivo
  IdConciliacionMultipagosBBVA int IDENTITY (1,1)

  ,FechaPago             DateTime       -- mp_date
  ,Nombre                varchar(50)
  ,UnidadDeNegocio       varchar (10)   -- mp_node
  ,CategoriaCobranza     varchar(1)     -- mp_concept
  ,TipoDePago            varchar(10)    -- mp_paymentMethod  --TDX   – Visa/Mastercard
                                        -- CIE   – Cheque electrónico Bancomer
                                        -- CLABE – CLABE Interbancaria
                                        -- SUC   – Sucursal
  ,Referencia            varchar(40)    -- mp_reference
  ,NumeroDeOrden         varchar(40)
  ,NumeroDeAprobacion    varchar(10)    -- mp_authorization
  ,NumeroDeVenta         INT            -- Identificador de venta -- ?
  ,ReferenciaMedioPago   varchar(20)

  ,Importe               MONEY          -- mp_amount
  ,Comision              Money
  ,IvaDeComision         Money
  ,FechaDispercion       DateTime
  ,PeriodoFinanciamiento Varchar(2)

  ,Moneda                char(1)        -- mp_currency 1 Pesos 2 Dolares
  ,BancoEmisor           Varchar(100)
  ,NombreDelPagador      Varchar(100)
  ,Correo                Varchar(50)    -- mp_mail
  ,Telefono              Varchar(50)    -- mp_phone


)


EXEC SpAgregarDescripcionTabla @nombreTabla = N'CXCCConciliacionMultipagosBBVA',
                               @descripcion = N'Tabla de Registros de Conciliacion de MultipagosMailOrder';

EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCConciliacionMultipagosBBVA',
                                          @nombreColumna = N'IdConciliacionMultipagosBBVA',
                                          @descripcion = N'Identificador de la aplicacion MultipagosMailOrder';

EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCConciliacionMultipagosBBVA',
                                          @nombreColumna = N'FechaPago',
                                          @descripcion = N'Fecha de pago';
EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCConciliacionMultipagosBBVA',
                                          @nombreColumna = N'Nombre',
                                          @descripcion = N'Nombre del cliente de Multipagos';
EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCConciliacionMultipagosBBVA',
                                          @nombreColumna = N'UnidadDeNegocio',
                                          @descripcion = N'ID de la división organizacional (mp_node)';
EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCConciliacionMultipagosBBVA',
                                          @nombreColumna = N'CategoriaCobranza',
                                          @descripcion = N'Categoría de cobranza (mp_concept) ';
EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCConciliacionMultipagosBBVA',
                                          @nombreColumna = N'TipoDePago',
                                          @descripcion = N'Tipo de Pago de la transaccion';

EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCConciliacionMultipagosBBVA',
                                          @nombreColumna = N'Referencia',
                                          @descripcion = N'Referencia ';
EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCConciliacionMultipagosBBVA',
                                          @nombreColumna = N'NumeroDeOrden',
                                          @descripcion = N'Número de orden de transaccion';
EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCConciliacionMultipagosBBVA',
                                          @nombreColumna = N'NumeroDeAprobacion',
                                          @descripcion = N'Numero de Aprobacion de la transaccion';
EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCConciliacionMultipagosBBVA',
                                          @nombreColumna = N'NumeroDeVenta',
                                          @descripcion = N'Folio asignado por multipagos';
EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCConciliacionMultipagosBBVA',
                                          @nombreColumna = N'ReferenciaMedioPago',
                                          @descripcion = N'Ultimos 4 digitos de la tarjeta de credito o referencia del cheque';

EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCConciliacionMultipagosBBVA',
                                          @nombreColumna = N'Importe',
                                          @descripcion = N'Importe de la transaccion';
EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCConciliacionMultipagosBBVA',
                                          @nombreColumna = N'Comision',
                                          @descripcion = N'Comision de la transaccion';
EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCConciliacionMultipagosBBVA',
                                          @nombreColumna = N'IvaDeComision',
                                          @descripcion = N'Iva de la transaccion';
EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCConciliacionMultipagosBBVA',
                                          @nombreColumna = N'FechaDispercion',
                                          @descripcion = N'Fecha de Dispersion de la transaccion';
EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCConciliacionMultipagosBBVA',
                                          @nombreColumna = N'PeriodoFinanciamiento',
                                          @descripcion = N'Periodo del financiamiento';

EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCConciliacionMultipagosBBVA',
                                          @nombreColumna = N'Moneda',
                                          @descripcion = N'pesos o dolares';
EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCConciliacionMultipagosBBVA',
                                          @nombreColumna = N'BancoEmisor',
                                          @descripcion = N'Banco que emitio la transaccion bancaria';
EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCConciliacionMultipagosBBVA',
                                          @nombreColumna = N'NombreDelPagador',
                                          @descripcion = N'Nombre del tarjetabiente o titular de la cuenta';
EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCConciliacionMultipagosBBVA',
                                          @nombreColumna = N'Correo',
                                          @descripcion = N'Correo del titular de la cuenta';
EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCConciliacionMultipagosBBVA',
                                          @nombreColumna = N'Telefono',
                                          @descripcion = N'Telefono asignado por el titular de la cuenta';
