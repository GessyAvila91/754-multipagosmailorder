-- =======================================================================================================
--                : MultipagosMailOrder CXCCMultipagoOrdenesCorreosBBVA
-- NOMBRE         : CXCCMultipagoOrdenesCorreosBBVA
-- AUTOR          : Getsemani Avila Quezada ༼ つ ◕_◕ ༽つ
-- FECHA CREACION : 08/07/2020
-- DESARROLLO     : ACW00073 links Bancomer
-- DESCRIPCION    : Tabla de Registros de MultipagosMailOrder by BBVA
-- =======================================================================================================
create TABLE CXCCMultipagoOrdenesCorreosBBVA (                  --
   IdMultipagoOrdenesCorreosBBVA  INT             --  id
  ,Origen             VARCHAR(15)                 --  INTERNO
  ,NombreFactura      VARCHAR(100)                --  name
  ,Cantidad           MONEY                       --  amount
  ,NombreCliente      VARCHAR(100)                --  payer_name
  ,CorreoCliente      VARCHAR(50)                 --  payer_email
  ,Referencia         VARCHAR(40)  --mp_reference --  reference
  ,Descripcion        VARCHAR(100)                --  description
  ,Token              VARCHAR(30)                 --  token
  ,FechaCreacion      DATETIME                    --  created_at
  ,FechaActualizado   DATETIME                    --  updated_at
  ,Expira             BIT                         --  expired
  ,FechaExpiracion    DATETIME                    --  expiration_date
  ,Moneda             VARCHAR(3)                  --  currency
  ,Lenguaje           VARCHAR(3)                  --  language
  ,Servicio           INT                         --  service_id
  ,UrlCodigoQr        VARCHAR(100)                --  qr
  ,Pagado             BIT                         --  payed
  ,Pago               INT                         --  payment_id
  ,PlantillaFactura   VARCHAR(40)                 --  invoice_template_id
  ,PlantillaPago      VARCHAR(40)                 --  payment_template_id
  ,UrlRespuesta       VARCHAR(100)                --  url
  ,CodigoRespuesta    VARCHAR(5)                  --  Codigo HTTPS
  ,MensajeRespuesta   VARCHAR(20)                 --  Mensaje HTTPS

)

EXEC SpAgregarDescripcionTabla @nombreTabla = N'CXCCMultipagoOrdenesCorreosBBVA',
                               @descripcion = N'Tabla de Registros de MultipagosMailOrder by BBVA';



EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCMultipagoOrdenesCorreosBBVA',
                                          @nombreColumna = N'IdMultipagoOrdenesCorreosBBVA',
                                          @descripcion = N'Identificador de la aplicacion MultipagosMailOrder';

EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCMultipagoOrdenesCorreosBBVA',
                                          @nombreColumna = N'Origen',
                                          @descripcion = N'Origen del Registro Mail Order, POST RESPONSE, Aplicacions Web';

EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCMultipagoOrdenesCorreosBBVA',
                                          @nombreColumna = N'NombreFactura',
                                          @descripcion = N'Nombre de la Factura enviada por MultipagosMailOrder';

EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCMultipagoOrdenesCorreosBBVA',
                                          @nombreColumna = N'Cantidad',
                                          @descripcion = N'Cantidad total de Pago o Pagos';

EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCMultipagoOrdenesCorreosBBVA',
                                          @nombreColumna = N'NombreCliente',
                                          @descripcion = N'Nombre del cliente Registrado en MultipagosMailOrder';



EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCMultipagoOrdenesCorreosBBVA',
                                          @nombreColumna = N'CorreoCliente',
                                          @descripcion = N'Correo del cliente en MultipagosMailOrder';

EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCMultipagoOrdenesCorreosBBVA',
                                          @nombreColumna = N'Referencia',
                                          @descripcion = N'Referencia generada por MultipagosMailOrder';

EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCMultipagoOrdenesCorreosBBVA',
                                          @nombreColumna = N'Descripcion',
                                          @descripcion = N'Descripcion dada por MultipagosMailOrder';

EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCMultipagoOrdenesCorreosBBVA',
                                          @nombreColumna = N'Token',
                                          @descripcion = N'Token Generado por MultipagosMailOrder';

EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCMultipagoOrdenesCorreosBBVA',
                                          @nombreColumna = N'FechaCreacion',
                                          @descripcion = N'Fecha de Creacion del Pago en MultipagosMailOrder';



EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCMultipagoOrdenesCorreosBBVA',
                                          @nombreColumna = N'FechaActualizado',
                                          @descripcion = N'Fecha de la ultima actualizacion';

EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCMultipagoOrdenesCorreosBBVA',
                                          @nombreColumna = N'Expira',
                                          @descripcion = N'Bandera de Expiracion';

EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCMultipagoOrdenesCorreosBBVA',
                                          @nombreColumna = N'FechaExpiracion',
                                          @descripcion = N'Fecha de Expiracion';

EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCMultipagoOrdenesCorreosBBVA',
                                          @nombreColumna = N'Moneda',
                                          @descripcion = N'Moneda en la cual se genero el pago ';

EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCMultipagoOrdenesCorreosBBVA',
                                          @nombreColumna = N'Lenguaje',
                                          @descripcion = N'Lenguaje en el cual se genero el pago';



EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCMultipagoOrdenesCorreosBBVA',
                                          @nombreColumna = N'Servicio',
                                          @descripcion = N'Id del Servicio de MultipagosMailOrder';

EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCMultipagoOrdenesCorreosBBVA',
                                          @nombreColumna = N'UrlCodigoQr',
                                          @descripcion = N'Codigo QR de MultipagosMailOrder';

EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCMultipagoOrdenesCorreosBBVA',
                                          @nombreColumna = N'Pagado',
                                          @descripcion = N'Estatus de pago';

EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCMultipagoOrdenesCorreosBBVA',
                                          @nombreColumna = N'Pago',
                                          @descripcion = N'Identificador del pago por MultipagosMailOrder';

EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCMultipagoOrdenesCorreosBBVA',
                                          @nombreColumna = N'PlantillaFactura',
                                          @descripcion = N'Identificador de la plantilla de la Factura';



EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCMultipagoOrdenesCorreosBBVA',
                                          @nombreColumna = N'PlantillaPago',
                                          @descripcion = N'Identificador de Plantilla del pago';

EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCMultipagoOrdenesCorreosBBVA',
                                          @nombreColumna = N'UrlRespuesta',
                                          @descripcion = N'URL de la respuesta de MultipagosMailOrder';

EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCMultipagoOrdenesCorreosBBVA',
                                          @nombreColumna = N'CodigoRespuesta',
                                          @descripcion = N'Codigo de Respuesta de MultipagosMailOrder';

EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCMultipagoOrdenesCorreosBBVA',
                                          @nombreColumna = N'MensajeRespuesta',
                                          @descripcion = N'Mensaje de Respuesta de MultipagosMailOrder';